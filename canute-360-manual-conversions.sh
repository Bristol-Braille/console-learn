clear
declare -a convs=("de-canute-360-handbuch.md" "nl-canute-360-handleiding.md" "pt-br-manual-do-canute-360.md" "fr-canute-360-manuel.md")

echo "Converting English source into Word file"
echo ""
modday=$(date -r ./manuals-sources/en-canute-360-manual.md +%F)
cp ./manuals-sources/en-canute-360-manual.md ./archive/$modday/en-canute-360-manual.md
pandoc ./manuals-sources/en-canute-360-manual.md --reference-doc=./templates/no-lang-large-print.docx -o ./manuals-word-files/en-canute-360-manual.md.docx

echo "Opening DeepL Pro. English source has been copied to the clipboard."
echo "Paste English source into the left hand side. Ready?"
read
xclip -sel clip ./manuals-sources/en-canute-360-manual.md
firefox -new-window https://www.deepl.com/translator
for i in "${convs[@]}"
do
	echo ""
	echo "Archiving old copy of" $i "and creating new blank copy"
	echo "Copying contents of English source file to the clipboard."
	if [ -f ./$i ]; then
		modday=$(date -r $i +%F)
		mkdir -p ./archive/$modday
		mv ./manuals-translations/$i ./archive/$modday/$i
		touch ./manuals-translations/$i
	fi
	echo "Go to DeepL Pro web page... "
	echo "On right side choose" $(echo $i | cut -d - -f 1) "language for:"
	echo ""
	echo $i
	echo ""
	echo "Then copy the right side to the clip board and come back to this window. Ready?"
	read
	echo "Pasting clipboard contents into the correct file,"
	echo "(so long as you copied correctly!)"
	xclip -sel clip -o > ./manuals-translations/$i
	echo ""
	echo "Then converting them into Word files"
	pandoc ./manuals-translations/$i --reference-doc=./templates/no-lang-large-print.docx -o ./manuals-word-files/$i.docx
done


