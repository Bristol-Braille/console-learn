Manual do console Canute
Tecnologia Braille da Bristol
Última atualização: 2024-09-05

# Notas

Observação sobre a tradução: todas as traduções que não são em inglês são traduções do inglês usando o DeepL Pro. Se você notar erros de tradução, informe-nos sobre eles.

Contato: enquiries@bristolbraille.org

# O que é o Canute Console?

O Canute Console é uma estação de trabalho Linux que inclui um monitor Canute 360 Braille. O Canute Console Premium é um pacote que inclui suporte ou desenvolvimento personalizado, um estojo de transporte com trava e garantia mais longa.

A grande diferença entre o Canute Console e outros produtos é que os monitores em braile e visual mostram a mesma quantidade de informações no mesmo layout, o que não prejudica nem o usuário de braile nem o de impressão.

Ele tem uma interface intuitiva de menus para ler livros da BRF e iniciar aplicativos. Ele também pode ser operado a partir da linha de comando e, nesse modo, destina-se principalmente à ciência da computação e à administração de sistemas. 

Há poucos limites para os usos que podem ser feitos depois que você se acostuma a trabalhar em várias linhas de Braille.

# Inserção e remoção do visor em Braille do Canute 360 no Console 

Se você comprou o console como um único dispositivo, não precisará fazer isso para começar a usar o console. 

Se você comprou o Console sem um monitor Braille (o “kit de atualização”), coloque os dois produtos em uma superfície plana onde pretende usá-los. Abra a tampa (o monitor) do Console Upgrade Kit. Levante o Canute 360 e coloque-o cuidadosamente sobre a superfície plana no meio do Console Upgrade Kit, tomando cuidado para alinhar as bordas. 

Na parte traseira direita, há uma saliência no kit de atualização. Esse é o conector de alimentação macho e precisa estar alinhado com o conector de alimentação fêmea do Canute 360. 

Quando isso estiver alinhado, mova o Canute 360 para trás para inserir os conectores de alimentação. Talvez seja necessário levantar um pouco o Canute 360 para fazer a conexão.

Agora, no lado esquerdo traseiro do Kit de atualização, você sentirá um conector macho USB-B (normalmente chamado de “conectores de impressora”) em um cabo muito curto de cerca de 5 cm. Puxe-o para fora do orifício em que está apoiado e insira-o na porta USB-B fêmea do Canute 360, à direita.

Para remover o Canute 360, faça o mesmo que o descrito acima, mas no sentido inverso.

# Ergonomia

O Console Canute deve ser usado em uma mesa ou escrivaninha estável e plana com uma superfície lisa.

Ele precisa de uma superfície lisa para que a bandeja com o teclado possa ser colocada e retirada.

Agora, levante o monitor (que também funciona como uma tampa) e retire o teclado.

Como o Console Canute tem muito Braille, que você precisa alcançar sobre um teclado para ler, a ergonomia é especialmente importante.

Portanto, certifique-se de que seu assento esteja o mais alto possível em relação à escrivaninha/mesa.

Certifique-se também de que o console esteja confortavelmente próximo à borda da escrivaninha/mesa.

Você deve poder apoiar as mãos confortavelmente na linha superior da tela Braille e na última linha do teclado.

# Layout das portas do console Canute

(Observe que, quando encaixado no Console, as portas do Canute 360 não estão mais em uso).

À esquerda do console Canute, de trás para a frente: 

- 1 conector USB-B macho, que fica dentro de um pequeno orifício no Console Upgrade Kit (quando o Canute 360 está desconectado) ou é inserido na porta USB-B fêmea do Canute 360.

- 3 conectores USB-A fêmea.

À direita do console Canute, de trás para a frente:

- 1 conector de alimentação fêmea.

- 1 conector de áudio fêmea de 35 mm.

- 1 botão para controle do monitor externo.

- 2 interruptores para controle do monitor externo.

- 1 conector HDMI fêmea.

Na parte traseira, do meio para a direita:

- 1 painel removível para portas adicionais futuras.

- 1 interruptor de energia.

Se você tiver um dos consoles Early Bird, o layout das portas poderá ser diferente do descrito acima.

# Conectando tudo

Depois de se certificar de que o USB-B está conectado ao Canute 360 e de inserir o cabo de alimentação, pressione o botão de alimentação na parte traseira direita para ligar a tela em Braille.

# Desconectando

Depois de desligar o Console da Canute (veja abaixo), aguarde até que a tela da Canute seja desligada e, em seguida, remova o cabo de alimentação. Observe que, se você não remover o cabo de alimentação, o monitor ainda terá energia e entrará no modo de proteção de tela. Portanto, um console Canute que não esteja em uso deve ser sempre desconectado.

# Layout do teclado

Supondo que você esteja usando o teclado britânico (caso contrário, consulte abaixo “Alteração do layout do teclado”), o layout do teclado é o seguinte:

Primeira linha: Escape, F1 a F10 (F11 é Fn+F1, F12 é FN+F2), Num lock, Print Screen, Delete (Insert é Fn+Del)

As linhas dois, três, quatro e cinco são o padrão para teclados Qwerty britânicos.

Linha seis: Controle (ctrl), Função (Fn), Super tecla, Alt, Espaço, Alt, Ctrl, Setas (Início é Fn+ESQUERDA, Fim é Fn+DIREITA, Página para cima é Fn+PARA CIMA, Página para baixo é Fn+PARA BAIXO)

# Iniciando no “Modo Leitor

Com o console corretamente configurado, como acima, conecte o cabo de alimentação e pressione o botão de alimentação na parte traseira.

A tela Braille levará aproximadamente um minuto para iniciar, durante o qual executará uma rotina de limpeza de pontos. 

Depois de iniciado, o console Canute estará no “Modo Leitor”. Ele funciona da mesma forma que o visor Braille autônomo do Canute 360 e não requer o teclado Qwerty. 

A tecla “H”, na parte superior esquerda da tela, inicia a ajuda contextual para explicar como cada um dos menus funciona. Resumidamente: o botão de polegar 'menu' abrirá o menu de opções ou, se você já estiver em um menu, retornará ao livro atual. Os botões “previous” (anterior) e “forward” (avançar) servem para navegar pelos livros e menus. Os botões laterais correspondem aos itens rotulados dos menus.

Você pode ler mais sobre isso no “Manual do usuário do Canute 360”, que também está neste diretório (~/learn/).

Quando a tela estiver encaixada no console, ela lerá todos os arquivos BRF *no seu diretório pessoal no armazenamento interno do console*. Ele não lerá o cartão SD do Canute 360, que só é lido quando o monitor Braille do Canute 360 é usado de forma autônoma (desacoplado).

# Entrando no 'Modo Console'

O “Modo Console” é onde todos os aplicativos interativos funcionam. Basicamente, trata-se da inicialização em um PC.

Para sair do “Reader Mode”, selecione a opção no “System Menu” ou puxe o teclado Qwerty e pressione a tecla Escape.

Agora você terá uma tela de login.

Se você tiver atualizado para esta versão a partir de uma versão anterior, o nome de usuário ou a senha não serão alterados.

Para novos usuários, o usuário padrão é “bbt” e a senha padrão é “bedminster”, todos em letras minúsculas.

É altamente recomendável alterar a senha padrão o mais rápido possível e anotá-la. Mais informações sobre isso abaixo, em “Alterar sua senha”.

No “Modo Console”, você terá um menu “Launcher” de opções muito semelhante ao menu de livros no “Modo Leitor”.

Cada linha é um aplicativo ou comando, como “File Explorer”, que abrirá arquivos no aplicativo “Word Processing”. Ao sair de um desses aplicativos, você retornará ao Launcher. Se, por algum motivo, você não retornar ao Launcher, mas sim à linha de comando, poderá carregá-lo novamente digitando

`$ launcher`

# Executando comandos do Launcher

Há alguns itens de menu importantes que listaremos e explicaremos a seguir:

- 'Shutdown' (Desligar). Isso desligará o Console Canute e a tela Braille. Uma vez desligados, os pontos em Braille se apagarão. Quando isso acontecer, você deverá retirar o cabo de alimentação do console.

- 'WiFi'. Isso abrirá uma lista de conexões disponíveis com sua intensidade de sinal. Depois de encontrar a conexão desejada, pressione 'q' para sair dela e você será solicitado a digitar o nome da conexão, seguido da senha.

- 'Upgrade'. Isso procurará uma atualização para a versão mais recente do sistema operacional do Canute Console. Isso requer primeiro uma conexão com a Internet. Enviaremos e-mails quando houver uma atualização importante, portanto, assine o boletim informativo no [site da Bristol Braille: https://bristolbraille.org/] (https://bristolbraille.org/). A atualização pode levar algum tempo, dependendo da velocidade de sua conexão. Após a conclusão, faça logout digitando `shutdown now`. Puxe o cabo de alimentação, conecte-o novamente e pressione o botão de alimentação.

- “Linha de comando”. Isso leva ao prompt da linha de comando. Exploraremos a linha de comando em detalhes a seguir. No entanto, não é necessário usar a linha de comando até e a menos que você queira.

# Execução de aplicativos no Launcher

A lista de aplicativos no Launcher está sempre crescendo. Experimente cada um deles para ver como funcionam. Todos esses aplicativos estão em desenvolvimento contínuo, alguns ainda são experimentais e todos serão aprimorados com as futuras versões do software. Informe-nos sobre o que mais você gostaria de ver. 

Aqui estão alguns aplicativos úteis para você começar:

- 'Explorador de arquivos'. Isso abrirá uma exibição em árvore do seu sistema de arquivos, na qual você pode navegar com os botões de avançar e voltar, assim como nos menus do Reader e do Launcher. Se você selecionar um arquivo, será aberto o aplicativo “Word Processing”, que oferece um menu para visualização e edição de documentos em formato rich text, incluindo uma visualização exclusiva de layout de impressão de arquivos PDF.

- 'Editor de texto' Abre um editor de texto chamado 'Micro' em um documento em branco. O Micro funciona de forma muito semelhante ao Notepad no Windows, com comandos similares. ctrl+x/c/v' para copiar e colar, 'ctrl-z' para desfazer, 'ctrl-y' para refazer, 'ctrl-f' para localizar, 'ctrl-s' para salvar, 'ctrl+q' para sair, por exemplo.

# Execução de alguns comandos na linha de comando

Os recursos a seguir serão adicionados em breve ao menu do Launcher, mas atualmente podem ser executados a partir da linha de comando.

Para fazer o seguinte, primeiro selecione a opção “linha de comando” no menu do Launcher. Isso abrirá um “prompt”, ou seja, uma linha na qual você pode digitar comandos. Essa linha é representada por um cifrão. Os comandos anteriores permanecerão na tela e um novo prompt aparecerá na parte inferior da tela. O último prompt é aquele em que você digitará.

Quando terminar, você poderá retornar ao menu digitando `launcher` no prompt.

## Alteração dos códigos Braille

Na linha de comando, há alguns comandos especiais para a troca a quente da tabela de códigos Braille que você está usando, incluindo:

* $ [alt]+[shift]+Q
(Grade One UEB)

* $ [alt]+[shift]+W
(Grau Dois UEB)

* $ [alt]+[shift]+E 
(9 linhas e 6 pontos US Computer Brl

* $ [alt]+[shift]+R 
(Alemão contratado, padrão 2015)

Observe que somente o US computer brl garante a representação de um caractere para um e, portanto, exatamente o mesmo layout da tela visual.

Se você souber o nome de uma tabela Braille BRLTTY diferente que deseja usar, poderá inseri-lo com:

`$ canute-table [table-name]`

## Alteração da senha

Para alterar a senha padrão, digite `$ passwd` e siga as instruções.

# Escape!

Agora que já sabemos o básico, os comandos mais importantes são fechar programas e encerrar com segurança.

Para interromper um programa que está em execução, geralmente há comandos específicos para esse programa. Geralmente, `q` ou `ctrl+q` é usado para sair ou a tecla `escape` (canto superior esquerdo do teclado do console). No entanto, pressionar `ctrl+c` geralmente funciona em situações extremas (se um programa estiver suspenso ou se você não souber como fechá-lo). 

Se você realmente não conseguir fechar um programa para voltar à linha de comando ou ao Launcher, poderá abrir uma nova guia de terminal. Veja abaixo.

--------

Isso é tudo o que você precisa saber para começar a usar o Canute Console. Se estiver interessado em saber mais sobre como usar o Console, especialmente na linha de comando, programar seu próprio aplicativo ou solucionar um problema que esteja tendo, continue lendo abaixo.

--------

# Noções básicas para navegar na linha de comando do Linux

## Nomenclatura e layout da linha de comando

Linux é o sistema operacional que o Canute Console está executando. Ele está executando especificamente uma versão do Linux chamada “Raspian”, que é baseada no “Debian”.

O “Modo Console” do Canute Console é pré-configurado para ser executado inteiramente a partir da linha de comando, o que significa que seus aplicativos são todos baseados em texto e terminais.

Você verá com frequência os termos “linha de comando” ou “terminal” (também “tty” e console") usados de forma intercambiável, pois têm significados muito semelhantes. Mas, em resumo, aqui estão suas diferenças:

- A “linha de comando” geralmente é a última linha da tela e é onde os comandos podem ser escritos. A linha de comando inclui um “prompt de comando”, que significa “onde você digita seus comandos”.

- A “linha de comando” está dentro de um “terminal”. O terminal inclui o histórico dos comandos anteriores que você digitou, além da saída deles.

Ao longo do restante deste documento, estamos usando instruções que começam com um cifrão, depois uma instrução e, às vezes, algumas outras palavras ou números separados por um espaço.

É assim que os comandos são executados na linha de comando do Linux. É muito semelhante ao prompt do DOS no Windows.

O cifrão é apenas um símbolo que significa “prompt de comando”, ou seja, “digite seus comandos depois disso”.

## Como digitar comandos e entender a saída do terminal

Portanto, quando escrevemos, por exemplo, uma instrução como “Run `$ ls`”, significa “Digite ‘ls’ na linha de comando”. Nesse caso, ela realmente significa “listar os subdiretórios e arquivos” em meus diretórios atuais.

Como outro exemplo, poderíamos escrever neste documento, `$ cd demos` significa “Execute o comando ‘cd’ com a instrução (chamada de argumento) ‘demos’”. Na verdade, nesse caso, significa “vá para o diretório chamado ‘demos’”.

É importante observar que o terminal armazena todos os seus comandos anteriores e não os apaga da tela, a menos que seja solicitado.

Pense nele como uma máquina de escrever ou um gravador, escrevendo uma linha de cada vez, sem apagar as linhas anteriores.

Sempre leia a última linha da tela e presuma que a *última linha que tem o prompt do dólar* é a linha de comando ativa, e as linhas acima são o histórico.

Por exemplo, se eu quisesse digitar um comando como “criar um diretório chamado ‘documents’, que deveria ser escrito como `mkdir documents`, mas cometesse um erro, a saída do visor em Braille poderia ser a seguinte

`$ mjdir documents`
`bash: mjdir: comando não encontrado`
`$`

Vamos dar uma olhada nisso novamente, linha por linha. 

`$ mjdir documents`

Eu cometi um erro de digitação com o comando.

`bash: mjdir: comando não encontrado `

Esta é a mensagem de erro (“bash” é o que chamamos de “shell”, ou seja, o programa que interpreta seus comandos).

`$ `

Portanto, agora temos outro prompt de comando em branco, mas observe que ainda podemos ver nossa primeira tentativa e a mensagem de erro acima.

Aqueles que estão acostumados com telas Braille de linha única talvez precisem se acostumar com isso. No entanto, esse é o comportamento padrão do terminal Linux e exatamente o mesmo layout e saída que os usuários com visão obtêm no monitor visual.

Se a sua tela estiver muito cheia, o comando `$ clear` é seu amigo. Ele redefinirá o terminal para um espaço em branco que não seja o prompt de comando atual na linha superior da tela.

## Como se locomover

O Linux usa um sistema de arquivos hierárquico, o que significa que os diretórios e arquivos estão todos dentro de outros diretórios, voltando à “raiz”. A raiz está no topo da hierarquia.

Os diretórios são separados por símbolos de traço, portanto, seu diretório pessoal é escrito como `/home/pi/`

Ao fazer login, você começará no diretório inicial do seu usuário. Para começar, você raramente precisará ir “para cima” além do seu diretório pessoal.

O `$ ls` lista os subdiretórios e arquivos no diretório atual.

Talvez você ache que essa lista de saídas é muito longa para ser lida. Você pode voltar para cima e para baixo no histórico de saída do terminal pressionando `ctrl+Fn+UP` ou `ctrl+Fn+DOWN`.

Para mover-se entre diretórios, use o comando `cd`. `$ cd directoryname`.

Para voltar um nível abaixo, use o comando `$ cd ..`.

Aqui observamos um aspecto importante sobre muitos comandos do Linux, como o `cd`. Quando você pede para entrar em um diretório, ele o faz, mas não fornece nenhum resultado que indique se o fez. 

Então, como saber onde você está? Você pode digitar `$ pwd`. Isso retorna sua localização atual no sistema de arquivos. 

## Dicas para facilitar a navegação e a digitação de comandos

Você pode notar que, para alguns comandos, a string de saída é mais longa do que a tela (40 células), portanto, o Canute foi deslocado totalmente para a direita. 

Para se deslocar para a esquerda e para a direita no visor Braille (isso não afeta o visor visual) e visualizar cadeias de caracteres maiores que 40 células, pressione os botões de polegar '[MENU]+[FORWARD]' na parte frontal do visor do Canute para ir para a direita ou '[MENU]+[PREV]' para ir para a esquerda.

Você pode completar automaticamente um comando ou um diretório ou nome de arquivo quando estiver digitando no prompt de comando tocando na tecla tab.

Se houver apenas uma opção disponível, a tecla Tab a preencherá automaticamente. Se houver várias opções, pressionar a tecla tab não fará nada, mas tocar duas vezes na tecla tab exibirá todas as opções e as gravará no terminal para que você possa visualizá-las.

## Leitura de arquivos de texto

Se houver um arquivo de texto que você queira ler, digite `$ less textfilename.txt`. Ele não precisa ter uma extensão TXT e, muitas vezes, no Linux, eles não terão nenhuma extensão. Em caso de dúvida, tente usar o `less` nele.

O Less imprimirá as primeiras oito linhas do arquivo. Use as teclas para cima e para baixo (Fn+UP, Fn+DOWN) para navegar pelo arquivo e pressione 'q' para voltar à linha de comando.

## Comandos úteis 

Incluir:

* ls|less
(Lista arquivos e subdiretórios)

* tree|less
(Exibe a árvore de diretórios)

* cd [DIRECTORYNAME]
(Ir para o subdiretório)

* cd ...
(Vai até o diretório pai).

* mkdir [DIRNAME]
(Criar novo diretório)

* micro [FILENAME]
(Abrir arquivo de texto)

* [ctrl]+[shift]+t
(Abre uma nova guia no terminal).

* [ctrl]+[shift]+[NUM] (0--9)
(Alterna para outra guia).

* shutdown now
(Desligar, mas ainda é necessário 
desligar o Canute depois que o desligamento 
concluído).

* touch [FILENAME]
(Criar novo arquivo em branco)

* rm [FILENAME]
(Excluir arquivo)

* rm -r [DIRNAME]
(Excluir o diretório e seu conteúdo; '-r' é a abreviação de 'recursivo').

* sudo [COMMAND]
(Executa o comando subsequente como superusuário, ou seja, com privilégios totais).

* sudo rm -r [DIRNAME]
(Excluir um diretório do repositório sem ser solicitado a fornecer cada arquivo protegido contra gravação).

## Recursos de aprendizado

Sugerimos que você ouça os seguintes episódios do Braillecast, patrocinado pela Bristol Braille, para familiarizar as pessoas com alguns dos conceitos básicos da linha de comando do Linux.

Todos os episódios podem ser encontrados em:
`cd ~/learn/braillists/`
e podem ser reproduzidos com:
`vlc 01[tab to complete]`

- 01: Primeiramente, uma visão geral de diferentes cientistas da computação que utilizam o Braille em seu trabalho. Isso pode ser pulado.

- 02: Em segundo lugar, uma introdução à linha de comando do Linux.

- 03: Em terceiro lugar, uma introdução ao Git, para controle de versão, compartilhamento e download de programas.

- 04: Em quarto lugar, uma aula magistral sobre programação no Canute Console.

O Canute Console inclui algumas ferramentas básicas de aprendizado do Linux. Você pode encontrá-las no diretório home digitando `cd learn`.
Por fim, você geralmente pode procurar respostas para qualquer pergunta que tenha na Web. 

A maioria das perguntas que você tiver será geral sobre o Linux. No entanto, se precisar de respostas mais específicas, você pode tentar pesquisar por “Debian” ou até mesmo especificamente por “Raspian”.

Recomendamos um site chamado Stackoverflow para esses tipos de perguntas.

--------

Agora você deve ter uma noção razoável dos conceitos básicos de operação na linha de comando do Linux quando quiser. As seções a seguir explicarão com mais detalhes as tarefas comuns.

-------

# Desligamento

Para desligar toda a unidade a partir da linha de comando, digite `$ shutdown now`. (Ou, para reinicializar, digite `$ reboot`.)

Após o desligamento e o desligamento do monitor Canute*, você deve retirar o cabo de alimentação do console (caso contrário, o monitor permanecerá ligado)

* N.B. Se você tiver atualizado o Canute 360 que vem com o console para a versão de firmware 2.0.10 ou superior, o Canute será desligado quando o console for desligado. No entanto, se estiver executando um firmware mais antigo, ele não se desligará sozinho, mas entrará no modo de leitura de livros autônomo. Nesse momento, será necessário pressionar o botão liga/desliga na parte traseira direita para desligar a tela do Canute antes de retirar o cabo de alimentação.

# Abrir uma nova guia de terminal

As guias do terminal funcionam como uma guia em um navegador da Web. Pressione `ctrl+shift+t` para abrir uma nova guia. A guia anterior ainda estará operando em segundo plano para que você possa alternar entre várias guias. 

Para alternar entre as guias, você pode pressionar `alt+[1--9]`. 

Para fechar uma guia a partir da linha de comando, digite `$ exit`. Observe que, ao fechar a guia original, você sairá da sessão, portanto, tenha o cuidado de salvar todas as alterações nos documentos antes de fazer isso.

Observe que a abertura de uma nova guia ou a alternância entre guias não alterará o código Braille atual; portanto, se você estiver em um aplicativo que esteja usando o Braille do computador e abrir uma nova guia, a nova guia também estará em Braille do computador.

# Som

Observe os comandos abaixo. O s duplo em sset não é um erro de digitação.

- `$ amixer sset Master 0%`

- `$ amixer sset Master 100%`

# Alteração do layout do teclado

O comando é relativamente simples na linha de comando. Por exemplo:

`$ setxkbmap us`

Alterará o teclado para o dos EUA. 

Para obter uma lista dos layouts de teclado disponíveis, digite:

`$ ls /usr/share/X11/xkb/symbols/ | less`

Para defini-lo automaticamente, adicione o mesmo comando ao `~/console-set-up/console-start.sh`:

`$ setxkbmap us`

# Uso de dispositivos USB

Os dispositivos USB são montados automaticamente no diretório `/media/`.

Por exemplo, o primeiro dispositivo USB inserido será montado em `/media/usb0`, o segundo dispositivo USB será montado em `/media/usb1` e assim por diante.

Para acessar arquivos no dispositivo USB, use o `cd` como em qualquer outro diretório, por exemplo, `$ cd /media/usb0/`.

Se o seu console não estiver atualizado, talvez seja necessário instalar um aplicativo chamado `usbmount` para que a funcionalidade acima funcione. Isso requer uma conexão com a Internet (veja abaixo). Uma vez conectado, digite este comando:

`$ sudo apt install usbmount`

Depois que a instalação for concluída, reinicie o console ou remova e reinsira o pendrive.

# Conexão ao WiFi

O Console da Canute se conectará automaticamente a qualquer rede WiFi que tenha salvo. 

Para escolher entre essas redes (se houver mais de uma), digite `$ wifi new`

Para se conectar a uma nova rede pela primeira vez, digite `$ wifi new` e, em seguida, pressione `q`, o que abrirá um prompt na última linha para que você digite o nome da conexão desejada, pressione `enter` e, em seguida, a senha.

Se a conexão for bem-sucedida, você verá uma mensagem do tipo `Device 'wlan0' successfully connected`; no entanto, talvez você não veja essa mensagem se já tiver se conectado.

Da próxima vez que quiser se conectar a essa conexão Wifi, mas não quiser digitar a senha novamente, você pode omitir o “new” e ele se lembrará dela da última vez: `$ wifi`

# Tethering para o seu celular por meio da porta USB-A

Usando um telefone Android, conecte-o ao console Canute por meio de um cabo USB. Nas notificações do seu telefone, aparecerá algo como “Charging from USB” (Carregando por USB). Toque nessa notificação e ela o levará ao menu de configurações USB. Altere “Charging only” (Somente carregamento) para “Tether” (os termos exatos podem mudar). O console agora terá acesso à Internet. 

Ao fazer tethering, essa conexão será usada em vez da conexão WiFi, mesmo que não seja tão rápida.

# Download de arquivos da Internet

Se você souber o URL de um arquivo na Internet, poderá fazer download dele usando o comando `curl`. A opção `-O` salva o arquivo em seu local atual no sistema de arquivos com seu nome original. Por exemplo:

`$ curl -O https://bristolbraille.org/shared/2024-03-22-music-demos.zip`

Nesse caso, talvez você queira descompactar o arquivo, o que pode ser feito com este comando:

`$ unzip 2024-03-22-music-demos.zip`

Em seguida, você pode remover o arquivo zip original com o comando `rm`:

`$ rm 2024-03-22-music-demos.zip`

Para procurar outras opções para o curl, como salvá-lo com um nome de arquivo diferente ou imprimi-lo na tela, consulte o manual do `curl`:

`$ man curl`

# Instalação de novos programas da Internet

Há muitas maneiras diferentes de instalar programas no Linux. Os métodos mais comuns são usar gerenciadores de pacotes como o 'apt', que permite atualizar e gerenciar facilmente os programas instalados.

Vamos testar isso instalando um jogo. Você pode procurar qualquer outro programa 'apt' que seja de seu interesse e tentar instalá-lo em vez disso. Pesquise na Web por “Linux apt [assunto de interesse]” para ver quais programas estão disponíveis. Há milhares deles!

Para instalar o programa:

`$ sudo apt install nethack`

Em poucas palavras, isso significa:

- `sudo` significa 'superuser do', ou seja, você está dando permissão para alterar o sistema.

- O cifrão indica que você deve digitar na linha de comando.

- O programa que você está executando é o `apt`, um instalador e gerenciador de pacotes.

- Você está dando o comando `install`.

- Finalmente, o nome do programa que você vai instalar, `nethack-console`. Nesse caso, não se trata de uma versão criada especialmente para o Canute Console, o termo “console” é mais ou menos análogo a “terminal”, significando a versão do programa destinada a ser executada a partir do terminal. 

A instalação pode levar alguns minutos, ao final dos quais você terá outra linha de comando em branco com um cifrão na parte inferior da tela.

Esse é um videogame antigo dos anos 90. O interessante é que, embora não tenha sido projetado para ser jogado em um monitor Braille, ele funciona imediatamente no Canute Console. Mas, para que ele seja exibido corretamente, você terá que mudar para o modo Braille do computador primeiro (veja acima).

Lembre-se de que esse jogo precisa ser pesquisado antes de ser jogado, pois é muito complexo. 

`$ man nethack`

“man” é a abreviação de ‘manual’. Isso lhe dará as instruções.

Agora tente executar o programa. 

`$ nethack`

Boa sorte! (Se quiser sair do Nethack, pressione ctrl+c e continue pressionando enter até que ele saia).

# Impressão de arquivos

Se a sua impressora tiver sido detectada automaticamente quando você se conectou, poderá começar a imprimir imediatamente. Caso contrário, siga estas instruções ou entre em contato com a Bristol Braille:

`$ links2 https://opensource.com/article/21/8/add-printer-linux`

Isso é a execução de um navegador chamado Links2 para ler uma página da Web. Você só precisa rolar para cima e para baixo com os botões scrollup scrolldown e sair pressionando ctrl+c.

Quando a impressora estiver configurada, você poderá imprimir documentos usando o LibreOffice. (Talvez seja necessário instalar o LibreOffice primeiro).

`$ sudo apt install LibreOffice`
`$ libreoffice --headless -p example.docx`

# Atualizando para a versão mais recente do Console OS

Uma vez conectado à Internet, execute o seguinte comando:

`$ curl -L https://bristolbraille.org/console-install | bash`

# Exemplos de aplicativos personalizados desenvolvidos para o console Canute

## Sonic Pi

Instale:

`$ git clone https://gitlab.com/bristol-braille/sonic-pi-canute/`

Veja os exemplos a seguir de como executar e editar programas do Sonic Pi usando algumas ferramentas que desenvolvemos ou escrevemos scripts para o Console:

`$ cd sonic-pi-canute` (Vá para o diretório `sonic-pi-canute`).

`$ micro examples.txt` (Veja --- e edite, se quiser --- o código de exemplo do Sonic Pi).

Agora você estará no editor de texto chamado `micro`. Movimente-se por ele, salve, desfaça etc. usando os comandos normais das teclas do bloco de notas. Para sair, pressione `ctrl+q` e digite `y` ou `n` para salvar ou não as alterações.

De volta à linha de comando...

`$ bash start-sonic-pi.sh` (Iniciará o programa gráfico do Sonic Pi e o minimizará para que possamos continuar a trabalhar com a linha de comando. O Bash é um programa que executa os chamados scripts `shell`, que geralmente terminam em `.sh`).

Uma vez de volta à linha de comando, o que levará cerca de 30 segundos...

`$ bash play-sonic.sh examples.txt` (Um script que passa o arquivo `examples.sh` para o Sonic Pi. Você perceberá que os sons estão tocando).

`$ bash stop-sonic.sh` (Interrompe qualquer coisa que esteja sendo reproduzida pelo Sonic Pi).

--------

# Avançado

## Visualização remota do terminal de outra pessoa

Para trabalhar com outro colega ou acompanhar um tutor, talvez você queira acompanhar a sessão de terminal on-line usando uma ferramenta como [`tty-share`](https://tty-share.com/).

## Controle remoto do seu console

Pode ser sensato permitir que outro usuário remoto, como a equipe da BBT, tenha o controle do console nos horários que você escolher. Para isso, recomendamos o uso do [`mosh`] (https://mosh.org/), que é semelhante ao `ssh`, mas com melhor estabilidade na conexão.

## Aplicativos que não cabem em 9 linhas

- Execute `$ screen` e pressione enter.

- Pressione “enter” para pular a parte do leia-me.

- Pressione '[ctrl]+a' e, em seguida, pressione ':' (dois pontos). Isso permite que você digite comandos, que aparecerão na 9ª linha do visor (ele mostrará dois pontos no canto inferior esquerdo)

- (É possível pular esta etapa.) Digite `width -w 40` e pressione Enter. Isso define a largura como 40 (você pode alterar esse número se o aplicativo que estiver usando precisar ser mais largo, embora este passo a passo presuma que não seja necessário, portanto, você provavelmente pode pular esta etapa).

- Pressione '[ctrl]+a' e, em seguida, pressione ':' (dois pontos) novamente para digitar outro comando.

- Digite `height -w 17` e pressione Enter. Isso torna a altura do terminal mais alta, neste caso, 17 linhas em vez de 9. Você pode escolher qualquer outro número, não precisa ser um múltiplo de 9 linhas.

* (Como exemplo de uso, definir a altura para 27 linhas tornaria o terminal três vezes mais alto que a tela Canute, ou seja, do mesmo tamanho que uma página Braille em papel padrão do Reino Unido).

- Para mover-se pelo terminal extragrande, pressione '[ctrl]+a' e, em seguida, pressione '['. Isso entra no que é chamado de “modo de cópia”, mas vamos usá-lo para nos movimentarmos.

- Agora o cursor (normalmente todos os seis pontos) se move livremente pelo terminal entre as linhas (normalmente ele permanece no prompt de comando).

- Mova o cursor até a parte superior da tela, mantendo-o pressionado. Ele moverá todo o conteúdo para baixo.

- Quando estiver mostrando a parte do aplicativo que deseja visualizar, pressione escape, o que sairá do “modo de cópia” e retornará ao modo normal de entrada de linha de comando.

________
(O texto acima foi extraído de uma pergunta no Stackoverflow que você pode ler se quiser entender mais, mas não é necessário: https://stackoverflow.com/questions/75316022/emulate-a-taller-terminal-within-gnome-terminal-and-pan-up-and-down)

# Desenvolvimento de aplicativos para o Canute Console

Esse é um tópico importante que abordaremos em um documento separado e no qual estamos trabalhando ativamente. 

Por enquanto, a explicação mais simples é a seguinte:

Qualquer coisa que você possa desenvolver para a linha de comando visual, em qualquer linguagem, que seja executada no Linux e caiba em 40x9, será executada no Canute Console. 

No entanto, há algumas considerações a serem feitas:
- Para garantir a paridade de 1 para 1 do layout entre as telas visual e Braille, recomendamos desenvolver o programa para usar o código Braille do computador. As tabelas em braile contratadas não respeitarão os espaços em branco horizontais.
- O Canute 360s é atualizado a 0,5-1 segundo por linha, portanto, os aplicativos devem evitar a rolagem constante.
A [NPYScreen](https://github.com/npcole/npyscreen/) pode ser uma maneira útil de criar rapidamente produtos visuais e táteis espaciais atraentes a partir de comandos simples do bash e do Python.

## Publicação de aplicativos como executáveis para outras plataformas

O Canute Console está executando um sistema operacional baseado no Debian em um processador ARM7 (o Raspberry Pi 4). Mas, ao publicar, você desejará fazer com que seus aplicativos também funcionem em outros sistemas.

Se estiver usando uma linguagem compilada, como C, examine os comandos do compilador.

Se estiver usando uma linguagem de script como Python, talvez queira criar um executável para que o usuário não veja seu código e não precise instalar as dependências das quais seu código depende. Você também pode querer criar um executável que funcione no Windows, no Linux x86 ou no Mac OS.

Há duas ferramentas para Python que recomendamos consultar:

- O `pyinstaller` é uma ferramenta Python que você pode instalar com o `pip` e que criará um executável *para o processador no qual você o está executando*, ou seja, neste caso, ARM7. Se quiser criar outros executáveis usando essa ferramenta, será necessário executá-la nessas plataformas.

- O `auto-py-to-exe` é uma ferramenta que cria executáveis x86 do Windows sem a necessidade de executar o Windows e, portanto, pode ser conveniente para os proprietários do Canute Console.

## Ter cuidado com comandos específicos do Canute Console

Independentemente da linguagem que estiver usando e do método que usar para criar a publicação, lembre-se de levar em conta se o usuário do outro lado deverá ter um monitor Canute conectado ou não. 

Se estiver desenvolvendo o script para um público comum que não lê Braille e não possui um Canute, tome cuidado para que as linhas específicas do Canute Console no seu script não causem o travamento do programa. Por exemplo, se você estiver chamando o comando `canute-cursor`, em qualquer outro sistema isso gerará um erro. 

Você pode contornar isso comentando manualmente essas linhas, ou criando estados de falha elegantes se esses comandos não forem encontrados, ou tendo uma função de wrapper que chame ou não esses comandos, dependendo do sistema.

## Publicando seu aplicativo publicamente

Há muitas maneiras de fazer isso. Descreveremos uma maneira que a Bristol Braille usou, como exemplo, que é usar o controle de versão e hospedá-lo em um site comumente usado.

Existe uma tecnologia chamada 'git' que é usada para rastrear alterações em software e publicar esse software em 'repositórios'. Um dos sites que fornece repositórios git (geralmente chamados de repositórios) como serviço é o Github, que usamos para manter nosso software disponível publicamente. Isso significa que as pessoas que acessam esse site podem ver o código-fonte da interface de usuário do Canute e podem baixá-lo e alterá-lo.

Em sites como esse, geralmente são fornecidos outros serviços úteis relacionados a software. Um deles é o “issues”, para rastrear bugs de software e alterações sugeridas. Outro é o “wiki”, que é usado para informações públicas sobre nossos serviços. Um wiki é uma série de páginas da Web editáveis pelo usuário.

Portanto, neste exemplo no Github, temos uma “organização” chamada “bristol-braille” e, dentro dela, temos um repositório chamado “canute-ui”. Dentro desse repositório, temos “problemas” relacionados e um “wiki” relacionado. Portanto:

- O código da interface de usuário do Canute está disponível publicamente aqui: https://github.com/bristol-braille/canute-ui/
- Os problemas relacionados à interface de usuário da Canute estão disponíveis publicamente aqui: https://github.com/bristol-braille/canute-ui/issues/
- O wiki que descreve informações úteis para a interface de usuário da Canute está disponível publicamente aqui: https://github.com/bristol-braille/canute-ui/wiki/

--------

# Solução de problemas 

## A tela Braille não está exibindo Braille

Se a exibição em Braille não estiver funcionando, mas você suspeitar ou souber que o computador está ligado e que a exibição visual e o som estão funcionando, será necessário seguir alguns procedimentos.

Primeiro, desligue o console. Se não for possível fazer isso de forma limpa sem conseguir ler o visor em Braille, faça o seguinte procedimento:

- Pressione 'ctrl+alt+f2'

- Agora, digite 'bbt' e dê enter, depois 'bedminster' e dê enter (ou seu próprio login e senha).

- Agora pressione escape (isso sairá do menu do Launcher).

- Agora digite shutdown now.

- Aguarde vinte segundos e, em seguida, puxe o cabo de alimentação.

Se tiver dificuldade para concluir o procedimento acima, pule para a desconexão do cabo de alimentação.

Em seguida, volte à parte superior deste manual para seguir as etapas para conectar corretamente o Canute 360 ao encaixe do console Canute, incluindo o cabo USB-B na parte traseira esquerda, *antes* de conectar o console.

Quando conectar o console novamente, lembre-se de pressionar o botão de energia na parte traseira direita.

Se isso não resolver o problema, entre em contato com o seu distribuidor ou com a Bristol Braille Technology para obter suporte.

## Problemas de login

Você está tendo dificuldades para fazer o login, por exemplo, está sendo redirecionado para a página de login? Isso pode acontecer com as versões mais antigas do software do Console, mas foi corrigido com as atualizações. É claro que, para aplicar a atualização, você terá que fazer login primeiro. Portanto, pressione esta combinação de teclas para alternar para um terminal de login diferente:

`ctrl+alt+f2`

Agora, insira seus detalhes de login. Depois de entrar, siga o procedimento de atualização abaixo (procure por “Upgrading”).

## Problemas de conexão com o WiFi

Se você receber o erro “Não foi possível conectar... Os segredos eram necessários, mas não foram fornecidos”, tente executar os dois comandos a seguir:

`$ nmcli con delete “NOME DO SSID”`
`$ nmcli dev wifi connect “NAME OF SSID” password PASSWORD ifname wlan0`

## Testar a conexão com a Internet

Teste a Internet no console digitando `$ clear; ping bbc.com -i 4`. Você deverá obter um registro de várias saídas continuamente atualizadas. 

(A parte `-i 4` desse comando é para dizer ao sistema para executar o ping nos servidores da BBC uma vez a cada quatro segundos).

Você deve receber uma resposta como

`64 Bytes from 151.101.192.81 [etc etc]`

(O endereço IP pode ser diferente.)

Isso significa que ele enviou uma solicitação ao site da BBC e recebeu uma confirmação positiva de volta, o que significa que está se conectando. Acontece que a mensagem de retorno tem 64 bytes de comprimento.

Pressione `[ctrl]+c` para cancelar o programa e obter os resultados.

Se o teste acima não lhe der quase 100% de “pacotes recebidos”, então você tem um problema com a sua conexão. 

## Problemas de Internet com IPv4 vs. IPv6

Uma causa de problemas de conexão é se a sua conexão estiver tentando usar endereços IPv6 quando o seu provedor de serviços de Internet não oferece suporte a eles. 

Se você suspeitar que essa é a causa, poderá criar uma configuração para desativar o IPv6:

`$ sudo echo “net.ipv6.conf.all.disable_ipv6=1” >> /etc/sysctl.d/local.conf`

Explicação: sudo = executar como superusuário; echo = escrever string; >> = anexar ao arquivo

## Solução de problemas adicionais

Entre em contato com a Bristol Braille se o seu problema não for abordado acima.

Se você se sentir confortável com as listas de problemas de programadores, poderá [registrar um problema em https://gitlab.com/Bristol-Braille/console-learn/-/issues] (https://gitlab.com/Bristol-Braille/console-learn/-/issues)

