% Canute 360 Manual
% Bristol Braille Technology
% Last update: 2025-01-10

Notes
=====

Translation note: All non-English translations are translations from the English using DeepL Pro. If you note translation errors please let us know about them.

Contact: enquiries@bristolbraille.org

Welcome
=======

Thank you for purchasing Canute 360 from Bristol Braille Technology, the
world's first multi-line Braille e-reader.

Canute 360 features a nine-line display, with each line comprised of 40
cells of Braille. Canute 360 is compatible with *.BRF and *.PEF file
formats and can display Braille text, mathematics, music, or any other
six-dot Braille code.

With Canute 360, you can read files in Braille, as well as insert, edit,
and navigate to bookmarks.

Bristol Braille Technology is a not for profit community interest
company. Your purchase will allow us to continue to invest in and
develop new Braille products to benefit the wider blind community.

Inside the box
==============

You will find the following included within the shipping box:

-   1 × Canute 360 Braille e-reader

-   1 × corded 19V power supply

-   1 × Braille quick start guide

Please let your distributor know if any of these items are missing or
damaged.

Document Conventions
====================

Within this user manual, where text displayed in Braille by Canute 360
is referenced, it is rendered in quotes. Where Braille is embossed on the surface of the device it is rendered here as Braille, for example, the menu button as (⠍⠑⠝⠥).

**Before using this information and the product it supports, be sure to
read and understand the important safety information at the end of this
document.**

Physical description
====================

Your Canute 360 should be placed flat on the table in front of you. The
three large control buttons on the front panel of the unit (labelled
back (⠃⠁⠉⠅), menu (⠍⠢⠥) and forward (⠿⠺⠜⠙) should be facing
towards you.

Your Canute 360 has a nine-line, 360 cell Braille display on its top
surface.

At the top left-hand corner of the top surface, you will find a circular
button, labelled "H" (⠓). Directly beneath this button, you will find
nine triangular buttons, labelled "1" (⠼⠁) through "9" (⠼⠊), and one
square button labelled "0" (⠼⠚). At the top right-hand corner of the top
surface, you will find a "BBT Canute 360" (⠄⠄⠃⠃⠞ ⠄⠉⠁⠝⠥⠞⠑ ⠼⠉⠋⠚) label.

In the centre of the top surface, beneath the final line of the Braille
display you will find back (⠃⠁⠉⠅), menu (⠍⠢⠥) and forward (⠿⠺⠜⠙)
labels. On the front surface of Canute 360, you will find the back, menu
and forward navigation buttons.

On the left side panel, you will find several peripheral ports. From the
front, you will find an HDMI port and a 3.5mm audio-out jack (the audio
jack is not currently supported). The SD card slot is at the bottom of
the left side panel, in the centre. Towards the rear of the left side
panel, you will find two standard USB ports and a USB-B port.

At the right-hand side of the rear panel, you will find the power on/off
button, which is proud of the surface. Immediately to the left of the
power on/off button is the power socket.

There are no controls or sockets on the right-hand side panel of your
Canute 360.

During operation, your Canute 360 will make a quiet ticking noise. This
is perfectly normal.

Your Canute 360 does not contain any user-serviceable parts. The base
panel and side plates should only be opened or removed by a qualified
service engineer.

Getting started
===============

When your Canute first arrives please leave it to reach normal room
temperature for several hours (or overnight if possible) before turning
it on for the first time. Skipping this step will not damage your Canute
but may result in errors on the display on first use. This is necessary
due to frequently very cold conditions during transit.

Place Canute 360 on a flat surface, with the three large control buttons
on the front panel facing towards you. Insert the power supply into the
socket towards the right of the rear panel of Canute 360, and briefly
press the small on/off button located immediately to the right of the
power socket once to turn the unit on.

Under normal conditions the Canute 360 will take around 50 seconds to turn on. However if it is starting from cold it can take around 4 minutes to start as it runs a warm-up routine. If restarting a machine that was on recently it may start within 20 seconds.

During start up the top line of the display will raise and lower momentarily several times. You will then hear, feel, and see each of the nine lines of Braille clearing any previous content one at a time. 

Navigation controls
===================

On the front edge of Canute 360 are three large control buttons. These
are labelled back (⠃⠁⠉⠅), menu (⠍⠢⠥), and forward (⠿⠺⠜⠙) in
Braille on the top of the reading surface.

Directly to the left of each line of Braille is a triangular line select
button. These are labelled with numbers one through to nine in Braille;
corresponding to the adjacent line on the display.

Immediately above the first triangular line select button is a circular
help button. This is labelled "H" (⠓) in Braille. You can press this
button at any time to bring up a help file for the page you are
currently viewing.

Immediately below the last triangular line select button is a square
button, labelled "0" (⠚) in Braille.

Getting files onto your Canute 360
==================================

Your Canute 360 is compatible with both *.BRF (Braille Ready Format)
and *.PEF (Portable Embosser Format) files. For best results, we
recommend using a *.BRF file, formatted for a page length of nine
lines, and a width of 40 characters per page.

Translating print files into Braille
====================================

A number of software packages and online services can translate a print
document into a Braille file, which can be read on your Canute.

The latest version of Duxbury DBT™ includes a "Canute 360" preset to
ensure correct formatting. To use Duxbury DBT™ you will need to purchase
a licence from Duxbury systems. You can find more information at
[https://www.duxburysystems.com](https://www.duxburysystems.com/).

You can also use a number of free software packages to translate print
documents into BRF format. BrailleBlaster™ is a free program suitable for 
preparing documents to be read on Canute 360, available for Windows, Mac 
and Linux users. You should ensure that Braille output is set to produce a
document with nine lines per page, with a line length of 40 characters.
You can find more information, and download Braille Blaster from
[https://www.brailleblaster.org](https://www.brailleblaster.org/).

RoboBraille™ is a free online translator from Sensus in Denmark which
can translate a print document or webpage into a BRF Braille document to
be read on Canute 360. To use RoboBraille™, navigate to
[www.robobraille.org](http://www.robobraille.org/) in your preferred web
browser. Select "file" from the "Source" menu to translate a file on
your computer. Select "URL" to translate a website.

To translate a file, click on "choose file", and select the file using
your file browser. Then click on "upload" to prepare your file for
translation.

To translate a webpage, enter the URL into the text field after
selecting the "URL" option, then click on "fetch and upload" to
prepare the webpage for translation. Please note that in order to work
with robobraille a webpage URL must end in a file format (e.g.
[www.example.com/example.htm](http://www.example.com/example.htm)). A
webpage ending in a top-level domain (e.g.
[www.example.com](http://www.example.com/)) will return an error
message.

Select "Braille" from the "select output format" menu.

From the "Specify Braille options" menu, you can select your preferred
language and Braille code settings. Select "Canute 360" from the
"Target Format" menu to ensure your file is correctly formatted for
your Canute 360.

Finally, enter your email address in the "Enter email address and
submit request" form. Click "Submit" to confirm your choices. You
will receive a BRF file formatted for Canute 360 in your email inbox
within a few hours.

Bristol Braille Technology cannot accept responsibility for the accuracy
or performance of any third-party Braille translation service.

Transferring Braille files to Canute 360 with an SD card
========================================================

To get a file onto Canute 360, you will need an SD card. Your Canute
should always have an SD card in it as that is your personal library and
where Canute saves its current page and system options like language. SD
cards can be either SD or SDHC, but not SDXC or SDUC. They should be
formatted to FAT32, but not FAT16, exFAT or any other format.

Copy and paste the file onto the SD card. With recent firmware upgrades 
you can now use folders in the Canute 360 if you prefer. Simply copy and 
paste files and folders directly onto the SD card. 

Insert the SD card back into the SD card slot towards the centre of the
bottom of the left side panel. Turn Canute 360 on, navigate to the
library menu, and the file will appear in the library menu. Files are
arranged in the menu first numerically, and then alphabetically.

Hot swapping USB sticks and multiple SDs
========================================

You can also use a standard USB memory stick to get files onto Canute
360. You should still use your SD card, or the bookmarks and current 
page will saved to the USB stick instead and will not be remembered 
when you reinsert the SD card. Memory sticks are therefore only 
intended to be used for quickly supplementing your library with new 
files.

To use a USB stick copy and paste the files onto the memory stick and 
insert it into a USB port on the left side panel of Canute 360.

You can also hot-swap between various SD cards, which is especially 
useful if more than one person is using the Canute. This allows 
everyone who uses the Canute to maintain their own bookmarks and 
current pages.

Make sure the page is not refreshing before you swap. After you swap
leave your Canute for about ten seconds; it will do a full reset and
return to the last page visited on that SD card. Remember that your
system information, bookmarks and current page are not stored on the
Canute itself, so will be swapped with the SD card and/or memory stick.

Special Files
=============

On your SD card you may notice two types of files called
"canute.book_name.brf.txt" (where "book_name" is the filename of your
book) and "canute_state.txt". The former is also found on memory
sticks.

These files are used for storing system information, bookmarks and page
information. You should not delete these files, or you will delete any
bookmarks stored within them. However you can edit the
"canute.book_name.brf.txt" file in a PC text editor like Notepad++ to
add or remove bookmarks manually.

Choosing a book from the library
================================

Canute 360 is designed with an easy to use Braille user interface. To
select a file to read on Canute 360, navigate to the library menu.

First, press the large menu button in the centre of the front edge below
the reading surface. The main menu will load, one line of Braille at a
time, from top to bottom. The entire page will take around 10 seconds to
display.

As with all menus on Canute 360, options are selected using the
triangular line select buttons. To select an item from a menu, press the
triangular line select button immediately to the left of that line of
Braille.

On the main menu 'view library menu' is displayed
towards the bottom of the reading surface. Press the line select button
immediately to the left of the menu option to select it. The library
menu will load, one line of Braille at a time, from top to bottom. The
entire page will take around 10 seconds to display.

The library menu will display a list of folders that contain recognised 
Braille files on the SD card or USB stick. You will see top level 
folders like "canute", "sd" and "usb". Select a folder by pressing the 
line select button to the left of the folder you want to enter.

Once you are in a folder you can go back up a level by selecting the
line select button which lines up with the option for going back up a 
level.

To select a file in a folder press the line select button to the left 
of the file you want to read.

If more than 8 files are present in the folder the required file may 
be on the next page of the library menu. Press the
large forward button to the right of centre on the front panel to
advance the library menu one page.

Press the large back button to the left of centre on the front panel to
move back one page in the library menu.

Once a file is selected, it will load one line at a time, taking around
6--14 seconds to load the whole page.

To return from the library menu to the book you were reading, press
either the large menu button or line select button one.

There are two files built into the library menu which will always
appear. One is for service engineers to use while cleaning and testing,
and the other contains information about Bristol Braille Technology. 
When using the Canute without an SD card or USB stick it will default 
to openning the latter.

Moving within a file
====================

To move within a file, use the three large control buttons on the front
panel of Canute 360.

Press the large forward button to the right of centre on the front panel
to move forward one page within a file.

Press the large back button to the left of centre to move back one page
within a file, similar to turning the pages in a physical book.

In the unlikely event you notice any errors in the Braille, or dots out
of place, you can refresh the Braille display by pressing and holding
the square "0" (⠚) button for a couple of seconds. The display will
reset after around 20 seconds.

Main menu
=========

From the main menu, you can insert a bookmark at the current page,
return to a bookmark that was previously placed within a file, or
navigate to a given page within a book.

To navigate to the main menu, press the large menu button. While in any
menu on Canute 360, pressing the menu button again will return you to
the last page you were reading.

As with all menus on Canute 360, to select an item from the main menu,
press the triangular line select button immediately to the left of the
menu item on the reading surface.

To navigate to a page within the file, press the line select button
immediately to the left of 'go to page' on the reading
surface. A help page will appear on the reading surface. To enter a page
number, use the triangular line select keys and the square zero button
located to the left of the reading surface.

To go to page 20, for example, press button number two, followed by the
zero button. You will hear, feel, and see one line of Braille click up
and down to confirm the selection, then press the forward button to
navigate to the selected page. Alternatively, press the back button to
undo your selection.

Bookmarks
=========

To place a new bookmark at the current page in a file or return to a
bookmark previously placed within a file, first navigate to the in-book
menu by pressing the large menu button on the front panel of Canute 360.

To insert a bookmark press the line select button immediately to the
left of 'insert bookmark at current page'.

A bookmark will be inserted at the current page. NB there is currently
no feedback when a bookmark is inserted, this will be fixed in a later
software release.

To retrieve a bookmark, press the line select button immediately to the
left of 'choose from existing bookmark'.

A list of bookmarks in a file will display on the reading surface. Use
the triangular line select buttons to select a bookmark. Canute 360 will
then display the selected page in the book.

From the bookmark menu, it is also possible to select 'start of book' or 'end of book' to navigate to the
beginning or end of a book using the line select button immediately to
the left of these options.

Changing language or Braille code
=================================

You can change the system language or Braille code (for example to
switch between contracted and uncontracted Braille) from the system
menu. Please note this will only change the language and code within the
menus, help file, and manuals; not the language or code in any books on
Canute 360.

To access the system menu, first access the main menu by pressing the
large menu button in the centre of the front edge of Canute 360. Then,
press the line select button immediately to the left of 'view system menu' to access the system menu. Press the line
select button immediately to the left of 'select language and code' to access the language selection menu. You
can then select a language or Braille code using the triangular line
select button immediately to the left of your chosen language or code.
Press the forward button once to confirm your selection.

Monitor output
==============

You can view text displayed by Canute in both print and Braille using a
standard computer monitor. Simply plug your monitor into the HDMI port
towards the front of the left side panel. The print representation will
display in a green font, and the Braille will display below in a white
font. The print representation is in Braille ASCII (it doesn't back
translate the text; it is meant for sighted Braille readers).

Canute as a display
===================

Canute 360 can be used as a display when plugged into a PC with the
USB-B port. Currently this is supported by BRLTTY and Duxbury DBT.
Please see the support section on our section on the website for using
the Canute 360 with a PC.

Shutdown
========

To safely turn off your Canute 360 either go to the system menu or 
press the power button located to the right of the power socket on 
the back panel. Then select "shutdown".

You will hear, see and feel each line of the display reset, and 
drop down into the display. After a few seconds the top line of the display will refresh to show
"PLEASE WAIT..." (⠠⠏⠇⠑⠁⠎⠑ ⠺⠁⠊⠞⠲⠲⠲), and the other 8 lines will lower.
After around ten seconds, the first line will also lower. It is now safe
to remove the power supply from Canute 360.

Caring for your Canute 360
==========================

Your Canute 360 does not contain any user serviceable parts. The base
panel and side plates should only be opened or removed by a qualified
service engineer.

You can clean the front and sides of your Canute 360 using lint-free 
cloth made a damp, but (not wet) with an alcohol cleaning solution. 

You can clean the dots of the display itself by going into the library
menu, selecting the build in "for testing and cleaning" book and 
running through it one page at a time, cleaning all the cells each 
time.

Bristol Braille Technology recommends cleaning your Canute 360 at 
least once a month. You should only attempt to clean
your Canute 360 when it is turned off, with the power disconnected. Do
not attempt to clean any mechanical parts of the Braille display
yourself.

If you store your Canute 360 inside the shipping box, you may notice a
powdery white residue building up on the front and side panels. This is
normal, does not affect operation, and can be removed by wiping with a
lint-free cloth.

Updating your Canute software
=============================

From time to time, Bristol Braille Technology may make available
software updates for Canute 360 to fix any issues; improve performance,
or add additional features.

Each update will come with installation instructions. To install an
update, you will require an empty USB stick or FAT32 format SD card. SD
cards can be either SD or SDHC, but not SDXC or SDUC. Most smaller
capacity SD cards will be FAT32 format. For full installation
instructions, consult the readme document packaged with your update.

Important safety information
============================

*CAUTION: before using this manual please read and understand all the
related safety information for this product.*

Service and Support
-------------------

Your Canute 360 does not contain any user serviceable parts. The base
panel and side plates should only be opened or removed by a qualified
service engineer. Do not attempt to service your Canute 360 yourself
unless instructed to do so by the customer support centre. Only use a
service provider who is approved by Bristol Braille Technology.

Power cords and power adapters
------------------------------

Use only the power cords and power adapters supplied by Bristol Braille
Technology. 

Never wrap a power cord around a power adapter or other
object. Doing so can stress the cord in ways that can cause the cord to
fray, crack, or crimp. This can present a safety hazard. 

Always route
power cords so that they will not be walked on, tripped over, or pinched
by objects. 

Protect power cord and power adapters from liquids. 

Do not
use any power adapter that shows corrosion at the AC input pins or shows
signs of overheating (such as deformed plastic) at the AC input or
anywhere on the power adapter. 

Do not 'daisy-chain' your power adapter by 
plugging it into an extension cord which is itself plugged into another
extension cord. We recommend only ever using extension cords that are 
properly earthed (grounded).

Heat and product ventilation
----------------------------

Your Canute 360 and its power adapter can generate heat while in use.
This is perfectly normal. Your Canute 360 is a desktop device and should
always be placed on a desk or table. You should not place your Canute
360 on a bed, sofa, carpet or other flexible surface that could 
accumulate heat. To avoid the risk
of injury you should not leave your Canute 360 in contact with your
lap or any other body parts for prolonged periods during operation. 
You should always turn off
your Canute 360 and disconnect the power adapter when not in use.

Operating environment
---------------------

Your Canute 360 is designed to work best at room temperature, between
10°C and 25°C (50°F-80°F) with humidity ranging between 35% and 80%. If
your Canute 360 is stored or transported in temperatures less than 10°C
(50°F), you should allow the temperature of your Canute 360 to rise
slowly to between 10°C and 25°C (50°F-80°F) before use. This process
could take up to two hours.

Failure to allow your device to rise to an optimal operating temperature
before use could result in impaired operation, or damage to your Canute
360.

Do not place any beverages on top of or beside your Canute 360. If
liquid is spilled on Canute 360 a short circuit or other damage might
occur. Do not eat or smoke over your Canute. Any particles that fall
into your Canute 360 may damage the mechanism.

We recommend cleaning your hands before using your Canute 360, 
especially if you have recently been eating, smoking, or handling dusty
or dirty items.

Overheating
-----------

The Canute has internal thermal management. If it goes above a certain
temperature, judged sub-optimal for the mechanism (approximately the
same as when the display is hot to the touch), it will pause all
operation until the temperature falls. Simply allow it to cool down and
it will restart, where it left off, of its own accord.

Electrical input
----------------

Your Canute 360 is designed to work under the following electrical
conditions:

Input voltage: minimum 100V(AC) maximum 240V(AC), input frequency
between 50Hz and 60Hz

Noises during start-up and operation
------------------------------------

When first turned on Canute 360 will make a louder than usual noise as
it performs a reset of the display. It will go through this twice before
starting up, taking about a minute in total. This may take as long as five minutes if your Canute is starting from cold.

Once on and being used the Canute 360 will make a gentler 'line setting'
noise for every line on the display that is being changed.

When your Canute 360 is left on and is not being used it may make a
gentle 'ticking' noise, depending on the firmare version you are using.

Support 
=======

For technical support, in the first instance contact your distributor.

To report a bug, or contact the team, please email
[support@bristolbraille.org](mailto:support@bristolbraille.org)

For more support information, please visit
[https://bristolbraille.org/support](http://www.bristolbraille.co.uk/support)

The many uses of the Canute 360
===============================

Braille is literacy, employment and independence, so having become more
familiar with the basic use of the Canute 360, users will find many
additional possibilities beyond simply reading books.

-   Save Paper: an obvious saving is in bulky, hard copy braille.

-   Tables: become much more readable when up to 9 lines can be
    displayed.

-   Mathematics: often contains long and complex equations which run
    over multiple lines.

-   Music: scores are invariably best understood when multiple lines can
    be read.

-   Learning: Braille teaching courses can easily be adapted to display
    on Canute.

-   Languages: displaying different languages in the same document is
    both possible and practical.

Notes on versions and copyright
===============================

The main manual is stored on the website. All copyright belongs to
Bristol Braille Technology CIC and Techno-Vision Systems.

Last edited 10^th^ of January, 2025

