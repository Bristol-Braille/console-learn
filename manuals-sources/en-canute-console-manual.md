% Canute Console Manual
% Bristol Braille Technology
% Last update: 2024-09-05

# Notes

Translation note: All non-English translations are translations from the English using DeepL Pro. If you note translation errors please let us know about them.

Contact: enquiries@bristolbraille.org

# What is the Canute Console?

The Canute Console is a Linux workstation which includes a Canute 360 Braille display. The Canute Console Premium is a bundle that includes custom support or development, a lockable flight case and longer warranty.

The big difference between the Canute Console and other products is that the Braille and the visual displays show the same amount of information in the same layout, disadvantaging neither Braille nor print user.

It has an intuitive interface of menus for reading BRF books and launching applications. It can also be operated from the command line and in that mode it is primarily intended for computer science and system administration. 

There are few limits on what uses it can be put to once you become used to working in multiple lines of Braille.

# Inserting and removing the Canute 360 Braille display into the Console 

If you bought your Console as a single device you do not need to do this to begin using the Console. 

If you bought the Console without a Braille display (the 'upgrade kit') then lay both products on a flat surface where you intend to use them. Open the lid (the monitor) on the Console Upgrade Kit. Lift the Canute 360 and put it gently on the flat surface in the middle of the Console Upgrade Kit, being careful to line the edges up. 

At the back right there is a protrusion from the Upgrade Kit. This is the male power connector and needs to line up with the female power connector on the Canute 360. 

Once that lines up move the Canute 360 backwards to insert the power connectors. You may need to lift the Canute 360 slightly to make the connexion.

Now on the back left hand side on the Upgrade Kit you will feel a USB-B male connector (these are commonly called 'printer connectors') on a very short cable of about 2 inches. Pull it out of the hole it is resting in and insert it into the Canute 360's female USB-B port on the right.

To remove the Canute 360 do the same as above but in reverse.

# Ergonomics

The Canute Console is meant to be used on a stable, flat desk or table with a smooth surface.

It needs the smooth surface in order for the tray with the keyboard to be taken in and out.

Now lift the monitor (which also acts as a lid) and pull out the keyboard.

Because the Canute Console has a lot of Braille which you need to reach over a keyboard to read, ergonomics are especially important.

Therefore make sure your seat is as high as possible compared to the desk/table.

Also make sure the Console is comfortably towards the edge of the desk/table.

You should be able to comfortably rest your hands on both the top line of the Braille display and the last line of the keyboard.

# Port layout of the Canute Console

(Please note that when docked in the Console the Canute 360's ports are no longer in use.)

On the left of the Canute Console, from the back to the front: 

 - 1x male USB-B connector, which is either inside a small hole in the Console Upgrade Kit (when the Canute 360 is disengaged), or is inserted into the Canute 360's female USB-B port.

 - 3x female USB-A connectors.

On the right of the Canute Console, from back to front:

 - 1x female power connector.

 - 1x female 35.mm audio jack.

 - 1x rocker for controlling external monitor.

 - 2x switches for controlling external monitor.

 - 1x female HDMI connector.

On the back, from middle to right:

 - 1x removable panel for additional future ports.

 - 1x power switch.

If you have one of the Early Bird Consoles then your port layout may vary from the above.

# Plugging everything in

After making sure the USB-B is plugged into the Canute 360, and inserting the power cable, press the power button on the back right had side to turn on the Braille display.

# Unplugging

After shutting down the Canute Console (see below) wait for the Canute display to shut down, then remove the power cable. Note that if you do not remove the power cable the monitor will still have power and go into screen-saver mode. Therefore a Canute Console not in use should always be unplugged.

# Keyboard layout

Assuming you are using the British keyboard (otherwise please see below 'Changing the keyboard layout'), the layout of the keyboard is as follows:

First line: Escape, F1 to F10 (F11 is Fn+F1, F12 is FN+F2), Num lock, Print Screen, Delete (Insert is Fn+Del)

Lines two, three, four and five are the default for British Qwerty keyboards.

Line six: Control (ctrl), Function (Fn), Super key, Alt, Space, Alt, Ctrl, Arrows (Home is Fn+LEFT, End is Fn+RIGHT, Page up is Fn+UP, Page down is Fn+DOWN)

# Starting in 'Reader Mode'

With the Console correctly set up, as above, plug the power cable in and press the power button at the back.

The Braille display will approximately take a minute to start, during which time it will run a dot clearing routine. 

Once it has started the Canute Console will be in 'Reader Mode'. This operates the same way as the Canute 360 stand-alone Braille display and does not require the Qwerty keyboard. 

The 'H' key on the top left of the display launches contextual help to explain how each of the menus works. Briefly: the 'menu' thumb button will open the menu of options or, if you are already in a menu, return to your current book. The 'previous' and 'forward' buttons are for moving through books and menus. The side buttons correspond to the labelled items for menus.

You can read more about this in the 'Canute 360 user manual' which is also in this directory (~/learn/).

When the display is docked in the Console it will read any BRF files *in your home directory in the Console's internal storage*. It will not read the SD card in your Canute 360, which is only read when the Canute 360 Braille display is used stand-alone (undocked).

# Going into 'Console Mode'

'Console Mode' is where all the interactive applications work. This is essentially booting into a PC.

To exit 'Reader Mode' either select the option from the 'System Menu' or pull out the Qwerty keyboard and press the Escape key.

You will now have a log in screen.

If you have upgraded to this version from an older version it will not change your username or password.

For new users the default user is "bbt", the default password is "bedminster", all lower case.

We strongly suggest changing the default password as soon as possible and making a note it. More on that below under "Changing your password".

In 'Console Mode' you will have a 'Launcher' menu of options very similar to the menu of books in 'Reader Mode' It works much the same way, except that the 'Menu' button does not operate in this mode.

Each line is an application or command, such as 'File Explorer', which will open files in the 'Word Processing' application. When you exit one of these applications you will be returned to the Launcher. If for some reason you are not returned to the Launcher but instead dropped to the command line you can load it again by typing:

`$ launcher`

# Running commands from the Launcher

There are a couple of important menu items that we will list and explain below:

 - 'Shutdown'. This will power down the Canute Console and Braille display. Once powered off the Braille dots will all go down. Once this happens you should take the power cable out of the Console.

 - 'WiFi'. This will open a list of available connexions with their signal strength. Once you have found the connexion you want press 'q' to quit it and you will be prompted to type in the name of the connexion, followed by the password.

 - 'Upgrade'. This will look for an upgrade to the latest version of the Canute Console OS. This requires an Internet connexion first. We will send out emails when there is a major upgrade so please subscribe to the newsletter on the [Bristol Braille website: https://bristolbraille.org/](https://bristolbraille.org/). The upgrade may take some time, depending on your connexion speed. Once completed log out by typing `shutdown now`. Pull the power cable, plug back in again and press the power button.

 - 'Command line'. This drops down to the command line prompt. We will explore the command line in detail below. However you do not need to use the command line until and unless you want to.

# Running applications from the Launcher

The list of applications in the Launcher is always growing. Try each one to see how they work. All these applications are under continued development, some are still experimental, and all will improve with future software releases. Let us know what else you would like to see. 

Here are a couple of useful ones to get started with:

 - 'File explorer'. This will open a tree view of your file system which you can navigate with the forward and back thumb buttons, just like the Reader and Launcher menus. If you select a file that will launch the 'Word Processing' application, which gives you a menu for viewing and editing rich text format documents, including a unique print-layout view of PDF files.

 - 'Text editor' This opens a text editor called 'Micro' on a blank document. Micro works very much like Notepad on Windows with similar commands. ctrl+x/c/v' for copying and pasting, 'ctrl-z' to undo, 'ctrl-y' to redo, 'ctrl-f' to find, 'ctrl-s' to save, 'ctrl+q' to quit, for example.

# Running a few commands from the command line

The following features will shortly be added to the Launcher menu but can currently be executed from the command line.

To do the following first select the 'command line' option from the Launcher menu. This will open a 'prompt', that is a line into which you can type commands. This line is represented by a dollar sign. Previous commands will stay on the display and a new prompt will appear at the bottom of the display. The last prompt is the one you will type into.

Once you are done you can return to the menu by typing `launcher` at the prompt.

## Changing Braille codes

In the command line there are some special commands for the hot-switching the Braille code table you are using, including:

 * $ [alt]+[shift]+Q
   (Grade One UEB)

 * $ [alt]+[shift]+W
   (Grade Two UEB)

 * $ [alt]+[shift]+E 
   (9-line 6-dot US Computer Brl

 * $ [alt]+[shift]+R 
   (Contracted German, 2015 standard)

Please note that only US computer brl guarantees one-to-one character representation and therefore exactly the same layout as the visual screen.

If you know the name of a different BRLTTY Braille table you want to use then you can enter that with:

`$ canute-table [table-name]`

## Changing your password

To change the default password type `$ passwd` and follow the instructions.

# Escape!

Now we know the basics, the most important commands are closing programs and shutting down safely.

To stop a program that is running there are often commands specific to that program. Often `q` or `ctrl+q` is used to exit, or the `escape` key (top left on the Console keyboard). However pressing `ctrl+c` often works in extremis (if a program is hanging or you don't know how to close it). 

If you really cannot close a program to get back to the command line or Launcher you can open a new terminal tab. See below.

 --------

That is all you need to know to get started with the Canute Console. If you are interested in learning more about using the Console, particularly in the command line, or programming your own application, or to troubleshoot an issue you are having, then read on below.

 --------

# Basics for navigating the Linux command line

## Nomenclature and the layout of the command line

Linux is the operating system Canute Console is running. It is specifically running a version of Linux called 'Raspian', which is based on 'Debian'.

The Canute Console's 'Console Mode' is prefigured to run entirely from the command line, meaning its applications are all text and terminal based.

You will often see the terms "command line" or "terminal" (also "tty" and console") used interchangeably as they have very similar meanings. But, in short, here are their differences:

 - The "command line" is usually the last line on the screen and it is where commands can be written. The command line includes a "command prompt", meaning "where you type your commands".

 - The "command line" is inside a "terminal". The terminal includes the history of previous commands you have entered, plus their output.

Throughout the rest of this document we are using instruction that start with a dollar sign, then an instruction, then sometimes some other words or numbers separated by a space.

This is how commands are run in the Linux command line. It is very similar to the DOS Prompt on Windows.

The dollar sign is just a symbol meaning "command prompt", that is, "type your commands after this".

## How to type commands and understand the terminal output

So when we write, for example, an instruction like "Run `$ ls`" means, "Type 'ls' into the command line and hit enter". In that case it actually means, "list the sub-directories and files" in my current directories.

As another example we might write in this document, `$ cd demos` means "Run the command 'cd' with the instruction (called an argument) 'demos'". Actually in that case it means "go to directory called 'demos'."

It is important to note that the terminal stores all your previous commands and doesn't clear them off the screen unless asked to.

Think of it like a typewriter or embosser, writing one line at a time, not erasing the previous lines.

Always read to the last line of the display and assume the *last line that has the dollar prompt on it* is the active command line, and the lines above are history.

For example if I wanted to type a command like "make a directory called 'documents', which should be written as `mkdir documents`, but made a mistake, then the Braille display output might look like this:

```
$ mjdir documents
bash: mjdir: command not found
$
```

Lets look at that again, line by line. 

`$ mjdir documents`

I have made a typo with the command.

`bash: mjdir: command not found `

This is the error message ('bash' is what's called the 'shell', meaning the program that is interpretting your commands).

`$ `

So now we are given another blank command prompt, but note that we can still see our first attempt and the error message above.

Those used to single line Braille displays might find this takes some getting used to. But it is, however, the standard behaviour for the Linux terminal and precisely the same layout and output as sighted users are getting on the visual monitor.

If your screen is too cluttered then the `$ clear` command is your friend. It will reset the terminal to blank other than the current command prompt on the top line of the display.

## Getting around

Linux uses a hierarchical file system, meaning directories and files are all inside other directories, back to the 'root'. The root is at the top of the hierarchy.

Directories are separated by stroke symbols, so your home directory is written as, `/home/[username]/`

When logging in you will start in your user's home directory. To start with you will seldom need to go 'up' beyond your home directory.

`$ ls` lists the sub-directories and files in the current directory.

You may find that outputs too long a list to read. You can scroll back up and down the terminal output history by pressing `ctrl+Fn+UP` or `ctrl+Fn+DOWN`.

To move between directories you use the `cd` command. `$ cd directoryname`.

To go back down a level use the `$ cd ..` command.

Here we note an important thing about many Linux commands, like `cd`. When you ask to go into a directory, it will do it but not give you any output to say whether it has done it. 

So how do you know where you are? You can type `$ pwd`. This returns your current location within the file system. 

## Tips to make navigation and typing commands easier

You may note that for some commands the output string is longer than the display (40 cells), therefore the Canute has panned all the way to the right. 

To pan left and right on the Braille display (this doesn't affect the visual display) to view strings longer than 40 cells you can press the '[MENU]+[FORWARD]' thumb buttons on the front of the Canute display to go right, or '[MENU]+[PREV]' to pan left.

You can auto-complete a command or a directory or file name when you are typing into the command prompt by tapping the tab key.

If there is only one available option then tab will auto fill it. If it could be multiple things then pressing tab won't do anything, but double tapping tab will bring up all the options and write them the the terminal so you can view them.

## Reading text files

If there is a text file you want to read you can type, `$ less textfilename.txt`. It doesn't have to have a TXT extension and often, in Linux, they won't have any extension at all. If in doubt, try `less` on it.

Less will print the first eight lines of the file. Use the page up and down (Fn+UP, Fn+DOWN) to move around it, press 'q' to exit back to the command line.

## Useful commands 

Include:

 * ls|less
   (Lists files and sub-directories)

 * tree|less
   (Displays directory tree)

 * cd [DIRECTORYNAME]
   (Go to sub-directory)

 * cd ..
   (Go up to parent directory.)

 * mkdir [DIRNAME]
   (Make new directory)

 * micro [FILENAME]
   (Open text file)

 * [ctrl]+[shift]+t
   (Open's new tab in terminal.)

 * [ctrl]+[shift]+[NUM] (0--9)
   (Switches to another tab.)

 * shutdown now
   (Shutdown, but still need to 
   turn off Canute after shutdown 
   completes.)

 * touch [FILENAME]
   (Make new blank file)

 * rm [FILENAME]
   (Delete file)

 * rm -r [DIRNAME]
   (Delete directory and its contents; '-r' being short for 'recursive'.)

 * sudo [COMMAND]
   (Run the subsequent command as super-user, i.e. with full privileges.)

 * sudo rm -r [DIRNAME]
   (Delete a repository directory without being prompted for each write-protected file.)

## Learning resources

We suggest you listen to the following episodes of Braillecast, sponsored by Bristol Braille, to familiarise people with some of the basics of the Linux command line.

All episodes can be found in:
`cd ~/learn/braillists/`
and can be played with:
`vlc 01[tab to complete]`

 - 01: Firstly, an overview of different computer scientists using Braille in their work. This can be skipped.

 - 02: Secondly, a masterclass introduction to the Linux command line.

 - 03: Thirdly, a masterclass introduction to Git, for version control and sharing and downloading programs.

 - 04: Fourthly, a masterclass about programming on the Canute Console.

Included with your Canute Console are a few Linux basics learning tools. You can find them from the home directory by typing `cd learn`.
Finally, you can usually search for answers to any questions you have on the web. 

Most questions you have will be general to Linux. However if you need more specific answers then you can try searching for 'Debian', or even specifically for 'Raspian'.

We recommend a website called Stackoverflow for these kinds of questions.

 --------

You should now have a reasonable grasp of the basics of operating in the Linux command line when you chose to. The following sections will explain in more detail common tasks.

 -------

# Shutdown

To shutdown the whole unit from the command line type, `$ shutdown now`. (Or to reboot type, `$ reboot`.)

Once you have shutdown and the Canute display has turned off* you should take the power cable out of the Console (otherwise the monitor will stay on)

* N.B. If you have upgraded your Canute 360 that comes with the Console to firmware version 2.0.10 or higher then your Canute will turn off when the Console turns off. However if it running an older firmware then it will not turn off on its own, it will instead go into stand-alone book reading mode, at which point you need to press the power button on the back right to turn the Canute display off before you take the power cable out.

# Open a new terminal tab

Terminal tabs work like a tab on a web browser. Press `ctrl+shift+t` to open a new tab. The previous tab will still be operating in the background so you can switch between multiple tabs. 

To switch between tabs you can press `alt+[1--9]`. 

To close a tab from the command line type `$ exit`. Please note that when you close the original tab you will exit your session, so be careful to save all changes to documents before doing so.

Please note that opening a new tab or switching between tabs will not change the current Braille Code, so if you were in an application that was using Computer Braille then you open a new tab the new tab will also be in Computer Braille.

# Sound

Please note the commands below. The double s in sset is not a typo.

 - `$ amixer sset Master 0%`

 - `$ amixer sset Master 100%`

# Changing the keyboard layout

The command is relatively simple from the command line. For example:

`$ setxkbmap us`

Will change it to the US keyboard. 

For a list of available keyboard layouts type:

`$ ls /usr/share/X11/xkb/symbols/ | less`

To set it automatically add the same command to `~/console-set-up/console-start.sh`:

`$ setxkbmap us`

# Using USB sticks

USB sticks are automatically mounted to the `/media/` directory.

For example the first USB stick you insert will be mounted to `/media/usb0`, the second USB stick will be mounted to `/media/usb1`, and so on.

To access files on the USB stick use the `cd` as with any other directory, for example, `$ cd /media/usb0/`.

If your Console is not up to date you may need to install an application called `usbmount` before the above functionality works. This requires an Internet connexion (see below). Once connected type this command:

`$ sudo apt install usbmount`

Then once the install is complete restart the Console or remove and reinsert the USB stick.

# Connecting to WiFi

The Canute Console will automatically connect to any WiFi network it has saved. 

To choose between these networks (if there is more than one) type, `$ wifi new`

To connect to a new network for the first time type, `$ wifi new`, then to hit `q` which will bring up a prompt on the last line for you to type in the name of the connexion you want, them `enter`, then the password.

If you successfully connect you will see a message like `Device 'wlan0' successfully connected`, however you may not see this if you have already connected.

The next time you want to connect to that Wifi connexion but don't want to retype the password you can leave out the 'new' and it will remember it from last time: `$ wifi`

# Tethering to your mobile via the USB-A port

Using an Android phone, plug your phone in to the Canute Console by USB cable. On your phone notifications it will now say something like, "Charging from USB". Tap that notification and it will take you to the USB settings menu. Change "Charging only" to "Tether" (exact terms may change). The Console will now have access to the Internet. 

When tethering this connexion will be used instead of the WiFi connexion, even if it is not as fast.

# Downloading files from the Internet

If you know the URL of a file on the Internet you can download it using the `curl` command. The option `-O` saves it to your current place in the file system with its original name. For example:

`$ curl -O https://bristolbraille.org/shared/2024-03-22-music-demos.zip`

In this case you may want to unzip the file, which you can do with this command:

`$ unzip 2024-03-22-music-demos.zip`

Then you can remove the original zip file with the `rm` command:

`$ rm 2024-03-22-music-demos.zip`

To look up other options for curl, like saving it with a different file name or printing it to the screen, check the `curl` manual:

`$ man curl`

# Installing new programs from the Internet

There are lots of different ways to install programs on Linux. The most common methods are to use package managers like 'apt' which let you update and manage installed programs easily.

Lets test this by installing a game. You can look up any other 'apt' program that is of interest to you and try installing that instead. Search the web for "Linux apt [subject of interest]" to see what programs are available. There are thousands!

To install the program:

`$ sudo apt install nethack`

Breaking that down it means:

 - `sudo` stands for 'superuser do', meaning you are giving it permission to change the system.

 - The dollar sign indicates to type into the command line.

 - The program you are running is `apt` is a package installer and manager.

 - You are giving the command `install`.

 - Finally the name of the program you are going to install, `nethack-console`. In this instance its not a version built especially for the Canute Console, the term 'console' is roughly analogous to 'terminal', meaning the version of the program meant to be run from the terminal. 

The install may take a few minutes, at the end of which you will have another blank command line with a dollar sign on it at the bottom of the screen.

This is an old videogame from the 90s. The interesting thing about it is that even though it wasn't designed for being played in a Braille display it actually works out of the box in the Canute Console. But for it to show up properly you will have to switch to computer Braille mode first (see above).

Please keep in mind this game needs to be looked up before you play it as it is very complex. 

`$ man nethack`

`man` is short for 'manual'. This will give you the instructions.

Now try running the program. 

`$ nethack`

Good luck! (If you want to leave Nethack press ctrl+c and keep pressing enter until it exits.)

# Printing files

If your printer has been automatically detected when you connected you can start printing straight away. If not then follow these instructions or contact Bristol Braille:

`$ links2 https://opensource.com/article/21/8/add-printer-linux`

That is running a browser called Links2 to read a web page. You only need to scroll up and down with the scrollup scrolldown buttons, and quit by pressing ctrl+c.

Once your printer is set up then you can print documents using LibreOffice. (You may need to install LibreOffice first.)

`$ sudo apt install LibreOffice`
`$ libreoffice --headless -p example.docx`

# Upgrading to the latest Console OS version

Once connected to the Internet run the following command:

`$ curl -L https://bristolbraille.org/console-install | bash`

# Example custom applications developed for the Canute Console

## Sonic Pi

Install:

`$ git clone https://gitlab.com/bristol-braille/sonic-pi-canute/`

See the following examples of how to run and edit Sonic Pi programs using some tools we developed or wrote scripts around for the Console:

`$ cd sonic-pi-canute` (Go into the `sonic-pi-canute` directory.)

`$ micro examples.txt` (Look at --- and edit if you want to --- the example Sonic Pi code.)

Now you will be in the text editor called `micro`. Move around it, save, undo etc using the normal notepad style key commands. To exit hit `ctrl+q` and type `y` or `n` to save or not save any changes.

Once back at the command line...

`$ bash start-sonic-pi.sh` (Will start the Sonic Pi graphical program and then minimise it so we can continue to work from the command line. Bash is a program that runs so-called `shell` scripts, which often end in `.sh`.)

Once back in the command line, which will take about 30 seconds...

`$ bash play-sonic.sh examples.txt` (A script which passes the `examples.sh` file to Sonic Pi. You will notice the sounds playing.)

`$ bash stop-sonic.sh` (Stop anything being played by Sonic Pi.)

 --------

# Advanced

## Remote viewing someone else's terminal

For the purposes of working with another colleague, or following a tutor, you may want to follow their terminal session online using a tool like [`tty-share`](https://tty-share.com/).

## Remote control of your Console

It may be sensible to allow another remote user, such as BBT staff, to have control of the Console at times of your choosing. For this we recommend using [`mosh`](https://mosh.org/), which is similar to `ssh` but better stability in the connexion.

## Apps that don't fit 9 lines

 - Run `$ screen` and press enter.

 - Press 'enter' to skip the readme stuff.

 - Press '[ctrl]+a' then press ':' (colon). This lets you type commands, which will appear on the 9th line of the display (it will show a colon on the bottom left corner)

 - (Can skip this stage.) Type `width -w 40` and press enter. This sets the width to 40. (You can change that number if the application you are using needs to be wider, though this walkthough assumes it doesn't need to be so you can probably skip this stage.)

 - Press '[ctrl]+a' then press ':' (colon) again to type another command.

 - Type `height -w 17` and press enter. This makes the terminal height taller, in this case 17 lines high rather than 9. You can pick any other number, it doesn't have to be a multiple of 9 lines.

 	* (As example usage, setting the height to 27 lines would make the terminal three times as tall as the Canute display, so the same size as a UK standard paper Braille page.)

 - To move around the extra large terminal press '[ctrl]+a' then press '['. This enters what is called "copy mode", but we are going to use it to move around.

 - Now the cursor (typically all six dots) moves freely around the terminal between lines (usually it stays on the command prompt).

 - Move the cursor all the way to the opt of the screen by just holding it down. It will move all the content down.

 - When you are showing the part of the application you want to view, press escape, this will then leave "copy mode" and return to normal command line entry mode.

________
(The above is taken from a question on Stackoverflow which you can read if you want to understand more, but it is not necessary to: https://stackoverflow.com/questions/75316022/emulate-a-taller-terminal-within-gnome-terminal-and-pan-up-and-down)

# Developing applications for the Canute Console

This is a big topic which we will cover in a separate document and is being actively worked on. 

For now the simplest explanation is this:

Anything you can develop for the visual command line, in any language, which runs on Linux and fits into 40x9, will run on the Canute Console. 

There are certain considerations however:
 - To ensure 1-to-1 parity of layout between the visual and Braille displays we recommend developing the program to use computer Braille code. Contracted Braille tables will not respect horizontal whitespace.
 - Canute 360s refresh at 0.5--1 seconds per line, therefore applications should avoid constant scrolling.
 [NPYScreen](https://github.com/npcole/npyscreen/) can be useful way to quickly make attractive spacial tactile and visual products out of simple bash and Python commands.

## Publishing applications as executables for other platforms

The Canute Console is running a Debian-based OS on an ARM7 processor (the Raspberry Pi 4). But if publishing you will want to make your applications work on other systems too.

If you are using a compiled language like C then look into the compiler commands.

If you are using a scripting language like Python then you may want to create an executable so the user doesn't see your code and so they don't have to install the dependencies your code relies on. You may also want to create an executable that can work on Windows, x86 Linux or Mac OS.

There are two tools for Python that we recommend looking at:

 - `pyinstaller` is Python tool you can install with `pip` that will create an executable *for the processor you are running it from*, i.e., in this case, ARM7. If you want to create other executables using this tool you will need to run it on those platforms.

 - `auto-py-to-exe` is a tool that creates x86 Windows executables without having to run Windows and as such may be convenient for Canute Console owners.

## Being careful with Canute Console specific commands

Whatever language you are using and whatever method you use to create the publish your , remember to account for whether the user at the other end is expected to have a Canute display plugged in or not. 

If you are developing it for a mainstream non-Braille reading non-Canute owning audience then be careful that the Canute Console specific lines in your script do not cause the program to crash. For example if you are calling the `canute-cursor` command then on any other system that will throw an error. 

You can get around this by manually commenting those lines out, or creating elegant fail-states if those commands are not find, or having a wrapper function that calls or doesn't call those commands depending on the system.

## Publishing your application publicly

There are many ways to do so. We will describe one way which Bristol Braille has used, as an example, which is to use version control and host it on a commonly used website.

There is a technology called 'git' which is used for tracking changes to software and publishing said software on 'repositories'. One of the websites which provides git repositories (often called repos) as a service is called Github, which we use to keep our publicly available software on. This means people who go to this site can see the source code for the Canute user interface and can download it and change it.

On websites like this there are commonly other useful services provided that are related to software. One is 'issues', for tracking software bugs and suggested changes. Another is a a 'wiki' which is used for public information about our services. A wiki is a series of user editable web pages.

So in this example on Github we have an 'organisation' called 'bristol-braille' and within that we have a respository called 'canute-ui'. Inside that repository we then have related 'issues' and a related 'wiki'. So:

 - The code for the Canute user interface is publicly available here: https://github.com/bristol-braille/canute-ui/
 - Issues relating to the Canute user interface are publicly available here: https://github.com/bristol-braille/canute-ui/issues/
 -  The wiki describing useful information for the Canute user interface is publicly available here: https://github.com/bristol-braille/canute-ui/wiki/

 --------

# Troubleshooting 

## The Braille display isn't showing Braille

If the Braille display isn't working but you suspect or know that the computer is on and visual display and sound are working then you need to follow a couple of procedures.

Firstly shut the Console down. If you are unable to do so cleanly without being able to read the Braille display do the following procedure:

 - Press 'ctrl+alt+f2'

 - Now type 'bbt' and enter, then 'bedminster' and enter (or your own log-in and password).

 - Now press escape (this will exit the Launcher menu).

 - Now type shutdown now.

 - Wait twenty seconds then pull the power cable.

If you have difficulty completing the above then skip to disconnecting the power cable.

Next return to the top of this manual to follow the steps to correctly attach the Canute 360 into the dock of the Canute Console, including the USB-B cable on the back left, *before* plugging the Console in.

When you do plug the Console in again remember to press the power button on the back right.

If this doesn't fix the issue please contact your distributor or Bristol Braille Technology for support.

## Log-in issues

Are you having difficulties logging in, such as being bounced back to the log in page? This can happen with the earliest releases of the Console software but has been fixed with upgrades. Of course, to apply the upgrade you will have to log in first. Therefore press this key combination to switch to a different login terminal:

`ctrl+alt+f2`

Now enter your log-in details. Once in follow the upgrade procedure below (search for 'Upgrading').

## Trouble connecting to WiFi

If you get the error, `Unable to connect ... Secrets were required but not provided`, then try running the folling two commands:

`$ nmcli con delete "NAME OF SSID"`
`$ nmcli dev wifi connect "NAME OF SSID" password PASSWORD ifname wlan0`

## Testing internet connection

Test Internet in the Console by typing, `$ clear; ping bbc.com -i 4`. You should get a log of lots of outputs continuously updating. 

(The `-i 4` part of that command is to tell it to ping the BBC servers once every four seconds.)

You should get a response something like

`64 Bytes from 151.101.192.81 [etc etc]`

(The IP address may be different.)

What that means is that it has sent a request to the BBC website and has received a positive affirmation back, meaning it is connecting. The message back just happens to be 64 bytes long.

Press `[ctrl]+c` to cancel the program and get the results.

If the test above does not give you close to 100% "packets received" then you have an issue with your connexion. 

## Internet problems with IPv4 vs. IPv6

One cause of connexion problems be if your connexion is trying to use IPv6 addresses when your Internet Service Provider doesn't support it. 

If you suspect this is the cause then you can create a config to disable IPv6:

`$ sudo echo "net.ipv6.conf.all.disable_ipv6=1" >> /etc/sysctl.d/local.conf`

Explanation: sudo = run as super user; echo = write string; >> = append to file

## Further troubleshooting

Please contact Bristol Braille if your issue is not covered above.

If you feel comfortable with programmer issue lists then you can [log an issue on at https://gitlab.com/Bristol-Braille/console-learn/-/issues](https://gitlab.com/Bristol-Braille/console-learn/-/issues)



