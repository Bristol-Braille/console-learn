% Canute 360-Handbuch
% Bristol Braille Technology
% Letzte Aktualisierung: 2025-01-10

Anmerkungen
=====

Übersetzungshinweis: Alle nicht-englischen Übersetzungen sind Übersetzungen aus dem Englischen mit DeepL Pro. Wenn Sie Übersetzungsfehler bemerken, teilen Sie uns dies bitte mit.

Kontakt: enquiries@bristolbraille.org

Willkommen
=======

Vielen Dank, dass Sie sich für Canute 360 von Bristol Braille Technology entschieden haben, den
weltweit ersten mehrzeiligen Braille-E-Reader.

Canute 360 verfügt über ein neunzeiliges Display, wobei jede Zeile aus 40
Braille-Zellen besteht. Canute 360 ist mit den Dateiformaten *.BRF und *.PEF kompatible
und kann Braille-Text, Mathematik, Musik oder jeden anderen
Sechspunkt-Braille-Code anzeigen.

Mit Canute 360 können Sie Dateien in Braille lesen, einfügen, bearbeiten
und zu Lesezeichen navigieren.

Bristol Braille Technology ist ein gemeinnütziges Unternehmen ohne Erwerbszweck.
 Durch Ihren Kauf können wir weiterhin in die
Entwicklung neuer Braille-Produkte investieren, die der gesamten blinden Gemeinschaft zugutekommen.

Inhalt der Verpackung
==============

In der Versandverpackung finden Sie Folgendes:

1 × Canute 360 Braille-E-Reader

1 × 19-V-Netzteil mit Kabel

1 × Braille-Kurzanleitung

Bitte informieren Sie Ihren Händler, wenn eines dieser Teile fehlt oder
beschädigt ist.

Dokumentkonventionen
====================

In dieser Bedienungsanleitung wird Text, der in Braille von Canute 360 angezeigt wird
verwendet wird, wird er in Anführungszeichen gesetzt. Wenn Braille auf der Oberfläche des Geräts eingeprägt ist, wird sie hier als Braille dargestellt, z. B. die Menütaste als (⠍⠑⠝⠥).

**Bevor Sie diese Informationen und das unterstützte Produkt verwenden, stellen Sie sicher, dass Sie
die wichtigen Sicherheitsinformationen am Ende dieses
Dokuments gelesen und verstanden haben.**

Physische Beschreibung
====================

Ihr Canute 360 sollte flach auf dem Tisch vor Ihnen liegen. Die
drei großen Steuertasten auf der Vorderseite des Geräts (beschriftet mit
zurück (⠃⠁⠉⠅), Menü (⠍⠢⠥) und vorwärts (⠿⠺⠜⠙) sollten
zu Ihnen zeigen.

Ihr Canute 360 verfügt über eine 9-zeilige Braillezeile mit 360 Zellen auf der Oberseite
.

In der oberen linken Ecke der Oberseite befindet sich eine runde
Taste mit der Beschriftung „H“ (⠓). Direkt unter dieser Taste befinden sich
neun dreieckige Tasten mit den Beschriftungen „1“ (⠼⠁) bis „9“ (⠼⠊) und eine
quadratische Taste mit der Beschriftung „0“ (⠼⠚). In der oberen rechten Ecke der Oberseite
befindet sich ein „BBT Canute 360“-Etikett (⠄⠄⠃⠃⠞ ⠄⠉⠁⠝⠥⠞⠑ ⠼⠉⠋⠚).

In der Mitte der Oberseite, unter der letzten Zeile der Braillezeile,
 befinden sich die Tasten „Zurück“ (⠃⠁⠉⠅), „Menü“ (⠍⠢⠥) und „Vor“ (⠿⠺⠜⠙).
 Auf der Vorderseite des Canute 360 befinden sich die Tasten „Zurück“, „Menü“
und ‚Vor‘.

Auf der linken Seitenwand befinden sich mehrere Peripherieanschlüsse. Auf der
Vorderseite befinden sich ein HDMI-Anschluss und eine 3,5-mm-Audioausgangsbuchse (die Audioausgangsbuchse
wird derzeit nicht unterstützt). Der SD-Kartensteckplatz befindet sich unten auf der
linken Seitenwand in der Mitte. Auf der Rückseite der linken Seitenwand
befinden sich zwei Standard-USB-Anschlüsse und ein USB-B-Anschluss.

Auf der rechten Seite der Rückseite befindet sich der
deutlich hervorstehende Ein-/Ausschalter. Direkt links neben dem
Ein-/Ausschalter befindet sich die Strombuchse.

Auf der rechten Seite Ihres Canute 360 befinden sich keine Bedienelemente oder Buchsen
.

Während des Betriebs gibt Ihr Canute 360 ein leises Ticken von sich. Dies
ist völlig normal.

Ihr Canute 360 enthält keine Teile, die vom Benutzer gewartet werden können. Die
Bodenplatte und die Seitenplatten dürfen nur von einem qualifizierten Servicetechniker geöffnet oder entfernt werden
.

Erste Schritte
===============

Wenn Ihr Canute zum ersten Mal ankommt, lassen Sie ihn bitte
einige Stunden (oder wenn möglich über Nacht) bei normaler Raumtemperatur stehen, bevor Sie
ihn zum ersten Mal einschalten. Wenn Sie diesen Schritt überspringen, wird Ihr Canute nicht beschädigt,
 es kann jedoch beim ersten Gebrauch zu Fehlern auf dem Display kommen. Dies ist notwendig,
 da die Bedingungen während des Transports häufig sehr kalt sind.

Legen Sie Canute 360 auf eine flache Oberfläche, sodass die drei großen Steuerknöpfe
auf der Vorderseite zu Ihnen zeigen. Stecken Sie das Netzteil in die
Buchse rechts auf der Rückseite von Canute 360 und
drücken Sie einmal kurz auf den kleinen Ein-/Ausschalter direkt rechts neben der
Netzbuchse, um das Gerät einzuschalten.

Unter normalen Bedingungen benötigt der Canute 360 etwa 50 Sekunden, um sich einzuschalten. Wenn er jedoch aus dem kalten Zustand gestartet wird, kann es etwa 4 Minuten dauern, bis er startet, da er eine Aufwärmroutine durchläuft. Wenn Sie ein Gerät neu starten, das kürzlich eingeschaltet war, kann es innerhalb von 20 Sekunden starten.

Während des Startvorgangs bewegt sich die oberste Zeile des Displays mehrmals kurz nach oben und unten. Anschließend hören, fühlen und sehen Sie, wie die neun Braillezeilen nacheinander den vorherigen Inhalt löschen. 

Navigationssteuerung
===================

An der Vorderseite des Canute 360 befinden sich drei große Steuertasten. Diese
sind mit „Zurück“ (⠃⠁⠉⠅), „Menü“ (⠍⠢⠥) und „Vor“ (⠿⠺⠜⠙) in
Brailleschrift auf der Oberseite der Lesefläche beschriftet.

Direkt links neben jeder Braillezeile befindet sich eine dreieckige
Auswahltaste. Diese sind mit den Zahlen eins bis neun in Braille beschriftet;
sie entsprechen der nebenstehenden Zeile auf dem Display.

Direkt über der ersten dreieckigen Auswahltaste befindet sich eine runde
Hilfetaste. Diese ist in Brailleschrift mit „H“ (⠓) beschriftet. Sie können diese
Taste jederzeit drücken, um eine Hilfedatei für die Seite aufzurufen, die Sie
gerade anzeigen.

Direkt unter der letzten dreieckigen Auswahltaste befindet sich eine quadratische
Taste, die in Brailleschrift mit „0“ (⠚) beschriftet ist.

Dateien auf Ihren Canute 360 übertragen
==================================

Ihr Canute 360 ist sowohl mit *.BRF- (Braille Ready Format)
als auch mit *.PEF-Dateien (Portable Embosser Format) kompatibel. Für beste Ergebnisse
empfehlen wir die Verwendung einer *.BRF-Datei, die für eine Seitenlänge von neun
Zeilen und eine Breite von 40 Zeichen pro Seite formatiert ist.

Druckdateien in Braille-Schrift übersetzen
====================================

Eine Reihe von Softwarepaketen und Online-Diensten können ein Druckdokument
in eine Braille-Schriftdatei übersetzen, die auf Ihrem Canute gelesen werden kann.

Die neueste Version von Duxbury DBT™ enthält eine „Canute 360“-Voreinstellung, um
eine korrekte Formatierung zu gewährleisten. Um Duxbury DBT™ verwenden zu können, müssen Sie
eine Lizenz von Duxbury Systems erwerben. Weitere Informationen finden Sie unter
[https://www.duxburysystems.com](https://www.duxburysystems.com/).

Sie können auch eine Reihe kostenloser Softwarepakete verwenden, um Druckdokumente
in das BRF-Format zu übersetzen. BrailleBlaster™ ist ein kostenloses Programm, das sich für die 
Vorbereitung von Dokumenten eignet, die auf Canute 360 gelesen werden sollen, und für Windows-, Mac- 
und Linux-Benutzer verfügbar ist. Sie sollten sicherstellen, dass die Braille-Ausgabe so eingestellt ist, dass ein
Dokument mit neun Zeilen pro Seite und einer Zeilenlänge von 40 Zeichen erstellt wird.
Weitere Informationen und den Download von Braille Blaster finden Sie unter
[https://www.brailleblaster.org](https://www.brailleblaster.org/).

RoboBraille™ ist ein kostenloser Online-Übersetzer von Sensus in Dänemark, der
ein Druckdokument oder eine Webseite in ein BRF-Braille-Dokument übersetzen kann, das
auf Canute 360 gelesen werden kann. Um RoboBraille™ zu verwenden, navigieren Sie zu
[www.robobraille.org](http://www.robobraille.org/) in Ihrem bevorzugten Webbrowser.
 Wählen Sie „Datei“ aus dem Menü „Quelle“, um eine Datei auf Ihrem Computer zu übersetzen.
 Wählen Sie „URL“, um eine Website zu übersetzen.

Um eine Datei zu übersetzen, klicken Sie auf „Datei auswählen“ und wählen Sie die Datei mit
Ihrem Dateibrowser aus. Klicken Sie dann auf „Hochladen“, um Ihre Datei für die
Übersetzung vorzubereiten.

Um eine Webseite zu übersetzen, geben Sie die URL in das Textfeld ein, nachdem Sie
die Option „URL“ ausgewählt haben, und klicken Sie dann auf „Abrufen und hochladen“, um
die Webseite für die Übersetzung vorzubereiten. Bitte beachten Sie, dass
eine Webseiten-URL mit einem Dateiformat enden muss (z. B.
[www.example.com/example.htm](http://www.example.com/example.htm)), um mit Robobraille verwendet werden zu können. Eine
Webseite, die mit einer Top-Level-Domain endet (z. B.
[www.example.com](http://www.example.com/)), gibt eine Fehlermeldung aus
.

Wählen Sie „Braille“ aus dem Menü „Ausgabeformat auswählen“ aus.

Im Menü „Braille-Optionen angeben“ können Sie Ihre bevorzugte
Sprache und Braille-Code-Einstellungen auswählen. Wählen Sie „Canute 360“ aus dem
Menü „Zielformat“, um sicherzustellen, dass Ihre Datei für
Canute 360

Geben Sie abschließend Ihre E-Mail-Adresse in das Formular „E-Mail-Adresse eingeben und
Anfrage senden“ ein. Klicken Sie auf „Senden“, um Ihre Auswahl zu bestätigen.
Sie erhalten dann innerhalb weniger Stunden eine für Canute 360 formatierte BRF-Datei in Ihrem E-Mail-Postfach
.

Bristol Braille Technology übernimmt keine Verantwortung für die Genauigkeit
oder Leistung von Braille-Übersetzungsdiensten Dritter.

Übertragung von Braille-Dateien an Canute 360 mit einer SD-Karte
========================================================

Um eine Datei an Canute 360 zu senden, benötigen Sie eine SD-Karte. Ihr Canute
sollte immer eine SD-Karte enthalten, da dies Ihre persönliche Bibliothek ist und
auf der Canute die aktuelle Seite und Systemoptionen wie die Sprache gespeichert werden.
SD-Karten können entweder SD- oder SDHC-Karten sein, aber keine SDXC- oder SDUC-Karten. Sie sollten
in FAT32 formatiert sein, aber nicht in FAT16, exFAT oder einem anderen Format.

Kopieren Sie die Datei und fügen Sie sie auf der SD-Karte ein. Verwenden Sie keine Ordner, sondern
kopieren Sie die Dateien einfach direkt auf die SD-Karte. Die Unterstützung für Ordner
wird in einem zukünftigen Firmware-Upgrade

Setzen Sie die SD-Karte wieder in den SD-Kartensteckplatz in der Mitte der
Unterseite der linken Seitenwand ein. Schalten Sie Canute 360 ein, navigieren Sie zum
Bibliotheksmenü, und die Datei wird im Bibliotheksmenü angezeigt. Die Dateien werden
im Menü zuerst numerisch und dann alphabetisch angeordnet.

Hot-Swapping von USB-Sticks und mehreren SD-Karten
========================================

Sie können auch einen Standard-USB-Speicherstick verwenden, um Dateien auf Canute 360 zu übertragen.
 Sie benötigen jedoch weiterhin eine SD-Karte, da sonst Lesezeichen und die aktuelle Seite
nicht gespeichert werden. Speichersticks sollten nur verwendet werden, um
Ihre Bibliothek schnell mit neuen Dateien zu ergänzen.

Kopieren Sie dazu die Datei und fügen Sie sie auf dem Speicherstick ein. Stecken Sie den Speicherstick dann
in einen USB-Anschluss auf der linken Seitenwand von Canute 360.

Sie können auch im laufenden Betrieb zwischen verschiedenen SD-Karten und Memory Sticks wechseln, was
besonders nützlich ist, wenn mehr als eine Person den Canute verwendet.

Stellen Sie sicher, dass die Seite nicht aktualisiert wird, bevor Sie den Wechsel vornehmen. Nach dem Wechsel
lassen Sie Ihren Canute etwa zehn Sekunden lang stehen; er führt einen vollständigen Reset durch und
kehrt zur zuletzt besuchten Seite auf dieser SD-Karte zurück. Denken Sie daran, dass Ihre
Systeminformationen, Lesezeichen und die aktuelle Seite nicht auf dem Canute selbst gespeichert sind
Canute selbst gespeichert sind, sondern auf der SD-Karte und/oder dem Memorystick.

Spezielle Dateien
=============

Auf Ihrer SD-Karte finden Sie zwei Arten von Dateien mit den Namen
„canute.book_name.brf.txt“ (wobei ‚book_name‘ der Dateiname Ihres
Buches ist) und ‚canute_state.txt‘. Die erstgenannte Datei befindet sich auch auf Memorysticks
.

Diese Dateien werden zum Speichern von Systeminformationen, Lesezeichen und Seiteninformationen verwendet.
 Sie sollten diese Dateien nicht löschen, da sonst auch alle
in ihnen gespeicherten Lesezeichen gelöscht werden. Sie können jedoch die
Datei „canute.book_name.brf.txt“ in einem PC-Texteditor wie Notepad++ bearbeiten, um
Lesezeichen manuell hinzuzufügen oder zu entfernen

Auswahl eines Buches aus der Bibliothek
================================

Canute 360 ist mit einer benutzerfreundlichen Braille-Benutzeroberfläche ausgestattet. Um
eine Datei zum Lesen auf Canute 360 auszuwählen, navigieren Sie zum Bibliotheksmenü.

Drücken Sie zunächst die große Menütaste in der Mitte der Vorderkante unterhalb
der Lesefläche. Das Hauptmenü wird geladen, eine Braillezeile nach der
anderen, von oben nach unten. Die gesamte Seite wird in etwa 10 Sekunden
angezeigt.

Wie bei allen Menüs auf Canute 360 werden die Optionen mit den
dreieckigen Zeilenauswahltasten ausgewählt. Um ein Element aus einem Menü auszuwählen, drücken Sie die
dreieckige Zeilenauswahltaste unmittelbar links neben der entsprechenden Braillezeile
.

Im Hauptmenü wird „Bibliotheksmenü anzeigen“
unten auf der Lesefläche angezeigt. Drücken Sie die Zeilenauswahltaste
unmittelbar links neben der Menüoption, um sie auszuwählen. Das Bibliotheksmenü
Menü wird eine Braillezeile nach der anderen von oben nach unten geladen.
Die Anzeige der gesamten Seite dauert etwa 10 Sekunden.

Das Bibliotheksmenü zeigt eine Liste aller erkannten Braille-Dateien
auf der SD-Karte oder dem USB-Stick an.

Um eine Datei auszuwählen, drücken Sie die Zeilenauswahltaste links neben der
zu lesenden Datei.

Wenn sich mehr als 8 Dateien auf der SD-Karte oder dem USB-Stick befinden,
befindet sich die gewünschte Datei möglicherweise auf der nächsten Seite des Bibliotheksmenüs. Drücken Sie die
große Vorwärts-Taste rechts von der Mitte auf der Vorderseite, um
eine Seite im Bibliotheksmenü weiterzublättern.

Drücken Sie die große Zurück-Taste links von der Mitte auf der Vorderseite, um
eine Seite im Bibliotheksmenü zurückzublättern.

Sobald eine Datei ausgewählt wurde, wird sie zeilenweise geladen, wobei das
Laden der gesamten Seite etwa 10 Sekunden dauert.

Um vom Bibliotheksmenü zum zuletzt gelesenen Buch zurückzukehren, drücken Sie
entweder die große Menütaste oder die Zeilenselektions-Taste 1.

Das Bibliotheksmenü enthält zwei Dateien, die immer
angezeigt werden. Eine ist für Servicetechniker zum Reinigen und Testen vorgesehen,
die andere enthält Informationen über die Bristol Braille-Technologie.

In einer Datei navigieren
====================

Um in einer Datei zu navigieren, verwenden Sie die drei großen Steuertasten auf der Vorderseite
von Canute 360.

Drücken Sie die große Vorwärts-Taste rechts von der Mitte auf der Vorderseite,
 um innerhalb einer Datei eine Seite vorwärts zu blättern.

Drücken Sie die große Zurück-Taste links von der Mitte, um innerhalb einer Datei eine Seite zurück zu blättern,
 ähnlich wie beim Umblättern der Seiten in einem physischen Buch.

Sollten Sie wider Erwarten Fehler in der Brailleschrift oder falsch gesetzte Punkte bemerken,
 können Sie die Braillezeile aktualisieren, indem Sie
die quadratische Taste „0“ (⠚) einige Sekunden lang gedrückt halten. Die Anzeige wird
nach etwa 20 Sekunden zurückgesetzt.

Hauptmenü
=========

Vom Hauptmenü aus können Sie ein Lesezeichen auf der aktuellen Seite einfügen,
zu einem Lesezeichen zurückkehren, das zuvor in einer Datei platziert wurde, oder
zu einer bestimmten Seite in einem Buch navigieren.

Um zum Hauptmenü zu navigieren, drücken Sie die große Menütaste. Wenn Sie sich in einem beliebigen
Menü auf Canute 360 befinden, kehren Sie durch erneutes Drücken der Menütaste zur
zuletzt gelesenen Seite zurück.

Wie bei allen Menüs auf Canute 360 können Sie einen Eintrag aus dem Hauptmenü auswählen, indem Sie
die dreieckige Auswahltaste direkt links neben dem
Menüeintrag auf der Lesefläche drücken.

Um zu einer Seite innerhalb der Datei zu navigieren, drücken Sie die Zeilenauswahltaste
unmittelbar links neben „Gehe zu Seite“ auf der Lesefläche.
 Auf der Lesefläche wird eine Hilfeseite angezeigt. Um eine Seitenzahl einzugeben,
 verwenden Sie die dreieckigen Zeilenauswahltasten und die quadratische Nulltaste
links neben der Lesefläche.

Um beispielsweise zu Seite 20 zu gelangen, drücken Sie die Taste Nummer zwei und anschließend die
Nulltaste. Sie hören, fühlen und sehen, wie eine Braillezeile nach oben
und unten klickt, um die Auswahl zu bestätigen. Drücken Sie dann die Vorwärtstaste, um
zur ausgewählten Seite zu navigieren. Alternativ können Sie die Zurück-Taste drücken, um
Ihre Auswahl rückgängig zu machen.

Lesezeichen
=========

Um ein neues Lesezeichen auf der aktuellen Seite in einer Datei zu setzen oder zu einem
zuvor in einer Datei gesetzten Lesezeichen zurückzukehren, navigieren Sie zunächst zum Menü im Buch,
 indem Sie die große Menütaste auf der Vorderseite des Canute 360 drücken.

Um ein Lesezeichen einzufügen, drücken Sie die Zeilenauswahltaste unmittelbar links neben
„Lesezeichen auf aktueller Seite einfügen“.

Ein Lesezeichen wird auf der aktuellen Seite eingefügt. Bitte beachten Sie, dass es derzeit
keine Rückmeldung gibt, wenn ein Lesezeichen eingefügt wird. Dies wird in einer späteren Softwareversion behoben werden
.

Um ein Lesezeichen aufzurufen, drücken Sie die Zeilenauswahltaste unmittelbar links neben
„Aus vorhandenen Lesezeichen auswählen“.

Auf der Lesefläche wird eine Liste der Lesezeichen in einer Datei angezeigt. Verwenden Sie
die dreieckigen Zeilenauswahlschaltflächen, um ein Lesezeichen auszuwählen. Canute 360
zeigt dann die ausgewählte Seite im Buch an.

Im Lesezeichenmenü können Sie auch „Beginn des Buches“ oder „Ende des Buches“ auswählen, um zum
Anfang oder Ende eines Buches zu navigieren. Verwenden Sie dazu die Zeilenauswahlschaltfläche direkt links neben
diesen Optionen.

Ändern der Sprache oder des Braille-Codes
=================================

Sie können die Systemsprache oder den Braille-Code (z. B. um
zwischen Kurz- und Langschrift zu wechseln) über das Systemmenü ändern.
 Bitte beachten Sie, dass dies nur die Sprache und den Code in den
Menüs, der Hilfedatei und den Handbüchern ändert; nicht die Sprache oder den Code in Büchern auf
Canute 360.

Um auf das Systemmenü zuzugreifen, rufen Sie zunächst das Hauptmenü auf, indem Sie die
große Menütaste in der Mitte der Vorderseite von Canute 360 drücken. Drücken Sie dann
die Zeilenauswahltaste direkt links neben „Systemmenü anzeigen“, um auf das Systemmenü zuzugreifen. Drücken Sie die
Zeilenauswahltaste direkt links neben „Sprache und Code auswählen“, um auf das Sprachauswahlmenü zuzugreifen.
können dann eine Sprache oder einen Braille-Code auswählen, indem Sie die dreieckige
Auswahltaste direkt links neben der ausgewählten Sprache oder dem ausgewählten Code drücken.
Drücken Sie einmal auf die Vorwärts-Taste, um Ihre Auswahl zu bestätigen.

Monitorausgabe
==============

Sie können den von Canute angezeigten Text sowohl in Druck- als auch in Brailleschrift auf einem
Standard-Computermonitor anzeigen. Schließen Sie Ihren Monitor einfach an den HDMI-Anschluss
an der Vorderseite der linken Seitenwand an. Die Druckdarstellung wird
in grüner Schrift angezeigt, die Brailleschrift darunter in weißer
Schrift. Die Druckdarstellung erfolgt in Braille-ASCII (der Text wird nicht
rückübersetzt; sie ist für sehende Braille-Leser gedacht).

Canute als Anzeige
===================

Canute 360 kann als Anzeige verwendet werden, wenn es über den USB-B-Anschluss an einen PC angeschlossen wird.
 Derzeit wird dies von BRLTTY und Duxbury DBT unterstützt.
Bitte lesen Sie den Abschnitt „Support“ in unserem Bereich auf der Website, um zu erfahren, wie Sie
Canute 360 mit einem PC verwenden können.

Herunterfahren
========

Um Ihren Canute 360 sicher auszuschalten, drücken Sie den Ein-/Ausschalter rechts
neben der Strombuchse auf der Rückseite. Sie hören, sehen und
spüren, wie jede Zeile des Displays zurückgesetzt wird und in das Display eintaucht.
Nach einigen Sekunden wird die oberste Zeile des Displays aktualisiert und zeigt
„BITTE WARTEN ...“ (⠠⠏⠇⠑⠁⠎⠑ ⠺⠁⠊⠞⠲⠲⠲) an, und die anderen 8 Zeilen werden abgesenkt.
Nach etwa zehn Sekunden wird auch die erste Zeile abgesenkt. Jetzt können Sie
das Netzteil sicher vom Canute 360 entfernen.

Pflege Ihres Canute 360
==========================

Ihr Canute 360 enthält keine Teile, die vom Benutzer gewartet werden können. Die
Grundplatte und die Seitenplatten dürfen nur von einem qualifizierten Servicetechniker geöffnet oder entfernt werden
.

Sie können die Vorderseite und die Seiten Ihres Canute 360 mit einem feuchten, aber
nicht nassen, fusselfreien Tuch reinigen. Bristol Braille Technology empfiehlt,
Ihren Canute 360 mindestens einmal im Monat zu reinigen. Sie sollten nur versuchen,
Ihren Canute 360 zu reinigen, wenn er ausgeschaltet und vom Stromnetz getrennt ist.
Versuchen Sie nicht, mechanische Teile der Braillezeile
selbst zu reinigen.

Wenn Sie Ihre Canute 360 im Versandkarton aufbewahren, können Sie feststellen, dass sich
an der Vorder- und Seitenverkleidung ein pudriger weißer Belag bildet. Dies ist
normal, beeinträchtigt die Funktion nicht und kann durch Abwischen mit einem
feuchten (aber nicht nassen) fusselfreien Tuch entfernt werden.

Aktualisieren Ihrer Canute-Software
=============================

Von Zeit zu Zeit kann Bristol Braille Technology
Software-Updates für Canute 360 zur Verfügung stellen, um Probleme zu beheben, die Leistung zu verbessern oder
zusätzliche Funktionen hinzuzufügen.

Jedes Update wird mit Installationsanweisungen geliefert. Um ein Update zu installieren,
 benötigen Sie einen leeren USB-Stick oder eine SD-Karte im FAT32-Format. SD-Karten
können entweder SD oder SDHC sein, aber nicht SDXC oder SDUC. Die meisten SD-Karten mit kleinerer
Kapazität sind im FAT32-Format. Die vollständige Installationsanleitung
finden Sie im Readme-Dokument, das mit Ihrem Update geliefert wird.

Wichtige Sicherheitsinformationen
============================

*ACHTUNG: Bitte lesen Sie vor der Verwendung dieses Handbuchs alle
Sicherheitsinformationen zu diesem Produkt und machen Sie sich damit vertraut.*

Service und Support
-------------------

Ihr Canute 360 enthält keine Teile, die vom Benutzer gewartet werden können. Die
Grundplatte und die Seitenplatten dürfen nur von einem qualifizierten Servicetechniker geöffnet oder entfernt werden.
 Versuchen Sie nicht, Ihren Canute 360 selbst zu warten,
 es sei denn, Sie werden vom Kundendienstzentrum dazu aufgefordert. Wenden Sie sich nur an einen
von Bristol Braille Technology zugelassenen Dienstleister.

Netzkabel und Netzadapter
------------------------------

Verwenden Sie nur die von Bristol Braille Technology gelieferten Netzkabel und Netzadapter.
 Wickeln Sie ein Netzkabel niemals um einen Netzadapter oder einen anderen
Gegenstand. Dadurch kann das Kabel so stark beansprucht werden, dass es
ausfranst, reißt oder sich verformt. Dies kann ein Sicherheitsrisiko darstellen. Verlegen Sie Netzkabel immer so, dass
sie nicht betreten, über sie gestolpert oder sie nicht
von Gegenständen eingeklemmt werden. Schützen Sie Netzkabel und Netzadapter vor Flüssigkeiten. Verwenden Sie keine
Netzadapter, die Korrosion an den Wechselstrom-Eingangspins oder
Anzeichen von Überhitzung (z. B. verformter Kunststoff) am Wechselstrom-Eingang oder
an einer anderen Stelle des Netzadapters aufweisen.

Wärme und Produktbelüftung
----------------------------

Ihr Canute 360 und sein Netzteil können während des Betriebs Wärme erzeugen.
Dies ist völlig normal. Ihr Canute 360 ist ein Desktop-Gerät und sollte
immer auf einem Schreibtisch oder Tisch aufgestellt werden. Sie sollten Ihren Canute 360 niemals
auf einem Bett, Sofa, Teppich oder einer anderen flexiblen Oberfläche aufstellen. Um das Risiko von Verletzungen zu vermeiden,
 sollten Sie Ihren Canute 360 während des Betriebs niemals auf Ihrem Schoß oder
Schoß oder andere Körperteile kommen. Schalten Sie
Ihren Canute 360 immer aus und trennen Sie das Netzteil, wenn Sie ihn nicht verwenden.

Betriebsumgebung
---------------------

Ihr Canute 360 funktioniert am besten bei Raumtemperatur zwischen
10 °C und 25 °C (50 °F–80 °F) und einer Luftfeuchtigkeit zwischen 35 % und 80 %. Wenn
Ihr Canute 360 bei Temperaturen unter 10 °C
(50 °F) gelagert oder transportiert wird, sollten Sie die Temperatur Ihres Canute 360
vor der Verwendung langsam auf 10 °C bis 25 °C (50 °F bis 80 °F) ansteigen lassen. Dieser Vorgang
kann bis zu zwei Stunden dauern.

Wenn Sie Ihr Gerät nicht vor der Verwendung auf eine optimale Betriebstemperatur bringen,
 kann dies zu einer Funktionsstörung oder zu Schäden an Ihrem Canute
360 führen.

Stellen Sie keine Getränke auf oder neben Ihren Canute 360. Wenn
Flüssigkeit auf den Canute 360 verschüttet wird, kann es zu einem Kurzschluss oder anderen Schäden kommen.
 Essen oder rauchen Sie nicht über Ihrem Canute. Partikel, die
in Ihren Canute 360 fallen, können den Mechanismus beschädigen.

Überhitzung
-----------

Der Canute verfügt über ein internes Wärmemanagement. Wenn eine bestimmte Temperatur überschritten wird,
 die als suboptimal für den Mechanismus eingestuft wird (ungefähr
so, als wäre das Display zu heiß zum Anfassen), wird der gesamte
Betrieb unterbrochen, bis die Temperatur sinkt. Lassen Sie ihn einfach abkühlen, und
er wird von selbst dort neu starten, wo er aufgehört hat.

Elektrische Eingabe
----------------

Ihr Canute 360 ist für den Betrieb unter folgenden elektrischen Bedingungen ausgelegt
:

Eingangsspannung: mindestens 100 V (AC), maximal 240 V (AC), Eingangsfrequenz
zwischen 50 Hz und 60 Hz

Geräusche während des Startvorgangs und des Betriebs
------------------------------------

Beim ersten Einschalten macht Canute 360 ein lauteres Geräusch als üblich, da
es einen Reset des Displays durchführt. Dies wird zweimal vor dem
Start durchgeführt und dauert insgesamt etwa eine Minute. Wenn Ihr Canute kalt ist, kann dies bis zu fünf Minuten dauern.

Sobald der Canute 360 eingeschaltet und in Betrieb ist, gibt er ein leiseres „Zeileneinstellungsgeräusch“
für jede Zeile auf dem Display von sich, die geändert wird.

Wenn Ihr Canute 360 eingeschaltet bleibt und nicht benutzt wird, kann er ein
leises ‚Ticken‘ von sich geben, je nachdem, welche Firmware-Version Sie verwenden.

Support 
=======

Für technische Unterstützung wenden Sie sich zunächst an Ihren Händler.

Um einen Fehler zu melden oder das Team zu kontaktieren, senden Sie bitte eine E-Mail an
[support@bristolbraille.](mailto:support@bristolbraille.co.uk)org.

Weitere Informationen zur Unterstützung finden Sie unter
https://[www.bristolbraille.org/support](http://www.bristolbraille.co.uk/support)

Die vielen Einsatzmöglichkeiten des Canute 360
===============================

Braille bedeutet Alphabetisierung, Beschäftigung und Unabhängigkeit. Wenn man sich also
mit der grundlegenden Verwendung des Canute 360 vertraut gemacht hat, werden die Benutzer viele
zusätzliche Möglichkeiten finden, die über das einfache Lesen von Büchern hinausgehen.

- Papier sparen: Eine offensichtliche Einsparung ergibt sich bei sperrigen, gedruckten Braille-Ausgaben.

- Tabellen: Werden viel besser lesbar, wenn bis zu 9 Zeilen angezeigt werden können
.

Mathematik: Enthält oft lange und komplexe Gleichungen, die
über mehrere Zeilen laufen.

Musik: Partituren werden immer dann am besten verstanden, wenn mehrere Zeilen
gelesen werden können.

Lernen: Braille-Lehrgänge können leicht an die Anzeige
auf Canute angepasst werden.

Sprachen: Die Anzeige verschiedener Sprachen im selben Dokument ist
sowohl möglich als auch praktisch.

Hinweise zu Versionen und Urheberrecht
===============================

Das Haupthandbuch ist auf der Website gespeichert. Alle Urheberrechte liegen bei
Bristol Braille Technology CIC und Techno-Vision Systems.

Letzte Bearbeitung am 10. Januar 2025
