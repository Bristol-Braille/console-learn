% Canute 360 Manual
% Tecnologia Bristol Braille
% Última atualização: 2025-01-10

Notas
=====

Nota de tradução: Todas as traduções que não são em inglês são traduções do inglês usando o DeepL Pro. Se você notar erros de tradução, informe-nos sobre eles.

Contato: enquiries@bristolbraille.org

Bem-vindo ao site
=======

Obrigado por adquirir o Canute 360 da Bristol Braille Technology, o primeiro
primeiro leitor eletrônico Braille multilinha do mundo.

O Canute 360 apresenta uma tela de nove linhas, sendo que cada linha é composta por 40
células de Braille. O Canute 360 é compatível com os formatos de arquivo *.BRF e *.PEF
e pode exibir texto em Braille, matemática, música ou qualquer outro código Braille de seis pontos.
código Braille de seis pontos.

Com o Canute 360, você pode ler arquivos em Braille, bem como inserir, editar,
e navegar até os marcadores.

A Bristol Braille Technology é uma empresa de interesse comunitário sem fins lucrativos.
sem fins lucrativos. Sua compra nos permitirá continuar a investir e
desenvolver novos produtos em Braille para beneficiar a comunidade cega em geral.

Dentro da caixa
==============

Você encontrará os seguintes itens incluídos na caixa de remessa:

- 1 × leitor eletrônico Canute 360 Braille

- 1 × fonte de alimentação com fio de 19V

- 1 × guia de início rápido em Braille

Informe ao seu distribuidor se algum desses itens estiver faltando ou
danificados.

Convenções de documentos
====================

Neste manual do usuário, quando o texto exibido em Braille pela Canute 360
for referenciado, ele será apresentado entre aspas. Quando o Braille estiver gravado em relevo na superfície do dispositivo, ele será apresentado aqui em Braille, por exemplo, o botão de menu como (⠍⠑⠝⠥).

**Antes de usar essas informações e o produto a que elas dão suporte, certifique-se de
de ler e entender as informações importantes de segurança no final deste documento.
documento.**

Descrição física
====================

O Canute 360 deve ser colocado sobre a mesa à sua frente. Os
Os três botões de controle grandes no painel frontal da unidade (rotulados como
(⠃⠁⠉⠅), menu (⠍⠢⠥) e avançar (⠿⠺⠜⠙) devem estar voltados para você.
voltados para você.

O Canute 360 tem um visor Braille de nove linhas e 360 células em sua superfície superior
superfície superior.

No canto superior esquerdo da superfície superior, você encontrará um botão circular
rotulado como “H” (⠓). Logo abaixo desse botão, você encontrará
nove botões triangulares, rotulados de “1” (⠼⠁) a “9” (⠼⠊), e um botão
botão quadrado rotulado como “0” (⠼⠚). No canto superior direito da superfície superior
superfície, você encontrará um rótulo “BBT Canute 360” (⠄⠄⠃⠃⠞ ⠄⠉⠁⠝⠥⠞⠑ ⠼⠉⠋⠚).

No centro da superfície superior, abaixo da última linha da tela em Braille, você encontrará
você encontrará os rótulos voltar (⠃⠁⠉⠅), menu (⠍⠢⠥) e avançar (⠿⠺⠜⠙)
rótulos. Na superfície frontal do Canute 360, você encontrará os botões de navegação para trás, menu
e avançar.

No painel lateral esquerdo, você encontrará várias portas de periféricos. Na parte
Na parte frontal, você encontrará uma porta HDMI e um conector de saída de áudio de 3,5 mm (o conector de áudio
não é suportado no momento). O slot para cartão SD fica na parte inferior do
painel do lado esquerdo, no centro. Na parte posterior do painel lateral esquerdo
esquerdo, você encontrará duas portas USB padrão e uma porta USB-B.

No lado direito do painel traseiro, você encontrará o botão liga/desliga
que se destaca da superfície. Imediatamente à esquerda do botão
botão liga/desliga está o soquete de alimentação.

Não há controles ou soquetes no painel do lado direito do seu
Canute 360.

Durante a operação, o Canute 360 emitirá um ruído de tique-taque silencioso. Esse
Isso é perfeitamente normal.

A Canute 360 não contém nenhuma peça que possa ser reparada pelo usuário. A base
O painel da base e as placas laterais só devem ser abertos ou removidos por um engenheiro de manutenção qualificado.
qualificado.

Primeiros passos
===============

Quando seu Canute chegar, deixe-o atingir a temperatura ambiente normal por várias horas
temperatura ambiente normal por várias horas (ou durante a noite, se possível) antes de
antes de ligá-lo pela primeira vez. Ignorar essa etapa não danificará o Canute
mas pode resultar em erros no visor na primeira utilização. Isso é necessário
Isso é necessário devido às condições frequentemente muito frias durante o transporte.

Coloque a Canute 360 em uma superfície plana, com os três botões de controle grandes
no painel frontal voltados para você. Insira a fonte de alimentação no soquete
soquete à direita do painel traseiro do Canute 360 e pressione brevemente o pequeno botão liga/desliga
pressione brevemente o pequeno botão liga/desliga localizado imediatamente à direita da
soquete de alimentação uma vez para ligar a unidade.

Em condições normais, o Canute 360 levará cerca de 50 segundos para ser ligado. No entanto, se estiver sendo ligado a frio, pode levar cerca de 4 minutos para ligar, pois ele executa uma rotina de aquecimento. Se estiver reiniciando uma máquina que foi ligada recentemente, ela poderá iniciar em 20 segundos.

Durante a inicialização, a linha superior do visor subirá e descerá momentaneamente várias vezes. Em seguida, você ouvirá, sentirá e verá cada uma das nove linhas de Braille apagando o conteúdo anterior, uma de cada vez. 

Controles de navegação
===================

Na borda frontal do Canute 360 há três botões de controle grandes. Esses
Eles estão identificados como voltar (⠃⠁⠉⠅), menu (⠍⠢⠥) e avançar (⠿⠺⠜⠙) em
Braille na parte superior da superfície de leitura.

Diretamente à esquerda de cada linha de Braille há um botão triangular de seleção de linha
triangular. Esses botões são rotulados com números de um a nove em Braille;
correspondentes à linha adjacente no visor.

Imediatamente acima do primeiro botão triangular de seleção de linha, há um botão circular de
botão de ajuda. Ele está identificado como “H” (⠓) em Braille. Você pode pressionar esse botão
botão a qualquer momento para abrir um arquivo de ajuda para a página que está sendo
que estiver visualizando no momento.

Imediatamente abaixo do último botão triangular de seleção de linha há um botão quadrado
rotulado como “0” (⠚) em Braille.

Transferência de arquivos para o Canute 360
==================================

A Canute 360 é compatível com os formatos *.BRF (Braille Ready Format)
e *.PEF (Portable Embosser Format). Para obter melhores resultados, recomendamos
recomendamos usar um arquivo *.BRF, formatado para um comprimento de página de nove
linhas e uma largura de 40 caracteres por página.

Tradução de arquivos impressos em Braille
====================================

Vários pacotes de software e serviços on-line podem traduzir um documento impresso
documento impresso em um arquivo Braille, que pode ser lido em seu Canute.

A versão mais recente do Duxbury DBT™ inclui uma predefinição “Canute 360” para
para garantir a formatação correta. Para usar o Duxbury DBT™, você precisará adquirir
uma licença da Duxbury Systems. Você pode encontrar mais informações em
[https://www.duxburysystems.com](https://www.duxburysystems.com/).

Você também pode usar vários pacotes de software gratuitos para traduzir documentos impressos
documentos impressos para o formato BRF. O BrailleBlaster™ é um programa gratuito adequado para 
preparar documentos para serem lidos no Canute 360, disponível para usuários de Windows, Mac 
e Linux. Você deve garantir que a saída em Braille esteja configurada para produzir um
documento com nove linhas por página, com um comprimento de linha de 40 caracteres.
Você pode obter mais informações e fazer download do Braille Blaster em
[https://www.brailleblaster.org](https://www.brailleblaster.org/).

O RoboBraille™ é um tradutor on-line gratuito da Sensus, na Dinamarca, que
pode traduzir um documento impresso ou uma página da Web em um documento BRF Braille para
ser lido no Canute 360. Para usar o RoboBraille™, navegue até
[www.robobraille.org](http://www.robobraille.org/) em seu navegador da Web preferido.
preferido. Selecione “file” (arquivo) no menu “Source” (fonte) para traduzir um arquivo em
seu computador. Selecione “URL” para traduzir um site.

Para traduzir um arquivo, clique em “choose file” (escolher arquivo) e selecione o arquivo usando
seu navegador de arquivos. Em seguida, clique em “upload” para preparar o arquivo para
tradução.

Para traduzir uma página da Web, digite o URL no campo de texto após
selecionar a opção “URL” e, em seguida, clique em “fetch and upload” para
preparar a página da Web para a tradução. Observe que, para funcionar com o
com o robobraille, o URL de uma página da Web deve terminar em um formato de arquivo (por exemplo
[www.example.com/example.htm](http://www.example.com/example.htm)). A
página da Web que termina em um domínio de nível superior (por exemplo
[www.example.com](http://www.example.com/)) retornará uma mensagem de erro.
mensagem.

Selecione “Braille” no menu “select output format” (selecionar formato de saída).

No menu “Specify Braille options” (Especificar opções de Braille), você pode selecionar o
idioma preferido e as configurações do código Braille. Selecione “Canute 360” no menu
“Target Format” (Formato de destino) para garantir que o arquivo seja formatado corretamente para o
seu Canute 360.

Por fim, digite seu endereço de e-mail no formulário “Enter email address and
submit request” (Digite o endereço de e-mail e envie a solicitação). Clique em “Submit” (Enviar) para confirmar suas escolhas. Você
receberá um arquivo BRF formatado para o Canute 360 em sua caixa de entrada de e-mail
em algumas horas.

A Bristol Braille Technology não pode se responsabilizar pela precisão ou pelo desempenho
ou pelo desempenho de qualquer serviço de tradução em Braille de terceiros.

Transferência de arquivos Braille para o Canute 360 com um cartão SD
========================================================

Para transferir um arquivo para o Canute 360, você precisará de um cartão SD. Sua Canute
deve sempre ter um cartão SD, pois ele é sua biblioteca pessoal e onde o Canute salva sua página atual.
onde o Canute salva sua página atual e as opções do sistema, como idioma. OS CARTÕES SD
podem ser SD ou SDHC, mas não SDXC ou SDUC. Eles devem ser
formatados em FAT32, mas não em FAT16, exFAT ou qualquer outro formato.

Copie e cole o arquivo no cartão SD. Não use pastas, simplesmente
copie e cole os arquivos diretamente no cartão SD. O suporte para pastas será
em uma futura atualização de firmware.

Insira o cartão SD novamente no slot de cartão SD em direção ao centro da parte inferior do painel lateral esquerdo.
parte inferior do painel do lado esquerdo. Ligue o Canute 360, navegue até o menu
e o arquivo aparecerá no menu da biblioteca. Os arquivos são
organizados no menu primeiro numericamente e depois em ordem alfabética.

Troca a quente de pendrives USB e vários SDs
========================================

Você também pode usar um cartão de memória USB padrão para colocar arquivos no Canute
360. Você ainda precisará de um cartão SD, ou os marcadores e a página atual não serão salvos.
não serão salvos. Os cartões de memória devem ser usados apenas para
complementar rapidamente sua biblioteca com novos arquivos.

Para fazer isso, copie e cole o arquivo no cartão de memória e insira-o
em uma porta USB no painel lateral esquerdo do Canute 360.

Você também pode fazer hot-swap entre vários cartões SD e cartões de memória, o que
é especialmente útil se mais de uma pessoa estiver usando o Canute.

Certifique-se de que a página não esteja sendo atualizada antes de fazer a troca. Depois de fazer a troca
deixe o Canute por cerca de dez segundos; ele fará uma reinicialização completa e
ele fará uma reinicialização completa e retornará à última página visitada no cartão SD. Lembre-se de que suas
Lembre-se de que as informações do sistema, os favoritos e a página atual não são armazenados no próprio Canute.
Portanto, serão trocados com o cartão SD e/ou o cartão de memória.

Arquivos especiais
=============

No seu cartão SD, você poderá notar dois tipos de arquivos chamados
“canute.book_name.brf.txt” (em que ‘book_name’ é o nome do arquivo do seu livro) e ‘canute_state.brf.txt’ (em que ‘book_name’ é o nome do arquivo do seu livro).
livro) e “canute_state.txt”. O primeiro também é encontrado em cartões de memória.
memória.

Esses arquivos são usados para armazenar informações do sistema, marcadores e informações da página.
informações da página. Você não deve excluir esses arquivos, ou excluirá todos os
favoritos armazenados neles. No entanto, você pode editar o arquivo
arquivo “canute.book_name.brf.txt” em um editor de texto para PC, como o Notepad++, para
adicionar ou remover marcadores manualmente

Escolhendo um livro da biblioteca
================================

O Canute 360 foi projetado com uma interface de usuário em Braille fácil de usar. Para
selecionar um arquivo para ler no Canute 360, navegue até o menu da biblioteca.

Primeiro, pressione o botão grande de menu no centro da borda frontal, abaixo da superfície de leitura.
a superfície de leitura. O menu principal será carregado, uma linha de Braille por vez, de cima para baixo.
de cada vez, de cima para baixo. A página inteira levará cerca de 10 segundos para ser
exibição.

Como em todos os menus do Canute 360, as opções são selecionadas usando os
botões triangulares de seleção de linha. Para selecionar um item de um menu, pressione o botão
botão triangular de seleção de linha imediatamente à esquerda da linha de
Braille.

No menu principal, a opção “view library menu” é exibida
na parte inferior da superfície de leitura. Pressione o botão de seleção de linha
imediatamente à esquerda da opção de menu para selecioná-la. O menu da biblioteca
O menu da biblioteca será carregado, uma linha de Braille por vez, de cima para baixo. A página
A página inteira levará cerca de 10 segundos para ser exibida.

O menu da biblioteca exibirá uma lista de todos os arquivos Braille reconhecidos
reconhecidos no cartão SD ou no pen drive USB.

Para selecionar um arquivo, pressione o botão de seleção de linha à esquerda do arquivo
que você deseja ler.

Se houver mais de 8 arquivos no cartão SD ou no pen drive USB, o arquivo desejado poderá estar na próxima página.
o arquivo desejado poderá estar na próxima página do menu da biblioteca. Pressione o botão
botão grande de avanço à direita do centro no painel frontal para
avançar o menu da biblioteca em uma página.

Pressione o botão grande de retrocesso à esquerda do centro no painel frontal para
retroceder uma página no menu da biblioteca.

Quando um arquivo for selecionado, ele carregará uma linha de cada vez, levando cerca de
10 segundos para carregar a página inteira.

Para voltar do menu da biblioteca para o livro que estava lendo, pressione
o botão grande do menu ou o botão de seleção de linha um.

Há dois arquivos incorporados ao menu da biblioteca que sempre aparecerão.
aparecerão. Um deles é para os engenheiros de serviço usarem durante a limpeza e o teste,
e o outro contém informações sobre a Bristol Braille Technology.

Movimentação em um arquivo
====================

Para mover-se dentro de um arquivo, use os três botões de controle grandes no painel frontal do Canute 360.
painel frontal do Canute 360.

Pressione o botão grande de avanço à direita do centro no painel frontal
para avançar uma página em um arquivo.

Pressione o botão grande de retrocesso à esquerda do centro para retroceder uma página em um arquivo.
dentro de um arquivo, semelhante a virar as páginas de um livro físico.

No caso improvável de você notar erros no Braille ou pontos fora do lugar, você pode atualizar o Braille.
fora do lugar, você pode atualizar a exibição em Braille pressionando e mantendo pressionado
o botão quadrado “0” (⠚) por alguns segundos. O visor será
será reiniciado após cerca de 20 segundos.

Menu principal
=========

No menu principal, você pode inserir um marcador na página atual,
retornar a um marcador que foi colocado anteriormente em um arquivo ou
navegar até uma determinada página em um livro.

Para navegar até o menu principal, pressione o botão grande de menu. Em qualquer menu do
menu no Canute 360, pressionar o botão de menu novamente o levará de volta à
última página que estava lendo.

Como em todos os menus do Canute 360, para selecionar um item do menu principal
pressione o botão triangular de seleção de linha imediatamente à esquerda do
item de menu na superfície de leitura.

Para navegar para uma página dentro do arquivo, pressione o botão de seleção de linha
imediatamente à esquerda de “ir para a página” na superfície de leitura.
superfície de leitura. Uma página de ajuda será exibida na superfície de leitura. Para inserir um número de página
página, use as teclas triangulares de seleção de linha e o botão quadrado de zero
localizado à esquerda da superfície de leitura.

Para ir para a página 20, por exemplo, pressione o botão número dois, seguido do botão
botão zero. Você ouvirá, sentirá e verá uma linha de Braille clicar para cima e para baixo para confirmar a seleção.
e para baixo para confirmar a seleção e, em seguida, pressione o botão avançar para
navegar até a página selecionada. Como alternativa, pressione o botão voltar para
desfazer sua seleção.

Marcadores
=========

Para colocar um novo marcador na página atual de um arquivo ou retornar a um
marcador previamente colocado em um arquivo, primeiro navegue até o menu no livro
pressionando o botão grande de menu no painel frontal do Canute 360.

Para inserir um marcador, pressione o botão de seleção de linha imediatamente à
esquerda de 'insert bookmark at current page' (inserir marcador na página atual).

Um marcador será inserido na página atual. Nota: no momento não há
não há feedback quando um marcador é inserido; isso será corrigido em uma versão posterior do software.
versão posterior do software.

Para recuperar um marcador, pressione o botão de seleção de linha imediatamente à
esquerda de 'choose from existing bookmark'.

Uma lista de marcadores em um arquivo será exibida na superfície de leitura. Use os
Use os botões triangulares de seleção de linha para selecionar um marcador. O Canute 360
exibirá a página selecionada no livro.

No menu de marcadores, também é possível selecionar “início do livro” ou “fim do livro” para navegar até o início ou o fim de um livro usando os botões de seleção de linha triangular.
início ou fim de um livro usando o botão de seleção de linha imediatamente à
à esquerda dessas opções.

Alteração do idioma ou do código Braille
=================================

Você pode alterar o idioma do sistema ou o código Braille (por exemplo, para
alternar entre Braille contraído e não contraído) no menu do sistema.
menu. Observe que isso só alterará o idioma e o código nos menus, no arquivo de ajuda e nos manuais, e não o idioma e o código Braille.
menus, no arquivo de ajuda e nos manuais; não o idioma ou o código em nenhum livro do Canute 360.
Canute 360.

Para acessar o menu do sistema, primeiro acesse o menu principal pressionando o botão
botão grande de menu no centro da borda frontal do Canute 360. Em seguida,
pressione o botão de seleção de linha imediatamente à esquerda de 'view system menu' para acessar o menu do sistema. Pressione o botão de seleção de linha
imediatamente à esquerda de “select language and code” (selecionar idioma e código) para acessar o menu de seleção de idioma. Você
Você pode então selecionar um idioma ou código Braille usando o botão triangular de seleção de linha
triangular imediatamente à esquerda do idioma ou código escolhido.
Pressione o botão avançar uma vez para confirmar a seleção.

Saída do monitor
==============

Você pode visualizar o texto exibido pelo Canute em impressão e em Braille usando um monitor de computador padrão.
monitor de computador padrão. Basta conectar o monitor à porta HDMI
na parte frontal do painel lateral esquerdo. A representação impressa será
em uma fonte verde, e o Braille será exibido abaixo em uma fonte branca.
branca. A representação impressa está em Braille ASCII (não traduz o texto; destina-se a pessoas com visão).
não traduz o texto; ela é destinada a leitores de Braille com visão).

Canute como uma tela
===================

O Canute 360 pode ser usado como um monitor quando conectado a um PC com a porta
porta USB-B. Atualmente, isso é suportado pelo BRLTTY e pelo Duxbury DBT.
Consulte a seção de suporte em nossa seção no site para saber como usar o Canute 360 com um PC.
o Canute 360 com um PC.

Desligamento
========

Para desligar o Canute 360 com segurança, pressione o botão de energia localizado à
à direita da tomada de energia no painel traseiro. Você ouvirá, verá e
Você ouvirá, verá e sentirá cada linha do visor ser reiniciada e cair no visor.
Após alguns segundos, a linha superior do visor será atualizada para mostrar
“PLEASE WAIT...” (⠠⠏⠇⠑⠁⠎⠑ ⠺⠁⠊⠞⠲⠲⠲), e as outras 8 linhas se abaixarão.
Após cerca de dez segundos, a primeira linha também baixará. Agora é seguro
é seguro remover a fonte de alimentação da Canute 360.

Cuidados com a Canute 360
==========================

O Canute 360 não contém nenhuma peça que possa ser reparada pelo usuário. A base
O painel da base e as placas laterais só devem ser abertos ou removidos por um engenheiro de manutenção qualificado.
qualificado.

Você pode limpar a frente e as laterais do Canute 360 usando um pano úmido, mas não molhado, que não solte fiapos.
um pano úmido, mas não molhado, que não solte fiapos. A Bristol Braille Technology recomenda a limpeza do Canute 360
limpeza do Canute 360 pelo menos uma vez por mês. Você só deve tentar limpar o Canute 360
limpar o Canute 360 somente quando ele estiver desligado, com a alimentação desconectada. Não
não tente limpar nenhuma parte mecânica do visor Braille
por conta própria.

Se você guardar o Canute 360 dentro da caixa de transporte, poderá notar o acúmulo de um resíduo branco em pó nos painéis frontal e lateral.
Se você guardar o Canute 360 dentro da caixa de transporte, poderá notar um resíduo branco em pó se acumulando nos painéis frontal e lateral. Isso é
normal, não afeta a operação e pode ser removido com um pano úmido (mas não molhado).
pano úmido (mas não molhado) que não solte fiapos.

Atualização do software Canute
=============================

De tempos em tempos, a Bristol Braille Technology poderá disponibilizar atualizações de software para o Canute 360
atualizações de software para o Canute 360 para corrigir problemas, melhorar o desempenho
ou acrescentar recursos adicionais.

Cada atualização virá com instruções de instalação. Para instalar uma
atualização, você precisará de um pendrive USB vazio ou de um cartão SD no formato FAT32. OS CARTÕES SD
podem ser SD ou SDHC, mas não SDXC ou SDUC. A maioria dos cartões
A maioria dos cartões SD de menor capacidade tem o formato FAT32. Para obter instruções completas de instalação
instruções completas de instalação, consulte o documento Leiame que acompanha a atualização.

Informações importantes sobre segurança
============================

*CUIDADO: antes de usar este manual, leia e compreenda todas as informações de segurança relacionadas a este produto.
informações de segurança relacionadas a este produto.*

Serviço e suporte
-------------------

O Canute 360 não contém nenhuma peça que possa ser reparada pelo usuário. A base
O painel da base e as placas laterais só devem ser abertos ou removidos por um
qualificado. Não tente fazer a manutenção do Canute 360 por conta própria
a menos que seja instruído a fazê-lo pelo centro de suporte ao cliente. Utilize somente um
Utilize somente um provedor de serviços aprovado pela Bristol Braille Technology.

Cabos de alimentação e adaptadores de energia
------------------------------

Use somente os cabos de alimentação e adaptadores de energia fornecidos pela Bristol Braille Technology.
Technology. Nunca enrole um cabo de alimentação em um adaptador de alimentação ou em outro
objeto. Isso pode estressar o cabo de forma que ele possa
desgastado, rachado ou amassado. Isso pode representar um risco à segurança. Sempre direcione os
sempre encaminhe os cabos de alimentação de modo que eles não sejam pisados, tropeçados ou
por objetos. Proteja o cabo de alimentação e os adaptadores de energia de líquidos. Não use
use qualquer adaptador de energia que apresente corrosão nos pinos de entrada de CA ou que mostre
sinais de superaquecimento (como plástico deformado) na entrada CA ou em
em qualquer parte do adaptador de energia.

Calor e ventilação do produto
----------------------------

O Canute 360 e seu adaptador de energia podem gerar calor durante o uso.
Isso é perfeitamente normal. O Canute 360 é um dispositivo de mesa e deve ser
sempre ser colocado em uma mesa ou escrivaninha. Você nunca deve colocar a Canute
360 sobre uma cama, sofá, carpete ou outra superfície flexível. Para evitar o risco de
de ferimentos, nunca deixe a Canute 360 em contato com seu colo ou
colo ou qualquer outra parte do corpo durante a operação. Você deve sempre desligar o
desligue o Canute 360 e desconecte o adaptador de energia quando não estiver em uso.

Ambiente operacional
---------------------

O Canute 360 foi projetado para funcionar melhor em temperatura ambiente, entre
10°C e 25°C (50°F-80°F) com umidade variando entre 35% e 80%. Se o
Se a Canute 360 for armazenada ou transportada em temperaturas inferiores a 10°C
(50°F), você deverá permitir que a temperatura do Canute 360 aumente
lentamente até uma temperatura entre 10°C e 25°C (50°F-80°F) antes do uso. Esse processo
Esse processo pode levar até duas horas.

Se você não permitir que o dispositivo atinja a temperatura ideal de operação antes do uso
antes do uso, poderá resultar em uma operação prejudicada ou em danos ao seu Canute
360.

Não coloque nenhuma bebida em cima ou ao lado do Canute 360. Se
líquido for derramado sobre o Canute 360, poderá ocorrer um curto-circuito ou outros danos.
curto-circuito ou outros danos poderão ocorrer. Não coma ou fume sobre a Canute. Quaisquer partículas que caiam
que caírem em seu Canute 360 poderão danificar o mecanismo.

Superaquecimento
-----------

O Canute tem um gerenciamento térmico interno. Se ele ultrapassar uma determinada
temperatura, considerada abaixo do ideal para o mecanismo (aproximadamente a mesma
(aproximadamente o mesmo que quando o visor está quente ao toque), ele pausará todas as
operação até que a temperatura caia. Basta permitir que ele esfrie e
ele reiniciará, de onde parou, por sua própria vontade.

Entrada elétrica
----------------

O Canute 360 foi projetado para funcionar sob as seguintes condições elétricas
condições elétricas a seguir:

Tensão de entrada: mínimo 100V (CA) máximo 240V (CA), frequência de entrada
entre 50Hz e 60Hz

Ruídos durante a inicialização e a operação
------------------------------------

Ao ser ligado pela primeira vez, o Canute 360 emitirá um ruído mais alto do que o normal ao
ao reiniciar a tela. Ele passará por isso duas vezes antes de
antes de começar a funcionar, levando cerca de um minuto no total. Isso pode levar até cinco minutos se o Canute estiver sendo ligado a frio.

Uma vez ligado e em uso, o Canute 360 emitirá um ruído mais suave de “ajuste de linha
para cada linha do visor que estiver sendo alterada.

Quando a Canute 360 for deixada ligada e não estiver sendo usada, ela poderá fazer um
ruído suave de “tique-taque”, dependendo da versão do firmware que estiver usando.

Suporte 
=======

Para obter suporte técnico, em primeiro lugar, entre em contato com o seu distribuidor.

Para relatar um bug ou entrar em contato com a equipe, envie um e-mail para
[support@bristolbraille.](mailto:support@bristolbraille.co.uk)org.

Para obter mais informações sobre suporte, visite
https://[www.bristolbraille.org/support](http://www.bristolbraille.co.uk/support)

Os muitos usos do Canute 360
===============================

Braile é alfabetização, emprego e independência.
familiarizados com o uso básico da Canute 360, os usuários encontrarão muitas
possibilidades adicionais além da simples leitura de livros.

- Economia de papel: uma economia óbvia é a de cópias impressas em braile, que são volumosas.

- Tabelas: tornam-se muito mais legíveis quando até 9 linhas podem ser
exibidas.

- Matemática: geralmente contém equações longas e complexas que se estendem por
que se estendem por várias linhas.

- Música: as partituras são invariavelmente mais bem compreendidas quando várias linhas podem ser lidas.
podem ser lidas.

- Aprendizagem: Os cursos de ensino em Braille podem ser facilmente adaptados para exibição
no Canute.

- Idiomas: a exibição de diferentes idiomas em um mesmo documento é
possível e prático.

Notas sobre versões e direitos autorais
===============================

O manual principal está armazenado no site. Todos os direitos autorais pertencem a
Bristol Braille Technology CIC e Techno-Vision Systems.

Última edição em 10^th^ de Janeiro, 2025
