% Canute Console Handleiding
% Bristol Braille Technologie
% Laatste bijwerking: 2024-09-05

# Opmerkingen

Vertaalnotitie: Alle niet-Engelse vertalingen zijn vertalingen vanuit het Engels met DeepL Pro. Als je vertaalfouten opmerkt, laat het ons dan weten.

Contact: enquiries@bristolbraille.org

# Wat is de Canute Console?

De Canute Console is een Linux werkstation met een Canute 360 brailleleesregel. De Canute Console Premium is een bundel met ondersteuning of ontwikkeling op maat, een afsluitbare flightcase en langere garantie.

Het grote verschil tussen de Canute Console en andere producten is dat de brailleleesregels en de visuele leesregels dezelfde hoeveelheid informatie in dezelfde lay-out weergeven, waardoor noch de braillegebruiker noch de gebruiker met een afdruk wordt benadeeld.

Het heeft een intuïtieve interface van menu's voor het lezen van BRF-boeken en het starten van toepassingen. Het kan ook bediend worden vanaf de commandoregel en in die modus is het vooral bedoeld voor informatica en systeembeheer. 

Er zijn weinig grenzen aan het gebruik als je eenmaal gewend bent aan het werken in meerdere regels braille.

# De Canute 360 brailleleesregel in de console plaatsen en verwijderen 

Als u de Console als enkel apparaat heeft gekocht, hoeft u dit niet te doen om de Console te gaan gebruiken. 

Als je de Console zonder brailleleesregel hebt gekocht (de 'upgrade kit'), leg dan beide producten op een plat oppervlak waar je ze wilt gebruiken. Open de deksel (het beeldscherm) van de Console Upgrade Kit. Til de Canute 360 op en leg hem voorzichtig op de vlakke ondergrond in het midden van de Console Upgrade Kit. 

Rechts achteraan zit een uitsteeksel van de upgradekit. Dit is de mannelijke voedingsconnector en moet uitgelijnd worden met de vrouwelijke voedingsconnector op de Canute 360. 

Zodra dit uitgelijnd is, beweegt u de Canute 360 naar achteren om de voedingsconnectoren aan te sluiten. Het kan zijn dat u de Canute 360 iets moet optillen om de aansluiting te maken.

Nu voelt u linksachter op de upgradekit een mannelijke USB-B-connector (deze worden gewoonlijk 'printerconnectors' genoemd) aan een zeer korte kabel van ongeveer 5 cm. Trek het uit het gat waar het in zit en steek het in de vrouwelijke USB-B poort van de Canute 360 aan de rechterkant.

Om de Canute 360 te verwijderen gaat u op dezelfde manier te werk als hierboven, maar dan in omgekeerde volgorde.

# Ergonomie

De Canute Console is bedoeld voor gebruik op een stabiel, plat bureau of tafel met een glad oppervlak.

Het gladde oppervlak is nodig om de lade met het toetsenbord in en uit te kunnen nemen.

Til nu de monitor op (die ook als deksel fungeert) en trek het toetsenbord eruit.

Omdat de Canute Console veel braille bevat die je over een toetsenbord moet reiken om te lezen, is ergonomie extra belangrijk.

Zorg er daarom voor dat je stoel zo hoog mogelijk staat ten opzichte van het bureau/de tafel.

Zorg er ook voor dat de console comfortabel tegen de rand van het bureau/tafel staat.

U moet uw handen comfortabel kunnen laten rusten op zowel de bovenste regel van de brailleleesregel als de laatste regel van het toetsenbord.

# Poortindeling van de Canute Console

(Houd er rekening mee dat de poorten van de Canute 360 niet meer in gebruik zijn wanneer deze in de console is gedockt).

Aan de linkerkant van de Canute Console, van achter naar voren: 

- 1x mannelijke USB-B connector, die zich in een klein gaatje in de Console Upgrade Kit bevindt (als de Canute 360 is losgekoppeld), of in de vrouwelijke USB-B poort van de Canute 360 is gestoken.

- 3x vrouwelijke USB-A aansluitingen.

Aan de rechterkant van de Canute Console, van achter naar voren:

- 1x vrouwelijke voedingsaansluiting.

- 1x vrouwelijke 35.mm audio-aansluiting.

- 1x tuimelschakelaar voor externe monitor.

- 2x schakelaars voor bediening van externe monitor.

- 1x vrouwelijke HDMI-aansluiting.

Aan de achterkant, van midden naar rechts:

- 1x verwijderbaar paneel voor extra toekomstige poorten.

- 1x aan/uit-schakelaar.

Als je een van de Early Bird Consoles hebt, kan de poortindeling afwijken van de bovenstaande.

# Alles aansluiten

Nadat je ervoor hebt gezorgd dat de USB-B in de Canute 360 is gestoken en de stroomkabel is aangesloten, druk je op de aan/uit-knop rechtsachter om de brailleleesregel in te schakelen.

# Loskoppelen

Wacht na het afsluiten van de Canute console (zie hieronder) tot de Canute display is uitgeschakeld en verwijder dan de voedingskabel. Houd er rekening mee dat als u de voedingskabel niet verwijdert, het beeldscherm nog steeds stroom krijgt en in de screensaver-modus gaat. Daarom moet een Canute Console die niet wordt gebruikt altijd worden losgekoppeld.

# Toetsenbordindeling

Ervan uitgaande dat u het Britse toetsenbord gebruikt (zie anders hieronder 'De indeling van het toetsenbord wijzigen'), is de indeling van het toetsenbord als volgt:

Eerste regel: Escape, F1 tot F10 (F11 is Fn+F1, F12 is FN+F2), Num lock, Print Screen, Delete (Invoegen is Fn+Del).

Regels twee, drie, vier en vijf zijn standaard voor Britse Qwerty-toetsenborden.

Regel zes: Control (ctrl), Function (Fn), Super toets, Alt, Space, Alt, Ctrl, Pijltjes (Home is Fn+LEFT, End is Fn+RIGHT, Page up is Fn+UP, Page down is Fn+DOWN).

# Starten in 'Reader Mode

Sluit de stroomkabel aan op de console en druk op de aan/uit-knop aan de achterkant.

Het duurt ongeveer een minuut voordat de brailleleesregel opstart en in die tijd worden de puntjes gewist. 

Zodra hij is opgestart, staat de Canute Console in 'Reader Mode'. Deze werkt op dezelfde manier als de Canute 360 stand-alone brailleleesregel en vereist geen Qwerty-toetsenbord. 

De 'H'-toets linksboven op het display start contextuele hulp op om uit te leggen hoe elk menu werkt. In het kort: de 'menu' duimknop opent het menu met opties of, als je al in een menu bent, ga je terug naar je huidige boek. Met de knoppen 'Vorige' en 'Vooruit' kun je door boeken en menu's bladeren. De knoppen aan de zijkant komen overeen met de gelabelde items voor menu's.

Je kunt hier meer over lezen in de 'Canute 360 gebruikershandleiding' die ook in deze map staat (~/learn/).

Als het scherm in de console is gedockt, leest het alle BRF-bestanden *in je thuismap in het interne geheugen van de console*. Het leest niet de SD-kaart in je Canute 360, die wordt alleen gelezen als het Canute 360 brailleleesregel stand-alone (gedockt) wordt gebruikt.

# Naar de 'Console-modus' gaan

In de 'Console-modus' werken alle interactieve toepassingen. Dit is in wezen opstarten in een PC.

Om de 'Reader Mode' te verlaten, selecteer je de optie in het 'System Menu' of haal je het Qwerty-toetsenbord tevoorschijn en druk je op de Escape-toets.

Je krijgt nu een inlogscherm.

Als je een upgrade van een oudere versie naar deze versie hebt uitgevoerd, wordt je gebruikersnaam of wachtwoord niet gewijzigd.

Voor nieuwe gebruikers is de standaard gebruikersnaam “bbt”, het standaard wachtwoord is “bedminster”, allemaal kleine letters.

We raden je sterk aan om het standaard wachtwoord zo snel mogelijk te veranderen en te noteren. Meer daarover hieronder onder “Je wachtwoord wijzigen”.

In 'Console-modus' heb je een 'Launcher'-menu met opties die erg lijken op het menu met boeken in 'Reader-modus'. Het werkt ongeveer hetzelfde, behalve dat de 'Menu'-knop niet werkt in deze modus.

Elke regel is een toepassing of opdracht, zoals 'Bestandsverkenner', die bestanden opent in de toepassing 'Tekstverwerking'. Als u een van deze toepassingen afsluit, keert u terug naar de Launcher. Als je om een of andere reden niet wordt teruggestuurd naar de Launcher, maar in plaats daarvan naar de commandoregel gaat, kun je deze opnieuw laden door te typen:

`$ launcher`

# Commando's uitvoeren vanuit de Launcher

Er zijn een paar belangrijke menu-items die we hieronder zullen opnoemen en uitleggen:

- Shutdown'. Dit schakelt de Canute Console en Braille display uit. Eenmaal uitgeschakeld zullen de braillepuntjes allemaal uitgaan. Zodra dit gebeurt, moet je de stroomkabel uit de console halen.

- WiFi'. Dit opent een lijst met beschikbare verbindingen met hun signaalsterkte. Als je de gewenste verbinding hebt gevonden, druk je op 'q' om deze te sluiten en wordt je gevraagd om de naam van de verbinding in te voeren, gevolgd door het wachtwoord.

- 'Upgrade'. Hiermee wordt gezocht naar een upgrade naar de nieuwste versie van het Canute Console OS. Hiervoor is eerst een internetverbinding nodig. We sturen e-mails wanneer er een belangrijke upgrade is, dus schrijf je in voor de nieuwsbrief op de [Bristol Braille website: https://bristolbraille.org/](https://bristolbraille.org/). De upgrade kan even duren, afhankelijk van je verbindingssnelheid. Eenmaal klaar log je uit door `shutdown now` te typen. Trek het netsnoer uit, steek de stekker weer in het stopcontact en druk op de aan/uit-knop.

- Opdrachtregel. Hiermee ga je naar de opdrachtprompt. We zullen de opdrachtregel hieronder in detail bespreken. Je hoeft de commandoregel echter niet te gebruiken totdat en tenzij je dat wilt.

# Applicaties uitvoeren vanuit de Launcher

De lijst met toepassingen in de Launcher groeit voortdurend. Probeer ze allemaal uit om te zien hoe ze werken. Al deze toepassingen zijn in voortdurende ontwikkeling, sommige zijn nog experimenteel, en ze zullen allemaal verbeteren met toekomstige software releases. Laat ons weten wat je nog meer zou willen zien. 

Hier zijn een paar nuttige om mee te beginnen:

- 'Bestandsverkenner'. Dit opent een boomstructuur van je bestandssysteem waarin je kunt navigeren met de duimknoppen vooruit en achteruit, net als in het Reader- en Launcher-menu. Als je een bestand selecteert, wordt de toepassing 'Tekstverwerking' gestart, die je een menu geeft voor het bekijken en bewerken van documenten in rich text format, inclusief een unieke print-layout weergave van PDF-bestanden.

- Tekstverwerker' Hiermee opent u een tekstverwerker genaamd 'Micro' op een leeg document. Micro lijkt erg op Notepad onder Windows met vergelijkbare commando's. ctrl+x/c/v' om te kopiëren en plakken, 'ctrl-z' om ongedaan te maken, 'ctrl-y' om opnieuw te doen, 'ctrl-f' om te zoeken, 'ctrl-s' om op te slaan, 'ctrl+q' om af te sluiten, bijvoorbeeld.

# Een paar commando's uitvoeren vanaf de commandoregel

De volgende functies zullen binnenkort worden toegevoegd aan het menu Launcher, maar kunnen momenteel worden uitgevoerd vanaf de opdrachtregel.

Om het volgende te doen selecteert u eerst de optie 'opdrachtregel' in het menu Launcher. Dit opent een 'prompt', dat is een regel waarin je commando's kunt typen. Deze regel wordt weergegeven door een dollarteken. Eerdere commando's blijven op het scherm staan en een nieuwe prompt verschijnt onderaan het scherm. De laatste prompt is degene waarin je gaat typen.

Als je klaar bent kun je terugkeren naar het menu door `launcher` te typen op de prompt.

## Braillecodes wijzigen

In de commandoregel zijn er een aantal speciale commando's om de braillecodetabel die je gebruikt te wijzigen, waaronder:

* $ [alt]+[shift]+Q
(Eerste graad UEB)

* $ [alt]+[shift]+W
(graad twee UEB)

* $ [alt]+[shift]+E 
(9-regelig 6-dot US Computer Brl)

* $ [alt]+[shift]+R 
(Gecontracteerd Duits, 2015 standaard)

Let op: alleen US computer brl garandeert een één-op-één tekenrepresentatie en dus exact dezelfde lay-out als het visuele scherm.

Als je de naam weet van een andere BRLTTY braille tabel die je wilt gebruiken dan kun je die invoeren met:

`$ canute-table [tabel-naam]`

## Wachtwoord wijzigen

Om het standaard wachtwoord te wijzigen typ `$ passwd` en volg de instructies.

# Escape!

Nu we de basis kennen, zijn de belangrijkste commando's het afsluiten van programma's en veilig afsluiten.

Om een programma dat draait te stoppen zijn er vaak commando's die specifiek zijn voor dat programma. Vaak wordt `q` of `ctrl+q` gebruikt om af te sluiten, of de `escape` toets (linksboven op het Console toetsenbord). In extreme gevallen werkt het echter ook om `ctrl+c` in te drukken (als een programma hangt of als je niet weet hoe je het moet sluiten). 

Als je een programma echt niet kunt sluiten om terug te gaan naar de opdrachtregel of Launcher, kun je een nieuw terminal tabblad openen. Zie hieronder.

--------

Dat is alles wat je moet weten om aan de slag te gaan met de Canute Console. Als je meer wilt leren over het gebruik van de Console, met name in de commandoregel, of het programmeren van je eigen applicatie, of om een probleem op te lossen dat je hebt, lees dan hieronder verder.

--------

# Grondbeginselen voor het navigeren door de Linux commandoregel

## Nomenclatuur en indeling van de opdrachtregel

Linux is het besturingssysteem waarop Canute Console draait. Het draait een versie van Linux die 'Raspian' heet en gebaseerd is op 'Debian'.

De 'Console Mode' van de Canute Console is voorgeconfigureerd om volledig vanaf de commandoregel te draaien, wat betekent dat alle applicaties tekst- en terminalgebaseerd zijn.

Je zult vaak zien dat de termen “commandoregel” of “terminal” (ook “tty” en console") door elkaar worden gebruikt omdat ze een vergelijkbare betekenis hebben. Maar, in het kort, zijn hier hun verschillen:

- De “opdrachtregel” is meestal de laatste regel op het scherm en het is waar commando's kunnen worden geschreven. De commandoregel bevat een “opdrachtprompt”, wat betekent “waar je je commando's typt”.

- De “commandoregel” bevindt zich in een “terminal”. De terminal bevat de geschiedenis van eerder ingevoerde commando's en hun uitvoer.

In de rest van dit document gebruiken we instructies die beginnen met een dollarteken, dan een instructie en soms nog wat andere woorden of getallen, gescheiden door een spatie.

Dit is hoe commando's worden uitgevoerd in de Linux commandoregel. Het lijkt erg op de DOS Prompt onder Windows.

Het dollarteken is gewoon een symbool dat “opdrachtprompt” betekent, dat wil zeggen “typ je commando's hierna”.

## Hoe commando's te typen en de terminaluitvoer te begrijpen

Dus als we bijvoorbeeld een instructie als “Voer `$ ls` uit” schrijven, betekent dat “Typ ‘ls’ in de commandoregel”. In dat geval betekent het eigenlijk, “maak een lijst van de submappen en bestanden” in mijn huidige mappen.

Als ander voorbeeld kunnen we in dit document schrijven, `$ cd demos` betekent “Voer het commando ‘cd’ uit met de instructie (argument genoemd) ‘demos’”. Eigenlijk betekent het in dat geval “ga naar de map genaamd ‘demos’”.

Het is belangrijk op te merken dat de terminal al je vorige commando's opslaat en ze niet van het scherm verwijdert tenzij daarom wordt gevraagd.

Zie het als een typemachine of embosser, die één regel per keer schrijft en de vorige regels niet wist.

Lees altijd tot de laatste regel van het scherm en neem aan dat de *laatste regel met de dollar erop* de actieve commandoregel is en dat de regels erboven geschiedenis zijn.

Als ik bijvoorbeeld een commando wil typen als “maak een map genaamd ‘documents’, wat geschreven zou moeten worden als `mkdir documents`, maar ik heb een fout gemaakt, dan zou de braille weergave er als volgt uit kunnen zien:

`$ mjdir documents`
bash: mjdir: opdracht niet gevonden`
`$`

Laten we dat nog eens regel voor regel bekijken. 

`$ mjdir documenten`

Ik heb een typefout gemaakt met het commando.

`bash: mjdir: opdracht niet gevonden`.

Dit is de foutmelding ('bash' is wat men noemt de 'shell', oftewel het programma dat je commando's interpreteert).

`$ `

Nu krijgen we dus weer een lege opdrachtprompt, maar we zien nog steeds onze eerste poging en de foutmelding hierboven.

Voor degenen die gewend zijn aan brailleleesregels is dit misschien even wennen. Maar het is wel het standaard gedrag voor de Linux terminal en precies dezelfde lay-out en uitvoer als ziende gebruikers krijgen op de visuele monitor.

Als je scherm te vol is, dan is het `$ clear` commando je vriend. Het zal de terminal resetten naar niets anders dan de huidige opdrachtprompt op de bovenste regel van het scherm.

## Rondkomen

Linux gebruikt een hiërarchisch bestandssysteem, wat betekent dat mappen en bestanden allemaal in andere mappen staan, terug naar de 'root'. De root staat bovenaan de hiërarchie.

Mappen worden gescheiden door streepsymbolen, dus je thuismap wordt geschreven als `/home/pi/`.

Bij het inloggen start je in de homedirectory van je gebruiker. Om te beginnen hoef je zelden verder te gaan dan je thuismap.

`$ ls` geeft een lijst van de submappen en bestanden in de huidige map.

Het kan zijn dat de uitvoer een te lange lijst is om te lezen. Je kunt terug omhoog en omlaag scrollen in de terminal geschiedenis door op `ctrl+Fn+UP` of `ctrl+Fn+DOWN` te drukken.

Om van directory te wisselen gebruik je het `cd` commando. `$ cd mapnaam`.

Om een niveau terug te gaan gebruik je het `$ cd ..` commando.

Hier merken we iets belangrijks op over veel Linux commando's, zoals `cd`. Als je vraagt om naar een map te gaan, zal het dat doen, maar het geeft je geen uitvoer om te zeggen of het gelukt is. 

Dus hoe weet je waar je bent? Je kunt `$ pwd` typen. Dit geeft je huidige locatie binnen het bestandssysteem. 

## Tips om navigatie en het typen van commando's makkelijker te maken

Het kan zijn dat voor sommige commando's de uitvoer langer is dan het scherm (40 cellen), daarom is de Canute helemaal naar rechts gepand. 

Om naar links en rechts te pannen op de brailleleesregel (dit heeft geen invloed op de visuele weergave) om strings langer dan 40 cellen te bekijken, kunt u op de duimtoetsen '[MENU]+[FORWARD]' aan de voorkant van de Canute-display drukken om naar rechts te gaan, of '[MENU]+[PREV]' om naar links te pannen.

U kunt een opdracht of een map- of bestandsnaam automatisch aanvullen wanneer u op de opdrachtprompt typt door op de tabtoets te tikken.

Als er maar één optie beschikbaar is, vult tab deze automatisch in. Als het meerdere dingen kunnen zijn dan zal op tab drukken niets doen, maar dubbel tikken op tab zal alle opties laten zien en ze naar de terminal schrijven zodat je ze kunt bekijken.

## Tekstbestanden lezen

Als er een tekstbestand is dat je wilt lezen kun je `$ less textfilename.txt` typen. Het hoeft geen TXT extensie te hebben en vaak hebben ze in Linux helemaal geen extensie. Als je twijfelt, probeer dan `less`.

Less drukt de eerste acht regels van het bestand af. Gebruik de pagina omhoog en omlaag (Fn+UP, Fn+DOWN) om er doorheen te gaan, druk op 'q' om terug te gaan naar de commandoregel.

## Nuttige commando's 

Inclusief:

* ls|less
(Geeft een lijst van bestanden en submappen)

* tree|less
(Toont directorystructuur)

* cd [DIRECTORYNAME]
(Naar een submap gaan)

* cd ..
(Naar boven naar de bovenliggende map gaan)

* mkdir [DIRECTORYNAME]
(Maak nieuwe map)

* micro [FILENAME]
(Tekstbestand openen)

* [ctrl]+[shift]+t
(Nieuw tabblad in terminal openen.)

* [ctrl]+[shift]+[NUM] (0--9)
(Schakelt over naar een ander tabblad.)

* nu afsluiten
(Afsluiten, maar nog steeds nodig om 
Canute uitschakelen nadat afsluiten 
voltooid is.)

* touch [FILENAME]
(Maak een nieuw leeg bestand)

* rm [FILENAME]
(Verwijder bestand)

* rm -r [DIRNAME]
(Verwijder directory en de inhoud ervan; '-r' is een afkorting voor 'recursief').

* sudo [COMMANDO]
(Voer de volgende opdracht uit als supergebruiker, dus met volledige rechten).

* sudo rm -r [DIRNAME]
(Verwijder een archiefmap zonder te worden gevraagd voor elk schrijfbeveiligd bestand).

## Leerbronnen

We raden je aan om te luisteren naar de volgende afleveringen van Braillecast, gesponsord door Bristol Braille, om mensen vertrouwd te maken met enkele basisprincipes van de Linux commandoregel.

Alle afleveringen kunnen gevonden worden in:
`cd ~/learn/braillists/`
en kunnen worden afgespeeld met:
`vlc 01[tab om te voltooien]`

- 01: Eerst een overzicht van verschillende computerwetenschappers die braille gebruiken in hun werk. Dit kan worden overgeslagen.

- 02: Ten tweede, een masterclass inleiding tot de Linux commandoregel.

- 03: Ten derde, een masterclass inleiding tot Git, voor versiebeheer en het delen en downloaden van programma's.

- 04: Ten vierde een masterclass over programmeren op de Canute Console.

Bij je Canute Console zitten een paar Linux basics learning tools. Je kunt ze vinden in de homedirectory door `cd learn` in te typen.
Tenslotte kunt u meestal op het web naar antwoorden op uw vragen zoeken. 

De meeste vragen zullen algemeen over Linux zijn. Als je echter meer specifieke antwoorden nodig hebt, kun je zoeken op 'Debian', of zelfs specifiek op 'Raspian'.

Voor dit soort vragen raden we de website Stackoverflow aan.

--------

Je zou nu de basisbeginselen van het werken met de Linux commandoregel redelijk onder de knie moeten hebben. In de volgende secties worden veelvoorkomende taken in meer detail uitgelegd.

-------

# Afsluiten

Om de hele unit af te sluiten vanaf de commandoregel typ je `$ shutdown now`. (Of om opnieuw op te starten typ `$ reboot`.)

Als je hebt afgesloten en het Canute scherm is uitgeschakeld* moet je de stroomkabel uit de Console halen (anders blijft de monitor aan).

* N.B. Als je je Canute 360 die bij de Console is geleverd hebt geüpgrade naar firmware versie 2.0.10 of hoger, dan zal je Canute uitschakelen als de Console wordt uitgeschakeld. Als het apparaat echter een oudere firmware heeft, dan zal het niet uit zichzelf uitschakelen, maar in plaats daarvan in de stand-alone boekleesmodus gaan. Op dat moment moet je op de aan/uit-knop rechtsachter drukken om het Canute display uit te schakelen voordat je het netsnoer eruit haalt.

# Een nieuw terminal tabblad openen

Terminal tabbladen werken als een tabblad in een webbrowser. Druk op `ctrl+shift+t` om een nieuw tabblad te openen. Het vorige tabblad blijft op de achtergrond actief zodat je tussen meerdere tabbladen kunt wisselen. 

Om tussen tabbladen te wisselen kun je op `alt+[1--9]` drukken. 

Om een tabblad vanaf de opdrachtregel te sluiten typ je `$ exit`. Houd er rekening mee dat wanneer je het oorspronkelijke tabblad sluit, je je sessie afsluit, dus zorg ervoor dat je alle wijzigingen in documenten opslaat voordat je dit doet.

Houd er rekening mee dat het openen van een nieuw tabblad of het wisselen tussen tabbladen de huidige Braillecode niet verandert, dus als je in een toepassing was die Computer Braille gebruikte en je opent een nieuw tabblad, dan zal het nieuwe tabblad ook in Computer Braille zijn.

# Geluid

Let op de onderstaande commando's. De dubbele s in sset is geen typefout.

- `$ amixer sset Master 0%`

- `$ amixer sset Master 100%`

# De toetsenbordindeling wijzigen

De opdracht is relatief eenvoudig vanaf de opdrachtregel. Bijvoorbeeld:

`$ setxkbmap us`

Verandert het toetsenbord in de VS. 

Voor een lijst met beschikbare toetsenbordindelingen typ:

`$ ls /usr/share/X11/xkb/symbols/ | less`

Om het automatisch in te stellen voeg je hetzelfde commando toe aan `~/console-set-up/console-start.sh`:

`$ setxkbmap us`

# USB-sticks gebruiken

USB-sticks worden automatisch gemount in de `/media/` map.

Bijvoorbeeld, de eerste USB-stick die u in de USB-stick steekt wordt gemount in `/media/usb0`, de tweede USB-stick wordt gemount in `/media/usb1`, enzovoort.

Om bestanden op de USB-stick te openen gebruikt u `cd` zoals bij elke andere directory, bijvoorbeeld `$ cd /media/usb0/`.

Als je Console niet up-to-date is, kan het zijn dat je een applicatie genaamd `usbmount` moet installeren voordat de bovenstaande functionaliteit werkt. Hiervoor is een internetverbinding nodig (zie hieronder). Eenmaal verbonden typ je dit commando:

`$ sudo apt install usbmount`

Zodra de installatie is voltooid, start u de console opnieuw op of verwijdert u de USB-stick en plaatst u deze opnieuw.

# Verbinden met WiFi

De Canute Console zal automatisch verbinding maken met elk WiFi-netwerk dat is opgeslagen. 

Om tussen deze netwerken te kiezen (als er meer dan één is) typ `$ wifi new`.

Om voor de eerste keer verbinding te maken met een nieuw netwerk typ je `$ wifi new`, vervolgens druk je op `q` waardoor er een prompt verschijnt op de laatste regel waar je de naam van de verbinding die je wilt, kunt intypen en vervolgens het wachtwoord.

Als je succesvol verbinding maakt zul je een bericht zien zoals `Device 'wlan0' successfully connected`, het kan echter zijn dat je dit niet ziet als je al eerder verbinding hebt gemaakt.

De volgende keer dat je verbinding wilt maken met die Wifi-verbinding, maar het wachtwoord niet opnieuw wilt intypen, kun je de `new` weglaten en dan onthoudt hij het van de vorige keer: `$ wifi`.

# Tethering naar je mobiel via de USB-A poort

Als je een Android-telefoon gebruikt, sluit je je telefoon via de USB-kabel aan op de Canute Console. Op de meldingen van je telefoon staat nu zoiets als “Opladen via USB”. Tik op die melding en u gaat naar het menu met USB-instellingen. Verander “Alleen opladen” in “Tether” (exacte termen kunnen veranderen). De Console heeft nu toegang tot het internet. 

Bij tethering wordt deze verbinding gebruikt in plaats van de WiFi-verbinding, ook al is deze niet zo snel.

# Bestanden downloaden van het internet

Als je de URL van een bestand op internet weet, kun je het downloaden met het `curl` commando. De optie `-O` slaat het op je huidige plaats in het bestandssysteem op met de oorspronkelijke naam. Bijvoorbeeld:

`$ curl -O https://bristolbraille.org/shared/2024-03-22-music-demos.zip`

In dit geval wil je het bestand misschien uitpakken, wat je met deze opdracht kunt doen:

`$ unzip 2024-03-22-music-demos.zip`

Vervolgens kunt u het originele zipbestand verwijderen met het commando `rm`:

`$ rm 2024-03-22-music-demos.zip`

Om andere opties voor curl op te zoeken, zoals het opslaan met een andere bestandsnaam of het afdrukken naar het scherm, bekijk je de `curl` handleiding:

`$ man curl`

# Nieuwe programma's installeren vanaf het Internet

Er zijn veel verschillende manieren om programma's op Linux te installeren. De meest gebruikelijke methoden zijn het gebruik van pakketbeheerders zoals 'apt', waarmee je geïnstalleerde programma's eenvoudig kunt bijwerken en beheren.

Laten we dit testen door een spel te installeren. Je kunt elk ander 'apt'-programma dat je interessant vindt opzoeken en in plaats daarvan proberen dat te installeren. Zoek op het web naar “Linux apt [onderwerp van interesse]” om te zien welke programma's beschikbaar zijn. Er zijn er duizenden!

Om het programma te installeren:

`$ sudo apt install nethack`

Als je dat uitlegt, betekent het het volgende:

- `sudo` staat voor `superuser do`, wat betekent dat je het toestemming geeft om het systeem te veranderen.

- Het dollarteken geeft aan dat je in de commandoregel moet typen.

- Het programma dat je uitvoert is `apt`, een pakketinstallateur en -beheerder.

- Je geeft het commando `install`.

- Tenslotte de naam van het programma dat je gaat installeren, `nethack-console`. In dit geval is het niet een versie die speciaal voor de Canute Console is gemaakt, de term `console` is ongeveer analoog aan `terminal`, wat betekent dat de versie van het programma bedoeld is om vanaf de terminal te draaien. 

De installatie kan een paar minuten duren en aan het einde daarvan heb je weer een lege opdrachtregel met een dollarteken erop onderaan het scherm.

Dit is een oude videogame uit de jaren 90. Het interessante eraan is dat het, ondanks dat het niet is ontworpen om op een brailleleesregel te worden gespeeld, gewoon werkt in de Canute Console. Maar om het goed te laten zien moet je eerst overschakelen naar computer Braille modus (zie hierboven).

Houd er rekening mee dat je dit spel moet opzoeken voordat je het speelt, want het is erg complex. 

`$ man nethack`

`man` is een afkorting van 'manual'. Dit geeft je de instructies.

Probeer nu het programma uit te voeren. 

`$ nethack`

Veel succes! (Als je Nethack wilt verlaten druk je op ctrl+c en blijf je op enter drukken totdat het programma afsluit).

# Bestanden afdrukken

Als je printer automatisch is gedetecteerd toen je verbinding maakte, kun je meteen beginnen met printen. Zo niet, volg dan deze instructies of neem contact op met Bristol Braille:

`$ links2 https://opensource.com/article/21/8/add-printer-linux`

Dit is het uitvoeren van een browser genaamd Links2 om een webpagina te lezen. Je hoeft alleen maar omhoog en omlaag te scrollen met de scrollup scrolldown knoppen, en afsluiten door op ctrl+c te drukken.

Zodra je printer is ingesteld, kun je documenten afdrukken met LibreOffice. (Het kan zijn dat je LibreOffice eerst moet installeren.)

`$ sudo apt install LibreOffice`
`$ libreoffice --headless -p voorbeeld.docx`

# Upgraden naar de nieuwste Console OS-versie

Voer de volgende opdracht uit zodra je verbonden bent met het internet:

`$ curl -L https://bristolbraille.org/console-install | bash`

# Voorbeeldtoepassingen ontwikkeld voor de Canute Console

## Sonic Pi

Installeren:

`$ git clone https://gitlab.com/bristol-braille/sonic-pi-canute/`

Bekijk de volgende voorbeelden van het uitvoeren en bewerken van Sonic Pi programma's met behulp van enkele tools die we ontwikkeld hebben of waar we scripts voor geschreven hebben voor de Console:

`$ cd sonic-pi-canute` (Ga naar de `sonic-pi-canute` directory.)

`$ micro examples.txt` (Bekijk --- en bewerk als je wilt --- de voorbeeld Sonic Pi code).

Nu bevindt u zich in de teksteditor genaamd `micro`. Beweeg rond de editor, sla op, maak ongedaan, etc. met de normale toetsencommando's in kladblokstijl. Om af te sluiten druk je op `ctrl+q` en typ je `y` of `n` om wijzigingen op te slaan of niet op te slaan.

Eenmaal terug op de commandoregel...

`$ bash start-sonic-pi.sh` (Hiermee wordt het grafische programma Sonic Pi gestart en vervolgens geminimaliseerd zodat we verder kunnen werken vanaf de commandoregel. Bash is een programma dat zogenaamde `shell` scripts uitvoert, die vaak eindigen op `.sh`).

Eenmaal terug in de commandoregel, wat ongeveer 30 seconden zal duren...

`$ bash play-sonic.sh examples.txt` (Een script dat het `examples.sh` bestand doorgeeft aan Sonic Pi. Je zult merken dat de geluiden spelen).

`$ bash stop-sonic.sh` (Stop alles dat wordt afgespeeld door Sonic Pi.)

--------

# Geavanceerd

## Op afstand de terminal van iemand anders bekijken

Om samen te werken met een andere collega, of om een docent te volgen, wilt u misschien hun terminalsessie online volgen met een tool als [`tty-share`](https://tty-share.com/).

## Afstandsbediening van uw console

Het kan verstandig zijn om een andere gebruiker op afstand, zoals BBT-medewerkers, de controle over de console te geven op door jou gekozen tijden. Hiervoor raden we aan om [`mosh`](https://mosh.org/) te gebruiken, wat vergelijkbaar is met `ssh` maar met een betere stabiliteit in de verbinding.

## Apps die niet op 9 regels passen

- Voer `$ screen` uit en druk op enter.

- Druk op 'enter' om de readme over te slaan.

- Druk op '[ctrl]+a' en vervolgens op ':' (dubbele punt). Hiermee kun je commando's typen, die op de 9e regel van het scherm verschijnen (er staat een dubbele punt linksonder)

- (Je kunt deze stap overslaan.) Typ `width -w 40` en druk op enter. Dit stelt de breedte in op 40. (Je kunt dat getal veranderen als de applicatie die je gebruikt breder moet zijn, maar deze handleiding gaat ervan uit dat dat niet nodig is, dus je kunt deze stap waarschijnlijk overslaan).

- Druk op '[ctrl]+a' en druk dan weer op ':' (dubbele punt) om een ander commando te typen.

- Typ `height -w 17` en druk op enter. Dit maakt de terminal hoger, in dit geval 17 regels hoog in plaats van 9. Je kunt elk ander getal kiezen, het hoeft geen veelvoud van 9 regels te zijn.

* (als voorbeeld: als u de hoogte instelt op 27 regels, wordt de terminal drie keer zo hoog als het Canute-display, dus even groot als een standaard papieren braillepagina in het VK).

- Om door de extra grote terminal te bewegen drukt u op '[ctrl]+a' en vervolgens op '['. Dit opent wat “kopieermodus” wordt genoemd, maar we gaan het gebruiken om rond te bewegen.

- Nu beweegt de cursor (meestal alle zes puntjes) vrij rond de terminal tussen de regels (meestal blijft hij op de opdrachtprompt).

- Verplaats de cursor helemaal naar de voorkant van het scherm door hem ingedrukt te houden. Het zal alle inhoud naar beneden verplaatsen.

- Wanneer je het deel van de applicatie ziet dat je wilt bekijken, druk dan op escape, dit zal dan de “kopieermodus” verlaten en terugkeren naar de normale invoermodus voor de commandoregel.

________
(Het bovenstaande komt van een vraag op Stackoverflow die je kunt lezen als je meer wilt begrijpen, maar dat is niet nodig: https://stackoverflow.com/questions/75316022/emulate-a-taller-terminal-within-gnome-terminal-and-pan-up-and-down)

# Toepassingen ontwikkelen voor de Canute Console

Dit is een groot onderwerp dat we in een apart document zullen behandelen en waaraan actief wordt gewerkt. 

Voor nu is de eenvoudigste uitleg het volgende:

Alles wat je kunt ontwikkelen voor de visuele commandoregel, in elke taal, dat draait op Linux en past in 40x9, zal draaien op de Canute Console. 

Er zijn echter bepaalde overwegingen:
- Om ervoor te zorgen dat de lay-out van de visuele en brailleleesregels 1-op-1 overeenkomen, raden we aan het programma zo te ontwikkelen dat het computerbraille gebruikt. Brailletabellen houden geen rekening met horizontale witruimte.
- Canute 360s verversen met 0.5--1 seconden per regel, daarom moeten toepassingen constant scrollen vermijden.
[NPYScreen](https://github.com/npcole/npyscreen/) kan een handige manier zijn om snel aantrekkelijke ruimtelijke tactiele en visuele producten te maken van eenvoudige bash en Python commando's.

## Applicaties publiceren als uitvoerbare bestanden voor andere platformen

De Canute Console draait een op Debian gebaseerd besturingssysteem op een ARM7 processor (de Raspberry Pi 4). Maar bij het publiceren zul je je applicaties ook op andere systemen willen laten werken.

Als je een gecompileerde taal zoals C gebruikt, kijk dan naar de compiler commando's.

Als je een scripttaal zoals Python gebruikt, dan wil je misschien een executable maken zodat de gebruiker je code niet ziet en de afhankelijkheden waar je code van afhankelijk is niet hoeft te installeren. Je wilt misschien ook een executable maken die kan werken op Windows, x86 Linux of Mac OS.

Er zijn twee tools voor Python die we aanraden te bekijken:

- `pyinstaller` is een Python-gereedschap dat je kunt installeren met `pip` en dat een uitvoerbaar bestand maakt *voor de processor waarop je het uitvoert*, in dit geval ARM7. Als je andere uitvoerbare bestanden wilt maken met dit gereedschap, moet je het op die platforms uitvoeren.

- `auto-py-to-exe` is een gereedschap dat x86 Windows uitvoerbare bestanden maakt zonder Windows te hoeven draaien en kan daarom handig zijn voor bezitters van Canute Console.

## Voorzichtig zijn met Canute Console specifieke commando's

Welke taal je ook gebruikt en welke methode je ook gebruikt om je publish te maken, vergeet niet om er rekening mee te houden of de gebruiker aan de andere kant een Canute display aangesloten heeft of niet. 

Als je het script ontwikkelt voor een publiek dat geen braille leest en geen Canute bezit, let er dan op dat de Canute Console specifieke regels in je script het programma niet laten crashen. Als je bijvoorbeeld het `canute-cursor` commando aanroept, zal dat op elk ander systeem een fout geven. 

Je kunt dit omzeilen door deze regels handmatig weg te laten, of door elegante fail-states aan te maken als deze commando's niet gevonden worden, of door een wrapper-functie te hebben die deze commando's wel of niet aanroept, afhankelijk van het systeem.

## Publiceer je applicatie openbaar

Er zijn veel manieren om dit te doen. We zullen als voorbeeld een manier beschrijven die Bristol Braille heeft gebruikt, namelijk versiebeheer gebruiken en het hosten op een veelgebruikte website.

Er is een technologie genaamd 'git' die gebruikt wordt voor het bijhouden van veranderingen aan software en het publiceren van die software op 'repositories'. Een van de websites die git repositories (vaak repos genoemd) als dienst aanbiedt heet Github, die wij gebruiken om onze publiekelijk beschikbare software op te bewaren. Dit betekent dat mensen die naar deze site gaan de broncode voor de Canute gebruikersinterface kunnen zien en deze kunnen downloaden en wijzigen.

Op websites als deze worden vaak andere nuttige diensten aangeboden die gerelateerd zijn aan software. Een daarvan is 'issues', voor het bijhouden van softwarebugs en wijzigingsvoorstellen. Een andere is een 'wiki' die wordt gebruikt voor openbare informatie over onze diensten. Een wiki is een reeks door gebruikers bewerkbare webpagina's.

Dus in dit voorbeeld op Github hebben we een 'organisatie' genaamd 'bristol-braille' en daarbinnen hebben we een respository genaamd 'canute-ui'. Binnen die repository hebben we dan gerelateerde 'issues' en een gerelateerde 'wiki'. Dus:

- De code voor de Canute gebruikersinterface is hier openbaar beschikbaar: https://github.com/bristol-braille/canute-ui/
- Problemen met betrekking tot de Canute gebruikersinterface zijn hier openbaar beschikbaar: https://github.com/bristol-braille/canute-ui/issues/
- De wiki die nuttige informatie beschrijft voor de Canute gebruikersinterface is hier openbaar beschikbaar: https://github.com/bristol-braille/canute-ui/wiki/

--------

# Problemen oplossen 

## De brailleleesregel toont geen braille

Als de brailleleesregel niet werkt, maar je vermoedt of weet dat de computer aan staat en de visuele weergave en het geluid werken, dan moet je een aantal procedures volgen.

Sluit eerst de console af. Als je dit niet netjes kunt doen zonder de brailleleesregel te kunnen lezen, volg dan de volgende procedure:

- Druk op 'ctrl+alt+f2'.

- Typ nu 'bbt' en enter, dan 'bedminster' en enter (of je eigen login en wachtwoord).

- Druk nu op escape (dit zal het menu Launcher verlaten).

- Typ nu shutdown now.

- Wacht twintig seconden en trek dan de stroomkabel uit.

Als je moeite hebt met het bovenstaande, sla dan het loskoppelen van de stroomkabel over.

Ga vervolgens terug naar het begin van deze handleiding om de stappen te volgen voor het correct bevestigen van de Canute 360 in het dock van de Canute Console, inclusief de USB-B kabel links achteraan, *voordat* u de Console aansluit.

Als je de Console weer aansluit, vergeet dan niet op de aan/uit-knop rechtsachter te drukken.

Als het probleem hiermee niet is opgelost, neem dan contact op met uw distributeur of Bristol Braille Technology voor ondersteuning.

## Problemen met inloggen

Heeft u problemen met inloggen, zoals teruggestuurd worden naar de inlogpagina? Dit kan gebeuren met de eerste versies van de Console-software, maar is verholpen met upgrades. Om de upgrade toe te passen moet je natuurlijk eerst inloggen. Druk daarom op deze toetsencombinatie om naar een andere inlogterminal te gaan:

`ctrl+alt+f2`

Voer nu je inloggegevens in. Eenmaal binnen volg je de upgrade procedure hieronder (zoek op 'Upgraden').

## Problemen met verbinden met WiFi

Als u de foutmelding `Unable to connect .... Secrets were required but not provided`, probeer dan de volgende twee commando's uit te voeren:

`$ nmcli con delete “NAAM VAN SSID”`
`$ nmcli dev wifi connect “NAAM VAN SSID” wachtwoord WACHTWOORD ifname wlan0`

## Internetverbinding testen

Test internet in de Console door `$ clear; ping bbc.com -i 4` in te typen. Je zou een log moeten krijgen met veel uitgangen die continu worden bijgewerkt. 

(Het `-i 4` gedeelte van dat commando is om aan te geven dat het elke vier seconden de BBC servers moet pingen).

Je zou een antwoord moeten krijgen zoals

64 bytes van 151.101.192.81 [etc etc]`

(Het IP adres kan anders zijn.)

Wat dat betekent is dat het een verzoek heeft gestuurd naar de BBC website en een positieve bevestiging terug heeft gekregen, wat betekent dat het verbinding maakt. Het bericht terug is toevallig 64 bytes lang.

Druk op `[ctrl]+c` om het programma te annuleren en de resultaten op te vragen.

Als de bovenstaande test geen 100% “pakketten ontvangen” oplevert, dan is er een probleem met de verbinding. 

## Internetproblemen met IPv4 vs. IPv6

Een oorzaak van verbindingsproblemen kan zijn dat je verbinding IPv6-adressen probeert te gebruiken terwijl je Internet Service Provider dat niet ondersteunt. 

Als je vermoedt dat dit de oorzaak is, kun je een configuratie maken om IPv6 uit te schakelen:

`$ sudo echo “net.ipv6.conf.all.disable_ipv6=1” >> /etc/sysctl.d/local.conf`

Uitleg: sudo = uitvoeren als supergebruiker; echo = string schrijven; >> = toevoegen aan bestand

## Verdere probleemoplossing

Neem contact op met Bristol Braille als uw probleem hierboven niet wordt behandeld.

Als u zich op uw gemak voelt met probleemlijsten voor programmeurs, dan kunt u [een probleem aanmelden op https://gitlab.com/Bristol-Braille/console-learn/-/issues](https://gitlab.com/Bristol-Braille/console-learn/-/issues)

