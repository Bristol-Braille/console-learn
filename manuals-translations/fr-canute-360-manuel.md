% Manuel Canute 360
% Bristol Braille Technology
% Dernière mise à jour : 2025-01-10

Notes
=====

Note sur la traduction : Toutes les traductions non anglaises sont des traductions de l'anglais à l'aide de DeepL Pro. Si vous constatez des erreurs de traduction, veuillez nous en informer.

Contact : enquiries@bristolbraille.org

Bienvenue sur le site
=======

Nous vous remercions d'avoir acheté Canute 360 de Bristol Braille Technology.
premier lecteur électronique en braille multi-lignes au monde.

Canute 360 dispose d'un écran de neuf lignes, chaque ligne étant composée de 40 cellules de braille.
cellules de braille. Canute 360 est compatible avec les formats de fichiers *.BRF et *.PEF
et peut afficher du texte braille, des mathématiques, de la musique ou tout autre code braille à six points.
ou tout autre code braille à six points.

Avec Canute 360, vous pouvez lire des fichiers en braille, ainsi qu'insérer, éditer,
et naviguer vers des signets.

Bristol Braille Technology est une entreprise d'intérêt communautaire à but non lucratif.
à but non lucratif. Votre achat nous permettra de continuer à investir et à développer de nouveaux produits en braille au profit des personnes handicapées.
et de développer de nouveaux produits en braille au profit de la communauté aveugle au sens large.

Dans la boîte
==============

Vous trouverez les éléments suivants dans la boîte d'expédition :

- 1 × Canute 360 Braille e-reader

- 1 × Bloc d'alimentation 19V avec cordon

- 1 × Guide de démarrage rapide en braille

Veuillez informer votre distributeur si l'un de ces éléments est manquant ou endommagé.
manquants ou endommagés.

Conventions relatives aux documents
====================

Dans ce manuel d'utilisation, lorsque le texte affiché en braille par Canute 360
est référencé, il est mis entre guillemets. Lorsque le braille est embossé sur la surface de l'appareil, il est rendu ici en braille, par exemple, le bouton de menu comme (⠍⠑⠝⠥).

**Avant d'utiliser ces informations et le produit qu'elles concernent, veillez à
de lire et de comprendre les informations de sécurité importantes figurant à la fin de ce document.
document.**

Description physique
====================

Votre Canute 360 doit être placé à plat sur la table devant vous. Les
trois gros boutons de commande sur la face avant de l'appareil (étiquetés
arrière (⠃⠁⠉⠅), menu (⠍⠢⠥) et avant (⠿⠺⠜⠙) doivent être tournés vers vous.
vers vous.

Votre Canute 360 possède un afficheur braille à neuf lignes et 360 cellules sur sa surface supérieure.
supérieure.

Dans le coin supérieur gauche de la surface supérieure, vous trouverez un bouton circulaire, étiqueté « H ».
circulaire, étiqueté « H » (⠓). Directement sous ce bouton, vous trouverez
neuf boutons triangulaires, étiquetés de « 1 » (⠼⠁) à « 9 » (⠼⠊), et un bouton carré étiqueté « 0 » (⠼⠊).
bouton carré étiqueté « 0 » (⠼⠚). Dans le coin supérieur droit de la surface
vous trouverez une étiquette « BBT Canute 360 » (⠄⠄⠃⠃⠞ ⠄⠉⠁⠝⠥⠞⠑ ⠼⠉⠋⠚).

Au centre de la surface supérieure, sous la dernière ligne de l'affichage en braille, vous trouverez back (ʙ⠄⠉⠋⠚).
vous trouverez les étiquettes back (⠃⠁⠉⠅), menu (⠍⠢⠥) et forward (⠿⠺⠜⠙).
étiquettes. Sur la face avant de Canute 360, vous trouverez les boutons de navigation arrière, menu
et les boutons de navigation vers l'avant.

Sur le panneau latéral gauche, vous trouverez plusieurs ports périphériques. Sur la face avant, vous trouverez un port HDMI.
l'avant, vous trouverez un port HDMI et une prise de sortie audio de 3,5 mm (la prise audio n'est actuellement pas prise en charge).
n'est pas prise en charge actuellement). L'emplacement pour carte SD se trouve en bas du panneau latéral gauche, au centre.
du panneau latéral gauche, au centre. Vers l'arrière du panneau latéral gauche
vous trouverez deux ports USB standard et un port USB-B.

Sur le côté droit du panneau arrière, vous trouverez le bouton d'allumage/extinction
qui se détache de la surface. Immédiatement à gauche du bouton
à gauche du bouton marche/arrêt se trouve la prise d'alimentation.

Le panneau latéral droit de votre Canute 360 ne comporte aucune commande ni prise.
Canute 360.

Pendant le fonctionnement, votre Canute 360 émettra un tic-tac silencieux. Ce bruit est tout à fait normal.
C'est tout à fait normal.

Votre Canute 360 ne contient aucune pièce réparable par l'utilisateur. Le panneau de base et les plaques latérales ne doivent être ouverts ou retirés que par un technicien qualifié.
Le panneau de base et les plaques latérales ne doivent être ouverts ou retirés que par un technicien
qualifié.

Mise en route
===============

Lorsque vous recevez votre Canute pour la première fois, veuillez le laisser atteindre la température ambiante normale pendant plusieurs heures (ou toute la nuit si possible) avant de le mettre en marche.
température ambiante normale pendant plusieurs heures (ou toute la nuit si possible) avant de l'allumer pour la première fois.
avant de l'allumer pour la première fois. Sauter cette étape n'endommagera pas votre Canute, mais peut entraîner des erreurs sur l'écran lors de la première utilisation.
mais peut entraîner des erreurs d'affichage lors de la première utilisation. Ceci est nécessaire
en raison des conditions souvent très froides pendant le transport.

Placez le Canute 360 sur une surface plane, avec les trois grands boutons de commande
sur le panneau avant, tournés vers vous. Insérez le bloc d'alimentation dans la prise
dans la prise située à droite du panneau arrière de Canute 360, et appuyez brièvement sur le petit bouton marche/arrêt situé juste à côté de la prise.
Appuyez brièvement sur le petit bouton marche/arrêt situé immédiatement à droite de la prise d'alimentation.
pour allumer l'appareil.

Dans des conditions normales, il faut environ 50 secondes à Canute 360 pour s'allumer. Cependant, s'il démarre à froid, il peut prendre environ 4 minutes pour démarrer, car il effectue une routine de préchauffage. Si vous redémarrez une machine qui a été allumée récemment, il se peut qu'elle démarre dans les 20 secondes.

Pendant le démarrage, la ligne supérieure de l'écran se lève et s'abaisse momentanément à plusieurs reprises. Vous entendrez, sentirez et verrez ensuite chacune des neuf lignes de braille qui effaceront tout contenu antérieur, une à la fois. 

Commandes de navigation
===================

Sur le bord avant de Canute 360 se trouvent trois gros boutons de commande. Ceux-ci
sont étiquetés back (⠃⠁⠉⠅), menu (⠍⠢⠥) et forward (⠿⠺⠜⠙) en braille sur la partie supérieure de l'écran de lecture.
Braille sur la partie supérieure de la surface de lecture.

Directement à gauche de chaque ligne en braille se trouve un bouton triangulaire de sélection de ligne
triangulaire. Ces boutons sont étiquetés avec des chiffres de un à neuf en braille ;
correspondant à la ligne adjacente sur l'écran.

Immédiatement au-dessus du premier bouton triangulaire de sélection de ligne se trouve un bouton circulaire d'aide.
circulaire. Elle est étiquetée « H » (⠓) en braille. Vous pouvez appuyer sur ce bouton
Vous pouvez appuyer sur ce bouton à tout moment pour faire apparaître un fichier d'aide pour la page que vous êtes en train de consulter.
en cours de consultation.

Immédiatement sous le dernier bouton triangulaire de sélection de ligne se trouve un bouton carré
carré, étiqueté « 0 » (⠚) en Braille.

Transférer des fichiers sur votre Canute 360
==================================

Votre Canute 360 est compatible avec les formats *.BRF (Braille Ready Format)
et *.PEF (Portable Embosser Format). Pour de meilleurs résultats, nous
recommandons d'utiliser un fichier *.BRF, formaté pour une longueur de page de neuf lignes et une largeur de 40 caractères.
lignes et une largeur de 40 caractères par page.

Traduire des fichiers imprimés en braille
====================================

Un certain nombre de logiciels et de services en ligne permettent de traduire un document imprimé en un fichier braille.
en un fichier braille, qui peut être lu sur votre Canute.

La dernière version de Duxbury DBT™ comprend un préréglage « Canute 360 » pour
pour assurer un formatage correct. Pour utiliser Duxbury DBT™, vous devez acheter
une licence auprès de Duxbury Systems. Vous trouverez de plus amples informations à l'adresse suivante
[https://www.duxburysystems.com](https://www.duxburysystems.com/).

Vous pouvez également utiliser un certain nombre de logiciels gratuits pour traduire des documents imprimés en format BRF.
documents imprimés en format BRF. BrailleBlaster™ est un programme gratuit qui convient pour 
pour préparer des documents à lire sur Canute 360, disponible pour les utilisateurs de Windows, Mac 
et Linux. Vous devez vous assurer que la sortie Braille est réglée pour produire un
avec neuf lignes par page et une longueur de ligne de 40 caractères.
Vous pouvez obtenir plus d'informations et télécharger Braille Blaster à l'adresse suivante
[https://www.brailleblaster.org](https://www.brailleblaster.org/).

RoboBraille™ est un traducteur en ligne gratuit de Sensus au Danemark qui
qui peut traduire un document imprimé ou une page web en un document Braille BRF pour
être lu sur Canute 360. Pour utiliser RoboBraille™, naviguez vers
[www.robobraille.org](http://www.robobraille.org/) dans votre navigateur web préféré.
préféré. Sélectionnez « fichier » dans le menu « Source » pour traduire un fichier sur votre ordinateur.
votre ordinateur. Sélectionnez « URL » pour traduire un site web.

Pour traduire un fichier, cliquez sur « choisir un fichier », et sélectionnez le fichier en utilisant
votre navigateur de fichiers. Cliquez ensuite sur « upload » pour préparer votre fichier à la traduction.
traduction.

Pour traduire une page web, entrez l'URL dans le champ de texte après avoir sélectionné l'option « URL », puis cliquez sur « upload » pour préparer votre fichier à la traduction.
après avoir sélectionné l'option « URL », puis cliquez sur « fetch and upload » pour
pour préparer la page web à la traduction. Veuillez noter que pour travailler
avec robobraille, l'URL d'une page web doit se terminer par un format de fichier (par ex.
[www.example.com/example.htm](http://www.example.com/example.htm)). A
page web se terminant par un domaine de premier niveau (par ex.
[www.example.com](http://www.example.com/)) renverra un message d'erreur.
d'erreur.

Sélectionnez « Braille » dans le menu « select output format ».

Dans le menu « Specify Braille options », vous pouvez sélectionner la langue et le code Braille de votre choix.
langue et le code braille de votre choix. Sélectionnez « Canute 360 » dans le menu
dans le menu « Target Format » pour vous assurer que votre fichier est correctement formaté pour votre Canute 360.
pour votre Canute 360.

Enfin, saisissez votre adresse électronique dans le formulaire « Saisir l'adresse électronique et
soumettre la demande ». Cliquez sur « Soumettre » pour confirmer vos choix. Vous recevrez
recevrez un fichier BRF formaté pour Canute 360 dans votre boîte aux lettres électronique
dans les heures qui suivent.

Bristol Braille Technology ne peut être tenu responsable de l'exactitude ou de la performance
ou de la performance de tout service de traduction en braille d'une tierce partie.

Transfert de fichiers braille vers Canute 360 à l'aide d'une carte SD
========================================================

Pour transférer un fichier sur Canute 360, vous aurez besoin d'une carte SD. Votre Canute
devrait toujours avoir une carte SD, car c'est votre bibliothèque personnelle et c'est là que Canute enregistre sa page actuelle.
où Canute enregistre sa page actuelle et les options du système comme la langue. LES CARTES SD
peuvent être SD ou SDHC, mais pas SDXC ou SDUC. Elles doivent être
formatées en FAT32, mais pas en FAT16, exFAT ou tout autre format.

Copiez et collez le fichier sur la carte SD. N'utilisez pas de dossiers, copiez et collez simplement les fichiers directement sur la carte SD.
copier et coller les fichiers directement sur la carte SD. La prise en charge des dossiers
dans une prochaine mise à jour du micrologiciel.

Réinsérez la carte SD dans la fente prévue à cet effet, au centre de la partie inférieure du panneau latéral gauche.
bas du panneau latéral gauche. Allumez Canute 360, naviguez jusqu'au menu Bibliothèque.
et le fichier apparaîtra dans le menu de la bibliothèque. Les fichiers sont
Les fichiers sont classés dans le menu d'abord par ordre numérique, puis par ordre alphabétique.

Échange à chaud de clés USB et de plusieurs cartes SD
========================================

Vous pouvez également utiliser une clé USB standard pour transférer des fichiers sur Canute
360. Vous aurez toujours besoin d'une carte SD, sinon les signets et la page actuelle ne seront pas sauvegardés.
ne seront pas sauvegardés. Les clés USB ne doivent être utilisées que pour
pour compléter rapidement votre bibliothèque avec de nouveaux fichiers.

Pour ce faire, copiez et collez le fichier sur la clé USB et insérez-la dans un port USB sur le panneau latéral gauche de Canute 360.
dans un port USB sur le panneau latéral gauche de Canute 360.

Vous pouvez également permuter à chaud entre différentes cartes SD et bâtons de mémoire, ce qui est particulièrement utile si plusieurs personnes se trouvent dans la même situation que vous.
est particulièrement utile si plus d'une personne utilise le Canute.

Assurez-vous que la page n'est pas en cours de rafraîchissement avant de procéder à la permutation. Après la permutation
laissez votre Canute pendant environ dix secondes ; il effectuera une réinitialisation complète et reviendra à la dernière page visitée sur ce site.
retournera à la dernière page visitée sur cette carte SD. Rappelez-vous que vos
informations système, les signets et la page actuelle ne sont pas stockés sur le Canute lui-même.
Canute lui-même, ils seront donc échangés avec la carte SD et/ou la clé USB.

Fichiers spéciaux
=============

Sur votre carte SD, vous pouvez remarquer deux types de fichiers appelés
canute.book_name.brf.txt » (où “book_name” est le nom de fichier de votre livre) et “canute_state.brf.txt” (où “book_name” est le nom de fichier de votre livre).
livre) et « canute_state.txt ». Le premier type de fichier se trouve également sur les
mémoire.

Ces fichiers sont utilisés pour stocker des informations sur le système, des signets et des informations sur les pages.
pages. Vous ne devez pas supprimer ces fichiers, sous peine d'effacer les signets qu'ils contiennent.
les signets qu'ils contiennent. Cependant, vous pouvez modifier le fichier
« canute.book_name.brf.txt » dans un éditeur de texte pour PC tel que Notepad++ afin d'ajouter ou de supprimer manuellement des signets.
ajouter ou supprimer des signets manuellement

Choisir un livre dans la bibliothèque
================================

Canute 360 est conçu avec une interface utilisateur en braille facile à utiliser. Pour
sélectionner un fichier à lire sur Canute 360, naviguez jusqu'au menu de la bibliothèque.

Tout d'abord, appuyez sur le grand bouton de menu au centre du bord avant sous la surface de lecture.
sous la surface de lecture. Le menu principal se charge, une ligne de braille à la fois, de haut en bas.
à la fois, de haut en bas. L'affichage de la page entière prend environ 10 secondes.
s'afficher.

Comme pour tous les menus de Canute 360, les options sont sélectionnées à l'aide des boutons triangulaires de sélection de ligne.
boutons triangulaires de sélection de ligne. Pour sélectionner un élément d'un menu, appuyez sur le bouton de sélection de ligne triangulaire situé immédiatement à gauche de cette ligne.
immédiatement à gauche de la ligne en braille.
Braille.

Dans le menu principal, l'option « Voir le menu de la bibliothèque » est affichée
vers le bas de la surface de lecture. Appuyez sur le bouton de sélection de ligne
immédiatement à gauche de l'option de menu pour la sélectionner. Le menu de la bibliothèque
se charge, une ligne de braille à la fois, de haut en bas. Il faut environ 10 secondes pour que la page entière se charge.
La page entière prendra environ 10 secondes à s'afficher.

Le menu de la bibliothèque affiche une liste de tous les fichiers Braille reconnus
présents sur la carte SD ou la clé USB.

Pour sélectionner un fichier, appuyez sur le bouton de sélection de ligne situé à gauche du fichier que vous souhaitez lire.
que vous souhaitez lire.

Si plus de 8 fichiers sont présents sur la carte SD ou la clé USB, le fichier souhaité peut se trouver sur la page suivante.
le fichier souhaité peut se trouver sur la page suivante du menu de la bibliothèque. Appuyez sur le
à droite du centre du panneau avant pour faire avancer le menu de la bibliothèque d'une page.
faire avancer le menu de la bibliothèque d'une page.

Appuyez sur le grand bouton de retour à gauche du centre sur le panneau avant pour
reculer d'une page dans le menu de la bibliothèque.

Une fois qu'un fichier est sélectionné, il se charge une ligne à la fois.
10 secondes pour charger la page entière.

Pour revenir du menu de la bibliothèque au livre que vous étiez en train de lire, appuyez sur
soit sur la grande touche de menu, soit sur la première touche de sélection de ligne.

Deux fichiers sont intégrés au menu de la bibliothèque et apparaissent toujours.
qui apparaîtront toujours. L'un d'entre eux est destiné aux techniciens de maintenance pour le nettoyage et les tests,
et l'autre contient des informations sur la Bristol Braille Technology.

Se déplacer dans un fichier
====================

Pour vous déplacer dans un fichier, utilisez les trois grands boutons de contrôle sur le panneau avant de Canute 360.
sur le panneau avant de Canute 360.

Appuyez sur le grand bouton d'avance à droite du centre sur le panneau avant
pour avancer d'une page dans un fichier.

Appuyez sur le grand bouton de retour à gauche du centre pour reculer d'une page dans un fichier.
dans un fichier, comme si vous tourniez les pages d'un livre.

Dans le cas improbable où vous constateriez des erreurs dans le braille, ou des points mal placés, vous pouvez actualiser le braille.
ou des points mal placés, vous pouvez rafraîchir l'affichage en braille en appuyant sur le carré « 0 » (↪So_284) et en le maintenant enfoncé.
le bouton carré « 0 » (⠚) pendant quelques secondes. L'affichage se réinitialise
se réinitialise au bout d'environ 20 secondes.

Menu principal
=========

À partir du menu principal, vous pouvez insérer un signet à la page actuelle,
revenir à un signet placé précédemment dans un fichier, ou
naviguer vers une page donnée d'un livre.

Pour accéder au menu principal, appuyez sur le grand bouton de menu. Dans n'importe quel menu de
dans n'importe quel menu de Canute 360, appuyez à nouveau sur le bouton menu pour revenir à la dernière page que vous étiez en train de lire.
la dernière page que vous étiez en train de lire.

Comme pour tous les menus de Canute 360, pour sélectionner un élément du menu principal,
appuyez sur le bouton triangulaire de sélection de ligne immédiatement à gauche de l'élément de menu sur la surface de lecture.
sur la surface de lecture.

Pour naviguer vers une page du fichier, appuyez sur le bouton de sélection de ligne
immédiatement à gauche de « aller à la page » sur la surface de lecture.
sur la surface de lecture. Une page d'aide s'affiche sur la surface de lecture. Pour saisir un numéro de page
numéro de page, utilisez les touches triangulaires de sélection de ligne et la touche carrée « zéro » située à gauche de la surface de lecture.
située à gauche de la surface de lecture.

Pour aller à la page 20, par exemple, appuyez sur la touche numéro deux, puis sur la touche zéro.
zéro. Vous entendrez, sentirez et verrez une ligne de braille cliquer de haut en bas pour confirmer la sélection.
et vers le bas pour confirmer la sélection, puis appuyez sur la touche avant pour naviguer vers la page sélectionnée.
pour naviguer jusqu'à la page sélectionnée. Vous pouvez également appuyer sur la touche « retour » pour
annuler votre sélection.

Signets
=========

Pour placer un nouveau signet à la page actuelle d'un fichier ou revenir à un signet placé précédemment dans un fichier, naviguez d'abord jusqu'à la page du livre.
un signet précédemment placé dans un fichier, naviguez d'abord vers le menu du livre en appuyant sur le grand bouton de menu sur le panneau avant de Canute 360.
en appuyant sur le grand bouton de menu sur le panneau avant de Canute 360.

Pour insérer un signet, appuyez sur le bouton de sélection de ligne immédiatement à gauche de « insérer un signet à la page actuelle ».
à gauche de « insérer un signet à la page actuelle ».

Un signet sera inséré à la page actuelle. NB : il n'y a actuellement
n'y a pas de retour d'information lorsqu'un signet est inséré, ceci sera corrigé dans une version ultérieure du logiciel.
Cela sera corrigé dans une version ultérieure du logiciel.

Pour récupérer un signet, appuyez sur le bouton de sélection de ligne situé immédiatement à gauche de l'option « choisir parmi les signets existants ».
à gauche de « choisir un signet existant ».

La liste des signets d'un fichier s'affiche sur la surface de lecture. Utilisez
les boutons triangulaires de sélection de ligne pour sélectionner un signet. Canute 360
affichera alors la page sélectionnée dans le livre.

Dans le menu des signets, il est également possible de sélectionner « début du livre “ ou ” fin du livre » pour naviguer au début ou à la fin d'un livre en utilisant les boutons de sélection de ligne.
pour naviguer vers le début ou la fin d'un livre en utilisant le bouton de sélection de ligne immédiatement à gauche de ces options.
à gauche de ces options.

Changement de langue ou de code braille
=================================

Vous pouvez changer la langue du système ou le code braille (par exemple pour
passer du braille contracté au braille non contracté) à partir du menu système.
du système. Veuillez noter que cette opération ne modifie que la langue et le code dans les menus, le fichier d'aide et les manuels, et non la langue et le code du système.
menus, le fichier d'aide et les manuels, mais pas la langue ou le code des livres sur Canute 360.
Canute 360.

Pour accéder au menu système, accédez d'abord au menu principal en appuyant sur la touche
au centre du bord avant de Canute 360. Ensuite,
appuyez sur le bouton de sélection de ligne immédiatement à gauche de « voir le menu système » pour accéder au menu système. Appuyez sur la touche de sélection de ligne
immédiatement à gauche de « sélectionner la langue et le code » pour accéder au menu de sélection de la langue. Vous pouvez alors sélectionner une langue ou un code braille.
pouvez alors sélectionner une langue ou un code braille à l'aide de la touche de sélection de ligne triangulaire située immédiatement à gauche de votre écran.
triangulaire située immédiatement à gauche de la langue ou du code choisi.
Appuyez une fois sur la touche d'avancement pour confirmer votre sélection.

Sortie du moniteur
==============

Vous pouvez visualiser le texte affiché par Canute en caractères d'imprimerie et en braille à l'aide d'un écran d'ordinateur standard.
écran d'ordinateur standard. Il suffit de brancher votre moniteur sur le port HDMI
situé à l'avant du panneau latéral gauche. La représentation imprimée s'affichera
s'affichera dans une police verte, et le Braille s'affichera en dessous dans une police blanche.
blanche. La représentation imprimée est en Braille ASCII (elle ne rétro-traduit pas le texte ; elle est destinée aux personnes voyantes).
Elle est destinée aux lecteurs Braille voyants).

Canute à l'écran
===================

Canute 360 peut être utilisé comme écran lorsqu'il est branché sur un PC avec le port
port USB-B. Actuellement, BRLTTY et Duxbury DBT prennent en charge cette fonction.
Veuillez consulter la section d'assistance de notre site Web pour utiliser le Canute 360 avec un PC.
Canute 360 avec un PC.

Arrêt
========

Pour éteindre votre Canute 360 en toute sécurité, appuyez sur le bouton d'alimentation situé à droite de la prise d'alimentation à l'arrière de l'appareil.
à droite de la prise d'alimentation sur le panneau arrière. Vous entendrez, verrez et
Vous entendrez, verrez et sentirez chaque ligne de l'écran se réinitialiser, et descendrez dans l'écran.
Après quelques secondes, la ligne supérieure de l'écran se réactualise et affiche
« PLEASE WAIT... » (⠠⠏⠇⠑⠁⠎⠑ ⠺⠁⠊⠞⠲⠲⠲), et les 8 autres lignes descendent.
Après une dizaine de secondes, la première ligne s'abaisse également. Il est maintenant possible
de retirer le bloc d'alimentation de Canute 360.

Entretien de votre Canute 360
==========================

Votre Canute 360 ne contient aucune pièce réparable par l'utilisateur. Le panneau de base et les plaques latérales ne doivent être ouverts ou retirés que par un technicien qualifié.
Le panneau de base et les plaques latérales ne doivent être ouverts ou retirés que par un technicien qualifié.
qualifié.

Vous pouvez nettoyer l'avant et les côtés de votre Canute 360 à l'aide d'un chiffon humide, mais non mouillé, non pelucheux.
humide, mais non mouillé, et non pelucheux. Bristol Braille Technology recommande de nettoyer
votre Canute 360 au moins une fois par mois. Vous ne devez essayer de nettoyer
votre Canute 360 que lorsqu'il est éteint, avec l'alimentation débranchée. N'essayez pas de nettoyer les pièces mécaniques de votre Canute 360.
n'essayez pas de nettoyer vous-même les pièces mécaniques de l'afficheur braille.
vous-même.

Si vous rangez votre Canute 360 dans sa boîte d'expédition, vous pouvez remarquer qu'un résidu blanc poudreux s'accumule sur la face avant et les côtés de l'appareil.
un résidu blanc poudreux s'accumulant sur les panneaux avant et latéraux. Ce phénomène est normal, n'affecte pas le fonctionnement de l'appareil et peut être évité.
normal, n'affecte pas le fonctionnement et peut être enlevé en l'essuyant avec un chiffon humide (mais pas mouillé).
avec un chiffon non pelucheux humide (mais non mouillé).

Mise à jour du logiciel Canute
=============================

De temps à autre, Bristol Braille Technology peut mettre à votre disposition des
des mises à jour logicielles pour Canute 360 afin de résoudre les problèmes, d'améliorer les performances ou d'ajouter des fonctions supplémentaires,
ou d'ajouter des fonctionnalités supplémentaires.

Chaque mise à jour est accompagnée d'instructions d'installation. Pour installer une
Pour installer une mise à jour, vous aurez besoin d'une clé USB vide ou d'une carte SD au format FAT32. LES CARTES SD
peuvent être de type SD ou SDHC, mais pas SDXC ou SDUC. La plupart des cartes SD
La plupart des cartes SD de petite capacité sont au format FAT32. Pour des instructions d'installation complètes
complètes, consultez le document readme fourni avec votre mise à jour.

Informations importantes en matière de sécurité
============================

*ATTENTION : avant d'utiliser ce manuel, veuillez lire et comprendre toutes les informations de sécurité relatives à ce produit.
les informations de sécurité relatives à ce produit.*

Service et assistance
-------------------

Votre Canute 360 ne contient aucune pièce réparable par l'utilisateur. Le panneau de base
Le panneau de base et les plaques latérales ne doivent être ouverts ou retirés que par un technicien qualifié.
qualifié. N'essayez pas de réparer vous-même votre Canute 360
à moins que le centre d'assistance à la clientèle ne vous le demande. Ne faites appel qu'à un
service après-vente agréé par Bristol Braille Technology.

Cordons d'alimentation et adaptateurs électriques
------------------------------

Utilisez uniquement les cordons et adaptateurs d'alimentation fournis par Bristol Braille Technology.
Braille Technology. N'enroulez jamais un cordon d'alimentation autour d'un adaptateur d'alimentation ou d'un autre objet.
objet. Vous risqueriez de soumettre le cordon à des contraintes susceptibles de l'effilocher, de le fissurer ou de le plisser.
s'effilocher, se fissurer ou se plisser. Cela peut présenter un risque pour la sécurité. Acheminez toujours
les cordons d'alimentation de manière à ce qu'on ne puisse pas marcher dessus, trébucher ou être pincé par des objets.
par des objets. Protégez le cordon d'alimentation et les adaptateurs de courant des liquides. Ne pas
n'utilisez pas un adaptateur d'alimentation qui présente de la corrosion au niveau des broches d'entrée CA ou qui montre des signes de surchauffe (comme un plastique déformé).
des signes de surchauffe (plastique déformé, par exemple) au niveau de l'entrée CA ou sur l'adaptateur.
n'importe où sur l'adaptateur d'alimentation.

Chaleur et ventilation du produit
----------------------------

Votre Canute 360 et son adaptateur d'alimentation peuvent générer de la chaleur lorsqu'ils sont utilisés.
C'est tout à fait normal. Votre Canute 360 est un appareil de bureau et doit toujours être placé sur un bureau ou une table.
toujours être placé sur un bureau ou une table. Vous ne devez jamais placer votre Canute
360 sur un lit, un canapé, un tapis ou toute autre surface souple. Pour éviter tout risque de
risque de blessure, vous ne devez jamais laisser votre Canute 360 en contact avec vos genoux ou toute autre partie de votre corps pendant le fonctionnement de l'appareil.
les genoux ou toute autre partie du corps pendant l'utilisation. Vous devez toujours éteindre
votre Canute 360 et débrancher l'adaptateur d'alimentation lorsqu'il n'est pas utilisé.

Environnement d'utilisation
---------------------

Votre Canute 360 est conçu pour fonctionner au mieux à température ambiante, entre
10°C et 25°C (50°F-80°F) avec une humidité comprise entre 35% et 80%. Si votre
votre Canute 360 est stocké ou transporté à des températures inférieures à 10°C
(50°F), vous devez laisser la température de votre Canute 360 augmenter lentement jusqu'à
lentement à une température comprise entre 10°C et 25°C (50°F-80°F) avant de l'utiliser. Ce processus
peut prendre jusqu'à deux heures.

Si vous ne laissez pas votre appareil atteindre une température de fonctionnement optimale
avant de l'utiliser, vous risquez d'entraver son fonctionnement ou d'endommager votre Canute
360.

Ne placez pas de boissons sur ou à côté de votre Canute 360. Si un
Canute 360, un court-circuit ou d'autres dommages pourraient se produire.
de court-circuit ou d'autres dommages. Ne mangez pas et ne fumez pas au-dessus de votre Canute. Toute particule tombant
dans votre Canute 360 peuvent endommager le mécanisme.

Surchauffe
-----------

Le Canute dispose d'une gestion thermique interne. S'il dépasse une certaine
température, jugée sous-optimale pour le mécanisme (approximativement la même
l'écran est chaud au toucher), il interrompt toute opération jusqu'à ce que la température baisse.
jusqu'à ce que la température redescende. Il suffit de le laisser refroidir pour qu'il redémarre là où il s'était arrêté.
Il suffit de le laisser refroidir pour qu'il redémarre de lui-même, là où il s'est arrêté.

Entrée électrique
----------------

Votre Canute 360 est conçu pour fonctionner dans les conditions électriques suivantes
électriques suivantes :

Tension d'entrée : minimum 100V(AC) maximum 240V(AC), fréquence d'entrée
entre 50Hz et 60Hz

Bruits pendant le démarrage et le fonctionnement
------------------------------------

Lorsqu'il est allumé pour la première fois, Canute 360 émet un bruit plus fort que d'habitude car il effectue une réinitialisation de l'écran.
Il effectue une réinitialisation de l'affichage. Il effectuera cette opération deux fois avant de démarrer, ce qui prendra environ une minute au total.
avant de démarrer, ce qui prend environ une minute au total. Cela peut prendre jusqu'à cinq minutes si votre Canute démarre à froid.

Une fois allumé et utilisé, le Canute 360 émet un bruit plus doux de « réglage de ligne » pour chaque ligne affichée sur l'écran.
pour chaque ligne de l'écran qui est changée.

Lorsque votre Canute 360 reste allumé et qu'il n'est pas utilisé, il peut émettre un léger bruit de tic-tac, en fonction de la taille de l'écran.
tic-tac », en fonction de la version du firmware que vous utilisez.

Support 
=======

Pour obtenir une assistance technique, contactez en premier lieu votre distributeur.

Pour signaler un bogue ou contacter l'équipe, veuillez envoyer un courrier électronique à l'adresse suivante
[support@bristolbraille.](mailto:support@bristolbraille.co.uk)org.

Pour plus d'informations sur l'assistance, veuillez consulter
https://[www.bristolbraille.org/support](http://www.bristolbraille.co.uk/support)

Les nombreuses utilisations du Canute 360
===============================

Le braille est synonyme d'alphabétisation, d'emploi et d'indépendance.
l'utilisation de base de Canute 360, les utilisateurs trouveront de nombreuses
Les utilisateurs qui se sont familiarisés avec l'utilisation de base de Canute 360 trouveront de nombreuses possibilités supplémentaires au-delà de la simple lecture de livres.

- Économiser du papier : une économie évidente est réalisée sur les copies papier encombrantes en braille.

- Tableaux : ils deviennent beaucoup plus lisibles lorsqu'il est possible d'afficher jusqu'à 9 lignes.
jusqu'à 9 lignes.

- Mathématiques : contiennent souvent des équations longues et complexes qui s'étendent sur plusieurs lignes.
sur plusieurs lignes.

- Musique : les partitions sont invariablement mieux comprises lorsque plusieurs lignes peuvent être lues.
lignes.

- Apprentissage : Les cours en braille peuvent facilement être adaptés à l'affichage sur Canute.
sur Canute.

- Langues : l'affichage de différentes langues dans un même document est à la fois possible et pratique.
possible et pratique.

Notes sur les versions et les droits d'auteur
===============================

Le manuel principal est stocké sur le site web. Tous les droits d'auteur appartiennent à
Bristol Braille Technology CIC et Techno-Vision Systems.

Dernière modification 10^th^ of January, 2025
