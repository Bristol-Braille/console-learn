% Canute 360 handleiding
% Bristol Braille Technologie
% Laatste bijwerking: 2025-01-10

Opmerkingen
=====

Vertaalfout: Alle niet-Engelse vertalingen zijn vertalingen uit het Engels met DeepL Pro. Als u vertaalfouten opmerkt, laat het ons dan weten.

Contact: enquiries@bristolbraille.org

Welkom
=======

Bedankt voor uw aankoop van Canute 360 van Bristol Braille Technology, de
eerste multi-line braille e-reader ter wereld.

Canute 360 heeft een scherm met negen regels, waarbij elke regel bestaat uit 40
cellen braille. Canute 360 is compatibel met de bestandsformaten *.BRF en *.PEF
formaten en kan brailletekst, wiskunde, muziek of elke andere
zes-dots braillecode.

Met Canute 360 kunt u bestanden in braille lezen, invoegen, bewerken en naar bladwijzers navigeren,
en navigeren naar bladwijzers.

Bristol Braille Technology is een non-profit
bedrijf. Uw aankoop stelt ons in staat om te blijven investeren in en
nieuwe brailleproducten te ontwikkelen die de blinde gemeenschap ten goede komen.

In de doos
==============

In de verzenddoos vind je het volgende:

- 1 × Canute 360 Braille e-reader

- 1 × 19V voeding met snoer

- 1 × Braille snelstartgids

Laat het uw distributeur weten als er iets ontbreekt of beschadigd is.
beschadigd zijn.

Documentconventies
====================

Wanneer in deze handleiding wordt verwezen naar tekst die door Canute 360 in braille wordt weergegeven, staat deze tussen aanhalingstekens.
wordt verwezen, tussen aanhalingstekens. Wanneer braille op het oppervlak van het apparaat is aangebracht, wordt dit hier als braille weergegeven, bijvoorbeeld de menuknop als (⠍⠑⠝⠥).

**Voordat u deze informatie en het product dat wordt ondersteund gebruikt, moet u eerst
de belangrijke veiligheidsinformatie aan het einde van dit document te lezen en te begrijpen.
document.**

Fysieke beschrijving
====================

Uw Canute 360 moet plat voor u op tafel worden gelegd. De
drie grote bedieningsknoppen op de voorkant van het apparaat (gelabeld
back (⠃⠁⠉⠅), menu (⠍⠢⠥) en forward (⠿⠺⠜⠙) moeten naar u toe zijn gericht.
naar u toe wijzen.

De Canute 360 heeft een brailleleesregel met negen regels en 360 cellen op de bovenkant.
oppervlak.

In de linkerbovenhoek van de bovenkant vindt u een ronde
knop met het label “H” (⠓). Direct onder deze knop vindt u
negen driehoekige knoppen met het label “1” (⠼⠁) tot en met “9” (⠼⠊) en één
vierkante knop met het label “0” (⠼⠚). In de rechterbovenhoek van het bovenste
oppervlak vind je een label “BBT Canute 360” (⠄⠄⠃⠞ ⠄⠉⠁⠝⠥⠞⠑ ⠼⠉⠋⠚).

In het midden van het bovenvlak, onder de laatste regel van de brailleleesregel
vindt u de labels Terug (⠃⠁⠉⠅), Menu (⠍⠢⠥) en Vooruit (⠿⠺⠜⠙).
labels. Op de voorkant van de Canute 360 vindt u de navigatieknoppen terug, menu
en voorwaartse navigatieknoppen.

Aan de linkerkant vind je verschillende poorten voor randapparatuur. Aan de
voorkant vindt u een HDMI-poort en een 3,5mm audio-uitgang (de audio
aansluiting wordt momenteel niet ondersteund). De SD-kaartsleuf bevindt zich onderaan
het linker zijpaneel, in het midden. Aan de achterkant van het
paneel vind je twee standaard USB-poorten en een USB-B-poort.

Aan de rechterkant van het achterpaneel vind je de aan/uit-knop, die boven het oppervlak uitsteekt.
knop, die boven het oppervlak uitsteekt. Direct links van de
aan/uit-knop zit het stopcontact.

Er zijn geen knoppen of stopcontacten aan de rechterkant van uw
Canute 360.

Tijdens het gebruik maakt uw Canute 360 een zacht tikkend geluid. Dit
is volkomen normaal.

Uw Canute 360 bevat geen onderdelen die door de gebruiker kunnen worden gerepareerd. Het basis
paneel en zijplaten mogen alleen worden geopend of verwijderd door een gekwalificeerde onderhoudsmonteur.
servicemonteur.

Aan de slag
===============

Als uw Canute arriveert, laat hem dan eerst enkele uren (of indien mogelijk de hele nacht) op kamertemperatuur komen voordat u hem aanzet.
normale kamertemperatuur (indien mogelijk de hele nacht) voordat u hem voor het eerst
aan te zetten. Als u deze stap overslaat, beschadigt u uw Canute niet
maar kan resulteren in fouten op het display bij het eerste gebruik. Dit is noodzakelijk
vanwege de vaak erg koude omstandigheden tijdens het transport.

Plaats de Canute 360 op een vlakke ondergrond, met de drie grote bedieningsknoppen
op het voorpaneel naar u toe zijn gericht. Steek de voeding in de
aansluiting aan de rechterkant van het achterpaneel van de Canute 360 en druk kort op de kleine aan/uit-knop.
druk kort op de kleine aan/uit-knop rechts van het stopcontact om het apparaat aan te zetten.
stopcontact om het apparaat aan te zetten.

Onder normale omstandigheden heeft de Canute 360 ongeveer 50 seconden nodig om aan te gaan. Als het apparaat echter koud wordt opgestart, kan het ongeveer 4 minuten duren om op te starten omdat het een opwarmroutine uitvoert. Als u een machine herstart die kort geleden aan stond, kan het binnen 20 seconden starten.

Tijdens het opstarten gaat de bovenste regel van het display een paar keer omhoog en omlaag. U hoort, voelt en ziet dan elk van de negen regels braille die de vorige inhoud één voor één wissen. 

Navigatieknoppen
===================

Aan de voorkant van de Canute 360 zitten drie grote bedieningsknoppen. Deze
zijn terug (⠃⠁⠉⠅), menu (⠍⠢⠥) en vooruit (⠿⠺⠜⠙) in braille aangegeven.
Braille aan de bovenkant van het leesvlak.

Direct links van elke regel braille staat een driehoekige regelkeuzeknop.
knop. Deze zijn gelabeld met nummers één tot en met negen in Braille;
corresponderend met de aangrenzende regel op het scherm.

Direct boven de eerste driehoekige lijnselectieknop zit een ronde helpknop.
helpknop. Deze is gelabeld met “H” (⠓) in Braille. Je kunt deze knop
knop drukken om een helpbestand te openen voor de pagina die je op dat moment bekijkt.
die je momenteel bekijkt.

Direct onder de laatste driehoekige regelkeuzetoets zit een vierkante
knop met het label “0” (⠚) in braille.

Bestanden op de Canute 360 zetten
==================================

Uw Canute 360 is compatibel met zowel *.BRF (Braille Ready Format)
en *.PEF (Portable Embosser Format) bestanden. Voor de beste resultaten
raden wij aan een *.BRF bestand te gebruiken, geformatteerd voor een paginalengte van negen
regels en een breedte van 40 tekens per pagina.

Gedrukte bestanden omzetten in braille
====================================

Een aantal softwarepakketten en online diensten kunnen een print
document vertalen naar een braillebestand, dat op de Canute kan worden gelezen.

De nieuwste versie van Duxbury DBT™ bevat een “Canute 360” preset om
voor een correcte opmaak. Om Duxbury DBT™ te kunnen gebruiken moet u
licentie kopen bij Duxbury systems. Meer informatie vindt u op
[https://www.duxburysystems.com](https://www.duxburysystems.com/).

U kunt ook een aantal gratis softwarepakketten gebruiken om afgedrukte
documenten te vertalen naar BRF-formaat. BrailleBlaster™ is een gratis programma dat geschikt is voor 
documenten voor te bereiden om te lezen op Canute 360, beschikbaar voor Windows, Mac 
en Linux-gebruikers. U dient ervoor te zorgen dat de braille-uitvoer is ingesteld op het produceren van een
document te produceren met negen regels per pagina, met een regellengte van 40 tekens.
Je kunt meer informatie vinden en Braille Blaster downloaden van
[https://www.brailleblaster.org](https://www.brailleblaster.org/).

RoboBraille™ is een gratis online vertaler van Sensus in Denemarken die
een printdocument of webpagina kan vertalen naar een BRF brailledocument om te
te lezen op Canute 360. Om RoboBraille™ te gebruiken, gaat u naar
[www.robobraille.org](http://www.robobraille.org/) in de webbrowser van uw voorkeur.
browser. Selecteer “bestand” in het menu “Bron” om een bestand op
uw computer. Selecteer “URL” om een website te vertalen.

Om een bestand te vertalen, klik op “bestand kiezen” en selecteer het bestand met behulp van
met behulp van uw bestandsbrowser. Klik dan op “uploaden” om je bestand voor te bereiden voor
vertalen.

Om een webpagina te vertalen, voert u de URL in het tekstveld in na
na het selecteren van de optie “URL”, klik dan op “ophalen en uploaden” om
de webpagina voor te bereiden op vertaling. Houd er rekening mee dat om te werken
met robobraille een webpagina URL moet eindigen op een bestandsindeling (bijv.
[www.example.com/example.htm](http://www.example.com/example.htm)). A
webpagina die eindigt op een topleveldomein (bijv.
[www.example.com](http://www.example.com/)) geeft een foutmelding.
bericht.

Selecteer “Braille” in het menu “Uitvoerformaat selecteren”.

In het menu “Braille-opties opgeven” kun je de gewenste taal en braillecode selecteren.
taal en braillecode selecteren. Selecteer “Canute 360” in het
“Doelformaat” om ervoor te zorgen dat uw bestand correct geformatteerd is voor
Canute 360.

Voer tenslotte uw e-mailadres in het formulier “E-mailadres invoeren en
verzoek indienen”. Klik op “Verzenden” om uw keuzes te bevestigen. U
ontvangt een BRF-bestand geformatteerd voor Canute 360 in uw e-mail inbox
binnen enkele uren.

Bristol Braille Technology kan geen verantwoordelijkheid aanvaarden voor de nauwkeurigheid
of prestaties van braillevertaalservices van derden.

Braillebestanden overbrengen naar Canute 360 met een SD-kaart
========================================================

Om een bestand op de Canute 360 te krijgen, hebt u een SD-kaart nodig. Uw Canute
moet altijd een SD-kaart bevatten omdat dit uw persoonlijke bibliotheek is en
waar Canute de huidige pagina en systeemopties zoals taal opslaat. SD
kaarten kunnen SD of SDHC zijn, maar geen SDXC of SDUC. Ze moeten
geformatteerd zijn met FAT32, maar niet met FAT16, exFAT of een andere indeling.

Kopieer en plak het bestand op de SD-kaart. Gebruik geen mappen, maar
kopieer en plak bestanden rechtstreeks op de SD-kaart. Ondersteuning voor mappen
komt in een toekomstige firmware-upgrade.

Plaats de SD-kaart terug in de SD-kaartsleuf in het midden van de
onderkant van het linker zijpaneel. Zet de Canute 360 aan, navigeer naar het
bibliotheekmenu en het bestand verschijnt in het bibliotheekmenu. Bestanden worden
eerst numeriek en daarna alfabetisch gerangschikt.

Hot swapping USB-sticks en meerdere SD's
========================================

U kunt ook een standaard USB-stick gebruiken om bestanden op de Canute
360. U hebt nog steeds een SD-kaart nodig, anders worden bladwijzers en de huidige pagina niet opgeslagen.
niet worden opgeslagen. Geheugensticks moeten alleen worden gebruikt om snel
uw bibliotheek aan te vullen met nieuwe bestanden.

Om dit te doen kopieert en plakt u het bestand op de geheugenstick en steekt u het
in een USB-poort aan de linkerkant van de Canute 360.

U kunt ook hot-swappen tussen verschillende SD-kaarten en geheugensticks.
wat vooral handig is als meer dan één persoon de Canute gebruikt.

Zorg ervoor dat de pagina niet wordt vernieuwd voordat u verwisselt. Nadat u hebt verwisseld
laat u de Canute ongeveer tien seconden met rust; hij wordt dan volledig gereset en
terugkeren naar de laatst bezochte pagina op die SD-kaart. Vergeet niet dat uw
systeeminformatie, bladwijzers en huidige pagina niet zijn opgeslagen op de
Canute zelf, dus worden verwisseld met de SD-kaart en/of geheugenstick.

Speciale bestanden
=============

Op uw SD-kaart ziet u mogelijk twee soorten bestanden genaamd
“canute.book_name.brf.txt” (waarbij ‘book_name’ de bestandsnaam is van uw
boek is) en “canute_state.txt”. De eerste is ook te vinden op geheugen
sticks.

Deze bestanden worden gebruikt voor het opslaan van systeeminformatie, bladwijzers en pagina
informatie. U mag deze bestanden niet verwijderen, anders verwijdert u alle
bladwijzers die erin zijn opgeslagen. U kunt echter wel het
“canute.book_name.brf.txt” bewerken in een PC teksteditor zoals Notepad++ om
bladwijzers handmatig toe te voegen of te verwijderen

Een boek uit de bibliotheek kiezen
================================

Canute 360 is ontworpen met een gebruiksvriendelijke braille-gebruikersinterface. Om
een bestand te selecteren om op de Canute 360 te lezen, navigeert u naar het bibliotheekmenu.

Druk eerst op de grote menuknop in het midden van de voorkant onder het leesoppervlak.
het leesvlak. Het hoofdmenu wordt geladen, één regel braille per keer, van boven naar beneden.
van boven naar beneden. Het duurt ongeveer 10 seconden om de hele pagina
weergeven.

Zoals bij alle menu's op de Canute 360, worden opties geselecteerd met de
driehoekige lijnselectieknoppen. Om een item in een menu te selecteren, drukt u op de
driehoekige lijnselectieknop direct links van die regel in Braille.
braille.

In het hoofdmenu wordt 'bekijk bibliotheekmenu' weergegeven
onderaan het leesvlak. Druk op de lijnselectieknop
direct links van de menuoptie om deze te selecteren. Het
menu wordt geladen, één regel braille per keer, van boven naar beneden. De
hele pagina duurt ongeveer 10 seconden.

Het bibliotheekmenu toont een lijst van alle herkende braillebestanden
op de SD-kaart of USB-stick.

Om een bestand te selecteren, druk je op de lijnselectieknop links van het bestand
dat je wilt lezen.

Als er meer dan 8 bestanden op de SD-kaart of USB-stick staan, kan het gewenste bestand op de volgende pagina staan.
het gewenste bestand mogelijk op de volgende pagina van het bibliotheekmenu. Druk op de
grote voorwaartse knop rechts van het midden op het voorpaneel om
het bibliotheekmenu één pagina vooruit te gaan.

Druk op de grote knop terug links van het midden op het voorpaneel om
om een pagina terug te gaan in het bibliotheekmenu.

Zodra een bestand is geselecteerd, wordt het één regel per keer geladen.
Het duurt ongeveer 10 seconden om de hele pagina te laden.

Om vanuit het bibliotheekmenu terug te gaan naar het boek dat je aan het lezen was, druk je op
op de grote menuknop of op regelkeuzeknop één.

Er zijn twee bestanden ingebouwd in het bibliotheekmenu die altijd
verschijnen. Het ene is voor onderhoudsmonteurs om te gebruiken tijdens het schoonmaken en testen,
en het andere bevat informatie over Bristol Braille Technology.

Binnen een bestand bewegen
====================

Gebruik de drie grote bedieningsknoppen op het voorpaneel van de Canute 360 om binnen een bestand te bewegen.
paneel van de Canute 360.

Druk op de grote voorwaartse knop rechts van het midden op het voorpaneel
om één pagina vooruit te gaan in een bestand.

Druk op de grote terugknop links van het midden om één pagina terug te gaan
in een bestand, vergelijkbaar met het omslaan van de pagina's in een fysiek boek.

In het onwaarschijnlijke geval dat je fouten in de Braille ziet, of puntjes die niet op hun plaats staan, kun je de Braille vernieuwen.
of puntjes die niet op hun plaats staan, kunt u de brailleleesregel vernieuwen door
de vierkante “0” (⠚) knop een paar seconden ingedrukt te houden. De
na ongeveer 20 seconden gereset.

Hoofdmenu
=========

Vanuit het hoofdmenu kunt u een bladwijzer invoegen op de huidige pagina,
terugkeren naar een bladwijzer die eerder in een bestand was geplaatst, of
naar een bepaalde pagina in een boek navigeren.

Druk op de grote menuknop om naar het hoofdmenu te navigeren. In elk
menu op de Canute 360, kunt u door nogmaals op de menuknop te drukken terugkeren naar
naar de laatste pagina die u aan het lezen was.

Zoals bij alle menu's op de Canute 360, drukt u op de driehoekige lijnselectieknop om een item in het hoofdmenu te selecteren,
op de driehoekige lijnselectieknop direct links van het menu-item op het leesoppervlak.
menu-item op het leesoppervlak.

Om naar een pagina in het bestand te navigeren, drukt u op de lijnselectieknop
direct links van 'ga naar pagina' op het leesoppervlak.
leesoppervlak. Er verschijnt een helppagina op het leesoppervlak. Om een paginanummer in te voeren
nummer in te voeren, gebruik je de driehoekige lijnselectietoetsen en de vierkante nulknop
links van het leesvlak.

Om bijvoorbeeld naar pagina 20 te gaan, druk je op knop nummer twee, gevolgd door de nultoets.
nultoets. Je hoort, voelt en ziet één regel braille omhoog en omlaag klikken om de selectie te bevestigen.
en omlaag klikken om de selectie te bevestigen.
naar de geselecteerde pagina te navigeren. Je kunt ook op de terug-toets drukken om
om uw selectie ongedaan te maken.

Bladwijzers
=========

Om een nieuwe bladwijzer op de huidige pagina in een bestand te plaatsen of terug te keren naar een
bladwijzer die u eerder in een bestand hebt geplaatst, navigeert u eerst naar het in-book
menu door op de grote menuknop op het voorpaneel van de Canute 360 te drukken.

Om een bladwijzer in te voegen, drukt u op de lijnselectieknop direct links van 'bladwijzer invoegen op huidige pagina'.
links van 'voeg bladwijzer toe aan huidige pagina'.

Er wordt een bladwijzer op de huidige pagina ingevoegd. NB er is momenteel
geen feedback wanneer een bladwijzer is ingevoegd.
softwareversie.

Om een bladwijzer op te halen, druk je op de lijnselectieknop direct links van 'kies uit bestaande bladwijzer'.
links van 'kies uit bestaande bladwijzer'.

Een lijst met bladwijzers in een bestand wordt weergegeven op het leesoppervlak. Gebruik
de driehoekige lijnselectieknoppen om een bladwijzer te selecteren. Canute 360
dan de geselecteerde pagina in het boek weergeven.

Vanuit het bladwijzermenu is het ook mogelijk om 'begin van boek' of 'einde van boek' te selecteren om naar het
om naar het begin of einde van een boek te navigeren.
links van deze opties.

Taal of braillecode wijzigen
=================================

U kunt de systeemtaal of braillecode wijzigen (bijvoorbeeld om
schakelen tussen gecontracteerde en ongecontracteerde braille) vanuit het systeemmenu.
menu. Let op: dit verandert alleen de taal en code in de
menu's, het helpbestand en de handleidingen; niet de taal of code in boeken op de Canute 360.
Canute 360.

Om het systeemmenu te openen, gaat u eerst naar het hoofdmenu door op de
grote menuknop in het midden van de voorkant van de Canute 360 te drukken. Vervolgens,
drukt u op de lijnselectieknop direct links van 'Systeemmenu weergeven' om het systeemmenu te openen. Druk op de lijnselectieknop
selectieknop direct links van 'selecteer taal en code' om het taalkeuzemenu te openen. U
kunt dan een taal of braillecode selecteren met de driehoekige lijnselectieknop
driehoekige lijnselectieknop direct links van de gekozen taal of code.
Druk eenmaal op de vooruit knop om je selectie te bevestigen.

Monitorweergave
==============

U kunt de tekst die Canute weergeeft zowel in gedrukte vorm als in braille bekijken met een
standaard computermonitor. Sluit uw monitor aan op de HDMI-poort
aan de voorkant van het linker zijpaneel. De gedrukte weergave
weergegeven in een groen lettertype en de braille wordt daaronder weergegeven in een wit
wit lettertype. De afdruk is in Braille ASCII (het vertaalt de tekst niet terug; het is bedoeld voor ziende Braillegebruikers).
vertaling van de tekst; het is bedoeld voor ziende braillelezers).

Canute als weergave
===================

Canute 360 kan als beeldscherm worden gebruikt als het op een PC is aangesloten met de
USB-B poort. Momenteel wordt dit ondersteund door BRLTTY en Duxbury DBT.
Zie de ondersteuningssectie op onze sectie op de website voor het gebruik van
de Canute 360 met een PC.

Afsluiten
========

Om uw Canute 360 veilig uit te schakelen, drukt u op de aan/uit-knop rechts van het stopcontact op de achterkant.
rechts van het stopcontact op het achterpaneel. U hoort, ziet en
en voelen dat elke regel van het display gereset wordt.
Na een paar seconden zal de bovenste regel van het display vernieuwen en het volgende tonen
“EVEN GEDULD...” (⠠⠏⠇⠑⠁⠎⠑ ⠺⠁⠊⠞⠲⠲⠲) en zakken de andere 8 regels naar beneden.
Na ongeveer tien seconden zakt ook de eerste regel. Het is nu veilig
om de voeding van de Canute 360 te verwijderen.

Uw Canute 360 onderhouden
==========================

Uw Canute 360 bevat geen onderdelen die door de gebruiker kunnen worden gerepareerd. Het basis
paneel en zijplaten mogen alleen worden geopend of verwijderd door een gekwalificeerde
servicemonteur.

U kunt de voor- en zijkanten van uw Canute 360 schoonmaken met een vochtige, maar
niet nat, met een pluisvrije doek. Bristol Braille Technology raadt aan om
de Canute 360 minstens één keer per maand schoon te maken. Probeer uw Canute 360 alleen schoon te maken
Probeer uw Canute 360 alleen schoon te maken als deze is uitgeschakeld en de stroom is uitgeschakeld. Probeer
mechanische onderdelen van de brailleleesregel niet zelf schoon te maken.
zelf schoon te maken.

Als u de Canute 360 in de verzenddoos bewaart, kunt u merken dat er een
poederachtig wit residu op de voor- en zijpanelen. Dit is
normaal, heeft geen invloed op de werking en kan verwijderd worden door het af te vegen met een
vochtige (maar niet natte) pluisvrije doek.

Uw Canute-software bijwerken
=============================

Van tijd tot tijd kan Bristol Braille Technology de volgende software-updates beschikbaar stellen
software-updates voor Canute 360 beschikbaar stellen om problemen op te lossen, de prestaties te verbeteren
of extra functies toe te voegen.

Elke update wordt geleverd met installatie-instructies. Om een
update te installeren, hebt u een lege USB-stick of een SD-kaart in FAT32-formaat nodig. SD
SD-kaarten kunnen SD of SDHC zijn, maar geen SDXC of SDUC. De meeste SD-kaarten met
SD-kaarten hebben meestal het FAT32-formaat. Voor volledige installatie
instructies, raadpleeg het leesmij-document dat bij je update is geleverd.

Belangrijke veiligheidsinformatie
============================

*LET OP: lees en begrijp alle veiligheidsinformatie voor dit product voordat u deze handleiding gebruikt.
gerelateerde veiligheidsinformatie voor dit product.*

Service en ondersteuning
-------------------

Uw Canute 360 bevat geen onderdelen die door de gebruiker kunnen worden gerepareerd. Het
paneel en zijplaten mogen alleen worden geopend of verwijderd door een gekwalificeerde onderhoudsmonteur.
servicemonteur. Probeer uw Canute 360 niet zelf te repareren
tenzij dit wordt opgedragen door de klantenservice. Gebruik alleen een
servicetechnicus die is goedgekeurd door Bristol Braille Technology.

Netsnoeren en stroomadapters
------------------------------

Gebruik alleen de netsnoeren en voedingsadapters die door Bristol Braille
Technologie. Wikkel een netsnoer nooit om een netadapter of ander
voorwerp. Dit kan het snoer zodanig belasten dat het snoer gaat
rafelen, barsten of krimpen. Dit kan een veiligheidsrisico vormen. Leid
snoeren altijd zo dat er niet over gelopen wordt, over gestruikeld kan worden of dat ze bekneld raken
door voorwerpen. Bescherm het netsnoer en de voedingsadapters tegen vloeistoffen. Gebruik geen
voedingsadapters die corrosie vertonen bij de AC-ingangspinnen of die
tekenen van oververhitting vertoont (zoals vervormd plastic) bij de AC-ingang of
op de adapter.

Hitte en productventilatie
----------------------------

Uw Canute 360 en de voedingsadapter kunnen warmte produceren tijdens het gebruik.
Dit is volkomen normaal. Uw Canute 360 is een desktopapparaat en moet
altijd op een bureau of tafel worden geplaatst. Plaats uw Canute
360 nooit op een bed, bank, tapijt of ander flexibel oppervlak. Om het risico op
letsel te voorkomen, mag u de Canute 360 nooit in contact laten komen met uw
schoot of andere lichaamsdelen tijdens het gebruik. Zet uw Canute 360 altijd uit
de Canute 360 uitzetten en de adapter loskoppelen als het apparaat niet wordt gebruikt.

Gebruiksomgeving
---------------------

Uw Canute 360 is ontworpen om het beste te werken bij kamertemperatuur, tussen
10°C en 25°C (50°F-80°F) met een luchtvochtigheid tussen 35% en 80%. Als
uw Canute 360 wordt opgeslagen of vervoerd bij temperaturen lager dan 10°C
(50°F), moet u de temperatuur van uw Canute 360 langzaam laten stijgen
langzaam laten stijgen tot tussen 10°C en 25°C (50°F-80°F) voor gebruik. Dit proces
kan tot twee uur duren.

Als u het apparaat niet op een optimale bedrijfstemperatuur laat komen
kan leiden tot een verminderde werking of beschadiging van uw Canute
360.

Plaats geen dranken op of naast uw Canute 360. Als
vloeistof op de Canute 360 wordt gemorst, kan kortsluiting of andere schade ontstaan.
ontstaan. Eet of rook niet boven uw Canute. Deeltjes die
in uw Canute 360 kunnen het mechanisme beschadigen.

Oververhitting
-----------

De Canute heeft een intern thermisch beheer. Als het boven een bepaalde
temperatuur komt, die niet optimaal wordt geacht voor het mechanisme (ongeveer hetzelfde
hetzelfde als wanneer het scherm heet aanvoelt), zal het alle werking pauzeren totdat de temperatuur daalt.
totdat de temperatuur daalt. Laat het gewoon afkoelen en
en hij start vanzelf weer op waar hij gebleven was.

Elektrische ingang
----------------

Uw Canute 360 is ontworpen om te werken onder de volgende elektrische
omstandigheden:

Ingangsspanning: minimaal 100V(AC) maximaal 240V(AC), ingangsfrequentie
tussen 50Hz en 60Hz

Geluiden tijdens opstarten en gebruik
------------------------------------

Als de Canute 360 voor het eerst wordt aangezet, maakt deze een harder geluid dan normaal als
het display wordt gereset. Dit zal twee keer gebeuren voordat
opstarten, wat in totaal ongeveer een minuut duurt. Dit kan wel vijf minuten duren als uw Canute koud opstart.

Eenmaal ingeschakeld en in gebruik zal de Canute 360 een zachter 'lijninstelling'-geluid maken voor elke regel op het display die op het display verschijnt.
geluid maken voor elke regel op het display die gewijzigd wordt.

Als de Canute 360 aanstaat en niet gebruikt wordt, kan hij een zacht 'tikkend' geluid maken.
een zacht 'tikkend' geluid maken, afhankelijk van de firmwareversie die u gebruikt.

Ondersteuning 
=======

Neem voor technische ondersteuning in eerste instantie contact op met uw distributeur.

Om een bug te melden of contact op te nemen met het team, stuurt u een e-mail naar
[support@bristolbraille.](mailto:support@bristolbraille.co.uk)org.

Ga voor meer ondersteuningsinformatie naar
https://[www.bristolbraille.org/support](http://www.bristolbraille.co.uk/support)

De vele toepassingen van de Canute 360
===============================

Braille is geletterdheid, werk en onafhankelijkheid.
vertrouwd geraakt met het basisgebruik van de Canute 360, zullen gebruikers veel
extra mogelijkheden dan alleen het lezen van boeken.

- Papier besparen: een voor de hand liggende besparing is die van lijvige, papieren brailleschriften.

- Tabellen: worden veel leesbaarder wanneer tot 9 regels kunnen worden
kunnen worden weergegeven.

- Wiskunde: bevat vaak lange en complexe vergelijkingen die
meerdere regels beslaan.

- Muziek: partituren worden altijd het best begrepen als er meerdere regels kunnen worden gelezen.
kunnen worden gelezen.

- Leren: Braillelessen kunnen eenvoudig worden aangepast voor weergave op Canute.
op Canute.

- Talen: verschillende talen weergeven in hetzelfde document is
zowel mogelijk als praktisch.

Opmerkingen over versies en copyright
===============================

De belangrijkste handleiding is opgeslagen op de website. Alle auteursrechten behoren toe aan
Bristol Braille Technology CIC en Techno-Vision Systems.

Laatst bewerkt 10^th^ januari, 2025
