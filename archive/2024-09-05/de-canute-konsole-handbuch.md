% Canute-Konsole Handbuch
% Bristol Braille Technologie
% Letzte Aktualisierung: 2024-09-05

# Hinweise

Caveat lector: Dies ist ein aktuelles Dokument, das aktualisiert wird, sobald weitere Anwendungsfälle für die Canute-Konsole auftauchen. Bitte machen Sie Vorschläge zur Aufnahme von Tipps, um die Anweisungen klarer zu gestalten, und melden Sie Ungenauigkeiten.

Hinweis zur Übersetzung: Alle nicht-englischen Übersetzungen sind Übersetzungen aus dem Englischen mit DeepL Pro. Wenn Sie Übersetzungsfehler bemerken, teilen Sie uns diese bitte mit.

Kontakt: enquiries@bristolbraille.org

# Was ist die Canute-Konsole?

Die Canute Console ist eine Linux-Workstation, die eine Canute 360 Braillezeile enthält. Die Canute Console Premium ist ein Paket, das kundenspezifischen Support oder Entwicklung, ein abschließbares Flightcase und eine längere Garantie beinhaltet.

Der große Unterschied zwischen der Canute Console und anderen Produkten besteht darin, dass die Braillezeile und die visuelle Anzeige die gleiche Menge an Informationen im gleichen Layout anzeigen, wodurch weder Braille- noch Print-Nutzer benachteiligt werden.

Sie verfügt über eine intuitive Schnittstelle mit Menüs zum Lesen von BRF-Büchern und zum Starten von Anwendungen. Es kann auch von der Kommandozeile aus bedient werden und ist in diesem Modus vor allem für die Informatik und Systemadministration gedacht. 

Den Einsatzmöglichkeiten sind kaum Grenzen gesetzt, wenn man sich erst einmal daran gewöhnt hat, in mehrzeiliger Brailleschrift zu arbeiten.

# Einsetzen und Entfernen der Canute 360 Braillezeile in die Konsole 

Wenn Sie Ihre Konsole als Einzelgerät gekauft haben, müssen Sie dies nicht tun, um die Konsole in Betrieb zu nehmen. 

Wenn Sie die Konsole ohne Braillezeile gekauft haben (das „Upgrade-Kit“), legen Sie beide Produkte auf eine ebene Fläche, auf der Sie sie verwenden möchten. Öffnen Sie den Deckel (den Monitor) des Konsolen-Upgrade-Kits. Heben Sie den Canute 360 an und legen Sie ihn vorsichtig auf die ebene Fläche in der Mitte des Konsolen-Upgrade-Kits, wobei Sie darauf achten müssen, dass die Kanten in einer Linie liegen. 

Hinten rechts ragt ein Teil des Upgrade-Kits heraus. Dabei handelt es sich um den Netzstecker, der mit der Netzbuchse des Canute 360 übereinstimmen muss. 

Sobald dies der Fall ist, bewegen Sie den Canute 360 nach hinten, um die Stromstecker einzuführen. Möglicherweise müssen Sie den Canute 360 leicht anheben, um die Verbindung herzustellen.

Auf der hinteren linken Seite des Upgrade-Kits befindet sich ein USB-B-Stecker (im Allgemeinen als „Druckerstecker“ bezeichnet) an einem sehr kurzen Kabel von etwa 5 cm Länge. Ziehen Sie ihn aus dem Loch, in dem er steckt, und stecken Sie ihn in die USB-B-Buchse des Canute 360 auf der rechten Seite.

Um den Canute 360 zu entfernen, gehen Sie wie oben beschrieben vor, jedoch in umgekehrter Reihenfolge.

# Ergonomie

Die Canute-Konsole sollte auf einem stabilen, flachen Schreibtisch oder Tisch mit einer glatten Oberfläche verwendet werden.

Die glatte Oberfläche ist erforderlich, damit das Tablett mit der Tastatur hinein- und herausgenommen werden kann.

Heben Sie nun den Monitor an (der auch als Deckel dient) und ziehen Sie die Tastatur heraus.

Da die Canute-Konsole viel Blindenschrift enthält, die Sie zum Lesen über eine Tastatur greifen müssen, ist die Ergonomie besonders wichtig.

Achten Sie deshalb darauf, dass Ihr Sitz im Vergleich zum Schreibtisch/Tisch möglichst hoch ist.

Achten Sie auch darauf, dass die Konsole bequem an der Tischkante steht.

Sie sollten Ihre Hände sowohl auf der obersten Zeile der Braillezeile als auch auf der letzten Zeile der Tastatur bequem ablegen können.

# Anschlussbelegung der Canute-Konsole

(Bitte beachten Sie, dass die Anschlüsse des Canute 360 nicht mehr verwendet werden, wenn er an die Konsole angedockt ist).

Auf der linken Seite der Canute-Konsole, von hinten nach vorne: 

- 1x USB-B-Stecker, der sich entweder in einem kleinen Loch im Konsolen-Upgrade-Kit befindet (wenn der Canute 360 abgekoppelt ist) oder in den USB-B-Buchsenanschluss des Canute 360 eingesteckt ist.

- 3x USB-A-Buchsen.

Auf der rechten Seite der Canute-Konsole, von hinten nach vorne:

- 1x Stromanschlussbuchse.

- 1x 35-mm-Audiobuchse.

- 1x Wippe zur Steuerung des externen Monitors.

- 2x Schalter für die Steuerung des externen Monitors.

- 1x HDMI-Buchse.

Auf der Rückseite, von der Mitte nach rechts:

- 1x abnehmbare Platte für zusätzliche Anschlüsse.

- 1x Netzschalter.

Wenn Sie eine der Early-Bird-Konsolen haben, kann Ihre Anschlussbelegung von der oben genannten abweichen.

# Alles einstecken

Nachdem Sie sich vergewissert haben, dass der USB-B-Anschluss an die Canute 360 angeschlossen ist und Sie das Netzkabel eingesteckt haben, drücken Sie den Netzschalter auf der rechten hinteren Seite, um die Braillezeile einzuschalten.

# Ausstecken

Warten Sie nach dem Ausschalten der Canute-Konsole (siehe unten), bis sich die Canute-Anzeige ausgeschaltet hat, und ziehen Sie dann das Netzkabel ab. Wenn Sie das Netzkabel nicht abziehen, wird der Monitor weiterhin mit Strom versorgt und geht in den Bildschirmschonermodus über. Daher sollte eine Canute-Konsole, die nicht benutzt wird, immer vom Netz getrennt werden.

# Tastaturlayout

Unter der Annahme, dass Sie die britische Tastatur verwenden (andernfalls siehe unten 'Ändern der Tastaturbelegung'), ist die Tastatur wie folgt belegt:

Erste Zeile: Escape, F1 bis F10 (F11 ist Fn+F1, F12 ist FN+F2), Num Lock, Print Screen, Delete (Insert ist Fn+Del)

Die Zeilen zwei, drei, vier und fünf sind die Standardwerte für britische Qwerty-Tastaturen.

Zeile sechs: Steuerung (ctrl), Funktion (Fn), Supertaste, Alt, Leertaste, Alt, Strg, Pfeile (Home ist Fn+LINKS, Ende ist Fn+RECHTS, Seite hoch ist Fn+UP, Seite runter ist Fn+RUNTER)

# Starten im 'Lesemodus'

Wenn die Konsole wie oben beschrieben korrekt eingerichtet ist, schließen Sie das Netzkabel an und drücken Sie die Einschalttaste auf der Rückseite.

Die Braillezeile braucht etwa eine Minute, um zu starten, und führt in dieser Zeit eine Punktlöschroutine durch. 

Nach dem Start befindet sich die Canute-Konsole im „Lesemodus“. Dieser funktioniert genauso wie die eigenständige Braillezeile Canute 360 und erfordert keine Qwerty-Tastatur. 

Mit der Taste „H“ oben links auf der Anzeige wird eine kontextbezogene Hilfe aufgerufen, die die Funktionsweise der einzelnen Menüs erklärt. Kurz gesagt: Mit der Daumentaste „Menü“ öffnen Sie das Optionsmenü oder kehren, wenn Sie sich bereits in einem Menü befinden, zum aktuellen Buch zurück. Mit den Tasten „Zurück“ und „Vorwärts“ können Sie sich durch Bücher und Menüs bewegen. Die seitlichen Tasten entsprechen den beschrifteten Einträgen in den Menüs.

Mehr dazu finden Sie im 'Canute 360 Benutzerhandbuch', das sich ebenfalls in diesem Verzeichnis (~/learn/) befindet.

Wenn das Display an der Konsole angedockt ist, liest es alle BRF-Dateien *in Ihrem Heimatverzeichnis im internen Speicher der Konsole*. Die SD-Karte in Ihrer Canute 360 wird nicht gelesen, da diese nur gelesen wird, wenn die Canute 360 Braillezeile eigenständig verwendet wird (nicht angedockt).

# In den 'Konsolenmodus' wechseln

Im 'Konsolenmodus' funktionieren alle interaktiven Anwendungen. Dies ist im Wesentlichen das Booten eines PCs.

Um den „Lesemodus“ zu beenden, wählen Sie entweder die Option im „Systemmenü“ oder ziehen Sie die Qwerty-Tastatur heraus und drücken Sie die Escape-Taste.

Sie erhalten nun einen Anmeldebildschirm.

Wenn Sie von einer älteren Version auf diese Version umgestiegen sind, werden Ihr Benutzername und Ihr Passwort nicht geändert.

Für neue Benutzer ist der Standard-Benutzername „bbt“, das Standard-Passwort ist „bedminster“, alles Kleinbuchstaben.

Wir empfehlen dringend, das Standard-Passwort so bald wie möglich zu ändern und es zu notieren. Mehr dazu weiter unten unter „Ändern des Passworts“.

Im „Konsolenmodus“ steht Ihnen ein „Startmenü“ mit Optionen zur Verfügung, das dem Menü der Bücher im „Lesemodus“ sehr ähnlich ist. Es funktioniert auf die gleiche Weise, nur dass die Schaltfläche „Menü“ in diesem Modus nicht funktioniert.

Jede Zeile steht für eine Anwendung oder einen Befehl, z. B. „Datei-Explorer“, der Dateien in der Anwendung „Textverarbeitung“ öffnet. Wenn Sie eine dieser Anwendungen verlassen, kehren Sie zum Launcher zurück. Wenn Sie aus irgendeinem Grund nicht zum Launcher zurückkehren, sondern in die Befehlszeile zurückkehren, können Sie ihn wieder laden, indem Sie Folgendes eingeben

`$ launcher`

# Befehle vom Launcher aus ausführen

Es gibt ein paar wichtige Menüpunkte, die wir im Folgenden auflisten und erklären:

- 'Herunterfahren'. Damit schalten Sie die Canute-Konsole und die Braillezeile aus. Nach dem Ausschalten erlöschen alle Braille-Punkte. Sobald dies geschieht, sollten Sie das Stromkabel aus der Konsole ziehen.

- WiFi“. Daraufhin wird eine Liste der verfügbaren Verbindungen mit deren Signalstärke angezeigt. Wenn Sie die gewünschte Verbindung gefunden haben, drücken Sie „q“, um sie zu verlassen. Sie werden dann aufgefordert, den Namen der Verbindung und das Passwort einzugeben.

- Aktualisieren“. Es wird nach einem Upgrade auf die neueste Version des Betriebssystems der Canute Console gesucht. Dazu ist zunächst eine Internetverbindung erforderlich. Wir werden E-Mails versenden, wenn es ein größeres Upgrade gibt, also abonnieren Sie bitte den Newsletter auf der [Bristol Braille Website: https://bristolbraille.org/](https://bristolbraille.org/). Die Aktualisierung kann einige Zeit dauern, abhängig von der Geschwindigkeit Ihrer Internetverbindung. Nach Abschluss der Aktualisierung melden Sie sich ab, indem Sie „shutdown now“ eingeben. Ziehen Sie das Netzkabel, stecken Sie es wieder ein und drücken Sie den Netzschalter.

- Befehlszeile“. Dadurch wird die Eingabeaufforderung für die Befehlszeile angezeigt. Wir werden die Befehlszeile weiter unten im Detail untersuchen. Sie müssen die Befehlszeile jedoch nicht verwenden, solange Sie dies nicht wollen.

# Anwendungen über den Launcher starten

Die Liste der Anwendungen im Launcher wird ständig erweitert. Probieren Sie jede einzelne aus, um zu sehen, wie sie funktioniert. Alle diese Anwendungen werden ständig weiterentwickelt, einige sind noch experimentell, und alle werden sich mit zukünftigen Softwareversionen verbessern. Lassen Sie uns wissen, was Sie noch gerne sehen würden. 

Hier sind ein paar nützliche Anwendungen für den Anfang:

- Datei-Explorer“. Dieser öffnet eine Baumansicht Ihres Dateisystems, in der Sie mit den Daumenknöpfen „Vor“ und „Zurück“ navigieren können, genau wie in den Menüs von Reader und Launcher. Wenn Sie eine Datei auswählen, wird die Anwendung „Textverarbeitung“ gestartet, die Ihnen ein Menü zum Anzeigen und Bearbeiten von Dokumenten im Rich-Text-Format bietet, einschließlich einer einzigartigen Drucklayout-Ansicht von PDF-Dateien.

- Texteditor“ Damit wird ein Texteditor namens ‚Micro‘ auf einem leeren Dokument geöffnet. Micro funktioniert ähnlich wie Notepad unter Windows mit ähnlichen Befehlen. ctrl+x/c/v' zum Kopieren und Einfügen, 'ctrl-z' zum Rückgängigmachen, 'ctrl-y' zum Wiederherstellen, 'ctrl-f' zum Suchen, 'ctrl-s' zum Speichern, 'ctrl+q' zum Beenden, zum Beispiel.

# Einige Befehle über die Kommandozeile ausführen

Die folgenden Funktionen werden demnächst in das Startmenü aufgenommen, können aber bereits jetzt über die Befehlszeile ausgeführt werden.

Wählen Sie dazu zunächst die Option „Kommandozeile“ aus dem Startmenü. Dadurch wird eine „Eingabeaufforderung“ geöffnet, d. h. eine Zeile, in die Sie Befehle eingeben können. Diese Zeile wird durch ein Dollarzeichen dargestellt. Vorherige Befehle bleiben auf dem Bildschirm, und eine neue Eingabeaufforderung wird am unteren Rand des Bildschirms angezeigt. Die letzte Eingabeaufforderung ist diejenige, in die Sie den Befehl eingeben.

Wenn Sie fertig sind, können Sie zum Menü zurückkehren, indem Sie an der Eingabeaufforderung „Launcher“ eingeben.

## Ändern der Braille-Codes

In der Befehlszeile gibt es einige spezielle Befehle für das Umschalten der von Ihnen verwendeten Braille-Code-Tabelle, darunter

* $ [alt]+[shift]+Q
(Grade One UEB)

* $ [alt]+[Umschalt]+W
(Grad Zwei UEB)

* $ [alt]+[Umschalt]+E 
(9-zeilige 6-Punkt-US-Computerschrift)

* $ [alt]+[Umschalt]+R 
(Vertragssprache Deutsch, Standard 2015)

Bitte beachten Sie, dass nur US Computer Brl eine Eins-zu-Eins-Darstellung der Zeichen und damit genau das gleiche Layout wie der visuelle Bildschirm garantiert.

Wenn Sie den Namen einer anderen BRLTTY-Braille-Tabelle kennen, die Sie verwenden möchten, können Sie diese mit eingeben:

`$ canute-table [table-name]`

## Ändern Sie Ihr Passwort

Um das Standardpasswort zu ändern, geben Sie `$ passwd` ein und folgen Sie den Anweisungen.

# Escape!

Nachdem wir nun die Grundlagen kennen, sind die wichtigsten Befehle das Beenden von Programmen und das sichere Herunterfahren.

Um ein laufendes Programm zu beenden, gibt es oft spezielle Befehle für dieses Programm. Oft wird `q` oder `ctrl+q` zum Beenden benutzt, oder die `escape` Taste (oben links auf der Konsolentastatur). In extremen Fällen (wenn ein Programm hängt oder Sie nicht wissen, wie Sie es beenden können) funktioniert auch `ctrl+c`. 

Wenn Sie ein Programm wirklich nicht schließen können, um zur Kommandozeile oder zum Launcher zurückzukehren, können Sie einen neuen Terminal-Tab öffnen. Siehe unten.

--------

Das ist alles, was Sie wissen müssen, um mit der Canute-Konsole zu arbeiten. Wenn Sie mehr über die Verwendung der Konsole erfahren möchten, insbesondere in der Kommandozeile, oder wenn Sie Ihre eigene Anwendung programmieren möchten, oder wenn Sie ein Problem beheben möchten, dann lesen Sie weiter unten.

--------

# Grundlagen zum Navigieren in der Linux-Kommandozeile

## Nomenklatur und Aufbau der Kommandozeile

Linux ist das Betriebssystem, auf dem Canute Console läuft. Genauer gesagt läuft auf ihr eine Linux-Version namens 'Raspian', die auf 'Debian' basiert.

Der „Konsolenmodus“ der Canute Console ist so konfiguriert, dass er vollständig über die Befehlszeile ausgeführt wird, d. h. die Anwendungen sind alle text- und terminalbasiert.

Sie werden oft sehen, dass die Begriffe „Kommandozeile“ oder „Terminal“ (auch „tty“ und „Konsole“) austauschbar verwendet werden, da sie sehr ähnliche Bedeutungen haben. Aber hier sind die Unterschiede in Kürze:

- Die „Kommandozeile“ ist in der Regel die letzte Zeile auf dem Bildschirm, in der Befehle eingegeben werden können. Die Befehlszeile enthält eine „Eingabeaufforderung“, d. h. „wo Sie Ihre Befehle eingeben“.

- Die „Befehlszeile“ befindet sich innerhalb eines „Terminals“. Das Terminal enthält die Historie früherer Befehle, die Sie eingegeben haben, sowie deren Ausgabe.

Im weiteren Verlauf dieses Dokuments verwenden wir Anweisungen, die mit einem Dollarzeichen beginnen, dann eine Anweisung, dann manchmal einige andere Wörter oder Zahlen, die durch ein Leerzeichen getrennt sind.

So werden die Befehle in der Linux-Befehlszeile ausgeführt. Sie ist der DOS-Eingabeaufforderung unter Windows sehr ähnlich.

Das Dollar-Zeichen ist nur ein Symbol, das „Eingabeaufforderung“ bedeutet, d. h. „geben Sie Ihre Befehle nach diesem Zeichen ein“.

## Wie man Befehle eingibt und die Terminalausgabe versteht

Wenn wir also zum Beispiel eine Anweisung wie „Führen Sie `$ ls` aus“ schreiben, bedeutet das: „Geben Sie ‚ls‘ in die Befehlszeile ein“. In diesem Fall bedeutet es eigentlich: „Listen Sie die Unterverzeichnisse und Dateien“ in meinen aktuellen Verzeichnissen auf.

Als weiteres Beispiel könnten wir in diesem Dokument schreiben, `$ cd demos` bedeutet „Führe den Befehl ‚cd‘ mit der Anweisung (Argument genannt) ‚demos‘ aus“. Tatsächlich bedeutet es in diesem Fall „Gehe zum Verzeichnis namens ‚demos‘“.

Es ist wichtig zu wissen, dass das Terminal alle vorherigen Befehle speichert und sie nicht vom Bildschirm löscht, es sei denn, Sie werden dazu aufgefordert.

Stellen Sie sich das Terminal wie eine Schreibmaschine oder einen Drucker vor, der eine Zeile nach der anderen schreibt und die vorherigen Zeilen nicht löscht.

Lesen Sie immer bis zur letzten Zeile des Bildschirms und gehen Sie davon aus, dass die *letzte Zeile mit dem Dollar-Prompt* die aktive Befehlszeile ist, und die Zeilen darüber sind Geschichte.

Wenn ich zum Beispiel einen Befehl wie „Erstelle ein Verzeichnis namens ‚documents‘, das als `mkdir documents` geschrieben werden sollte, aber einen Fehler gemacht habe, dann könnte die Ausgabe der Braillezeile wie folgt aussehen

`$ mjdir documents`
bash: mjdir: Befehl nicht gefunden
`$`

Schauen wir uns das noch einmal an, Zeile für Zeile. 

`$ mjdir documents`

Ich habe einen Tippfehler bei dem Befehl gemacht.

`bash: mjdir: Befehl nicht gefunden`

Dies ist die Fehlermeldung („Bash“ ist die so genannte „Shell“, also das Programm, das Ihre Befehle interpretiert).

`$ `

Jetzt erhalten wir eine weitere leere Eingabeaufforderung, aber wir können immer noch unseren ersten Versuch und die obige Fehlermeldung sehen.

Für diejenigen, die an einzeilige Braillezeilen gewöhnt sind, ist dies vielleicht etwas gewöhnungsbedürftig. Es ist jedoch das Standardverhalten des Linux-Terminals und entspricht genau dem Layout und der Ausgabe, die sehende Benutzer auf dem Bildschirm erhalten.

Wenn Ihr Bildschirm zu unübersichtlich ist, ist der Befehl `$ clear` Ihr Freund. Er setzt das Terminal bis auf die aktuelle Eingabeaufforderung in der obersten Zeile des Bildschirms auf leer zurück.

## Sich zurechtfinden

Linux verwendet ein hierarchisches Dateisystem, d.h. Verzeichnisse und Dateien befinden sich alle innerhalb anderer Verzeichnisse, zurück zum „Stamm“. Die Wurzel steht an der Spitze der Hierarchie.

Verzeichnisse werden durch Strichsymbole getrennt, so dass Ihr Home-Verzeichnis wie folgt geschrieben wird: `/home/pi/`

Wenn Sie sich anmelden, starten Sie im Home-Verzeichnis Ihres Benutzers. Für den Anfang werden Sie nur selten über Ihr Home-Verzeichnis hinausgehen müssen.

`$ ls` listet die Unterverzeichnisse und Dateien im aktuellen Verzeichnis auf.

Sie werden feststellen, dass diese Liste zu lang ist, um sie zu lesen. Sie können mit `ctrl+Fn+UP` oder `ctrl+Fn+DOWN` in der Liste der Terminalausgaben nach oben und unten blättern.

Um zwischen Verzeichnissen zu wechseln, verwenden Sie den Befehl `cd`. `$ cd Verzeichnisname`.

Um eine Ebene tiefer zu gehen, verwenden Sie den Befehl `$ cd ..`.

Hier ist eine wichtige Sache über viele Linux-Befehle, wie `cd`, zu beachten. Wenn Sie darum bitten, in ein Verzeichnis zu gehen, wird der Befehl dies tun, aber Sie erhalten keine Ausgabe, die Ihnen sagt, ob er es getan hat. 

Wie können Sie also wissen, wo Sie sich befinden? Sie können `$ pwd` eingeben. Dies gibt Ihre aktuelle Position innerhalb des Dateisystems zurück. 

## Tipps, die die Navigation und die Eingabe von Befehlen erleichtern

Bei einigen Befehlen ist der Ausgabestring länger als die Anzeige (40 Zellen), weshalb der Canute ganz nach rechts geschwenkt ist. 

Um auf der Braillezeile nach links und rechts zu schwenken (dies hat keinen Einfluss auf die visuelle Anzeige), um Zeichenketten anzuzeigen, die länger als 40 Zellen sind, können Sie die Daumentasten '[MENU]+[VORWÄRTS]' auf der Vorderseite der Canute-Anzeige drücken, um nach rechts zu schwenken, oder '[MENU]+[PREV]', um nach links zu schwenken.

Sie können einen Befehl oder einen Verzeichnis- oder Dateinamen automatisch vervollständigen, wenn Sie in die Eingabeaufforderung tippen, indem Sie die Tabulatortaste drücken.

Wenn nur eine Option verfügbar ist, wird diese mit der Tabulatortaste automatisch ausgefüllt. Wenn es sich um mehrere Optionen handelt, bewirkt das Drücken der Tabulatortaste nichts, aber ein doppeltes Tippen auf die Tabulatortaste zeigt alle Optionen an und schreibt sie in das Terminal, so dass Sie sie sehen können.

## Lesen von Textdateien

Wenn es eine Textdatei gibt, die du lesen willst, kannst du „$ less textfilename.txt“ eingeben. Die Datei muss keine TXT-Erweiterung haben, und unter Linux haben sie oft überhaupt keine Erweiterung. Im Zweifelsfall probieren Sie `less` aus.

Less gibt die ersten acht Zeilen der Datei aus. Benutzen Sie die Bildlauftasten (Fn+UP, Fn+DOWN), um sich in der Datei zu bewegen, und drücken Sie 'q', um zurück zur Befehlszeile zu gelangen.

## Nützliche Befehle 

Einschließen:

* ls|less
(Listet Dateien und Unterverzeichnisse auf)

* tree|less
(Zeigt den Verzeichnisbaum an)

* cd [VERZEICHNISNAME]
(Wechselt in ein Unterverzeichnis)

* cd ..
(Wechselt zum übergeordneten Verzeichnis.)

* mkdir [VERZEICHNISNAME]
(Neues Verzeichnis erstellen)

* micro [DATEINAME]
(Textdatei öffnen)

* [ctrl]+[shift]+t
(Öffnet eine neue Registerkarte im Terminal.)

* [ctrl]+[shift]+[NUM] (0--9)
(Wechselt zu einer anderen Registerkarte.)

* Jetzt herunterfahren
(Herunterfahren, aber Sie müssen noch 
Canute nach dem Herunterfahren ausschalten 
abgeschlossen ist.)

* touch [FILENAME]
(Neue leere Datei erstellen)

* rm [DATEINAME]
(Datei löschen)

* rm -r [DIRNAME]
(Verzeichnis und dessen Inhalt löschen; '-r' ist die Abkürzung für 'rekursiv'.)

* sudo [KOMMANDO]
(Führt den nachfolgenden Befehl als Superuser aus, d.h. mit vollen Rechten.)

* sudo rm -r [DIRNAME]
(Ein Repository-Verzeichnis löschen, ohne nach jeder schreibgeschützten Datei gefragt zu werden.)

## Lernressourcen

Wir empfehlen Ihnen, sich die folgenden Episoden von Braillecast, gesponsert von Bristol Braille, anzuhören, um sich mit einigen der Grundlagen der Linux-Befehlszeile vertraut zu machen.

Alle Episoden sind zu finden in:
`cd ~/learn/braillists/`
und können abgespielt werden mit:
`vlc 01[tab to complete]`

- 01: Zunächst ein Überblick über verschiedene Informatiker, die Braille bei ihrer Arbeit verwenden. Dies kann übersprungen werden.

- 02: Zweitens, eine Einführung in die Linux-Befehlszeile in der Meisterklasse.

- 03: Drittens, eine Einführung in Git, zur Versionskontrolle und zum Austausch und Herunterladen von Programmen.

- 04: Viertens: Ein Meisterkurs über die Programmierung mit der Canute-Konsole.

Im Lieferumfang der Canute-Konsole sind einige Tools zum Erlernen der Linux-Grundlagen enthalten. Sie finden sie im Home-Verzeichnis, indem Sie `cd learn` eingeben.
Schließlich können Sie normalerweise im Internet nach Antworten auf Ihre Fragen suchen. 

Die meisten Fragen, die Sie haben, betreffen Linux im Allgemeinen. Wenn Sie jedoch spezifischere Antworten benötigen, können Sie versuchen, nach „Debian“ oder sogar speziell nach „Raspian“ zu suchen.

Wir empfehlen für diese Art von Fragen die Website Stackoverflow.

--------

Sie sollten nun die Grundlagen der Linux-Befehlszeile einigermaßen beherrschen, wenn Sie sich dafür entscheiden. In den folgenden Abschnitten werden häufige Aufgaben ausführlicher erklärt.

-------

# Shutdown

Um das gesamte Gerät über die Kommandozeile herunterzufahren, geben Sie `$ shutdown now` ein. (Oder um neu zu starten, geben Sie `$ reboot` ein.)

Sobald Sie das Gerät heruntergefahren haben und der Canute-Bildschirm sich ausgeschaltet hat, sollten Sie das Netzkabel aus der Konsole ziehen (sonst bleibt der Monitor eingeschaltet).

* Hinweis: Wenn Sie Ihren Canute 360, der mit der Konsole geliefert wird, auf Firmware-Version 2.0.10 oder höher aktualisiert haben, schaltet sich Ihr Canute aus, wenn sich die Konsole ausschaltet. Wenn er jedoch mit einer älteren Firmware läuft, schaltet er sich nicht von selbst aus, sondern geht in den eigenständigen Buchlesemodus über. In diesem Fall müssen Sie die Einschalttaste auf der rechten Rückseite drücken, um das Canute-Display auszuschalten, bevor Sie das Netzkabel herausziehen.

# Eine neue Terminal-Registerkarte öffnen

Terminal-Registerkarten funktionieren wie eine Registerkarte in einem Webbrowser. Drücken Sie „Strg+Umschalt+t“, um eine neue Registerkarte zu öffnen. Die vorherige Registerkarte läuft im Hintergrund weiter, sodass Sie zwischen mehreren Registerkarten wechseln können. 

Um zwischen den Registerkarten zu wechseln, können Sie `alt+[1--9]` drücken. 

Um eine Registerkarte über die Befehlszeile zu schließen, geben Sie `$ exit` ein. Bitte beachten Sie, dass Sie Ihre Sitzung beenden, wenn Sie die ursprüngliche Registerkarte schließen. Achten Sie also darauf, alle Änderungen an Dokumenten zu speichern, bevor Sie dies tun.

Bitte beachten Sie, dass das Öffnen einer neuen Registerkarte oder das Wechseln zwischen Registerkarten den aktuellen Braille-Code nicht verändert. Wenn Sie also in einer Anwendung waren, die Computer-Braille verwendet, und dann eine neue Registerkarte öffnen, wird auch die neue Registerkarte in Computer-Braille dargestellt.

# Ton

Bitte beachten Sie die folgenden Befehle. Das doppelte s in sset ist kein Tippfehler.

- `$ amixer sset Master 0%`

- `$ amixer sset Master 100%`

# Ändern der Tastaturbelegung

Der Befehl ist von der Kommandozeile aus relativ einfach. Zum Beispiel:

`$ setxkbmap us`

Ändert die Tastatur auf die US-Tastatur. 

Für eine Liste der verfügbaren Tastaturlayouts geben Sie ein:

`$ ls /usr/share/X11/xkb/symbols/ | less`

Um es automatisch zu setzen, fügen Sie den gleichen Befehl in `~/console-set-up/console-start.sh` ein:

`$ setxkbmap us`

# USB-Sticks verwenden

USB-Sticks werden automatisch in das Verzeichnis `/media/` eingebunden.

Zum Beispiel wird der erste USB-Stick, den Sie einstecken, in das Verzeichnis `/media/usb0` eingehängt, der zweite USB-Stick in das Verzeichnis `/media/usb1`, und so weiter.

Um auf Dateien auf dem USB-Stick zuzugreifen, verwenden Sie `cd` wie bei jedem anderen Verzeichnis, zum Beispiel `$ cd /media/usb0/`.

Wenn Ihre Konsole nicht auf dem neuesten Stand ist, müssen Sie möglicherweise eine Anwendung namens `usbmount` installieren, bevor die oben genannten Funktionen funktionieren. Dazu ist eine Internetverbindung erforderlich (siehe unten). Sobald Sie verbunden sind, geben Sie diesen Befehl ein:

`$ sudo apt install usbmount`

Sobald die Installation abgeschlossen ist, starten Sie die Konsole neu oder entfernen Sie den USB-Stick und stecken Sie ihn wieder ein.

# Verbinden mit WiFi

Die Canute-Konsole verbindet sich automatisch mit jedem gespeicherten WiFi-Netzwerk. 

Um zwischen diesen Netzwerken zu wählen (falls es mehr als eines gibt), geben Sie `$ wifi new` ein.

Um sich zum ersten Mal mit einem neuen Netzwerk zu verbinden, tippen Sie `$ wifi new`, dann drücken Sie `q`, woraufhin in der letzten Zeile eine Eingabeaufforderung erscheint, in der Sie den Namen der gewünschten Verbindung, `enter` und dann das Passwort eingeben.

Wenn die Verbindung erfolgreich hergestellt werden konnte, wird eine Meldung wie „Gerät ‚wlan0‘ erfolgreich verbunden“ angezeigt, die jedoch möglicherweise nicht erscheint, wenn Sie bereits eine Verbindung hergestellt haben.

Wenn du dich das nächste Mal mit dieser Wifi-Verbindung verbinden willst, aber das Passwort nicht erneut eingeben willst, kannst du das 'new' weglassen und es wird sich an das letzte Mal erinnern: `$ wifi`

# Tethering mit Ihrem Handy über den USB-A-Anschluss

Schließen Sie Ihr Android-Handy über ein USB-Kabel an die Canute-Konsole an. In den Benachrichtigungen Ihres Telefons steht nun etwas wie „Aufladen über USB“. Tippen Sie auf diese Benachrichtigung und Sie gelangen zum USB-Einstellungsmenü. Ändern Sie „Nur Laden“ in „Tether“ (die genauen Begriffe können sich ändern). Die Konsole hat nun Zugriff auf das Internet. 

Beim Tethering wird diese Verbindung anstelle der WiFi-Verbindung verwendet, auch wenn sie nicht so schnell ist.

# Herunterladen von Dateien aus dem Internet

Wenn Sie die URL einer Datei im Internet kennen, können Sie sie mit dem Befehl `curl` herunterladen. Mit der Option `-O` wird die Datei unter ihrem ursprünglichen Namen an ihrem aktuellen Ort im Dateisystem gespeichert. Zum Beispiel:

`$ curl -O https://bristolbraille.org/shared/2024-03-22-music-demos.zip`

In diesem Fall möchten Sie die Datei vielleicht entpacken, was Sie mit diesem Befehl tun können:

`$ unzip 2024-03-22-music-demos.zip`

Dann können Sie die ursprüngliche Zip-Datei mit dem Befehl `rm` entfernen:

`$ rm 2024-03-22-music-demos.zip`

Um andere Optionen für curl nachzuschlagen, wie z.B. das Speichern unter einem anderen Dateinamen oder die Ausgabe auf dem Bildschirm, lesen Sie das `curl`-Handbuch:

`$ man curl`

# Neue Programme aus dem Internet installieren

Es gibt viele verschiedene Möglichkeiten, Programme unter Linux zu installieren. Die gebräuchlichste Methode ist die Verwendung von Paketmanagern wie 'apt', mit denen Sie installierte Programme einfach aktualisieren und verwalten können.

Lassen Sie uns dies anhand der Installation eines Spiels testen. Sie können jedes andere „apt“-Programm, das Sie interessiert, nachschlagen und stattdessen versuchen, es zu installieren. Suchen Sie im Internet nach „Linux apt [Thema von Interesse]“, um zu sehen, welche Programme verfügbar sind. Es gibt Tausende!

So installieren Sie das Programm:

`$ sudo apt install nethack`

Im Klartext bedeutet das:

- `sudo` steht für 'superuser do', was bedeutet, dass Sie ihm die Erlaubnis geben, das System zu verändern.

- Das Dollarzeichen steht für die Eingabe in die Befehlszeile.

- Das Programm, das Sie ausführen, ist `apt`, ein Paket-Installer und -Manager.

- Sie geben den Befehl `install` ein.

- Schließlich der Name des Programms, das Sie installieren wollen, `nethack-console`. In diesem Fall handelt es sich nicht um eine Version, die speziell für die Canute-Konsole entwickelt wurde. Der Begriff „Konsole“ ist in etwa analog zu „Terminal“, d.h. die Version des Programms, die über das Terminal ausgeführt werden soll. 

Die Installation kann einige Minuten in Anspruch nehmen. Am Ende der Installation erscheint eine weitere leere Befehlszeile mit einem Dollarzeichen am unteren Rand des Bildschirms.

Dies ist ein altes Videospiel aus den 90er Jahren. Das Interessante daran ist, dass es, obwohl es nicht für die Wiedergabe auf einer Braillezeile konzipiert wurde, auf der Canute-Konsole sofort funktioniert. Aber damit es richtig angezeigt wird, müssen Sie zuerst in den Computer-Braille-Modus wechseln (siehe oben).

Bitte denken Sie daran, dass dieses Spiel vor dem Spielen nachgeschlagen werden muss, da es sehr komplex ist. 

$man nethack`

`man` ist die Abkürzung für 'Handbuch'. Damit erhalten Sie die Anleitung.

Versuchen Sie nun, das Programm zu starten. 

`$ nethack`

Viel Erfolg! (Wenn Sie Nethack verlassen wollen, drücken Sie ctrl+c und halten Sie die Eingabetaste gedrückt, bis das Programm beendet wird.)

# Dateien drucken

Wenn Ihr Drucker beim Anschließen automatisch erkannt wurde, können Sie sofort mit dem Drucken beginnen. Falls nicht, befolgen Sie diese Anweisungen oder wenden Sie sich an Bristol Braille:

`$ links2 https://opensource.com/article/21/8/add-printer-linux`

Damit wird ein Browser namens Links2 gestartet, um eine Webseite zu lesen. Sie brauchen nur mit den Tasten scrollup scrolldown nach oben und unten zu scrollen und mit der Tastenkombination ctrl+c zu beenden.

Sobald Ihr Drucker eingerichtet ist, können Sie Dokumente mit LibreOffice ausdrucken. (Möglicherweise müssen Sie zuerst LibreOffice installieren.)

`$ sudo apt install LibreOffice`
`$ libreoffice --headless -p beispiel.docx`

# Upgrade auf die neueste Version von Console OS

Sobald Sie mit dem Internet verbunden sind, führen Sie den folgenden Befehl aus:

`$ curl -L https://bristolbraille.org/console-install | bash`

# Beispiel für benutzerdefinierte Anwendungen, die für die Canute Console entwickelt wurden

## Sonic Pi

Installieren:

`$ git clone https://gitlab.com/bristol-braille/sonic-pi-canute/`

Die folgenden Beispiele zeigen, wie Sonic Pi-Programme mit Hilfe einiger Tools, die wir für die Konsole entwickelt oder um die herum wir Skripte geschrieben haben, ausgeführt und bearbeitet werden können:

`$ cd sonic-pi-canute` (Gehen Sie in das Verzeichnis `sonic-pi-canute`.)

`$ micro examples.txt` (Sieh dir --- und editiere, wenn du willst --- den Sonic Pi Beispielcode an.)

Jetzt bist du im Texteditor namens `micro`. Bewegen Sie sich darin, speichern Sie, machen Sie es rückgängig usw., indem Sie die normalen Tastaturbefehle im Stil von Notepad verwenden. Um den Editor zu verlassen, drücke `ctrl+q` und tippe `y` oder `n`, um Änderungen zu speichern oder nicht zu speichern.

Wenn Sie wieder in der Kommandozeile sind...

$ bash start-sonic-pi.sh“ (Startet das grafische Programm Sonic Pi und minimiert es dann, damit wir von der Kommandozeile aus weiterarbeiten können. Bash ist ein Programm, das so genannte `Shell`-Skripte ausführt, die oft auf `.sh` enden.)

Sobald wir wieder in der Kommandozeile sind, was etwa 30 Sekunden dauern wird...

$ bash play-sonic.sh examples.txt` (Ein Skript, das die Datei `examples.sh` an Sonic Pi übergibt. Sie werden sehen, dass die Töne abgespielt werden.)

`$ bash stop-sonic.sh` (Stoppt alles, was von Sonic Pi abgespielt wird.)

--------

# Erweitert

## Das Terminal eines anderen Kollegen fernsehen

Um mit einem anderen Kollegen zu arbeiten oder einem Tutor zu folgen, möchten Sie vielleicht dessen Terminal-Sitzung online mit einem Tool wie [`tty-share`](https://tty-share.com/) verfolgen.

## Fernsteuerung Ihrer Konsole

Es kann sinnvoll sein, einem anderen Fernbenutzer, z. B. einem BBT-Mitarbeiter, die Kontrolle über die Konsole zu einem von Ihnen gewählten Zeitpunkt zu ermöglichen. Hierfür empfehlen wir die Verwendung von [`mosh`](https://mosh.org/), das ähnlich wie `ssh` funktioniert, aber die Stabilität der Verbindung verbessert.

## Apps, die nicht in 9 Zeilen passen

- Führen Sie `$ screen` aus und drücken Sie Enter.

- Drücken Sie 'enter', um den Readme-Kram zu überspringen.

- Drücken Sie '[ctrl]+a' und dann ':' (Doppelpunkt). Damit können Sie Befehle eingeben, die in der 9. Zeile des Bildschirms erscheinen (unten links wird ein Doppelpunkt angezeigt)

- (Dieser Schritt kann übersprungen werden.) Geben Sie `width -w 40` ein und drücken Sie die Eingabetaste. Dies setzt die Breite auf 40. (Sie können diese Zahl ändern, wenn die Anwendung, die Sie verwenden, breiter sein muss, obwohl diese Anleitung davon ausgeht, dass dies nicht nötig ist, so dass Sie diesen Schritt wahrscheinlich überspringen können).

- Drücken Sie '[ctrl]+a' und dann erneut ':' (Doppelpunkt), um einen weiteren Befehl einzugeben.

- Geben Sie `height -w 17` ein und drücken Sie Enter. Dadurch wird die Höhe des Terminals größer, in diesem Fall 17 Zeilen statt 9. Sie können jede andere Zahl wählen, sie muss kein Vielfaches von 9 Zeilen sein.

* (Wenn Sie zum Beispiel die Höhe auf 27 Zeilen einstellen, ist das Terminal dreimal so hoch wie die Canute-Anzeige, also genauso groß wie eine britische Standard-Papier-Braille-Seite).

- Um sich auf dem extra großen Terminal zu bewegen, drücken Sie '[ctrl]+a' und dann '['. Damit gelangen Sie in den so genannten „Kopiermodus“, aber wir werden ihn benutzen, um uns zu bewegen.

- Jetzt bewegt sich der Cursor (normalerweise alle sechs Punkte) frei auf dem Terminal zwischen den Zeilen (normalerweise bleibt er auf der Eingabeaufforderung).

- Bewegen Sie den Cursor ganz an den Rand des Bildschirms, indem Sie ihn einfach gedrückt halten. Dadurch wird der gesamte Inhalt nach unten verschoben.

- Wenn Sie den Teil der Anwendung sehen, den Sie anzeigen möchten, drücken Sie die Escape-Taste. Damit verlassen Sie den Kopiermodus und kehren in den normalen Befehlszeilen-Eingabemodus zurück.

________
(Der obige Text ist einer Frage auf Stackoverflow entnommen, die Sie lesen können, wenn Sie mehr verstehen wollen, aber das ist nicht nötig: https://stackoverflow.com/questions/75316022/emulate-a-taller-terminal-within-gnome-terminal-and-pan-up-and-down)

# Entwicklung von Anwendungen für die Canute-Konsole

Dies ist ein großes Thema, das wir in einem separaten Dokument behandeln werden und an dem aktiv gearbeitet wird. 

Im Moment ist die einfachste Erklärung die folgende:

Alles, was Sie für die visuelle Kommandozeile entwickeln können, in jeder Sprache, was unter Linux läuft und in das Format 40x9 passt, wird auf der Canute Console laufen. 

Es gibt jedoch einige Überlegungen:
- Um die 1:1-Parität des Layouts zwischen der visuellen und der Braillezeile zu gewährleisten, empfehlen wir, das Programm so zu entwickeln, dass es Computer-Braille-Code verwendet. Bei kontrahierten Braille-Tabellen wird der horizontale Leerraum nicht berücksichtigt.
- Canute 360s aktualisiert sich mit 0,5-1 Sekunden pro Zeile, daher sollten Anwendungen ständiges Scrollen vermeiden.
[NPYScreen](https://github.com/npcole/npyscreen/) kann ein nützlicher Weg sein, um aus einfachen Bash- und Python-Befehlen schnell attraktive räumliche, taktile und visuelle Produkte zu erstellen.

## Anwendungen als ausführbare Dateien für andere Plattformen veröffentlichen

Auf der Canute Console läuft ein Debian-basiertes Betriebssystem auf einem ARM7-Prozessor (dem Raspberry Pi 4). Aber wenn Sie Ihre Anwendungen veröffentlichen, werden Sie sie auch auf anderen Systemen zum Laufen bringen wollen.

Wenn Sie eine kompilierte Sprache wie C verwenden, sollten Sie sich die Compiler-Befehle ansehen.

Wenn Sie eine Skriptsprache wie Python verwenden, möchten Sie vielleicht eine ausführbare Datei erstellen, damit der Benutzer Ihren Code nicht sieht und die Abhängigkeiten, auf die Ihr Code angewiesen ist, nicht installieren muss. Vielleicht möchten Sie auch eine ausführbare Datei erstellen, die unter Windows, x86 Linux oder Mac OS funktioniert.

Es gibt zwei Tools für Python, die wir empfehlen:

- `pyinstaller` ist ein Python-Tool, das Sie mit `pip` installieren können und das eine ausführbare Datei *für den Prozessor, auf dem Sie es ausführen*, d.h. in diesem Fall ARM7, erstellt. Wenn Sie mit diesem Tool andere ausführbare Dateien erstellen wollen, müssen Sie es auf diesen Plattformen ausführen.

- auto-py-to-exe“ ist ein Tool, das ausführbare x86-Windows-Programme erstellt, ohne dass Windows gestartet werden muss, und kann daher für Canute Console-Besitzer praktisch sein.

## Vorsicht mit Canute Console-spezifischen Befehlen

Unabhängig davon, welche Sprache Sie verwenden und welche Methode Sie für die Erstellung des Publish your verwenden, sollten Sie berücksichtigen, ob der Benutzer am anderen Ende der Leitung ein Canute-Display angeschlossen hat oder nicht. 

Wenn Sie das Skript für ein Publikum entwickeln, das keine Braille-Schrift liest und keinen Canute-Bildschirm besitzt, sollten Sie darauf achten, dass die Canute-Konsolen-spezifischen Zeilen in Ihrem Skript nicht zum Absturz des Programms führen. Wenn Sie z.B. den Befehl `canute-cursor` aufrufen, wird dies auf jedem anderen System zu einem Fehler führen. 

Sie können dies umgehen, indem Sie diese Zeilen manuell auskommentieren, oder elegante Fail-States erstellen, wenn diese Befehle nicht gefunden werden, oder eine Wrapper-Funktion haben, die diese Befehle je nach System aufruft oder nicht aufruft.

## Veröffentlichen Sie Ihre Anwendung öffentlich

Es gibt viele Möglichkeiten, dies zu tun. Wir werden einen Weg beschreiben, den Bristol Braille als Beispiel verwendet hat, nämlich die Verwendung einer Versionskontrolle und das Hosten auf einer häufig genutzten Website.

Es gibt eine Technologie namens „git“, die für die Verfolgung von Änderungen an Software und die Veröffentlichung dieser Software in „Repositories“ verwendet wird. Eine der Websites, die Git-Repositories (oft auch Repos genannt) als Dienst anbietet, heißt Github, auf der wir unsere öffentlich zugängliche Software speichern. Das bedeutet, dass die Besucher dieser Website den Quellcode der Canute-Benutzeroberfläche sehen, herunterladen und ändern können.

Auf Websites wie dieser werden in der Regel weitere nützliche Dienste angeboten, die mit Software zu tun haben. Einer davon ist „Issues“, um Softwarefehler und Änderungsvorschläge zu verfolgen. Ein anderer ist ein „Wiki“, das für öffentliche Informationen über unsere Dienste genutzt wird. Ein Wiki ist eine Reihe von Webseiten, die von Benutzern bearbeitet werden können.

In diesem Beispiel auf Github haben wir also eine „Organisation“ namens „bristol-braille“ und innerhalb dieser ein Repository namens „canute-ui“. Innerhalb dieses Repositorys haben wir dann verwandte „Issues“ und ein verwandtes „Wiki“. So:

- Der Code für die Canute-Benutzeroberfläche ist hier öffentlich zugänglich: https://github.com/bristol-braille/canute-ui/
- Probleme im Zusammenhang mit der Canute-Benutzeroberfläche sind hier öffentlich zugänglich: https://github.com/bristol-braille/canute-ui/issues/
- Das Wiki mit nützlichen Informationen zur Canute-Benutzeroberfläche ist hier öffentlich zugänglich: https://github.com/bristol-braille/canute-ui/wiki/

--------

# Fehlersuche 

## Die Braillezeile zeigt keine Brailleschrift an

Wenn die Braillezeile nicht funktioniert, Sie aber vermuten oder wissen, dass der Computer eingeschaltet ist und die visuelle Anzeige und der Ton funktionieren, müssen Sie einige Schritte unternehmen.

Schalten Sie zunächst die Konsole aus. Wenn dies nicht möglich ist, ohne dass Sie die Braillezeile lesen können, gehen Sie wie folgt vor:

- Drücken Sie 'ctrl+alt+f2'.

- Geben Sie nun 'bbt' und Enter ein, dann 'bedminster' und Enter (oder Ihr eigenes Login und Passwort).

- Drücken Sie nun die Escape-Taste (dadurch wird das Launcher-Menü verlassen).

- Geben Sie nun shutdown now ein.

- Warten Sie zwanzig Sekunden und ziehen Sie dann das Netzkabel.

Wenn Sie Schwierigkeiten haben, die obigen Schritte auszuführen, fahren Sie mit dem Abziehen des Stromkabels fort.

Kehren Sie dann zum Anfang dieses Handbuchs zurück, um die Schritte zum korrekten Einstecken des Canute 360 in das Dock der Canute-Konsole zu befolgen, einschließlich des USB-B-Kabels auf der linken Rückseite, *bevor* Sie die Konsole einstecken.

Wenn Sie die Konsole wieder einstecken, denken Sie daran, die Einschalttaste auf der rechten Rückseite zu drücken.

Wenn dies das Problem nicht behebt, wenden Sie sich bitte an Ihren Händler oder an Bristol Braille Technology, um Unterstützung zu erhalten.

## Probleme beim Einloggen

Haben Sie Schwierigkeiten beim Einloggen, z.B. wenn Sie zur Anmeldeseite zurückkehren? Dies kann bei den ersten Versionen der Konsolensoftware vorkommen, wurde aber mit den Upgrades behoben. Um das Upgrade durchzuführen, müssen Sie sich natürlich zuerst anmelden. Drücken Sie daher diese Tastenkombination, um zu einem anderen Login-Terminal zu wechseln:

Strg+Alt+F2“.

Geben Sie nun Ihre Anmeldedaten ein. Sobald Sie eingeloggt sind, folgen Sie dem Upgrade-Verfahren unten (suchen Sie nach 'Upgrading').

## Probleme bei der Verbindung mit WiFi

Wenn Sie die Fehlermeldung `Unable to connect ... Secrets were required but not provided`, dann versuchen Sie die folgenden zwei Befehle auszuführen:

`$ nmcli con delete „NAME DER SSID“`
`$ nmcli dev wifi connect „NAME DER SSID“ password PASSWORD ifname wlan0`

## Testen der Internetverbindung

Testen Sie die Internetverbindung in der Konsole, indem Sie „$ clear; ping bbc.com -i 4“ eingeben. Sie sollten ein Protokoll mit vielen Ausgaben erhalten, das ständig aktualisiert wird. 

(Der Teil „-i 4“ dieses Befehls bedeutet, dass die BBC-Server alle vier Sekunden angefunkt werden sollen).

Sie sollten eine Antwort erhalten, die etwa so aussieht

64 Bytes von 151.101.192.81 [etc etc]`

(Die IP-Adresse kann unterschiedlich sein.)

Das bedeutet, dass eine Anfrage an die BBC-Website gesendet und eine positive Antwort erhalten wurde, was bedeutet, dass eine Verbindung besteht. Die Rückmeldung ist zufällig 64 Byte lang.

Drücken Sie `[ctrl]+c`, um das Programm zu beenden und die Ergebnisse zu erhalten.

Wenn der obige Test nicht annähernd 100 % „empfangene Pakete“ ergibt, haben Sie ein Problem mit Ihrer Verbindung. 

## Internet-Probleme mit IPv4 vs. IPv6

Eine Ursache für Verbindungsprobleme kann sein, dass Ihre Verbindung versucht, IPv6-Adressen zu verwenden, obwohl Ihr Internetdienstanbieter dies nicht unterstützt. 

Wenn Sie vermuten, dass dies die Ursache ist, können Sie eine Konfiguration erstellen, um IPv6 zu deaktivieren:

`$ sudo echo „net.ipv6.conf.all.disable_ipv6=1“ >> /etc/sysctl.d/local.conf`

Erläuterung: sudo = als Superuser ausführen; echo = String schreiben; >> = an Datei anhängen

## Weitere Fehlerbehebung

Bitte wenden Sie sich an Bristol Braille, wenn Ihr Problem oben nicht behandelt wurde.

Wenn Sie sich mit den Problemlisten für Programmierer wohl fühlen, können Sie [ein Problem auf https://gitlab.com/Bristol-Braille/console-learn/-/issues](https://gitlab.com/Bristol-Braille/console-learn/-/issues) anmelden.

