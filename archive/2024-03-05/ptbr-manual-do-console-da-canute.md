# Manual do console da Canute

## Notas

Última atualização: 2024-01-31

Aviso ao leitor: Este é um documento ativo que é atualizado à medida que surgem mais casos de uso para o Canute Console. Faça sugestões sobre dicas a serem incluídas, como tornar as instruções mais claras e informe sobre imprecisões.

Observação sobre a tradução: todas as traduções que não são em inglês são traduções do inglês usando o DeepL Pro. Se você notar erros de tradução, informe-nos sobre eles.

Contato: enquiries@bristolbraille.org

Registre um problema: [https://gitlab.com/Bristol-Braille/console-learn/-/issues](https://gitlab.com/Bristol-Braille/console-learn/-/issues)

## O que é o Canute Console?

O Canute Console é uma estação de trabalho Linux que inclui um monitor Braille Canute 360. 
A grande diferença entre o Canute Console e outros produtos é que os visores em braile e visual mostram a mesma quantidade de informações no mesmo layout, não prejudicando nem o usuário de braile nem o de impressão.

Ele opera a partir da linha de comando e é destinado principalmente à ciência da computação. No entanto, há poucos limites para os usos que podem ser feitos depois que você se acostuma a trabalhar em várias linhas de Braille.

O Canute Console é composto por dois produtos eletrônicos diferentes: O monitor Braille Canute 360 e o Console Upgrade Kit. 

O Canute 360 fica dentro do Canute Upgrade. 

## Inserção e remoção do Canute 360 no Console Upgrade Kit

Coloque os dois produtos em uma superfície plana onde pretende usá-los.

Abra a tampa (o monitor) do Console Upgrade Kit. Levante o Canute 360 e coloque-o cuidadosamente na superfície plana no meio do Console Upgrade Kit, tomando cuidado para alinhar as bordas. 

Na parte traseira direita, há uma saliência no kit de atualização. Esse é o conector de alimentação macho e precisa estar alinhado com o conector de alimentação fêmea do Canute 360. 

Quando isso estiver alinhado, mova o Canute 360 para trás para inserir os conectores de alimentação. Talvez seja necessário levantar um pouco o Canute 360 para fazer a conexão.

Agora, no lado esquerdo traseiro do Kit de atualização, você sentirá um conector macho USB-B (geralmente chamado de "conectores de impressora") em um cabo muito curto de cerca de 5 cm. Puxe-o para fora do orifício em que está apoiado e insira-o na porta USB-B fêmea do Canute 360, à direita.

Para remover o Canute 360, faça o mesmo que o descrito acima, mas no sentido inverso.

## Ergonomia

O Console Canute deve ser usado em uma mesa ou escrivaninha estável e plana com uma superfície lisa.

Ele precisa de uma superfície lisa para que a bandeja com o teclado possa ser colocada e retirada.

Agora, levante o monitor (que também funciona como uma tampa) e puxe o teclado para fora.

Como o Console Canute tem muito Braille, que você precisa alcançar sobre um teclado para ler, a ergonomia é especialmente importante.

Portanto, certifique-se de que seu assento esteja o mais alto possível em relação à escrivaninha/mesa.

Certifique-se também de que o console esteja confortavelmente próximo à borda da escrivaninha/mesa.

Você deve poder apoiar as mãos confortavelmente na linha superior da tela Braille e na última linha do teclado.

## Layout das portas do console Canute

(Observe que, quando encaixado no Console, as portas do Canute 360 não estão mais em uso).

À esquerda do console Canute, de trás para a frente: 

 - 1 conector USB-B macho, que fica dentro de um pequeno orifício no Console Upgrade Kit (quando o Canute 360 está desconectado) ou é inserido na porta USB-B fêmea do Canute 360.

 - 3 conectores USB-A fêmea.

À direita do console Canute, de trás para a frente:

 - 1 conector de alimentação fêmea.

 - 1 conector de áudio fêmea de 35 mm.

 - 1 botão para controle do monitor externo.

 - 2 interruptores para controle do monitor externo.

 - 1 conector HDMI fêmea.

Na parte traseira, do meio para a direita:

 - 1 painel removível para portas adicionais futuras.

 - 1 interruptor de energia.

## Conectando tudo

Depois de certificar-se de que o USB-B está conectado ao Canute 360 e imediatamente após inserir o cabo de alimentação, pressione o botão de alimentação na parte traseira direita para ligar a tela em Braille.

## Desconectando

Depois de desligar o Console da Canute (veja abaixo), aguarde até que a tela da Canute seja desligada e, em seguida, remova o cabo de alimentação. Observe que, se você não remover o cabo de alimentação, o monitor ainda terá energia e entrará no modo de proteção de tela. Portanto, um console Canute que não esteja em uso deve ser sempre desconectado.

## Layout do teclado

Supondo que você esteja usando o teclado britânico (caso contrário, consulte abaixo "Alteração do layout do teclado"), o layout do teclado é o seguinte:

Primeira linha: Escape, F1 a F10 (F11 é Fn+F1, F12 é FN+F2), Num lock, Print Screen, Delete (Insert é Fn+Del)

As linhas dois, três, quatro e cinco são o padrão para teclados Qwerty britânicos.

Linha seis: Controle (ctrl), Função (Fn), Super tecla, Alt, Espaço, Alt, Ctrl, Setas (Home é Fn+ESQUERDA, End é Fn+DIREITA, Page up é Fn+UP, Page down é Fn+DOWN)

## Aguarde a exibição do Canute...

Quando estiver pronto para ser usado (aproximadamente um minuto), ele exibirá:
`Controlador do Raspbian Canute ...
... login raspberrypi: _`

## Efetuando login

O usuário padrão é "pi", a senha padrão é "raspberry", tudo em letras minúsculas.

Parabéns, agora você está pronto para usar o Console da Canute!

É altamente recomendável alterar a senha o mais rápido possível e anotá-la.

Para alterar a senha, digite `$ passwd` e siga as instruções.

Comandos de teclas iniciais a serem inseridos:

`pi` (Nome de usuário.)

`raspberry` (Senha padrão.)

Aguarde até que esteja conectado...

### Solução de problemas

Você está tendo dificuldades para fazer login, por exemplo, está sendo levado de volta à página de login? Isso pode acontecer com as versões mais antigas do software do Console, mas foi corrigido posteriormente com as atualizações. É claro que, para aplicar a atualização, você terá que fazer login primeiro. Portanto, pressione esta combinação de teclas para alternar para um terminal de login diferente:

`ctrl+alt+f2`

Agora repita as instruções de login acima. Uma vez conectado, siga o procedimento de atualização abaixo (procure por "Upgrading").

## Noções básicas para navegar na linha de comando do Linux

### Nomenclatura e layout da linha de comando

Linux é o sistema operacional que o Canute Console está executando. Ele está executando especificamente uma versão do Linux chamada "Raspian", que é baseada no "Debian".

O Canute Console é pré-configurado para ser executado inteiramente a partir da linha de comando, o que significa que ele não tem uma interface gráfica de usuário.

Você verá com frequência os termos "linha de comando" ou "terminal" (também "tty" e console") usados de forma intercambiável on-line. 

Todos eles são muito semelhantes e frequentemente usados de forma intercambiável. Mas, em resumo, aqui estão suas diferenças:

 - A "linha de comando" geralmente é a última linha da tela e é onde os comandos podem ser escritos. A linha de comando inclui um prompt, ou seja, "onde você digita seus comandos".

 - A "linha de comando" está dentro de um "terminal". O terminal inclui o histórico dos comandos anteriores que você digitou, além da saída deles.

Ao longo deste documento, estamos usando instruções que começam com um cifrão, depois uma instrução e, às vezes, algumas outras palavras ou números separados por um espaço.

É assim que os comandos são executados na linha de comando do Linux. É muito semelhante ao prompt do DOS no Windows.

O cifrão é apenas um símbolo que significa "prompt de comando", ou seja, "digite seus comandos depois disso".

### Como digitar comandos e entender a saída do terminal

Portanto, quando escrevemos, por exemplo, uma instrução como "Run `$ ls`", significa "Digite 'ls' na linha de comando". Nesse caso, ela realmente significa "liste os subdiretórios e arquivos" nos meus diretórios atuais.

Como outro exemplo, podemos escrever neste documento, `$ cd demos` significa "Execute o comando 'cd' com a instrução (chamada de argumento) 'demos'". Na verdade, nesse caso, significa "vá para o diretório chamado 'demos'".

É importante observar que o terminal armazena todos os seus comandos anteriores e não os apaga da tela, a menos que seja solicitado.

Pense nele como uma máquina de escrever ou um gravador, escrevendo uma linha de cada vez, sem apagar as linhas anteriores.

Sempre leia até a última linha da tela e presuma que a *última linha que tem o prompt do dólar* é a linha de comando ativa e as linhas acima são o histórico.

Por exemplo, se eu quisesse digitar um comando como "criar um diretório chamado 'documents', que deveria ser escrito como `mkdir documents`, mas cometesse um erro, a saída do visor em Braille poderia ser a seguinte

`$ mjdir documents`
`bash: mjdir: comando não encontrado`
`$`

Vamos dar uma olhada nisso novamente, linha por linha. 

`$ mjdir documents`

Eu cometi um erro de digitação com o comando.

`bash: mjdir: comando não encontrado `

Esta é a mensagem de erro ("bash" é o que chamamos de "shell", ou seja, o programa que interpreta seus comandos).

`$ `

Portanto, agora temos outro prompt de comando em branco, mas observe que ainda podemos ver nossa primeira tentativa e a mensagem de erro acima.

Aqueles que estão acostumados com telas Braille de linha única talvez precisem se acostumar com isso. No entanto, esse é o comportamento padrão do terminal Linux e exatamente o mesmo layout e saída que os usuários com visão obtêm no monitor visual.

Se a sua tela estiver muito cheia, o comando `$ clear` é seu amigo. Ele redefinirá o terminal para um espaço em branco que não seja o prompt de comando atual na linha superior da tela.

### Como se locomover

O Linux usa um sistema de arquivos hierárquico, o que significa que os diretórios e arquivos estão todos dentro de outros diretórios, voltando à "raiz". A raiz está no topo da hierarquia.

Os diretórios são separados por símbolos de traço, portanto, seu diretório pessoal é escrito como `/home/pi/`

Ao fazer login, você começará no diretório inicial do seu usuário. Para começar, você raramente precisará ir "para cima" além do seu diretório pessoal.

O `$ ls` lista os subdiretórios e arquivos no diretório atual.

Talvez você ache que essa lista é muito longa para ser lida. Você pode voltar para cima e para baixo no histórico de saída do terminal pressionando `ctrl+Fn+UP` ou `ctrl+Fn+DOWN`.

Para mover-se entre diretórios, use o comando `cd`. `$ cd directoryname`.

Para voltar um nível abaixo, use o comando `$ cd ..`.

Aqui observamos um aspecto importante sobre muitos comandos do Linux, como o `cd`. Quando você pede para entrar em um diretório, ele o faz, mas não fornece nenhum resultado que indique se o fez. 

Então, como saber onde você está? Você pode digitar `$ pwd`. Isso retorna sua localização atual no sistema de arquivos. 

### Dicas para facilitar a navegação e a digitação de comandos

Você pode observar que, para alguns comandos, a string de saída é mais longa do que a tela (40 células), portanto, o Canute foi deslocado totalmente para a direita. 

Para se deslocar para a esquerda e para a direita no visor Braille (isso não afeta o visor visual) e visualizar cadeias de caracteres maiores que 40 células, pressione os botões de polegar '[MENU]+[FORWARD]' na parte frontal do visor do Canute para ir para a direita ou '[MENU]+[PREV]' para ir para a esquerda.

Você pode completar automaticamente um comando ou um diretório ou nome de arquivo quando estiver digitando no prompt de comando tocando na tecla tab.

Se houver apenas uma opção disponível, a tecla Tab a preencherá automaticamente. Se houver várias opções, pressionar a tecla tab não fará nada, mas tocar duas vezes na tecla tab exibirá todas as opções e as gravará no terminal para que você possa visualizá-las.

### Leitura de arquivos de texto

Se houver um arquivo de texto que você queira ler, digite `$ less textfilename.txt`. Ele não precisa ter uma extensão TXT e, muitas vezes, no Linux, eles não terão nenhuma extensão. Em caso de dúvida, tente usar o `less` nele.

O Less imprimirá as primeiras oito linhas do arquivo. Use a página para cima e para baixo (Fn+UP, Fn+DOWN) para se movimentar, pressione 'q' para voltar à linha de comando.

### Comandos úteis 

Incluir:

 * ls|less
   (Lista arquivos e subdiretórios)

 * tree|less
   (Exibe a árvore de diretórios)

 * cd [DIRECTORYNAME]
   (Ir para o subdiretório)

 * cd ...
   (Vai até o diretório pai).

 * mkdir [DIRNAME]
   (Criar novo diretório)

 * micro [FILENAME]
   (Abrir arquivo de texto)

 * [ctrl]+[shift]+t
   (Abre uma nova guia no terminal).

 * [ctrl]+[shift]+[NUM] (0--9)
   (Alterna para outra guia).

 * shutdown now
   (Desligar, mas ainda é necessário 
   desligar o Canute depois que o desligamento 
   concluído).

### Comandos especiais para códigos Braille

Há alguns comandos especiais para o 
o Canute Console, incluindo:

 * $ [alt]+[shift]+Q
   (Grade One UEB)

 * $ [alt]+[shift]+W
   (Grade Two UEB)

 * $ [alt]+[shift]+E 
   (9 linhas e 6 pontos US Computer Brl

 * $ [alt]+[shift]+R 
   (Alemão contratado, padrão 2015)

Observe que somente o US computer brl garante a representação de um caractere para um e, portanto, exatamente o mesmo layout da tela visual.

Avançado: Se você souber o nome de uma tabela Braille BRLTTY diferente que deseja usar, poderá inseri-lo com:
`$ canute-table [table-name]`

## Escape!

Agora que já sabemos o básico, os comandos mais importantes são fechar programas e encerrar o computador com segurança.

Para interromper um programa que está em execução, geralmente há comandos específicos para esse programa. Geralmente, a letra `q` é usada para sair ou a tecla `exit` (canto superior esquerdo do teclado do console). Entretanto, pressionar `ctrl+c` geralmente funciona em situações extremas (se um programa estiver suspenso ou se você não souber como fechá-lo). 

## Desligamento

Para desligar a unidade inteira, digite `$ shutdown now`. (Ou, para reinicializar, digite `$ reboot`).

Após o desligamento e o desligamento da tela do Canute*, você deverá retirar o cabo de alimentação do console (caso contrário, o monitor permanecerá ligado)

* N.B. Se você tiver atualizado o Canute 360 que vem com o console para a versão de firmware 2.0.10 ou superior, o Canute será desligado quando o console for desligado. No entanto, se estiver executando um firmware mais antigo, ele não se desligará sozinho, mas entrará no modo de leitura de livros autônomo. Nesse momento, será necessário pressionar o botão liga/desliga na parte traseira direita para desligar a tela do Canute antes de retirar o cabo de alimentação.

## Recursos de aprendizado

Sugerimos que você ouça os seguintes episódios do Braillecast, patrocinado pela Bristol Braille, para familiarizar as pessoas com alguns dos conceitos básicos da linha de comando do Linux.

Todos os episódios podem ser encontrados em:
`cd ~/learn/braillists/`
e podem ser reproduzidos com:
`vlc 01[tab to complete]`

 - 01: Primeiramente, uma visão geral de diferentes cientistas da computação que utilizam o Braille em seu trabalho. Isso pode ser pulado.

 - 02: Em segundo lugar, uma introdução à linha de comando do Linux.

 - 03: Em terceiro lugar, uma introdução ao Git, para controle de versão, compartilhamento e download de programas.

 - 04: Em quarto lugar, uma aula magistral sobre programação no Canute Console.

O Canute Console inclui algumas ferramentas básicas de aprendizado do Linux. Você pode encontrá-las no diretório home digitando `cd learn`.
Por fim, você geralmente pode procurar respostas para qualquer pergunta que tenha na Web. 

A maioria das perguntas que você tiver será geral sobre o Linux. No entanto, se precisar de respostas mais específicas, você pode tentar pesquisar por "Debian" ou até mesmo especificamente por "Raspian".

Recomendamos um site chamado Stackoverflow para esses tipos de perguntas.

## Som

Observe os comandos abaixo. O s duplo em sset não é um erro de digitação.

 - `$ amixer sset Master 0%`

 - `$ amixer sset Master 100%`

## Alteração do layout do teclado

O comando é relativamente simples na linha de comando. Por exemplo:

`$ setxkbmap us`

Alterará o teclado para o dos EUA. 

Para obter uma lista dos layouts de teclado disponíveis, digite:

`$ ls /usr/share/X11/xkb/symbols/ | less`

Para defini-lo automaticamente, adicione o mesmo comando ao `~/console-set-up/console-start.sh`:

`$ setxkbmap us`

## Conexão com a Internet

### WiFi

O Console da Canute se conectará automaticamente a qualquer rede WiFi que tenha sido salva. 

Para escolher entre essas redes (se houver mais de uma), digite `$ wifi`

Para se conectar a uma nova rede pela primeira vez, digite `$ wifi new`

#### Solução de problemas

Se você receber o erro `Unable to connect ... Os segredos eram necessários, mas não foram fornecidos", tente executar os dois comandos a seguir:

`$ nmcli con delete "NAME OF SSID"`
`$ nmcli dev wifi connect "NOME DO SSID" password PASSWORD ifname wlan0`

### Tethering para o seu celular via USB-A

Usando um telefone Android, conecte o telefone ao console da Canute por meio de um cabo USB. Nas notificações do seu telefone, aparecerá algo como "Charging from USB" (Carregando por USB). Toque nessa notificação e ela o levará ao menu de configurações de USB. Altere "Charging only" (Somente carregamento) para "Tether" (os termos exatos podem mudar). O console agora terá acesso à Internet. 

Ao fazer tethering, essa conexão será usada em vez da conexão WiFi, mesmo que não seja tão rápida.

### Testando a conexão com a Internet

Teste a Internet no Console digitando `$ ping bbc.com -i 4`. Você deverá obter um registro de várias saídas continuamente atualizadas. 

(A parte `-i 9` desse comando é para dizer ao sistema para executar o ping nos servidores da BBC uma vez a cada quatro segundos).

Pressione `[ctrl]+c` para cancelar o programa e obter os resultados.

## Fazendo upgrade para a versão mais recente do sistema operacional do console

Uma vez conectado à Internet, execute o seguinte comando:

`$ curl -L https://bristolbraille.org/console-install | bash`

A atualização pode levar algum tempo, dependendo da velocidade da sua conexão e do tamanho dos aplicativos de demonstração que estão sendo baixados (que variam entre as versões).

Após a conclusão, efetue o logout: `$ exit`

Em seguida, faça login novamente.

## Exemplos de aplicativos personalizados desenvolvidos para o console Canute

### Sonic Pi

Instale:

`$ git clone https://gitlab.com/bristol-braille/sonic-pi-canute/`

Veja os exemplos a seguir de como executar e editar programas do Sonic Pi usando algumas ferramentas que desenvolvemos ou escrevemos scripts para o Console:

`$ cd sonic-pi-canute` (Vá para o diretório `sonic-pi-canute`).

`$ micro examples.txt` (Veja --- e edite, se quiser --- o código de exemplo do Sonic Pi).

Agora você estará no editor de texto chamado `micro`. Movimente-se por ele, salve, desfaça etc. usando os comandos normais das teclas do bloco de notas. Para sair, pressione `ctrl+q` e digite `y` ou `n` para salvar ou não as alterações.

De volta à linha de comando...

`$ bash start-sonic-pi.sh` (Iniciará o programa gráfico do Sonic Pi e o minimizará para que possamos continuar a trabalhar com a linha de comando. Bash é um programa que executa os chamados scripts `shell`, que geralmente terminam em `.sh`).

Uma vez de volta à linha de comando, o que levará cerca de 30 segundos...

`$ bash play-sonic.sh examples.txt` (Um script que passa o arquivo `examples.sh` para o Sonic Pi. Você perceberá que os sons estão tocando).

`$ bash stop-sonic.sh` (Interrompe qualquer coisa que esteja sendo reproduzida pelo Sonic Pi).




## Avançado

### Visualização remota do terminal de outra pessoa

Para trabalhar com outro colega ou acompanhar um tutor, talvez você queira acompanhar a sessão de terminal on-line usando uma ferramenta como [`tty-share`](https://tty-share.com/).

### Controle remoto do seu console

Pode ser sensato permitir que outro usuário remoto, como a equipe da BBT, tenha o controle do console nos horários que você escolher. Para isso, recomendamos o uso do [`mosh`] (https://mosh.org/), que é semelhante ao `ssh`, mas com melhor estabilidade na conexão.

### Desenvolvendo aplicativos para o Canute Console

Esse é um tópico importante que abordaremos em um documento separado e no qual estamos trabalhando ativamente. 

Por enquanto, a explicação mais simples é a seguinte:

Qualquer coisa que você possa desenvolver para a linha de comando visual, em qualquer linguagem, que seja executada no Linux e caiba em 40x9, será executada no Canute Console. 

No entanto, há algumas considerações a serem feitas:
 - Para garantir a paridade de 1 para 1 do layout entre as telas visual e Braille, recomendamos desenvolver o programa para usar o código Braille do computador. As tabelas em braile contratadas não respeitarão os espaços em branco horizontais.
 - O Canute 360s é atualizado a 0,5-1 segundo por linha, portanto, os aplicativos devem evitar a rolagem constante.
 [NPYScreen](https://github.com/npcole/npyscreen/) pode ser uma maneira útil de criar rapidamente produtos visuais e táteis espaciais atraentes a partir de comandos bash e Python simples.


### Aplicativos que não cabem em 9 linhas

 - Execute `$ screen` e pressione enter.

 - Pressione "enter" para pular a parte do leia-me.

 - Pressione '[ctrl]+a' e, em seguida, pressione ':' (dois pontos). Isso permite que você digite comandos, que aparecerão na 9ª linha do visor (ele mostrará dois pontos no canto inferior esquerdo)

 - (É possível pular esta etapa.) Digite `width -w 40` e pressione Enter. Isso define a largura como 40 (você pode alterar esse número se o aplicativo que estiver usando precisar ser mais largo, embora este passo a passo presuma que não seja necessário, portanto, você provavelmente pode pular esta etapa).

 - Pressione '[ctrl]+a' e, em seguida, pressione ':' (dois pontos) novamente para digitar outro comando.

 - Digite `height -w 17` e pressione Enter. Isso torna a altura do terminal mais alta, neste caso, 17 linhas em vez de 9. Você pode escolher qualquer outro número, não precisa ser um múltiplo de 9 linhas.

 	* (Como exemplo de uso, definir a altura para 27 linhas tornaria o terminal três vezes mais alto que a tela Canute, ou seja, do mesmo tamanho que uma página Braille em papel padrão do Reino Unido).

 - Para mover-se pelo terminal extragrande, pressione '[ctrl]+a' e, em seguida, pressione '['. Isso entra no que é chamado de "modo de cópia", mas vamos usá-lo para nos movimentarmos.

 - Agora o cursor (normalmente todos os seis pontos) se move livremente pelo terminal entre as linhas (normalmente ele permanece no prompt de comando).

 - Mova o cursor até a parte superior da tela, mantendo-o pressionado. Ele moverá todo o conteúdo para baixo.

 - Quando estiver mostrando a parte do aplicativo que deseja visualizar, pressione escape, o que sairá do "modo de cópia" e retornará ao modo normal de entrada de linha de comando.

________
(O texto acima foi extraído de uma pergunta no Stackoverflow, que você pode ler se quiser entender melhor, mas não é necessário: https://stackoverflow.com/questions/75316022/emulate-a-taller-terminal-within-gnome-terminal-and-pan-up-and-down)

