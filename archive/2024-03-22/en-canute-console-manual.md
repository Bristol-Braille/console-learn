# Canute Console Manual

## Notes

Last update: 2024-01-31

Caveat lector: This is a live document that is updated as more use cases for the Canute Console emerge. Please make suggestions about tips to include, how to make instructions clearer, and report or inaccuracies.

Translation note: All non-English translations are translations from the English using DeepL Pro. If you note translation errors please let us know about them.

Contact: enquiries@bristolbraille.org

Log an issue: [https://gitlab.com/Bristol-Braille/console-learn/-/issues](https://gitlab.com/Bristol-Braille/console-learn/-/issues)

## What is the Canute Console?

The Canute Console is a Linux workstation which includes a Canute 360 Braille display. 
The big difference between the Canute Console and other products is that the Braille and the visual displays show the same amount of information in the same layout, disadvantaging neither Braille nor print user.

It operates from the command line and is primarily intended for computer science. However there are few limits on what uses it can be put to once you become used to working in multiple lines of Braille.

The Canute Console is made up of two different electronics products: The Canute 360 Braille display and the Console Upgrade Kit. 

The Canute 360 sits inside the Canute Upgrade. 

## Inserting and removing the Canute 360 into the Console Upgrade Kit

Lay both products on a flat surface where you intend to use them.

Open the lid (the monitor) on the Console Upgrade Kit. Lift the Canute 360 and put it gently on the flat surface in the middle of the Console Upgrade Kit, being careful to line the edges up. 

At the back right there is a protrusion from the Upgrade Kit. This is the male power connector and needs to line up with the female power connector on the Canute 360. 

Once that lines up move the Canute 360 backwards to insert the power connectors. You may need to lift the Canute 360 slightly to make the connexion.

Now on the back left hand side on the Upgrade Kit you will feel a USB-B male connector (these are commonly called 'printer connectors') on a very short cable of about 2 inches. Pull it out of the hole it is resting in and insert it into the Canute 360's female USB-B port on the right.

To remove the Canute 360 do the same as above but in reverse.

## Ergonomics

The Canute Console is meant to be used on a stable, flat desk or table with a smooth surface.

It needs the smooth surface in order for the tray with the keyboard to be taken in and out.

Now lift the monitor (which also acts as a lid) and pull out the keyboard.

Because the Canute Console has a lot of Braille which you need to reach over a keyboard to read, ergonomics are especially important.

Therefore make sure your seat is as high as possible compared to the desk/table.

Also make sure the Console is comfortably towards the edge of the desk/table.

You should be able to comfortably rest your hands on both the top line of the Braille display and the last line of the keyboard.

## Port layout of the Canute Console

(Please note that when docked in the Console the Canute 360's ports are no longer in use.)

On the left of the Canute Console, from the back to the front: 

 - 1x male USB-B connector, which is either inside a small hole in the Console Upgrade Kit (when the Canute 360 is disengaged), or is inserted into the Canute 360's female USB-B port.

 - 3x female USB-A connectors.

On the right of the Canute Console, from back to front:

 - 1x female power connector.

 - 1x female 35.mm audio jack.

 - 1x rocker for controlling external monitor.

 - 2x switches for controlling external monitor.

 - 1x female HDMI connector.

On the back, from middle to right:

 - 1x removable panel for additional future ports.

 - 1x power switch.

## Plugging everything in

After making sure the USB-B is plugged into the Canute 360, and immediately after inserting the power cable, press the power button on the back right had side to turn on the Braille display.

## Unplugging

After shutting down the Canute Console (see below) wait for the Canute display to shut down, then remove the power cable. Note that if you do not remove the power cable the monitor will still have power and go into screen-saver mode. Therefore a Canute Console not in use should always be unplugged.

## Keyboard layout

Assuming you are using the British keyboard (otherwise please see below 'Changing the keyboard layout'), the layout of the keyboard is as follows:

First line: Escape, F1 to F10 (F11 is Fn+F1, F12 is FN+F2), Num lock, Print Screen, Delete (Insert is Fn+Del)

Lines two, three, four and five are the default for British Qwerty keyboards.

Line six: Control (ctrl), Function (Fn), Super key, Alt, Space, Alt, Ctrl, Arrows (Home is Fn+LEFT, End is Fn+RIGHT, Page up is Fn+UP, Page down is Fn+DOWN)

## Wait for the Canute display...

When it is ready to be used (approximately one minute) it will read:
`Raspbian Canute Controller ...
... raspberrypi login: _`

## Logging in

The default user is "pi", the default password is "raspberry", all lower-case.

Congratulations, you are now ready to use the Canute Console!

We strongly suggest changing the password as soon as possible and making a note it.

To change the password type `$ passwd` and follow the instructions.

Starting key commands to enter:

`pi` (Username.)

`raspberry` (Default password.)

Wait until logged in...

### Troubleshooting

Are you having difficulties logging in, such as being bounced back to the log in page? This can happen with the earliest releases of the Console software but has subsequently been fixed with upgrades. Of course, to apply the upgrade you will have to log in first. Therefore press this key combination to switch to a different login terminal:

`ctrl+alt+f2`

Now repeat the log in instructions above. Once in follow the upgrade procedure below (search for 'Upgrading').

## Basics for navigating the Linux command line

### Nomenclature and the layout of the command line

Linux is the operating system Canute Console is running. It is specifically running a version of Linux called 'Raspian', which is based on 'Debian'.

The Canute Console is prefigured to run entirely from the command line, meaning it does not have a graphical user interface.

You will often see the terms "command line" or "terminal" (also "tty" and console") used interchangeably online. 

They are all very similar and often used interchangeably. But, in short, here are their differences:

 - The "command line" is usually the last line on the screen and it is where commands can be written. The command line includes a prompt, meaning "where you type your commands".

 - The "command line" is inside a "terminal". The terminal includes the history of previous commands you have entered, plus their output.

Throughout this document we are using instruction that start with a dollar sign, then an instruction, then sometimes some other words or numbers separated by a space.

This is how commands are run in the Linux command line. It is very similar to the DOS Prompt on Windows.

The dollar sign is just a symbol meaning "command prompt", that is, "type your commands after this".

### How to type commands and understand the terminal output

So when we write, for example, an instruction like "Run `$ ls` means, "Type 'ls' into the command line". In that case it actually means, "list the sub-directories and files" in my current directories.

As another example we might write in this document, `$ cd demos` means "Run the command 'cd' with the instruction (called an argument) 'demos'". Actually in that case it means "go to directory called 'demos'."

It is important to note that the terminal stores all your previous commands and doesn't clear them off the screen unless asked to.

Think of it like a typewriter or embosser, writing one line at a time, not erasing the previous lines.

Always read to the last line of the display and assume the *last line that has the dollar prompt on it* is the active command line, and the lines above are history.

For example if I wanted to type a command like "make a directory called 'documents', which should be written as `mkdir documents`, but made a mistake, then the Braille display output might look like this:

`$ mjdir documents`
`bash: mjdir: command not found`
`$`

Lets look at that again, line by line. 

`$ mjdir documents`

I have made a typo with the command.

`bash: mjdir: command not found `

This is the error message ('bash' is what's called the 'shell', meaning the program that is interpretting your commands).

`$ `

So now we are given another blank command prompt, but note that we can still see our first attempt and the error message above.

Those used to single line Braille displays might find this takes some getting used to. But it is, however, the standard behaviour for the Linux terminal and precisely the same layout and output as sighted users are getting on the visual monitor.

If your screen is too cluttered then the `$ clear` command is your friend. It will reset the terminal to blank other than the current command prompt on the top line of the display.

### Getting around

Linux uses a hierarchical file system, meaning directories and files are all inside other directories, back to the 'root'. The root is at the top of the hierarchy.

Directories are separated by stroke symbols, so your home directory is written as, `/home/pi/`

When logging in you will start in your user's home directory. To start with you will seldom need to go 'up' beyond your home directory.

`$ ls` lists the sub-directories and files in the current directory.

You may find that outputs too long a list to read. You can scroll back up and down the terminal output history by pressing `ctrl+Fn+UP` or `ctrl+Fn+DOWN`.

To move between directories you use the `cd` command. `$ cd directoryname`.

To go back down a level use the `$ cd ..` command.

Here we note an important thing about many Linux commands, like `cd`. When you ask to go into a directory, it will do it but not give you any output to say whether it has done it. 

So how do you know where you are? You can type `$ pwd`. This returns your current location within the file system. 

### Tips to make navigation and typing commands easier

You may note that for some commands the output string is longer than the display (40 cells), therefore the Canute has panned all the way to the right. 

To pan left and right on the Braille display (this doesn't affect the visual display) to view strings longer than 40 cells you can press the '[MENU]+[FORWARD]' thumb buttons on the front of the Canute display to go right, or '[MENU]+[PREV]' to pan left.

You can auto-complete a command or a directory or file name when you are typing into the command prompt by tapping the tab key.

If there is only one available option then tab will auto fill it. If it could be multiple things then pressing tab won't do anything, but double tapping tab will bring up all the options and write them the the terminal so you can view them.

### Reading text files

If there is a text file you want to read you can type, `$ less textfilename.txt`. It doesn't have to have a TXT extension and often, in Linux, they won't have any extension at all. If in doubt, try `less` on it.

Less will print the first eight lines of the file. Use the page up and down (Fn+UP, Fn+DOWN) to move around it, press 'q' to exit back to the command line.

### Useful commands 

Include:

 * ls|less
   (Lists files and sub-directories)

 * tree|less
   (Displays directory tree)

 * cd [DIRECTORYNAME]
   (Go to sub-directory)

 * cd ..
   (Go up to parent directory.)

 * mkdir [DIRNAME]
   (Make new directory)

 * micro [FILENAME]
   (Open text file)

 * [ctrl]+[shift]+t
   (Open's new tab in terminal.)

 * [ctrl]+[shift]+[NUM] (0--9)
   (Switches to another tab.)

 * shutdown now
   (Shutdown, but still need to 
   turn off Canute after shutdown 
   completes.)

### Special commands for Braille codes

There are some special commands for 
the Canute Console, including:

 * $ [alt]+[shift]+Q
   (Grade One UEB)

 * $ [alt]+[shift]+W
   (Grade Two UEB)

 * $ [alt]+[shift]+E 
   (9-line 6-dot US Computer Brl

 * $ [alt]+[shift]+R 
   (Contracted German, 2015 standard)

Please note that only US computer brl guarantees one-to-one character representation and therefore exactly the same layout as the visual screen.

Advanced: If you know the name of a different BRLTTY Braille table you want to use then you can enter that with:
`$ canute-table [table-name]`

## Escape!

Now we know the basics, the most important commands are closing programs and shutting down safely.

To stop a program that is running there are often commands specific to that program. Often the letter `q` is used to exit, or the `exit` key (top left on the Console keyboard). However pressing `ctrl+c` often works in extremis (if a program is hanging or you don't know how to close it). 

## Shutdown

To shutdown the whole unit type, `$ shutdown now`. (Or to reboot type, `$ reboot`.)

Once you have shutdown and the Canute display has turned off* you should take the power cable out of the Console (otherwise the monitor will stay on)

* N.B. If you have upgraded your Canute 360 that comes with the Console to firmware version 2.0.10 or higher then your Canute will turn off when the Console turns off. However if it running an older firmware then it will not turn off on its own, it will instead go into stand-alone book reading mode, at which point you need to press the power button on the back right to turn the Canute display off before you take the power cable out.

## Learning resources

We suggest you listen to the following episodes of Braillecast, sponsored by Bristol Braille, to familiarise people with some of the basics of the Linux command line.

All episodes can be found in:
`cd ~/learn/braillists/`
and can be played with:
`vlc 01[tab to complete]`

 - 01: Firstly, an overview of different computer scientists using Braille in their work. This can be skipped.

 - 02: Secondly, a masterclass introduction to the Linux command line.

 - 03: Thirdly, a masterclass introduction to Git, for version control and sharing and downloading programs.

 - 04: Fourthly, a masterclass about programming on the Canute Console.

Included with your Canute Console are a few Linux basics learning tools. You can find them from the home directory by typing `cd learn`.
Finally, you can usually search for answers to any questions you have on the web. 

Most questions you have will be general to Linux. However if you need more specific answers then you can try searching for 'Debian', or even specifically for 'Raspian'.

We recommend a website called Stackoverflow for these kinds of questions.

## Sound

Please note the commands below. The double s in sset is not a typo.

 - `$ amixer sset Master 0%`

 - `$ amixer sset Master 100%`

## Changing the keyboard layout

The command is relatively simple from the command line. For example:

`$ setxkbmap us`

Will change it to the US keyboard. 

For a list of available keyboard layouts type:

`$ ls /usr/share/X11/xkb/symbols/ | less`

To set it automatically add the same command to `~/console-set-up/console-start.sh`:

`$ setxkbmap us`

## Connecting to the Internet

### WiFi

The Canute Console will automatically connect to any WiFi network it has saved. 

To choose between these networks (if there is more than one) type, `$ wifi`

To connect to a new network for the first time type, `$ wifi new`

#### Troubleshooting

If you get the error, `Unable to connect ... Secrets were required but not provided`, then try running the folling two commands:

`$ nmcli con delete "NAME OF SSID"`
`$ nmcli dev wifi connect "NAME OF SSID" password PASSWORD ifname wlan0`

### Tethering to your mobile via USB-A

Using an Android phone, plug your phone in to the Canute Console by USB cable. On your phone notifications it will now say something like, "Charging from USB". Tap that notification and it will take you to the USB settings menu. Change "Charging only" to "Tether" (exact terms may change). The Console will now have access to the Internet. 

When tethering this connexion will be used instead of the WiFi connexion, even if it is not as fast.

### Testing internet connection

Test Internet in the Console by typing, `$ ping bbc.com -i 4`. You should get a log of lots of outputs continuously updating. 

(The `-i 9` part of that command is to tell it to ping the BBC servers once every four seconds.)

Press `[ctrl]+c` to cancel the program and get the results.

## Upgrading to the latest Console OS version

Once connected to the Internet run the following command:

`$ curl -L https://bristolbraille.org/console-install | bash`

The upgrade may take some time, depending on your connexion speed and the size of the demonstration applications being downloaded (which vary between versions).

Once completed log out: `$ exit`

Then log back in again.

## Example custom applications developed for the Canute Console

### Sonic Pi

Install:

`$ git clone https://gitlab.com/bristol-braille/sonic-pi-canute/`

See the following examples of how to run and edit Sonic Pi programs using some tools we developed or wrote scripts around for the Console:

`$ cd sonic-pi-canute` (Go into the `sonic-pi-canute` directory.)

`$ micro examples.txt` (Look at --- and edit if you want to --- the example Sonic Pi code.)

Now you will be in the text editor called `micro`. Move around it, save, undo etc using the normal notepad style key commands. To exit hit `ctrl+q` and type `y` or `n` to save or not save any changes.

Once back at the command line...

`$ bash start-sonic-pi.sh` (Will start the Sonic Pi graphical program and then minimise it so we can continue to work from the command line. Bash is a program that runs so-called `shell` scripts, which often end in `.sh`.)

Once back in the command line, which will take about 30 seconds...

`$ bash play-sonic.sh examples.txt` (A script which passes the `examples.sh` file to Sonic Pi. You will notice the sounds playing.)

`$ bash stop-sonic.sh` (Stop anything being played by Sonic Pi.)




## Advanced

### Remote viewing someone else's terminal

For the purposes of working with another colleague, or following a tutor, you may want to follow their terminal session online using a tool like [`tty-share`](https://tty-share.com/).

### Remote control of your Console

It may be sensible to allow another remote user, such as BBT staff, to have control of the Console at times of your choosing. For this we recommend using [`mosh`](https://mosh.org/), which is similar to `ssh` but better stability in the connexion.

### Developing applications for the Canute Console

This is a big topic which we will cover in a separate document and is being actively worked on. 

For now the simplest explanation is this:

Anything you can develop for the visual command line, in any language, which runs on Linux and fits into 40x9, will run on the Canute Console. 

There are certain considerations however:
 - To ensure 1-to-1 parity of layout between the visual and Braille displays we recommend developing the program to use computer Braille code. Contracted Braille tables will not respect horizontal whitespace.
 - Canute 360s refresh at 0.5--1 seconds per line, therefore applications should avoid constant scrolling.
 [NPYScreen](https://github.com/npcole/npyscreen/) can be useful way to quickly make attractive spacial tactile and visual products out of simple bash and Python commands.


### Apps that don't fit 9 lines

 - Run `$ screen` and press enter.

 - Press 'enter' to skip the readme stuff.

 - Press '[ctrl]+a' then press ':' (colon). This lets you type commands, which will appear on the 9th line of the display (it will show a colon on the bottom left corner)

 - (Can skip this stage.) Type `width -w 40` and press enter. This sets the width to 40. (You can change that number if the application you are using needs to be wider, though this walkthough assumes it doesn't need to be so you can probably skip this stage.)

 - Press '[ctrl]+a' then press ':' (colon) again to type another command.

 - Type `height -w 17` and press enter. This makes the terminal height taller, in this case 17 lines high rather than 9. You can pick any other number, it doesn't have to be a multiple of 9 lines.

 	* (As example usage, setting the height to 27 lines would make the terminal three times as tall as the Canute display, so the same size as a UK standard paper Braille page.)

 - To move around the extra large terminal press '[ctrl]+a' then press '['. This enters what is called "copy mode", but we are going to use it to move around.

 - Now the cursor (typically all six dots) moves freely around the terminal between lines (usually it stays on the command prompt).

 - Move the cursor all the way to the opt of the screen by just holding it down. It will move all the content down.

 - When you are showing the part of the application you want to view, press escape, this will then leave "copy mode" and return to normal command line entry mode.

________
(The above is taken from a question on Stackoverflow which you can read if you want to understand more, but it is not necessary to: https://stackoverflow.com/questions/75316022/emulate-a-taller-terminal-within-gnome-terminal-and-pan-up-and-down)

