# Canute-Konsole Handbuch

## Anmerkungen

Letzte Aktualisierung: 2024-01-31

Vorbehaltlich der Lektüre: Dies ist ein aktuelles Dokument, das aktualisiert wird, sobald weitere Anwendungsfälle für die Canute-Konsole auftauchen. Bitte machen Sie Vorschläge zur Aufnahme von Tipps, um die Anweisungen klarer zu gestalten, und melden Sie Ungenauigkeiten.

Hinweis zur Übersetzung: Alle nicht-englischen Übersetzungen sind Übersetzungen aus dem Englischen mit DeepL Pro. Wenn Sie Übersetzungsfehler bemerken, teilen Sie uns diese bitte mit.

Kontakt: enquiries@bristolbraille.org

Melden Sie ein Problem: [https://gitlab.com/Bristol-Braille/console-learn/-/issues](https://gitlab.com/Bristol-Braille/console-learn/-/issues)

## Was ist die Canute-Konsole?

Die Canute Console ist eine Linux-Workstation, die eine Canute 360 Braillezeile enthält. 
Der große Unterschied zwischen der Canute Console und anderen Produkten besteht darin, dass die Braillezeile und die visuelle Anzeige die gleiche Menge an Informationen im gleichen Layout anzeigen, wodurch weder Braille- noch Print-Benutzer benachteiligt werden.

Sie wird über die Befehlszeile bedient und ist in erster Linie für die Informatik gedacht. Allerdings sind den Einsatzmöglichkeiten kaum Grenzen gesetzt, wenn man sich erst einmal daran gewöhnt hat, in mehrzeiliger Brailleschrift zu arbeiten.

Die Canute-Konsole besteht aus zwei verschiedenen elektronischen Produkten: Die Braillezeile Canute 360 und das Konsolen-Upgrade-Kit. 

Die Canute 360 befindet sich im Inneren des Canute Upgrade. 

## Einsetzen und Entfernen der Canute 360 in das Konsolen-Upgrade-Kit

Legen Sie beide Produkte auf eine ebene Fläche, auf der Sie sie verwenden möchten.

Öffnen Sie den Deckel (den Monitor) des Konsolen-Upgrade-Kits. Heben Sie den Canute 360 an und legen Sie ihn vorsichtig auf die ebene Fläche in der Mitte des Konsolen-Upgrade-Kits, wobei Sie darauf achten müssen, dass die Kanten in einer Linie liegen. 

Hinten rechts ragt ein Teil des Upgrade-Kits heraus. Dabei handelt es sich um den Netzstecker, der mit der Netzbuchse des Canute 360 übereinstimmen muss. 

Sobald dies der Fall ist, bewegen Sie den Canute 360 nach hinten, um die Stromstecker einzuführen. Möglicherweise müssen Sie den Canute 360 leicht anheben, um die Verbindung herzustellen.

Auf der hinteren linken Seite des Upgrade-Kits befindet sich ein USB-B-Stecker (im Allgemeinen als "Druckerstecker" bezeichnet) an einem sehr kurzen Kabel von etwa 5 cm Länge. Ziehen Sie ihn aus dem Loch, in dem er steckt, und stecken Sie ihn in die USB-B-Buchse des Canute 360 auf der rechten Seite.

Um den Canute 360 zu entfernen, gehen Sie wie oben beschrieben vor, jedoch in umgekehrter Reihenfolge.

## Ergonomie

Die Canute-Konsole sollte auf einem stabilen, flachen Schreibtisch oder Tisch mit einer glatten Oberfläche verwendet werden.

Die glatte Oberfläche ist erforderlich, damit das Tablett mit der Tastatur hinein- und herausgenommen werden kann.

Heben Sie nun den Monitor an (der auch als Deckel dient) und ziehen Sie die Tastatur heraus.

Da die Canute-Konsole viel Blindenschrift enthält, die Sie zum Lesen über eine Tastatur greifen müssen, ist die Ergonomie besonders wichtig.

Achten Sie deshalb darauf, dass Ihr Sitz im Vergleich zum Schreibtisch/Tisch möglichst hoch ist.

Achten Sie auch darauf, dass die Konsole bequem an der Tischkante steht.

Sie sollten Ihre Hände sowohl auf der oberen Zeile der Braillezeile als auch auf der letzten Zeile der Tastatur bequem ablegen können.

## Anschlussbelegung der Canute-Konsole

(Bitte beachten Sie, dass die Anschlüsse des Canute 360 nicht mehr verwendet werden, wenn er an die Konsole angedockt ist).

Auf der linken Seite der Canute-Konsole, von hinten nach vorne: 

 - 1x USB-B-Stecker, der sich entweder in einem kleinen Loch im Konsolen-Upgrade-Kit befindet (wenn der Canute 360 abgekoppelt ist) oder in den USB-B-Buchsenanschluss des Canute 360 eingesteckt ist.

 - 3x USB-A-Buchsen.

Auf der rechten Seite der Canute-Konsole, von hinten nach vorne:

 - 1x Stromanschlussbuchse.

 - 1x 35-mm-Audiobuchse.

 - 1x Wippe zur Steuerung des externen Monitors.

 - 2x Schalter für die Steuerung des externen Monitors.

 - 1x HDMI-Buchse.

Auf der Rückseite, von der Mitte nach rechts:

 - 1x abnehmbare Platte für zusätzliche Anschlüsse.

 - 1x Netzschalter.

## Alles einstecken

Nachdem Sie sich vergewissert haben, dass der USB-B-Anschluss an die Canute 360 angeschlossen ist, und nachdem Sie das Netzkabel eingesteckt haben, drücken Sie den Netzschalter auf der Rückseite rechts, um die Braillezeile einzuschalten.

## Trennen der Verbindung

Nachdem Sie die Canute-Konsole heruntergefahren haben (siehe unten), warten Sie, bis die Canute-Anzeige heruntergefahren ist, und ziehen Sie dann das Netzkabel ab. Wenn Sie das Netzkabel nicht abziehen, wird der Monitor weiterhin mit Strom versorgt und geht in den Bildschirmschonermodus über. Daher sollte eine Canute-Konsole, die nicht benutzt wird, immer vom Netz getrennt werden.

## Tastaturlayout

Unter der Annahme, dass Sie die britische Tastatur verwenden (andernfalls siehe unten 'Ändern des Tastaturlayouts'), sieht das Layout der Tastatur wie folgt aus:

Erste Zeile: Escape, F1 bis F10 (F11 ist Fn+F1, F12 ist FN+F2), Num lock, Print Screen, Delete (Insert ist Fn+Del)

Die Zeilen zwei, drei, vier und fünf sind die Standardwerte für britische Qwerty-Tastaturen.

Zeile sechs: Steuerung (ctrl), Funktion (Fn), Supertaste, Alt, Leertaste, Alt, Strg, Pfeile (Home ist Fn+LINKS, Ende ist Fn+RECHTS, Seite hoch ist Fn+UP, Seite runter ist Fn+RUNTER)

## Warte auf die Canute-Anzeige...

Wenn es einsatzbereit ist (etwa eine Minute), wird es lauten:
`Raspbian Canute Controller ...
... raspberrypi login: _`

## Einloggen

Der Standardbenutzer ist "pi", das Standardpasswort ist "raspberry", alles Kleinbuchstaben.

Herzlichen Glückwunsch, Sie sind nun bereit, die Canute-Konsole zu benutzen!

Wir empfehlen dringend, das Passwort so bald wie möglich zu ändern und es zu notieren.

Um das Passwort zu ändern, geben Sie `$ passwd` ein und folgen Sie den Anweisungen.

Starten Sie mit der Eingabe von Tastaturbefehlen:

`pi` (Benutzername.)

`raspberry` (Standard-Passwort.)

Warten Sie, bis Sie eingeloggt sind...

### Fehlersuche

Haben Sie Schwierigkeiten bei der Anmeldung, z. B. wenn Sie zur Anmeldeseite zurückkehren? Dies kann bei den ersten Versionen der Konsolensoftware vorkommen, wurde aber durch Upgrades behoben. Um das Upgrade durchzuführen, müssen Sie sich natürlich zuerst anmelden. Drücken Sie daher diese Tastenkombination, um zu einem anderen Login-Terminal zu wechseln:

Strg+Alt+F2".

Wiederholen Sie nun die obigen Anweisungen zum Einloggen. Sobald Sie eingeloggt sind, folgen Sie der Upgrade-Prozedur unten (suchen Sie nach 'Upgrading').

## Grundlagen zur Navigation in der Linux-Kommandozeile

### Nomenklatur und Aufbau der Befehlszeile

Linux ist das Betriebssystem, auf dem Canute Console läuft. Genauer gesagt läuft darauf eine Linux-Version namens 'Raspian', die auf 'Debian' basiert.

Die Canute Console ist so vorkonfiguriert, dass sie ausschließlich über die Befehlszeile läuft, d. h. sie hat keine grafische Benutzeroberfläche.

Im Internet werden die Begriffe "Kommandozeile" oder "Terminal" (auch "tty" und "Konsole") oft synonym verwendet. 

Sie sind alle sehr ähnlich und werden oft austauschbar verwendet. Aber hier sind die Unterschiede in Kürze:

 - Die "Befehlszeile" ist in der Regel die letzte Zeile auf dem Bildschirm, in der Befehle eingegeben werden können. Die Befehlszeile enthält eine Eingabeaufforderung, d. h. "wo Sie Ihre Befehle eingeben".

 - Die "Befehlszeile" befindet sich innerhalb eines "Terminals". Das Terminal enthält die Historie der zuvor eingegebenen Befehle sowie deren Ausgabe.

In diesem Dokument werden Befehle verwendet, die mit einem Dollar-Zeichen beginnen, dann folgt ein Befehl, dann manchmal einige andere Wörter oder Zahlen, die durch ein Leerzeichen getrennt sind.

So werden Befehle in der Linux-Kommandozeile ausgeführt. Sie ist der DOS-Eingabeaufforderung unter Windows sehr ähnlich.

Das Dollar-Zeichen ist nur ein Symbol, das "Eingabeaufforderung" bedeutet, d. h. "geben Sie Ihre Befehle nach diesem Zeichen ein".

### Wie man Befehle eingibt und die Terminalausgabe versteht

Wenn wir also zum Beispiel eine Anweisung wie "Führen Sie `$ ls` aus" schreiben, bedeutet das: "Geben Sie 'ls' in die Befehlszeile ein". In diesem Fall bedeutet es eigentlich: "Listen Sie die Unterverzeichnisse und Dateien" in meinen aktuellen Verzeichnissen auf.

Als weiteres Beispiel könnten wir in diesem Dokument schreiben, `$ cd demos` bedeutet "Führe den Befehl 'cd' mit der Anweisung (Argument genannt) 'demos' aus". Tatsächlich bedeutet es in diesem Fall "Gehe zum Verzeichnis namens 'demos'".

Es ist wichtig zu wissen, dass das Terminal alle vorherigen Befehle speichert und sie nicht vom Bildschirm löscht, es sei denn, Sie werden dazu aufgefordert.

Stellen Sie sich das Terminal wie eine Schreibmaschine oder einen Drucker vor, der eine Zeile nach der anderen schreibt und die vorherigen Zeilen nicht löscht.

Lesen Sie immer bis zur letzten Zeile des Bildschirms und gehen Sie davon aus, dass die *letzte Zeile mit dem Dollar-Prompt* die aktive Befehlszeile ist, und die Zeilen darüber sind Geschichte.

Wenn ich zum Beispiel einen Befehl wie "Erstelle ein Verzeichnis namens 'documents', das als `mkdir documents` geschrieben werden sollte, aber einen Fehler gemacht habe, dann könnte die Ausgabe der Braillezeile wie folgt aussehen

`$ mjdir documents`
bash: mjdir: Befehl nicht gefunden
`$`

Schauen wir uns das noch einmal an, Zeile für Zeile. 

`$ mjdir documents`

Ich habe einen Tippfehler bei dem Befehl gemacht.

`bash: mjdir: Befehl nicht gefunden`

Dies ist die Fehlermeldung ("Bash" ist die so genannte "Shell", also das Programm, das Ihre Befehle interpretiert).

`$ `

Jetzt erhalten wir eine weitere leere Eingabeaufforderung, aber wir können immer noch unseren ersten Versuch und die obige Fehlermeldung sehen.

Für diejenigen, die an einzeilige Braillezeilen gewöhnt sind, ist das vielleicht etwas gewöhnungsbedürftig. Es ist jedoch das Standardverhalten des Linux-Terminals und entspricht genau dem Layout und der Ausgabe, die sehende Benutzer auf dem Bildschirm erhalten.

Wenn Ihr Bildschirm zu unübersichtlich ist, ist der Befehl `$ clear` Ihr Freund. Er setzt das Terminal bis auf die aktuelle Eingabeaufforderung in der obersten Zeile des Bildschirms auf leer zurück.

### Herumkommen

Linux verwendet ein hierarchisches Dateisystem, d.h. Verzeichnisse und Dateien befinden sich alle innerhalb anderer Verzeichnisse, zurück zum "Stamm". Die Wurzel steht an der Spitze der Hierarchie.

Verzeichnisse werden durch Strichsymbole getrennt, so dass Ihr Home-Verzeichnis wie folgt geschrieben wird: `/home/pi/`

Wenn Sie sich anmelden, starten Sie im Home-Verzeichnis Ihres Benutzers. Für den Anfang werden Sie nur selten über Ihr Home-Verzeichnis hinausgehen müssen.

`$ ls` listet die Unterverzeichnisse und Dateien im aktuellen Verzeichnis auf.

Sie werden feststellen, dass diese Liste zu lang ist, um sie zu lesen. Sie können mit `ctrl+Fn+UP` oder `ctrl+Fn+DOWN` in der Liste der Terminalausgaben nach oben und unten blättern.

Um zwischen Verzeichnissen zu wechseln, verwenden Sie den Befehl `cd`. `$ cd Verzeichnisname`.

Um eine Ebene tiefer zu gehen, verwenden Sie den Befehl `$ cd ..`.

Hier ist eine wichtige Sache über viele Linux-Befehle, wie `cd`, zu beachten. Wenn Sie darum bitten, in ein Verzeichnis zu gehen, wird der Befehl dies tun, aber Sie erhalten keine Ausgabe, die Ihnen sagt, ob er es getan hat. 

Wie können Sie also wissen, wo Sie sich befinden? Sie können `$ pwd` eingeben. Dies gibt Ihre aktuelle Position innerhalb des Dateisystems zurück. 

### Tipps, die die Navigation und die Eingabe von Befehlen erleichtern

Bei einigen Befehlen ist der Ausgabestring länger als die Anzeige (40 Zellen), weshalb der Canute ganz nach rechts geschwenkt ist. 

Um auf der Braillezeile nach links und rechts zu schwenken (dies hat keinen Einfluss auf die visuelle Anzeige), um Zeichenketten anzuzeigen, die länger als 40 Zellen sind, können Sie die Daumentasten '[MENU]+[VORWÄRTS]' auf der Vorderseite der Canute-Anzeige drücken, um nach rechts zu schwenken, oder '[MENU]+[PREV]', um nach links zu schwenken.

Sie können einen Befehl oder einen Verzeichnis- oder Dateinamen automatisch vervollständigen, wenn Sie in die Eingabeaufforderung tippen, indem Sie die Tabulatortaste drücken.

Wenn nur eine Option verfügbar ist, wird diese mit der Tabulatortaste automatisch ausgefüllt. Wenn es sich um mehrere Optionen handelt, bewirkt das Drücken der Tabulatortaste nichts, aber ein doppeltes Tippen auf die Tabulatortaste zeigt alle Optionen an und schreibt sie in das Terminal, damit Sie sie sehen können.

### Lesen von Textdateien

Wenn Sie eine Textdatei lesen wollen, können Sie "$ less textfilename.txt" eingeben. Die Datei muss keine TXT-Erweiterung haben, und unter Linux haben sie oft überhaupt keine Erweiterung. Im Zweifelsfall probieren Sie `less` aus.

Less gibt die ersten acht Zeilen der Datei aus. Benutzen Sie die Bildlauftasten (Fn+UP, Fn+DOWN), um sich in der Datei zu bewegen, und drücken Sie 'q', um wieder zur Befehlszeile zurückzukehren.

### Nützliche Befehle 

Einschließen:

 * ls|less
   (Listet Dateien und Unterverzeichnisse auf)

 * tree|less
   (Zeigt den Verzeichnisbaum an)

 * cd [VERZEICHNISNAME]
   (Wechselt in ein Unterverzeichnis)

 * cd ..
   (Wechselt zum übergeordneten Verzeichnis.)

 * mkdir [VERZEICHNISNAME]
   (Neues Verzeichnis erstellen)

 * micro [DATEINAME]
   (Textdatei öffnen)

 * [ctrl]+[shift]+t
   (Öffnet eine neue Registerkarte im Terminal.)

 * [ctrl]+[shift]+[NUM] (0--9)
   (Wechselt zu einer anderen Registerkarte.)

 * Jetzt herunterfahren
   (Herunterfahren, aber Sie müssen noch 
   Canute nach dem Herunterfahren ausschalten 
   abgeschlossen ist.)

### Spezielle Befehle für Braille-Codes

Es gibt einige spezielle Befehle für 
die Canute-Konsole, darunter:

 * $ [alt]+[shift]+Q
   (Grade One UEB)

 * $ [alt]+[Umschalt]+W
   (Grad Zwei UEB)

 * $ [alt]+[Umschalt]+E 
   (9-zeilige 6-Punkt-US-Computerschrift)

 * $ [alt]+[Umschalt]+R 
   (Vertragssprache Deutsch, Standard 2015)

Bitte beachten Sie, dass nur US Computer Brl eine Eins-zu-Eins-Darstellung der Zeichen garantiert und somit genau das gleiche Layout wie der visuelle Bildschirm.

Fortgeschrittene: Wenn Sie den Namen einer anderen BRLTTY-Braille-Tabelle kennen, die Sie verwenden möchten, können Sie diese mit eingeben:
`$ canute-table [table-name]`

## Escape!

Nachdem wir nun die Grundlagen kennen, sind die wichtigsten Befehle das Beenden von Programmen und das sichere Herunterfahren.

Um ein laufendes Programm zu beenden, gibt es oft spezielle Befehle für dieses Programm. Oft wird der Buchstabe `q` zum Beenden verwendet, oder die Taste `exit` (oben links auf der Konsolentastatur). In extremen Fällen (wenn ein Programm hängt oder Sie nicht wissen, wie Sie es beenden sollen) funktioniert jedoch auch die Tastenkombination `ctrl+c`. 

## Herunterfahren

Um die gesamte Einheit herunterzufahren, tippe `$ shutdown now`. (Oder um neu zu starten, tippe `$ reboot`.)

Sobald Sie das Gerät heruntergefahren haben und der Canute-Bildschirm sich ausgeschaltet hat, sollten Sie das Netzkabel aus der Konsole ziehen (sonst bleibt der Monitor eingeschaltet).

* Hinweis: Wenn Sie Ihren Canute 360, der mit der Konsole geliefert wird, auf Firmware-Version 2.0.10 oder höher aktualisiert haben, schaltet sich Ihr Canute aus, wenn sich die Konsole ausschaltet. Wenn er jedoch mit einer älteren Firmware läuft, schaltet er sich nicht von selbst aus, sondern geht in den eigenständigen Buchlesemodus über. In diesem Fall müssen Sie die Einschalttaste auf der rechten Rückseite drücken, um den Canute-Bildschirm auszuschalten, bevor Sie das Netzkabel herausziehen.

## Lernressourcen

Wir empfehlen Ihnen, sich die folgenden Episoden von Braillecast, gesponsert von Bristol Braille, anzuhören, um sich mit einigen der Grundlagen der Linux-Befehlszeile vertraut zu machen.

Alle Episoden sind zu finden in:
`cd ~/learn/braillists/`
und können abgespielt werden mit:
`vlc 01[tab to complete]`

 - 01: Zunächst ein Überblick über verschiedene Informatiker, die Braille bei ihrer Arbeit verwenden. Dies kann übersprungen werden.

 - 02: Zweitens, eine Einführung in die Linux-Befehlszeile in der Meisterklasse.

 - 03: Drittens, eine Einführung in Git, zur Versionskontrolle und zum Austausch und Herunterladen von Programmen.

 - 04: Viertens: Ein Meisterkurs über die Programmierung mit der Canute-Konsole.

Im Lieferumfang der Canute-Konsole sind einige Tools zum Erlernen der Linux-Grundlagen enthalten. Sie finden sie im Home-Verzeichnis, indem Sie `cd learn` eingeben.
Schließlich können Sie normalerweise im Internet nach Antworten auf Ihre Fragen suchen. 

Die meisten Fragen, die Sie haben, betreffen Linux im Allgemeinen. Wenn Sie jedoch spezifischere Antworten benötigen, können Sie versuchen, nach "Debian" oder sogar speziell nach "Raspian" zu suchen.

Wir empfehlen für diese Art von Fragen die Website Stackoverflow.

## Ton

Bitte beachten Sie die folgenden Befehle. Das doppelte s in sset ist kein Tippfehler.

 - `$ amixer sset Master 0%`

 - `$ amixer sset Master 100%`

## Ändern des Tastaturlayouts

Der Befehl ist von der Kommandozeile aus relativ einfach. Zum Beispiel:

`$ setxkbmap us`

Ändert die Tastatur auf die US-Tastatur. 

Für eine Liste der verfügbaren Tastaturbelegungen geben Sie ein:

`$ ls /usr/share/X11/xkb/symbols/ | less`

Um es automatisch zu setzen, fügen Sie den gleichen Befehl in `~/console-set-up/console-start.sh` ein:

`$ setxkbmap us`

## Verbinden mit dem Internet

## WiFi

Die Canute-Konsole verbindet sich automatisch mit jedem gespeicherten WiFi-Netzwerk. 

Um zwischen diesen Netzwerken zu wählen (falls es mehr als eines gibt), geben Sie `$ wifi` ein.

Um sich zum ersten Mal mit einem neuen Netzwerk zu verbinden, geben Sie "$ wifi new" ein.

#### Fehlersuche

Wenn Sie die Fehlermeldung `Unable to connect ... Secrets were required but not provided`, dann versuchen Sie die folgenden zwei Befehle auszuführen:

`$ nmcli con delete "NAME DER SSID"`
`$ nmcli dev wifi connect "NAME DER SSID" password PASSWORD ifname wlan0`

### Tethering mit Ihrem Mobiltelefon über USB-A

Schließen Sie Ihr Android-Telefon über ein USB-Kabel an die Canute-Konsole an. In den Benachrichtigungen Ihres Telefons steht nun etwas wie "Aufladen über USB". Tippen Sie auf diese Benachrichtigung und Sie gelangen zum USB-Einstellungsmenü. Ändern Sie "Nur Laden" in "Tether" (die genauen Begriffe können sich ändern). Die Konsole hat nun Zugriff auf das Internet. 

Beim Tethering wird diese Verbindung anstelle der WiFi-Verbindung verwendet, auch wenn sie nicht so schnell ist.

### Testen der Internetverbindung

Testen Sie die Internetverbindung in der Konsole, indem Sie "$ ping bbc.com -i 4" eingeben. Sie sollten ein Protokoll mit vielen sich ständig aktualisierenden Ausgaben erhalten. 

(Der Teil "-i 9" dieses Befehls bedeutet, dass die BBC-Server alle vier Sekunden angefunkt werden sollen).

Drücken Sie `[ctrl]+c`, um das Programm zu beenden und die Ergebnisse zu erhalten.

## Aktualisieren auf die neueste Version des Konsolen-Betriebssystems

Sobald Sie mit dem Internet verbunden sind, führen Sie den folgenden Befehl aus:

`$ curl -L https://bristolbraille.org/console-install | bash`

Das Upgrade kann einige Zeit in Anspruch nehmen, abhängig von der Verbindungsgeschwindigkeit und der Größe der heruntergeladenen Demo-Anwendungen (die von Version zu Version variieren).

Melden Sie sich nach Abschluss ab: `$ exit`

Melden Sie sich dann wieder an.

## Beispiele für benutzerdefinierte Anwendungen, die für die Canute-Konsole entwickelt wurden

### Sonic Pi

Installieren:

`$ git clone https://gitlab.com/bristol-braille/sonic-pi-canute/`

Die folgenden Beispiele zeigen, wie Sie Sonic Pi-Programme ausführen und bearbeiten können, indem Sie einige Tools verwenden, die wir für die Konsole entwickelt oder Skripte dafür geschrieben haben:

`$ cd sonic-pi-canute` (Gehen Sie in das Verzeichnis `sonic-pi-canute`.)

`$ micro examples.txt` (Sieh dir --- und editiere, wenn du willst --- den Sonic Pi Beispielcode an.)

Jetzt bist du im Texteditor namens `micro`. Bewegen Sie sich darin, speichern Sie, machen Sie es rückgängig usw., indem Sie die normalen Tastaturbefehle im Stil von Notepad verwenden. Um den Editor zu verlassen, drücke `ctrl+q` und tippe `y` oder `n`, um Änderungen zu speichern oder nicht zu speichern.

Wenn Sie wieder in der Kommandozeile sind...

$ bash start-sonic-pi.sh" (Startet das grafische Programm Sonic Pi und minimiert es dann, damit wir von der Kommandozeile aus weiterarbeiten können. Bash ist ein Programm, das so genannte `Shell`-Skripte ausführt, die oft auf `.sh` enden.)

Sobald wir wieder in der Kommandozeile sind, was etwa 30 Sekunden dauern wird...

$ bash play-sonic.sh examples.txt` (Ein Skript, das die Datei `examples.sh` an Sonic Pi übergibt. Sie werden sehen, dass die Töne abgespielt werden.)

`$ bash stop-sonic.sh` (Stoppt alles, was von Sonic Pi abgespielt wird.)




## Erweitert

### Das Terminal einer anderen Person aus der Ferne betrachten

Wenn Sie mit einem anderen Kollegen zusammenarbeiten oder einem Tutor folgen möchten, können Sie dessen Terminal-Sitzung online mit einem Tool wie [`tty-share`](https://tty-share.com/) verfolgen.

### Fernsteuerung Ihrer Konsole

Es kann sinnvoll sein, einem anderen Remote-Benutzer, z. B. einem BBT-Mitarbeiter, die Kontrolle über die Konsole zu einem von Ihnen gewählten Zeitpunkt zu ermöglichen. Hierfür empfehlen wir die Verwendung von [`mosh`](https://mosh.org/), das ähnlich wie `ssh` funktioniert, aber die Stabilität der Verbindung verbessert.

### Entwicklung von Anwendungen für die Canute-Konsole

Dies ist ein großes Thema, das wir in einem separaten Dokument behandeln werden und an dem aktiv gearbeitet wird. 

Im Moment ist die einfachste Erklärung die folgende:

Alles, was Sie für die visuelle Kommandozeile entwickeln können, in jeder Sprache, was unter Linux läuft und in das Format 40x9 passt, läuft auch auf der Canute Console. 

Es gibt jedoch einige Überlegungen:
 - Um die 1:1-Parität des Layouts zwischen der visuellen und der Braillezeile zu gewährleisten, empfehlen wir, das Programm so zu entwickeln, dass es Computer-Braille-Code verwendet. Bei kontrahierten Braille-Tabellen wird der horizontale Leerraum nicht berücksichtigt.
 - Canute 360s aktualisiert sich mit 0,5-1 Sekunden pro Zeile, daher sollten Anwendungen ständiges Scrollen vermeiden.
 [NPYScreen](https://github.com/npcole/npyscreen/) kann eine nützliche Methode sein, um aus einfachen Bash- und Python-Befehlen schnell attraktive räumliche, taktile und visuelle Produkte zu erstellen.


### Anwendungen, die nicht in 9 Zeilen passen

 - Führen Sie `$ screen` aus und drücken Sie Enter.

 - Drücken Sie 'enter', um die Readme zu überspringen.

 - Drücken Sie '[ctrl]+a' und dann ':' (Doppelpunkt). Damit können Sie Befehle eingeben, die in der 9. Zeile des Bildschirms erscheinen (unten links wird ein Doppelpunkt angezeigt)

 - (Dieser Schritt kann übersprungen werden.) Geben Sie `width -w 40` ein und drücken Sie die Eingabetaste. Dies setzt die Breite auf 40. (Sie können diese Zahl ändern, wenn die Anwendung, die Sie verwenden, breiter sein muss, obwohl diese Anleitung davon ausgeht, dass dies nicht nötig ist, so dass Sie diesen Schritt wahrscheinlich überspringen können).

 - Drücken Sie '[ctrl]+a' und dann erneut ':' (Doppelpunkt), um einen weiteren Befehl einzugeben.

 - Geben Sie `height -w 17` ein und drücken Sie Enter. Dadurch wird die Höhe des Terminals größer, in diesem Fall 17 Zeilen statt 9. Sie können jede andere Zahl wählen, sie muss kein Vielfaches von 9 Zeilen sein.

 	* (Wenn Sie zum Beispiel die Höhe auf 27 Zeilen einstellen, ist das Terminal dreimal so hoch wie die Canute-Anzeige, also genauso groß wie eine britische Standard-Papier-Braille-Seite).

 - Um sich auf dem extra großen Terminal zu bewegen, drücken Sie '[ctrl]+a' und dann '['. Damit gelangen Sie in den so genannten "Kopiermodus", aber wir werden ihn benutzen, um uns zu bewegen.

 - Jetzt bewegt sich der Cursor (normalerweise alle sechs Punkte) frei auf dem Terminal zwischen den Zeilen (normalerweise bleibt er auf der Eingabeaufforderung).

 - Bewegen Sie den Cursor ganz an den Rand des Bildschirms, indem Sie ihn einfach gedrückt halten. Dadurch wird der gesamte Inhalt nach unten verschoben.

 - Wenn Sie den Teil der Anwendung sehen, den Sie anzeigen möchten, drücken Sie die Escape-Taste. Damit verlassen Sie den Kopiermodus und kehren in den normalen Befehlszeilen-Eingabemodus zurück.

________
(Der obige Text ist einer Frage auf Stackoverflow entnommen, die Sie lesen können, wenn Sie mehr verstehen wollen, aber nicht müssen: https://stackoverflow.com/questions/75316022/emulate-a-taller-terminal-within-gnome-terminal-and-pan-up-and-down)

