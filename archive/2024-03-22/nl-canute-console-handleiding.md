# Canute Console Handleiding

## Opmerkingen

Laatste bijwerking: 2024-01-31

Caveat lector: Dit is een levend document dat wordt bijgewerkt naarmate er meer gebruikssituaties voor de Canute Console ontstaan. Geef suggesties over tips die moeten worden opgenomen, hoe de instructies duidelijker kunnen worden gemaakt en meld onnauwkeurigheden.

Vertaalnotitie: Alle niet-Engelse vertalingen zijn vertalingen vanuit het Engels met DeepL Pro. Als je vertaalfouten opmerkt, laat het ons dan weten.

Contact: enquiries@bristolbraille.org

Een probleem melden: [https://gitlab.com/Bristol-Braille/console-learn/-/issues](https://gitlab.com/Bristol-Braille/console-learn/-/issues)

## Wat is de Canute Console?

De Canute Console is een Linux werkstation met een Canute 360 brailleleesregel. 
Het grote verschil tussen de Canute Console en andere producten is dat de brailleleesregels en de visuele leesregels dezelfde hoeveelheid informatie in dezelfde lay-out tonen, waardoor noch de brailleleesregel noch de gedrukte gebruiker benadeeld wordt.

Het werkt vanaf de commandoregel en is voornamelijk bedoeld voor informatica. Er zijn echter weinig grenzen aan het gebruik als je eenmaal gewend bent aan het werken in meerdere regels braille.

De Canute Console bestaat uit twee verschillende elektronicaproducten: De Canute 360 brailleleesregel en de Console Upgrade Kit. 

De Canute 360 zit in de Canute Upgrade. 

## De Canute 360 in de Console Upgrade Kit plaatsen en verwijderen

Leg beide producten op een vlakke ondergrond waar u ze wilt gebruiken.

Open het deksel (de monitor) van de Console Upgrade Kit. Til de Canute 360 op en leg hem voorzichtig op het vlakke oppervlak in het midden van de Console Upgrade Kit. 

Rechts achteraan zit een uitsteeksel van de upgradekit. Dit is de mannelijke voedingsconnector en moet uitgelijnd worden met de vrouwelijke voedingsconnector op de Canute 360. 

Zodra dit uitgelijnd is, beweegt u de Canute 360 naar achteren om de voedingsconnectoren aan te sluiten. Het kan zijn dat u de Canute 360 iets moet optillen om de aansluiting te maken.

Nu voelt u linksachter op de upgradekit een mannelijke USB-B-connector (deze worden gewoonlijk 'printerconnectors' genoemd) aan een zeer korte kabel van ongeveer 5 cm. Trek het uit het gat waar het in zit en steek het in de vrouwelijke USB-B poort van de Canute 360 aan de rechterkant.

Om de Canute 360 te verwijderen gaat u op dezelfde manier te werk als hierboven, maar dan in omgekeerde volgorde.

## Ergonomie

De Canute Console is bedoeld voor gebruik op een stabiel, plat bureau of tafel met een glad oppervlak.

Het gladde oppervlak is nodig om de lade met het toetsenbord in en uit te kunnen nemen.

Til nu de monitor op (die ook als deksel fungeert) en trek het toetsenbord eruit.

Omdat de Canute Console veel braille bevat die je over een toetsenbord moet reiken om te lezen, is ergonomie extra belangrijk.

Zorg er daarom voor dat je stoel zo hoog mogelijk staat ten opzichte van het bureau/de tafel.

Zorg er ook voor dat de console comfortabel tegen de rand van het bureau/tafel staat.

U moet uw handen comfortabel kunnen laten rusten op zowel de bovenste regel van de brailleleesregel als de laatste regel van het toetsenbord.

## Poortindeling van de Canute Console

(Houd er rekening mee dat de poorten van de Canute 360 niet meer in gebruik zijn wanneer deze in de console is gedockt).

Aan de linkerkant van de Canute Console, van achter naar voren: 

 - 1x mannelijke USB-B connector, die zich in een klein gaatje in de Console Upgrade Kit bevindt (als de Canute 360 is losgekoppeld), of in de vrouwelijke USB-B poort van de Canute 360 is gestoken.

 - 3x vrouwelijke USB-A aansluitingen.

Aan de rechterkant van de Canute Console, van achter naar voren:

 - 1x vrouwelijke voedingsaansluiting.

 - 1x vrouwelijke 35.mm audio-aansluiting.

 - 1x tuimelschakelaar voor externe monitor.

 - 2x schakelaars voor bediening van externe monitor.

 - 1x vrouwelijke HDMI-aansluiting.

Aan de achterkant, van midden naar rechts:

 - 1x verwijderbaar paneel voor extra toekomstige poorten.

 - 1x aan/uit-schakelaar.

## Alles aansluiten

Nadat je ervoor hebt gezorgd dat de USB-B in de Canute 360 is gestoken en meteen na het insteken van de stroomkabel, druk je op de aan/uit-knop rechtsachter om de brailleleesregel in te schakelen.

## Loskoppelen

Wacht na het afsluiten van de Canute Console (zie hieronder) tot het Canute-display is uitgeschakeld en verwijder dan de voedingskabel. Merk op dat als u de voedingskabel niet verwijdert, het beeldscherm nog steeds stroom krijgt en in de screensaver-modus gaat. Daarom moet een Canute Console die niet wordt gebruikt altijd worden losgekoppeld.

## Toetsenbordindeling

Ervan uitgaande dat u het Britse toetsenbord gebruikt (zie anders hieronder 'De indeling van het toetsenbord wijzigen'), is de indeling van het toetsenbord als volgt:

Eerste regel: Escape, F1 tot F10 (F11 is Fn+F1, F12 is FN+F2), Num lock, Print Screen, Delete (Invoegen is Fn+Del).

Regels twee, drie, vier en vijf zijn standaard voor Britse Qwerty-toetsenborden.

Regel zes: Control (ctrl), Function (Fn), Super toets, Alt, Space, Alt, Ctrl, Pijlen (Home is Fn+LEFT, End is Fn+RIGHT, Page up is Fn+UP, Page down is Fn+DOWN)

## Wacht op de Canute-weergave...

Wanneer het klaar is voor gebruik (ongeveer een minuut) zal het lezen:
`Raspbian Canute Controller ...
... raspberrypi login: _`

## Inloggen

De standaard gebruiker is "pi", het standaard wachtwoord is "raspberry", allemaal kleine letters.

Gefeliciteerd, je bent nu klaar om de Canute Console te gebruiken!

We raden sterk aan om het wachtwoord zo snel mogelijk te veranderen en te noteren.

Om het wachtwoord te wijzigen typ `$ passwd` en volg de instructies.

Start sleutelcommando's om in te voeren:

`pi` (Gebruikersnaam.)

`raspberry` (Standaard wachtwoord.)

Wacht tot u ingelogd bent...

### Problemen oplossen

Ondervindt u problemen bij het inloggen, zoals teruggekaatst worden naar de inlogpagina? Dit kan gebeuren met de eerste versies van de Console-software, maar is daarna verholpen met upgrades. Om de upgrade toe te passen moet je natuurlijk eerst inloggen. Druk daarom op deze toetsencombinatie om naar een andere inlogterminal te gaan:

`ctrl+alt+f2`

Herhaal nu de inloginstructies hierboven. Eenmaal ingelogd volgt u de upgrade procedure hieronder (zoek naar 'Upgraden').

## Grondbeginselen voor het navigeren door de Linux commandoregel

### Nomenclatuur en indeling van de opdrachtregel

Linux is het besturingssysteem waarop Canute Console draait. Het draait een versie van Linux die 'Raspian' heet en gebaseerd is op 'Debian'.

Canute Console is vooraf ingesteld om volledig vanaf de commandoregel te draaien, wat betekent dat het geen grafische gebruikersinterface heeft.

De termen "commandoregel" of "terminal" (ook "tty" en console") worden online vaak door elkaar gebruikt. 

Ze lijken allemaal erg op elkaar en worden vaak door elkaar gebruikt. Maar, in het kort, zijn hier hun verschillen:

 - De "opdrachtregel" is meestal de laatste regel op het scherm en het is waar commando's kunnen worden geschreven. De commandoregel bevat een prompt, wat betekent "waar je je commando's typt".

 - De "commandoregel" bevindt zich in een "terminal". De terminal bevat de geschiedenis van eerder ingevoerde commando's en hun uitvoer.

In dit document gebruiken we instructies die beginnen met een dollarteken, dan een instructie en soms andere woorden of getallen, gescheiden door een spatie.

Dit is hoe commando's worden uitgevoerd in de Linux commandoregel. Het lijkt erg op de DOS Prompt onder Windows.

Het dollarteken is gewoon een symbool dat "opdrachtprompt" betekent, dat wil zeggen "typ je commando's hierna".

### Commando's typen en de terminaluitvoer begrijpen

Dus als we bijvoorbeeld een instructie als "Voer `$ ls` uit" schrijven, betekent dat "Typ 'ls' in de commandoregel". In dat geval betekent het eigenlijk, "maak een lijst van de submappen en bestanden" in mijn huidige mappen.

Als ander voorbeeld kunnen we in dit document schrijven, `$ cd demos` betekent "Voer het commando 'cd' uit met de instructie (argument genoemd) 'demos'". Eigenlijk betekent het in dat geval "ga naar de map genaamd 'demos'".

Het is belangrijk op te merken dat de terminal al je vorige commando's opslaat en ze niet van het scherm verwijdert tenzij daarom wordt gevraagd.

Zie het als een typemachine of embosser, die één regel per keer schrijft en de vorige regels niet wist.

Lees altijd tot de laatste regel van het scherm en neem aan dat de *laatste regel met de dollar erop* de actieve commandoregel is en dat de regels erboven geschiedenis zijn.

Als ik bijvoorbeeld een commando wil typen als "maak een map genaamd 'documents', wat geschreven zou moeten worden als `mkdir documents`, maar ik heb een fout gemaakt, dan zou de braille weergave er als volgt uit kunnen zien:

`$ mjdir documents`
bash: mjdir: opdracht niet gevonden`
`$`

Laten we dat nog eens regel voor regel bekijken. 

`$ mjdir documenten`

Ik heb een typefout gemaakt met het commando.

`bash: mjdir: opdracht niet gevonden`.

Dit is de foutmelding ('bash' is wat men noemt de 'shell', oftewel het programma dat je commando's interpreteert).

`$ `

Nu krijgen we dus weer een lege opdrachtprompt, maar we zien nog steeds onze eerste poging en de foutmelding hierboven.

Voor degenen die gewend zijn aan brailleleesregels is dit misschien even wennen. Maar het is wel het standaard gedrag voor de Linux terminal en precies dezelfde lay-out en uitvoer als ziende gebruikers krijgen op de visuele monitor.

Als je scherm te vol is, dan is het `$ clear` commando je vriend. Het zal de terminal resetten naar niets anders dan de huidige opdrachtprompt op de bovenste regel van het scherm.

### Rondkomen

Linux gebruikt een hiërarchisch bestandssysteem, wat betekent dat mappen en bestanden allemaal in andere mappen staan, terug naar de 'root'. De root staat bovenaan de hiërarchie.

Mappen worden gescheiden door streepsymbolen, dus je thuismap wordt geschreven als `/home/pi/`.

Bij het inloggen start je in de homedirectory van je gebruiker. Om te beginnen hoef je zelden verder te gaan dan je thuismap.

`$ ls` geeft een lijst van de submappen en bestanden in de huidige map.

Het kan zijn dat de uitvoer een te lange lijst is om te lezen. Je kunt terug omhoog en omlaag scrollen in de terminal geschiedenis door op `ctrl+Fn+UP` of `ctrl+Fn+DOWN` te drukken.

Om van directory te wisselen gebruik je het `cd` commando. `$ cd mapnaam`.

Om een niveau terug te gaan gebruik je het `$ cd ..` commando.

Hier merken we iets belangrijks op over veel Linux commando's, zoals `cd`. Als je vraagt om naar een map te gaan, zal het dat doen, maar het geeft je geen uitvoer om te zeggen of het gelukt is. 

Dus hoe weet je waar je bent? Je kunt `$ pwd` typen. Dit geeft je huidige locatie binnen het bestandssysteem. 

### Tips om navigatie en het typen van commando's makkelijker te maken

Je zult merken dat voor sommige commando's de uitvoer langer is dan het scherm (40 cellen), daarom is de Canute helemaal naar rechts gepand. 

Om naar links en rechts te pannen op de brailleleesregel (dit heeft geen invloed op de visuele weergave) om strings langer dan 40 cellen te bekijken, kunt u op de duimtoetsen '[MENU]+[FORWARD]' aan de voorkant van de Canute-display drukken om naar rechts te gaan, of '[MENU]+[PREV]' om naar links te pannen.

U kunt een opdracht of een map- of bestandsnaam automatisch aanvullen wanneer u op de opdrachtprompt typt door op de tabtoets te tikken.

Als er maar één optie beschikbaar is, vult tab deze automatisch in. Als het om meerdere dingen kan gaan dan zal op tab drukken niets doen, maar dubbel tikken op tab zal alle opties oproepen en ze naar de terminal schrijven zodat je ze kunt bekijken.

### Tekstbestanden lezen

Als er een tekstbestand is dat je wilt lezen, typ dan `$ less textfilename.txt`. Het hoeft geen TXT extensie te hebben en vaak hebben ze in Linux helemaal geen extensie. Als je twijfelt, probeer dan `less`.

Less drukt de eerste acht regels van het bestand af. Gebruik de pagina omhoog en omlaag (Fn+UP, Fn+DOWN) om er doorheen te gaan, druk op 'q' om terug te gaan naar de commandoregel.

### Nuttige commando's 

Inclusief:

 * ls|less
   (Geeft een overzicht van bestanden en submappen)

 * tree|less
   (Toont directorystructuur)

 * cd [DIRECTORYNAME]
   (Naar een submap gaan)

 * cd ..
   (Naar boven naar de bovenliggende map gaan)

 * mkdir [DIRECTORYNAME]
   (Maak nieuwe map)

 * micro [FILENAME]
   (Tekstbestand openen)

 * [ctrl]+[shift]+t
   (Nieuw tabblad in terminal openen.)

 * [ctrl]+[shift]+[NUM] (0--9)
   (Schakelt over naar een ander tabblad.)

 * nu afsluiten
   (Afsluiten, maar nog steeds nodig om 
   Canute uitschakelen nadat afsluiten 
   voltooid is)

### Speciale opdrachten voor braillecodes

Er zijn enkele speciale commando's voor 
de Canute Console, waaronder:

 * $ [alt]+[shift]+Q
   (graad één UEB)

 * $ [alt]+[shift]+W
   (graad twee UEB)

 * $ [alt]+[shift]+E 
   (9-regelig 6-dot US Computer Brl)

 * $ [alt]+[shift]+R 
   (Gecontracteerd Duits, 2015 standaard)

Let op: alleen US computer brl garandeert een één-op-één tekenrepresentatie en dus exact dezelfde lay-out als het visuele scherm.

Geavanceerd: Als je de naam weet van een andere BRLTTY brailletabel die je wilt gebruiken dan kun je die invoeren met:
`$ canute-table [tabel-naam]`

## Escape!

Nu we de basis kennen, zijn de belangrijkste commando's het afsluiten van programma's en veilig afsluiten.

Om een lopend programma te stoppen zijn er vaak commando's die specifiek zijn voor dat programma. Vaak wordt de letter `q` gebruikt om af te sluiten, of de `exit` toets (linksboven op het Console toetsenbord). Maar in extreme gevallen (als een programma hangt of als je niet weet hoe je het moet afsluiten) werkt het vaak ook om op `ctrl+c` te drukken. 

## Afsluiten

Om de hele unit af te sluiten typ je `$ shutdown now`. (Of om opnieuw op te starten: `$ reboot`.)

Als je hebt afgesloten en het Canute beeldscherm is uitgeschakeld* moet je de stroomkabel uit de Console halen (anders blijft het beeldscherm aan).

* N.B. Als je je Canute 360 die bij de Console is geleverd hebt geüpgrade naar firmware versie 2.0.10 of hoger, dan zal je Canute uitschakelen als de Console wordt uitgeschakeld. Als het apparaat echter een oudere firmware heeft, zal het niet uit zichzelf uitschakelen. In plaats daarvan zal het in de stand-alone boekleesmodus gaan en op dat moment moet u op de aan/uit-knop rechtsachter drukken om het Canute-display uit te schakelen voordat u het netsnoer eruit haalt.

## Leerbronnen

We raden je aan om naar de volgende afleveringen van Braillecast te luisteren, gesponsord door Bristol Braille, om mensen vertrouwd te maken met de basis van de Linux commandoregel.

Alle afleveringen zijn te vinden in:
`cd ~/learn/braillists/`
en kunnen worden afgespeeld met:
`vlc 01[tab om te voltooien]`

 - 01: Eerst een overzicht van verschillende computerwetenschappers die braille gebruiken in hun werk. Dit kan worden overgeslagen.

 - 02: Ten tweede, een masterclass inleiding tot de Linux commandoregel.

 - 03: Ten derde, een masterclass inleiding tot Git, voor versiebeheer en het delen en downloaden van programma's.

 - 04: Ten vierde een masterclass over programmeren op de Canute Console.

Bij je Canute Console zitten een paar Linux basics learning tools. Je kunt ze vinden in de homedirectory door `cd learn` in te typen.
Tenslotte kunt u meestal op het web naar antwoorden op uw vragen zoeken. 

De meeste vragen zullen algemeen over Linux zijn. Als je echter meer specifieke antwoorden nodig hebt, kun je zoeken op 'Debian', of zelfs specifiek op 'Raspian'.

Voor dit soort vragen raden we de website Stackoverflow aan.

## Geluid

Let op de onderstaande commando's. De dubbele s in sset is geen typefout.

 - `$ amixer sset Master 0%`

 - `$ amixer sset Master 100%`

## De toetsenbordindeling wijzigen

De opdracht is relatief eenvoudig vanaf de opdrachtregel. Bijvoorbeeld:

`$ setxkbmap us`

Verandert het toetsenbord in de VS. 

Voor een lijst van beschikbare toetsenbordindelingen typ:

`$ ls /usr/share/X11/xkb/symbols/ | less`

Om het automatisch in te stellen voeg je hetzelfde commando toe aan `~/console-set-up/console-start.sh`:

`$ setxkbmap us`

## Verbinding maken met het internet

### WiFi

De Canute Console maakt automatisch verbinding met elk WiFi-netwerk dat is opgeslagen. 

Om tussen deze netwerken te kiezen (als er meer dan één is) typ `$ wifi`.

Om voor de eerste keer verbinding te maken met een nieuw netwerk typt u `$ wifi new`.

#### Problemen oplossen

Als u de foutmelding `Unable to connect .... Secrets were required but not provided`, probeer dan de volgende twee commando's uit te voeren:

`$ nmcli con delete "NAAM VAN SSID"`
`$ nmcli dev wifi connect "NAAM VAN SSID" wachtwoord WACHTWOORD ifname wlan0`

### Tethering naar je mobiel via USB-A

Sluit een Android-telefoon met een USB-kabel aan op de Canute Console. In de meldingen op uw telefoon staat nu zoiets als "Opladen via USB". Tik op die melding en je komt in het menu met USB-instellingen. Verander "Alleen opladen" in "Tether" (exacte termen kunnen veranderen). De Console heeft nu toegang tot het internet. 

Bij tethering wordt deze verbinding gebruikt in plaats van de WiFi-verbinding, ook al is deze minder snel.

### Internetverbinding testen

Test het internet in de console door `$ ping bbc.com -i 4` in te voeren. U zou een log moeten krijgen met veel uitgangen die voortdurend worden bijgewerkt. 

(Het `-i 9` deel van dat commando is om aan te geven dat de BBC servers elke vier seconden gepingd worden).

Druk op `[ctrl]+c` om het programma te annuleren en het resultaat te zien.

## Upgraden naar de laatste Console OS versie

Eenmaal verbonden met het internet voer het volgende commando uit:

`$ curl -L https://bristolbraille.org/console-install | bash`

De upgrade kan even duren, afhankelijk van je verbindingssnelheid en de grootte van de demonstratietoepassingen die worden gedownload (die variëren per versie).

Log na voltooiing uit: `$ exit`

Log vervolgens weer in.

## Voorbeeldtoepassingen ontwikkeld voor de Canute Console

### Sonic Pi

Installeren:

`$ git clone https://gitlab.com/bristol-braille/sonic-pi-canute/`

Bekijk de volgende voorbeelden van het uitvoeren en bewerken van Sonic Pi-programma's met behulp van enkele tools die we hebben ontwikkeld of waar we scripts omheen hebben geschreven voor de Console:

`$ cd sonic-pi-canute` (Ga naar de `sonic-pi-canute` directory.)

`$ micro examples.txt` (Bekijk --- en bewerk als je wilt --- de voorbeeld Sonic Pi code).

Nu bevindt u zich in de teksteditor genaamd `micro`. Beweeg rond de editor, sla op, maak ongedaan, etc. met de normale toetsencommando's in kladblokstijl. Om af te sluiten druk je op `ctrl+q` en typ je `y` of `n` om wijzigingen op te slaan of niet op te slaan.

Eenmaal terug op de commandoregel...

`$ bash start-sonic-pi.sh` (Hiermee wordt het grafische programma Sonic Pi gestart en vervolgens geminimaliseerd zodat we verder kunnen werken vanaf de commandoregel. Bash is een programma dat zogenaamde `shell` scripts uitvoert, die vaak eindigen op `.sh`).

Eenmaal terug in de commandoregel, wat ongeveer 30 seconden zal duren...

`$ bash play-sonic.sh examples.txt` (Een script dat het `examples.sh` bestand doorgeeft aan Sonic Pi. Je zult merken dat de geluiden spelen).

`$ bash stop-sonic.sh` (Stop alles dat door Sonic Pi wordt afgespeeld).




## Geavanceerd

### Op afstand de terminal van iemand anders bekijken

Om samen te werken met een andere collega of om een tutor te volgen, kunt u hun terminalsessie online volgen met een tool als [`tty-share`](https://tty-share.com/).

### Afstandsbediening van uw console

Het kan verstandig zijn om een andere gebruiker op afstand, zoals BBT-medewerkers, de controle over de console te geven op door jou gekozen tijden. Hiervoor raden we aan om [`mosh`](https://mosh.org/) te gebruiken, wat vergelijkbaar is met `ssh` maar met een betere stabiliteit in de verbinding.

### Toepassingen ontwikkelen voor de Canute Console

Dit is een groot onderwerp dat we in een apart document zullen behandelen en waaraan actief wordt gewerkt. 

Voor nu is de eenvoudigste uitleg het volgende:

Alles wat je kunt ontwikkelen voor de visuele commandoregel, in elke taal, dat draait op Linux en past in 40x9, zal draaien op de Canute Console. 

Er zijn echter bepaalde overwegingen:
 - Om ervoor te zorgen dat de lay-out van de visuele en brailleleesregels 1-op-1 overeenkomen, raden we aan het programma zo te ontwikkelen dat het computerbraille gebruikt. Brailletabellen houden geen rekening met horizontale witruimte.
 - Canute 360s verversen met 0.5--1 seconden per regel, daarom moeten toepassingen constant scrollen vermijden.
 [NPYScreen](https://github.com/npcole/npyscreen/) kan een handige manier zijn om snel aantrekkelijke ruimtelijke tactiele en visuele producten te maken van eenvoudige bash- en Python-commando's.


### Apps die niet in 9 regels passen

 - Voer `$ screen` uit en druk op enter.

 - Druk op 'enter' om de readme over te slaan.

 - Druk op '[ctrl]+a' en vervolgens op ':' (dubbele punt). Hiermee kun je commando's typen, die op de 9e regel van het scherm verschijnen (er staat een dubbele punt linksonder)

 - (Je kunt deze stap overslaan.) Typ `width -w 40` en druk op enter. Dit stelt de breedte in op 40. (Je kunt dat getal veranderen als de applicatie die je gebruikt breder moet zijn, maar deze handleiding gaat ervan uit dat dat niet nodig is, dus je kunt deze stap waarschijnlijk overslaan).

 - Druk op '[ctrl]+a' en druk dan weer op ':' (dubbele punt) om een ander commando te typen.

 - Typ `height -w 17` en druk op enter. Dit maakt de terminal hoger, in dit geval 17 regels hoog in plaats van 9. Je kunt elk ander getal kiezen, het hoeft geen veelvoud van 9 regels te zijn.

 	* (als voorbeeld: als u de hoogte instelt op 27 regels, wordt de terminal drie keer zo hoog als het Canute-display, dus even groot als een standaard papieren braillepagina in het VK).

 - Om door de extra grote terminal te bewegen drukt u op '[ctrl]+a' en vervolgens op '['. Dit opent wat "kopieermodus" wordt genoemd, maar we gaan het gebruiken om rond te bewegen.

 - Nu beweegt de cursor (meestal alle zes puntjes) vrij rond de terminal tussen de regels (meestal blijft hij op de opdrachtprompt).

 - Verplaats de cursor helemaal naar de voorkant van het scherm door hem ingedrukt te houden. Het zal alle inhoud naar beneden verplaatsen.

 - Wanneer je het deel van de applicatie ziet dat je wilt bekijken, druk dan op escape, dit zal dan de "kopieermodus" verlaten en terugkeren naar de normale invoermodus voor de commandoregel.

________
(Het bovenstaande komt uit een vraag op Stackoverflow die je kunt lezen als je meer wilt begrijpen, maar dat is niet nodig: https://stackoverflow.com/questions/75316022/emulate-a-taller-terminal-within-gnome-terminal-and-pan-up-and-down)

